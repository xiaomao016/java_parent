package com.zx.spring.controller;

import com.alibaba.fastjson.JSONObject;
import com.zx.spring.aspect.WebLog;
import com.zx.spring.web.Result;
import com.zx.spring.web.ResultGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/20 0020 11:09
 * @description:
 */
@RestController
@RequestMapping("/chain")
public class ChainController {

    @Resource
    private TestController testController;

    @GetMapping("m1")
    public Result method(){
        JSONObject ob = new JSONObject();
        ob.put("result","m1");
        return ResultGenerator.genSuccessResult(ob);
    }

    @GetMapping("m2")
    public Result method1(){
        JSONObject ob = new JSONObject();
        ob.put("result","m2");
        return ResultGenerator.genSuccessResult(ob);
    }

    @GetMapping("m3")
    public Result method2(){
        return testController.method(1);
    }
}
