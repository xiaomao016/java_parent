package com.zx.spring.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2023-03-03
 */
@RestController
@RequestMapping("/words")
public class WordsController {

}
