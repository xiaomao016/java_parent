package com.zx.spring.controller;


import com.google.common.collect.Sets;
import com.zx.spring.adjust.AliDNSHelper;
import com.zx.spring.web.Result;
import com.zx.spring.web.ResultGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zx
 * @since 2023-03-08
 */
@RestController
@RequestMapping("/adjust-pb")
@Slf4j
public class AdjustPbController {

    /**
     * 给库中已经购买的域名，添加解析记录
     * @param domains
     * @return
     */
    @GetMapping("/batchSaveRecord")
    public Result<Object> save(@RequestParam(value = "domains") List<String> domains) {
        AliDNSHelper.addDomainRecord(Sets.newHashSet(domains));
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/updateGroup")
    public Result<Object> updateGroup(@RequestParam(value = "domains") List<String> domains) {
        AliDNSHelper.updateDomainToDomainGroup(Sets.newHashSet(domains));
        return ResultGenerator.genSuccessResult();
    }


}
