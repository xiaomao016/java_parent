package com.zx.spring.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.domain20180129.models.QueryTaskDetailListResponseBody;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.zx.spring.adjust.AliDNSHelper;
import com.zx.spring.adjust.DomainUtil;
import com.zx.spring.entity.AdjustPb;
import com.zx.spring.entity.Words;
import com.zx.spring.service.IAdjustPbService;
import com.zx.spring.service.IWordsService;
import com.zx.spring.web.Result;
import com.zx.spring.web.ResultGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/20 0020 11:09
 * @description:
 */
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {


    @Resource
    private IWordsService service;
    @Resource
    private IAdjustPbService adjustPbService;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("m1")
    public Result method(@RequestParam(value = "num",required = false) Integer num){
//        QueryWrapper<Words> wrapper = new QueryWrapper<>();
//        wrapper.likeLeft("word","a");
//        wrapper.last("limit 10");
//        List<Words> list = service.list(wrapper);
//        JSONObject ob = new JSONObject();
//        ob.put("result","test");
//        ob.put("words",list);
        if(num==null){
            num = 1;
        }
        Integer finalNum = num;
        Thread thread = new Thread(()->{
            Set<String> domains = DomainUtil.createDomains(finalNum);
            log.info("controller domains:{}",JSON.toJSONString(domains));
            log.info("start buy...");
            String taskNo = AliDNSHelper.batchRegisterDomain(domains);
            log.info("buy task no:{}",taskNo);
            //10min后添加记录
            try {
                TimeUnit.MINUTES.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("add record...");
            AliDNSHelper.addDomainRecord(domains);
            //10min后，判断是否生效，如果生效。
            try {
                TimeUnit.MINUTES.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("ping domain...");
            List<String> readyDomains = Lists.newArrayList();
            List<String> failsDomains = Lists.newArrayList();
            domains.stream().forEach(d->{
                boolean re = ping(d);
                if(re){
                    readyDomains.add(d);
                }else{
                    failsDomains.add(d);
                }
            });
            //之后插入数据库
            saveToDB(readyDomains);
            log.error("failsDomains:{}",JSON.toJSONString(failsDomains));
        });
        thread.setDaemon(true);
        thread.start();

        return ResultGenerator.genSuccessResult("后台正在创建...");
    }

    private void saveToDB(List<String> domains) {
        List<AdjustPb> list = Lists.newArrayList();
        //批量插入
        domains.stream().forEach(d->{
            AdjustPb adjustPb = new AdjustPb();
            adjustPb.setPbDomain(d);
            adjustPb.setPlatformId(31);
            adjustPb.setStatus("pendding");
            list.add(adjustPb);
        });
        boolean re = adjustPbService.saveBatch(list);
//        int re = adjustPbService
        log.info("save re:{}",re);
    }

    private boolean ping(String domain){
        //http://pb.sasquatahuvwkx.com/
        ResponseEntity<String> forEntity = null;
        try {
            StringBuffer url = new StringBuffer("http://pb."+domain);
            forEntity = restTemplate.getForEntity(url.toString(), String.class);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if(forEntity.getBody().equalsIgnoreCase("1")) {
            return true;
        }
        return false;
    }

    @GetMapping("m2")
    public Result method1(@RequestParam(value = "domains",required = false) Set<String> domains){
        log.info("domains m2:{}",JSON.toJSONString(domains));
        AliDNSHelper.addDomainRecord(domains);
        return ResultGenerator.genSuccessResult("success");
    }

    @GetMapping("domains")
    public Result method3(@RequestParam(value = "num",required = false) Integer num){
        if(num==null){
            num = 1;
        }
        Set<String> domains = DomainUtil.createDomains(num);
        return ResultGenerator.genSuccessResult(domains);
    }

    @GetMapping("queryBuyTask")
    public Result method3(@RequestParam(value = "taskNo",required = false) String taskNo){
        List<QueryTaskDetailListResponseBody.QueryTaskDetailListResponseBodyDataTaskDetail> list = AliDNSHelper.queryTaskDetailList(taskNo, 1, 10);
        return ResultGenerator.genSuccessResult(list);
    }

    @GetMapping("ping")
    public Result method4(@RequestParam(value = "domain",required = false) String domain){
       boolean re = ping(domain);
       return ResultGenerator.genSuccessResult(re);
    }

    @GetMapping("save")
    public Result method5(@RequestParam(value = "domains",required = false) List<String> domains){
        List<AdjustPb> list = createEntities(domains);
        log.info("save list:{}",JSON.toJSONString(list));
        boolean re = adjustPbService.saveBatch(list);
        return ResultGenerator.genSuccessResult(re);
    }

    @GetMapping("saveSingle")
    public Result method5(@RequestParam(value = "domain",required = false) String domain){
        AdjustPb adjustPb = new AdjustPb();
        adjustPb.setPbDomain(domain);
        adjustPb.setPlatformId(31);
        adjustPb.setStatus("pendding");

        boolean re = adjustPbService.saveOrUpdate(adjustPb);

        return ResultGenerator.genSuccessResult(re);
    }

    private List<AdjustPb> createEntities(List<String> domains){
        List<AdjustPb> list = Lists.newArrayList();
        //批量插入
        domains.stream().forEach(d->{
            AdjustPb adjustPb = new AdjustPb();
            adjustPb.setPbDomain(d);
            adjustPb.setPlatformId(31);
            adjustPb.setStatus("pendding");
            list.add(adjustPb);
        });
        return list;
    }
}
