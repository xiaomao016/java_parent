package com.zx.spring.controller;

import com.alibaba.fastjson.JSONObject;
import com.zx.spring.aspect.WebLog;
import com.zx.spring.web.Result;
import com.zx.spring.web.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/20 0020 11:09
 * @description:
 */
@RestController
@RequestMapping("/spring/aspect/log")
public class LogAspectController {


    @GetMapping
    @WebLog(value = "msg")
    public Result aspect(){
        JSONObject ob = new JSONObject();
        ob.put("result","success");
        return ResultGenerator.genSuccessResult(ob);
    }
}
