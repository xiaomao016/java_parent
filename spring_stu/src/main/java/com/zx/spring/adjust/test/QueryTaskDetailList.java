package com.zx.spring.adjust.test;

import com.alibaba.fastjson.JSON;
import com.aliyun.domain20180129.models.QueryTaskDetailListResponse;
import com.aliyun.tea.TeaException;

/**
 * 查询注册任务信息。传入任务编号
 */
public class QueryTaskDetailList {

    /**
     * 使用AK&SK初始化账号Client
     *
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.domain20180129.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 必填，您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 必填，您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "domain.aliyuncs.com";
        return new com.aliyun.domain20180129.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        // 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html
        com.aliyun.domain20180129.Client client = QueryTaskDetailList.createClient(Credentials.ACCESS_KEY_ID, Credentials.ACCESS_KEY_SECRET);
        com.aliyun.domain20180129.models.QueryTaskDetailListRequest queryTaskDetailListRequest = new com.aliyun.domain20180129.models.QueryTaskDetailListRequest()
                .setTaskStatus(2)
//                .setTaskNo("3b85747e-8353-4ecd-a09a-0cda8ed15621")
                .setTaskNo("d0ddc229-46aa-40ad-83e9-dd600a37cf4d")
                .setPageNum(1)
                .setPageSize(10);
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            QueryTaskDetailListResponse reqs = client.queryTaskDetailListWithOptions(queryTaskDetailListRequest, runtime);
            System.out.println(JSON.toJSONString(reqs));
        } catch (TeaException error) {
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
    }

}
