package com.zx.spring.adjust;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.alidns20150109.models.AddDomainRecordResponse;
import com.aliyun.domain20180129.Client;
import com.aliyun.domain20180129.models.*;
import com.aliyun.tea.TeaException;
import com.aliyun.teautil.models.RuntimeOptions;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.zx.spring.adjust.test.*;
import com.zx.spring.adjust.test.Credentials;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * @author zx
 * @date 2023-03-08
 */
@Slf4j
public class AliDNSHelper {

    private static List<String> elb = Lists.newArrayList(
            "PB-bidzetamedias-com-ELB-1548961610.us-east-2.elb.amazonaws.com",
            "unionnethk-com-nlb-pb-f0bcdb985d2e266a.elb.us-east-2.amazonaws.com",
            "dynmarc3y-com-nlb-pb-50146c2f7db9e0ae.elb.us-east-2.amazonaws.com",
            "luxmediakx-com-lb-alb-379bc69967d28b0e.elb.us-east-2.amazonaws.com",
            "PB-yuncaiads-com-ALB-295114589.us-east-2.elb.amazonaws.com"
    );

    private static Client client;

    private static Client get() {
        if (client == null) {
            try {
                client = DomainCheckBuy.createClient(Credentials.ACCESS_KEY_ID, Credentials.ACCESS_KEY_SECRET);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return client;
    }

    private static com.aliyun.alidns20150109.Client dns;

    public static com.aliyun.alidns20150109.Client getDNS() {
        if (dns == null) {
            com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                    // 必填，您的 AccessKey ID
                    .setAccessKeyId(Credentials.ACCESS_KEY_ID)
                    // 必填，您的 AccessKey Secret
                    .setAccessKeySecret(Credentials.ACCESS_KEY_SECRET);
            // 访问的域名
            config.endpoint = "alidns.cn-hangzhou.aliyuncs.com";
            try {
                dns = new com.aliyun.alidns20150109.Client(config);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dns;
    }

    public static boolean domainCheckBuy(String domain) {
        Client client = get();
        CheckDomainRequest checkDomainRequest = new CheckDomainRequest()
                .setDomainName(domain)
                .setFeeCommand("create")
                .setFeePeriod(1)
                .setLang("en");
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            CheckDomainResponse reps = client.checkDomainWithOptions(checkDomainRequest, runtime);
            log.info("body:{}", JSON.toJSONString(reps.getBody()));
            //如果价钱过高，则过滤
            if (reps.getBody().getAvail().equalsIgnoreCase("1") && reps.getBody().getPrice() == null) {
                return true;
            }
        } catch (TeaException error) {
            log.error(error.getMessage(), error);
        } catch (Exception error) {
            log.error(error.getMessage(), error);
        }
        return false;
    }

    public static String batchRegisterDomain(Set<String> domains) {
        Client client = get();
        List<SaveBatchTaskForCreatingOrderActivateRequest.SaveBatchTaskForCreatingOrderActivateRequestOrderActivateParam> orderActivateParams = Lists.newArrayList();
        domains.stream().forEach(d -> {
            SaveBatchTaskForCreatingOrderActivateRequest.SaveBatchTaskForCreatingOrderActivateRequestOrderActivateParam orderActivateParam = new SaveBatchTaskForCreatingOrderActivateRequest.SaveBatchTaskForCreatingOrderActivateRequestOrderActivateParam()
                    .setDomainName(d)
                    .setRegistrantProfileId(11552613L)
                    .setRegistrantType("1");
            orderActivateParams.add(orderActivateParam);
        });

        SaveBatchTaskForCreatingOrderActivateRequest saveBatchTaskForCreatingOrderActivateRequest =
                new SaveBatchTaskForCreatingOrderActivateRequest()
                        .setLang("en")
                        .setOrderActivateParam(orderActivateParams);
        try {
            RuntimeOptions runtime = new RuntimeOptions();
            SaveBatchTaskForCreatingOrderActivateResponse reps = client.saveBatchTaskForCreatingOrderActivateWithOptions(saveBatchTaskForCreatingOrderActivateRequest, runtime);
            log.info("batchRegister result:{}", JSON.toJSONString(reps.getBody()));
            return reps.getBody().taskNo;
        } catch (TeaException error) {
            log.error(error.getMessage(), error);
        } catch (Exception error) {
            log.error(error.getMessage(), error);
        }
        return null;
    }

    public static void addDomainRecord(Set<String> domains) {
        com.aliyun.alidns20150109.Client client = getDNS();
        domains.stream().forEach(d -> {
            com.aliyun.alidns20150109.models.AddDomainRecordRequest addDomainRecordRequest = new com.aliyun.alidns20150109.models.AddDomainRecordRequest()
                    .setLang("en")
                    .setDomainName(d)
                    .setType("CNAME")
                    .setValue(elb.get(random(elb.size())))
                    .setRR("pb");
            com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
            try {
                AddDomainRecordResponse reqs = client.addDomainRecordWithOptions(addDomainRecordRequest, runtime);
                log.info("add domain record:{}",JSON.toJSONString(reqs.getBody()));
            } catch (TeaException error) {
                log.error(error.getMessage(), error);
            } catch (Exception error) {
                log.error(error.getMessage(), error);
            }
        });
    }

    public static List<QueryTaskDetailListResponseBody.QueryTaskDetailListResponseBodyDataTaskDetail> queryTaskDetailList(String taskNo,int pageNum,int pageSize){
        com.aliyun.domain20180129.Client client = get();
        QueryTaskDetailListRequest queryTaskDetailListRequest = new QueryTaskDetailListRequest()
                .setTaskStatus(2)
                .setTaskNo(taskNo)
                .setPageNum(pageNum)
                .setPageSize(pageSize);
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            QueryTaskDetailListResponse reqs = client.queryTaskDetailListWithOptions(queryTaskDetailListRequest, runtime);
            log.info("task info:{}",JSON.toJSONString(reqs.getBody().data.taskDetail));
            return reqs.getBody().data.taskDetail;
        } catch (TeaException error) {
            log.error(error.getMessage(), error);
        } catch (Exception error) {
            log.error(error.getMessage(), error);
        }
        return null;
    }

    public static boolean fuzzyMatch(String domain){
        com.aliyun.domain20180129.Client client = get();
        FuzzyMatchDomainSensitiveWordRequest fuzzyMatchDomainSensitiveWordRequest = new FuzzyMatchDomainSensitiveWordRequest()
                .setLang("en")
                .setKeyword(domain);
        try {
            RuntimeOptions runtime = new RuntimeOptions();
            FuzzyMatchDomainSensitiveWordResponse response = client.fuzzyMatchDomainSensitiveWordWithOptions(fuzzyMatchDomainSensitiveWordRequest, runtime);
            log.info("fuzzy body:{}",JSON.toJSONString(response.body));
            return response.body.getExist();
        } catch (TeaException error) {
            log.error(error.getMessage(), error);
        } catch (Exception error) {
            log.error(error.getMessage(), error);
        }
        return false;
    }

    public static void updateDomainToDomainGroup(Set<String> domains){
        Client client = get();
        com.aliyun.domain20180129.models.UpdateDomainToDomainGroupRequest updateDomainToDomainGroupRequest = new com.aliyun.domain20180129.models.UpdateDomainToDomainGroupRequest()
                .setDomainGroupId(609473L)
                .setDomainName(Lists.newArrayList(domains))
                .setDataSource(1)
                .setReplace(false);
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            client.updateDomainToDomainGroupWithOptions(updateDomainToDomainGroupRequest, runtime);
        } catch (TeaException error) {
            log.error(error.getMessage(), error);
        } catch (Exception error) {
            log.error(error.getMessage(), error);
        }
    }

    private static int random(int range) {
        Random ran = new Random();
        return ran.nextInt(range);
    }
}
