package com.zx.spring.adjust.test;

import com.zx.spring.adjust.AliDNSHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {

    public static void main(String[] args) {
        //1.检查是否可以购买
        boolean re = AliDNSHelper.domainCheckBuy("xiaomi.com");
        log.info("re:{}",re);

        boolean exist = AliDNSHelper.fuzzyMatch("fuckingljsgl");
        log.info("e:{}",exist);
    }
}
