package com.zx.spring.adjust;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.zx.spring.component.BeanFactoryUtils;
import com.zx.spring.entity.Words;
import com.zx.spring.mapper.WordsMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zx
 * @date 2023-03-02
 * 生成一个随机域名
 * 单词选择规则：单词长度不小于2 ; 每次选择数量大于=1,小于=2个;
 * 子缀规则：可选子缀长度：1,2 可选数量：1,2 。 根据单词拼接长度决定选几个，长度是几个； 子缀位置可以在前，中，后；
 * 拼接串规则：长度不能超过20 不能少于6; 最大连续重复字符不超过3个;
 */
@Slf4j
public class DomainUtil {

    private static String ALPHABET_WEIGHT = "{\"a\":7,\"b\":5,\"c\":9,\"d\":5,\"e\":4,\"f\":4,\"g\":3,\"h\":4,\"i\":4,\"j\":1,\"k\":1,\"l\":3,\"m\":5,\"n\":2,\"o\":2,\"p\":10,\"q\":1,\"r\":5,\"s\":10,\"t\":5,\"u\":3,\"v\":2,\"w\":2,\"x\":1,\"y\":1,\"z\":1}";
    private static Map<Integer, String> ALPHABET_TOKEN = new HashMap<>();
    private static String COLUMN_WORLD = "word";

    static {
        Map<String, Integer> alphaMap = JSON.parseObject(ALPHABET_WEIGHT, Map.class);
        alphaMap.keySet().stream().forEach(a -> {
            Integer weight = alphaMap.get(a);
            for (int i = 0; i < weight; ++i) {
                ALPHABET_TOKEN.put(ALPHABET_TOKEN.size(), a);
            }
        });
    }

    public static Set<String> createDomains(int count) {
        Set<String> re = new HashSet<>();
        for (int d = count; d > 0; ) {
            String domain = createOneDomain();
            if (checkDomain(domain)) {
                log.info("good domain:{}", domain);
                re.add(domain);
            }
            d = count - re.size();
        }
        return re;
    }

    private static String createOneDomain() {
        //1.单词预选
        List<String> words = Lists.newArrayList();
        int wc = (int) (System.currentTimeMillis() % 2 + 1);
        while (wc > 0) {
            String word = getWord();
            if (word.length() < 2 || word.length() > 20 || maxRepeatCharCount(word) >= 3) {
                continue;
            }
            if (word.length() > 10 && calWordsLength(words) > 10) {
                continue;
            }
            words.add(word);
            --wc;
            if (calWordsLength(words) > 8) {
                break;
            }
        }
        log.info("words:{}", JSON.toJSONString(words));
        //2.子缀预选
        int wordsLength = calWordsLength(words);
        List<String> substrings = getSubString(wordsLength);
        log.info("substrings:{}", JSON.toJSONString(substrings));
        //3.字符串拼接
        // 小于6，一到两个个子串 ;小于20 替换其中n个字符;
        if (wordsLength > 6 && wordsLength < 20) {
            words = words.stream()
                    .map(w -> w.replace(w.charAt(random(w.length())), randomAlphabet()))
                    .collect(Collectors.toList());
        }
        StringBuilder sb = new StringBuilder();
        int word_index = 0;
        int subs_index = 0;
        while (word_index < words.size() && subs_index < substrings.size()) {
            sb.append(words.get(word_index));
            sb.append(substrings.get(subs_index));
            word_index++;
            subs_index++;
        }
        if (word_index == words.size()) {
            while (subs_index < substrings.size()) {
                sb.append(substrings.get(subs_index));
                subs_index++;
            }
        } else if (subs_index == substrings.size()) {
            while (word_index < words.size()) {
                sb.append(words.get(word_index));
                word_index++;
            }
        }
        sb.append(".com");
        log.info("result:{}", sb.toString());
        return sb.toString();
    }

    private static boolean checkDomain(String domain) {
        if (StringUtils.isEmpty(domain)) {
            return false;
        }
        //追加：.com的长度
        if (domain.length() > 24 || domain.length() < 9 || maxRepeatCharCount(domain) > 4) {
            return false;
        }
        if(!AliDNSHelper.domainCheckBuy(domain) || AliDNSHelper.fuzzyMatch(domain)){
            return false;
        }
        return true;
    }

    private static List<String> getSubString(int wordsLength) {
        int num = random(2) + 1;
        if (wordsLength < 0) {
            throw new RuntimeException("word length is less than zero");
        }
        List<String> subStrings = Lists.newArrayList();
        for (; num > 0; --num) {
            int len = random(3) + 1;
            char[] chars = new char[len];
            for (; len > 0; --len) {
                chars[chars.length - len] = randomAlphabet();
            }
            subStrings.add(new String(chars));
        }
        return subStrings;
    }

    private static String getWord() {
        int index = random(100);
        String alpha = ALPHABET_TOKEN.get(index);
        WordsMapper mapper = BeanFactoryUtils.getBean(WordsMapper.class);
        mapper.selectList(new QueryWrapper<Words>()
                .likeLeft(COLUMN_WORLD, alpha)
        );
        List<Words> words = mapper.selectList(new QueryWrapper<Words>().likeLeft(COLUMN_WORLD, alpha));
        Collections.shuffle(words);
        return words.get(random(words.size())).getWord();
    }

    private static int random(int range) {
        Random ran = new Random();
        return ran.nextInt(range);
    }

    private static char randomAlphabet() {
        int index = random(26);
        return (char) (97 + index);
    }

    private static int calWordsLength(List<String> words) {
        return words.stream().map(w -> w.length()).reduce(0, (acc, len) -> acc + len);
    }

    private static int maxRepeatCharCount(String s) {
        int cur_len = 1;
        int max_len = 1;
        int i = 1;
        while (i < s.length()) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                cur_len += 1;
            } else {
                cur_len = 1;
            }
            i += 1;
            if (max_len < cur_len) {
                max_len = cur_len;
            }
        }
        return max_len;
    }

    public static void main(String[] args) throws InterruptedException {
        //test
    }


}
