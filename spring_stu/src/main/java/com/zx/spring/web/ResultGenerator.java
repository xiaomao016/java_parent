package com.zx.spring.web;

/**
 * 响应结果生成工具
 * @author zhuzhaoyang
 * @date 2021年3月3日13:45:38
 */
public class ResultGenerator {
    private static final String DEFAULT_SUCCESS_MESSAGE = "请求成功";
    private static final String DEFAULT_FAIL_MESSAGE = "操作失败";

    public static Result genSuccessResult() {
        return Result.builder()
                .code(ResultCode.SUCCESS.code())
                .message(DEFAULT_SUCCESS_MESSAGE)
                .build();
    }

    public static <T> Result<T> genSuccessResult(T data) {
        return (Result<T>) Result.builder()
                .code(ResultCode.SUCCESS.code())
                .message(DEFAULT_SUCCESS_MESSAGE)
                .data(data)
                .build();
    }

    public static <T> Result<T> genLoginSuccessResult(T data) {
        return (Result<T>) Result.builder()
                .code(ResultCode.SUCCESS.code())
                .message(DEFAULT_SUCCESS_MESSAGE)
                .data(data)
                .type("login")
                .build();
    }

    public static Result genFailResult(String message) {
        return Result.builder()
                .code(ResultCode.FAIL.code())
                .message(message)
                .build();

    }
    public static Result genFailResult(){
        return Result.builder()
                .code(ResultCode.FAIL.code())
                .message(DEFAULT_FAIL_MESSAGE)
                .build();
    }

    public static Result genUnauthorized(String message) {
        return Result.builder()
                .code(ResultCode.UNAUTHORIZED.code())
                .message(message)
                .build();
    }

    public static Result genForbidden(String message) {
        return Result.builder()
                .code(ResultCode.FORBIDDEN.code())
                .message(message)
                .build();
    }
}
