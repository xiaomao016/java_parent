package com.zx.spring.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一API响应结果封装
 * @author zhuzhaoyang
 * @date 2021年3月3日13:44:45
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Result<T> {
    private int code;
    private String message;
    private T data;
    private String type;
}
