package com.zx.spring.web;

/**
 * 响应码枚举，参考HTTP状态码的语义
 * @author zhuzhaoyang
 * @date 2021年3月3日13:43:04
 */
public enum ResultCode {
    /**
     * 成功
     */
    SUCCESS(200),
    /**
     * 失败
     */
    FAIL(400),
    /**
     * token没有 或者token过期
     */
    UNAUTHORIZED(401),

    /**
     * 禁止访问,未授权
     */
    FORBIDDEN(403),
    /**
     * 接口不存在
     */
    NOT_FOUND(404),

    /**
     * 服务器内部错误
     */
    INTERNAL_SERVER_ERROR(500);

    private final int code;

    ResultCode(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }
}
