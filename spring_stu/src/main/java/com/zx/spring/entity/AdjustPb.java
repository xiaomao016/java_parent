package com.zx.spring.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-03-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("adjust_pb")
public class AdjustPb extends BaseEntity {

    private Integer platformId;

    private String pbDomain;

    //active=正在使用的    deprecated=弃用的（已经被adjust拉黑的） pendding = 待用的
    private String status;

    private String remark;


}
