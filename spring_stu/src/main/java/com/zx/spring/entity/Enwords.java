package com.zx.spring.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2023-03-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_enwords")
public class Enwords implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "word", type = IdType.ASSIGN_ID)
    private String word;

    private String translation;


}
