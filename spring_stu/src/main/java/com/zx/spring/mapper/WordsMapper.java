package com.zx.spring.mapper;

import com.zx.spring.entity.Words;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-03-03
 */
public interface WordsMapper extends BaseMapper<Words> {

}
