package com.zx.spring.mapper;

import com.aurora.dao.ad.entity.AffiliateTrafficTag;
import com.zx.spring.entity.AdjustPb;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zx
 * @since 2023-03-08
 */
public interface AdjustPbMapper extends BaseMapper<AdjustPb> {
    int insertBatch(@Param("records") List<AdjustPb> records);
}
