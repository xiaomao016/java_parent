package com.zx.spring.service.impl;

import com.zx.spring.entity.Words;
import com.zx.spring.mapper.WordsMapper;
import com.zx.spring.service.IWordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-03-03
 */
@Service
public class WordsServiceImpl extends ServiceImpl<WordsMapper, Words> implements IWordsService {

}
