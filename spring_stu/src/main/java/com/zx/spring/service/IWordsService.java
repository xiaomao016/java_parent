package com.zx.spring.service;

import com.zx.spring.entity.Words;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-03-03
 */
public interface IWordsService extends IService<Words> {

}
