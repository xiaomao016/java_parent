package com.zx.spring.service;

import com.zx.spring.entity.AdjustPb;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zx
 * @since 2023-03-08
 */
public interface IAdjustPbService extends IService<AdjustPb> {

}
