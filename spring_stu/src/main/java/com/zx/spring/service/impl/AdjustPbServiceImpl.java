package com.zx.spring.service.impl;

import com.zx.spring.entity.AdjustPb;
import com.zx.spring.mapper.AdjustPbMapper;
import com.zx.spring.service.IAdjustPbService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zx
 * @since 2023-03-08
 */
@Service
public class AdjustPbServiceImpl extends ServiceImpl<AdjustPbMapper, AdjustPb> implements IAdjustPbService {

}
