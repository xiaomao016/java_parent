package com.zx.spring.component;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.annotation.Configuration;

/**
 * @author zx
 * @description:
 * @date 2022/7/26
 */
@Configuration
public class BeanFactoryUtils implements BeanFactoryAware {

    private static BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        BeanFactoryUtils.beanFactory = beanFactory;
    }

    public static <T> T getBean(String signName) {
        return (T) beanFactory.getBean(signName);
    }

    public static <T> T getBean(Class<T> tClass) {
        return beanFactory.getBean(tClass);
    }

}
