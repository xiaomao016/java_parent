package com.zx.spring.component;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@Component
@Slf4j
public class ClickHouseClient implements InitializingBean {



    @Override
    public void afterPropertiesSet() throws Exception {
        Class.forName("com.clickhouse.jdbc.ClickHouseDriver");
        Connection connection = DriverManager.getConnection("jdbc:clickhouse://cc-0xi15d5d51swsq834.public.clickhouse.ads.aliyuncs.com:8123/aurora_ads?allowMultiQueries=true","wangmeng_test","Dzk2023#2023");
//        Connection connection = DriverManager.getConnection("jdbc:clickhouse://47.253.50.171:8123/aurora_ads?allowMultiQueries=true","default","rUWDat19");
        log.info("clickhouse");
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from aurora_ads.aff_postback_report where 1=1 limit 10");

        while (resultSet.next()) {
            // 获取每一行的数据
            String column1 = resultSet.getString("country");
            log.info(column1);
        }
        resultSet.close(); // 最后记得关闭ResultSet


//        for(int i=0;i<10;++i) {
//            long next = IdWorker.getId();
//            log.info("id:{}",next);
//        }
    }

//    public static void main(String[] args) {
//        ClickHouseClient clickHouseClient = new ClickHouseClient();
//        clickHouseClient.afterPropertiesSet();
//    }
}
