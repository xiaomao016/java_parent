package com.zx.spring.component;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/19 0019 11:12
 * @description:
 */
@Order(2)
@Component
@WebFilter(urlPatterns = "/chain/*",filterName = "chainFilter2")
public class ServletFilter2 implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("拦截处理:方法前2");
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        String s = "掉包abc";
//        response.setCharacterEncoding("UTF-8");
//        response.getWriter().write(s);

        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("拦截处理:方法后2");

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Filter init2");
    }

    @Override
    public void destroy() {
        System.out.println("Filter destroy2");
    }
}
