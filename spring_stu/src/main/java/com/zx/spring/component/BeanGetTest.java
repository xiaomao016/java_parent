package com.zx.spring.component;

import com.zx.spring.entity.BeanDemo;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class BeanGetTest implements InitializingBean {


    @Override
    public void afterPropertiesSet() throws Exception {

        BeanDemo beanDemo = BeanFactoryUtils.getBean("getSecond");
        System.out.println("bean name:"+beanDemo.getName());
    }
}
