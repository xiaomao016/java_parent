package com.zx.spring.component;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/19 0019 11:03
 * @description:
 */
@Configuration //等价于一个Spring的xml的配置文件
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] path = {
                "/chain/**"
        };

        registry.addInterceptor(new Inteceptor()).addPathPatterns(path);
        registry.addInterceptor(new Inteceptor2()).addPathPatterns(path);
    }
}
