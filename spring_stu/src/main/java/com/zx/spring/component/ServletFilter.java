package com.zx.spring.component;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/19 0019 11:12
 * @description:
 */
@Order(1)
@Component
@WebFilter(urlPatterns = "/chain/*",filterName = "chainFilter")
public class ServletFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("拦截处理:方法前");
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("拦截处理:方法后");

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Filter init");
    }

    @Override
    public void destroy() {
        System.out.println("Filter destroy");
    }
}
