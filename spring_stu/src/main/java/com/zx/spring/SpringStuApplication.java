package com.zx.spring;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.zx.spring.mapper")
@SpringBootApplication
@EnableScheduling
public class SpringStuApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringStuApplication.class, args);
    }

}
