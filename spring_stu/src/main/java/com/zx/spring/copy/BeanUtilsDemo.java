package com.zx.spring.copy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/27 0027 11:01
 * @description:
 */
@Slf4j
public class BeanUtilsDemo {

    public static void main(String[] args) {
        UserDO userDO = DataUtil.createData();
        log.info("拷贝前,userDO:{}", userDO);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userDO,userDTO);
        log.info("拷贝后,userDO:{}", userDTO);
        int i =10;
        if(i==10){
            System.out.println("if01");
        }else if(i>10){
            System.out.println("if02");
        }

        System.out.println("==========");
        Map<String,Long> m = new HashMap<>();
        String key = "aap";
        m.put(key,1232L);

        key = "ppp";
        System.out.println(m.get(key));
        System.out.println(m.get("aap"));
        System.out.println("===================");
        String dirPath = "data/20230619151846/1669251301379461121/";
        String file="/opt/hbase_data/20230619151846/1669251301379461121/bidmatrix-android-IN-20230619151846_0";
        String fileName = Paths.get(file).getFileName().toString();
        System.out.println(fileName);
        int index = Integer.valueOf(fileName.substring(fileName.indexOf("_") + 1)) + 1;
        //初始化中间文件路径
        String filePath = dirPath + "bidmatrix-android-IN-20230619151846" + "_" + index;
        System.out.println(filePath);

    }
}
