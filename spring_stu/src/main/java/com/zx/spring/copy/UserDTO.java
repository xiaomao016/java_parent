package com.zx.spring.copy;

import lombok.Data;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/27 0027 11:05
 * @description:
 */
@Data
public class UserDTO {
    private int id;
    private String userName;
}
