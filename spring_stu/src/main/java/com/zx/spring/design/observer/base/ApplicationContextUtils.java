package com.zx.spring.design.observer.base;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 17:37
 * @description:
 */
@Component
public class ApplicationContextUtils implements ApplicationContextAware {
    private static ApplicationContext context;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context=applicationContext;
    }

    //根据bean名字获取工厂中指定的bean对象
    public static Object getBean(String beanName){
        System.out.println("进行中");
        return context.getBean(beanName);
    }

    //通过class获取Bean.
    public static <T> T getBean(Class<T> clazz){
        return context.getBean(clazz);
    }
}