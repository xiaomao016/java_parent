package com.zx.spring.design.observer.base;

import com.zx.spring.design.observer.entity.User;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:04
 * @description:
 */
@Service
public class UserService {
    public long register(User user) {
        return UUID.randomUUID().getLeastSignificantBits();
    }
}
