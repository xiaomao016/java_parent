package com.zx.spring.design.observer.base;

import org.springframework.stereotype.Service;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:05
 * @description:
 */
@Service
public class NotificationService {
    public void sendInboxMessage(long userId) {
        System.out.println("发送邮箱消息:"+userId+":");
    }
}
