package com.zx.spring.design.observer.base;

import org.springframework.stereotype.Service;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:04
 * @description:
 */
@Service
public class PromotionService {
    public void issueNewUserExperienceCash(long userId) {
        System.out.println("发送优惠券");
    }
}
