package com.zx.spring.design.observer.observers;


import com.zx.spring.design.observer.Subscribe;
import com.zx.spring.design.observer.base.PromotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:03
 * @description:
 */
@Component
public class RegPromotionObserver {
    @Autowired
    private PromotionService promotionService; // 依赖注入

    @Subscribe
    public void handleRegSuccess(Long userId) {
        System.out.println("promotion");
        promotionService.issueNewUserExperienceCash(userId);
    }
}
