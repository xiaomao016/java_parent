package com.zx.spring.design.observer.base;

import com.zx.spring.design.observer.AsyncEventBus;
import com.zx.spring.design.observer.EventBus;
import com.zx.spring.design.observer.observers.MultiParamsObserver;
import com.zx.spring.design.observer.observers.RegNotificationObserver;
import com.zx.spring.design.observer.observers.RegPromotionObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 16:17
 * @description:
 */
@Configuration
public class BaseBeans {

    private static final int DEFAULT_EVENTBUS_THREAD_POOL_SIZE = 20;




    @Bean("eventBus")
    public EventBus eventBus(){
        EventBus eventBus = new EventBus();
        registObservers(eventBus);
        return eventBus;
    }

    @Bean("asyncEventBus")
    public AsyncEventBus asyncEventBus(){
        AsyncEventBus eventBus =new AsyncEventBus(Executors.newFixedThreadPool(DEFAULT_EVENTBUS_THREAD_POOL_SIZE));//异步非阻塞模式
        registObservers(eventBus);
        return eventBus;
    }

    public void registObservers(EventBus eventBus){
        eventBus.register(ApplicationContextUtils.getBean(RegNotificationObserver.class));
        eventBus.register(ApplicationContextUtils.getBean(RegPromotionObserver.class));
        eventBus.register(ApplicationContextUtils.getBean(MultiParamsObserver.class));
    }



}
