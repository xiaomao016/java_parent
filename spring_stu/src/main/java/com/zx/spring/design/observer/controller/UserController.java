package com.zx.spring.design.observer.controller;

import com.zx.spring.design.observer.EventBus;
import com.zx.spring.design.observer.base.UserService;
import com.zx.spring.design.observer.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:02
 * @description:
 */
@RestController
@RequestMapping("/design")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private EventBus eventBus;


    @PostMapping("register")
    public Long register(@RequestBody User user) {
        //省略输入参数的校验代码
        // 省略userService.register()异常的try-catch代码
        long userId = userService.register(user);
        System.out.println(userId);
        eventBus.post(userId);
//        eventBus.post();
        return userId;
    }

    @GetMapping("/t")
    public String t(){
        return "success";
    }
}
