package com.zx.spring.design.observer.entity;

import lombok.Data;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 16:28
 * @description:
 */
@Data
public class User {
    private String phone;
    private String password;
}
