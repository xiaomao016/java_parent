package com.zx.spring.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/20 0020 11:29
 * @description:
 */
@Aspect
@Component
@Slf4j
public class LogAspect {

    @Pointcut("@annotation(com.zx.spring.aspect.WebLog)")
    public void annotationPointcut() {
    }

//    @Before("annotationPointcut()")
//    public void beforePointcut(JoinPoint joinPoint) {
//        MethodSignature methodSignature =  (MethodSignature) joinPoint.getSignature();
//        Method method = methodSignature.getMethod();
//        WebLog annotation = method.getAnnotation(WebLog.class);
//        String value = annotation.value();
//        System.out.println("准备"+value);
//    }

    @Around("@annotation(webLog)")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint,WebLog webLog) throws Throwable {
        long startTime = System.currentTimeMillis();
        log.info("before");
        Object result = proceedingJoinPoint.proceed();
        // 打印出参
        log.info("Response Args  : {}", result);
        // 执行耗时
        log.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
        return result;
    }

//    @After("@annotation(WebLog)")
//    public void afterPointcut(JoinPoint joinPoint) {
//        MethodSignature methodSignature =  (MethodSignature) joinPoint.getSignature();
//        Method method = methodSignature.getMethod();
//        WebLog annotation = method.getAnnotation(WebLog.class);
//        String value = annotation.value();
//        System.out.println("结束"+value);
//    }


}
