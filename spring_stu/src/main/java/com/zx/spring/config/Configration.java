package com.zx.spring.config;

import com.zx.spring.entity.BeanDemo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class Configration {

    @Bean
    public BeanDemo getHoloDataSource(){
        try {
           BeanDemo b = new BeanDemo();
           b.setName("first");
            return b;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }

    @Bean
    @Qualifier("second")
    public BeanDemo getSecond(){
        try {
            BeanDemo b = new BeanDemo();
            b.setName("second");
            return b;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }


}
