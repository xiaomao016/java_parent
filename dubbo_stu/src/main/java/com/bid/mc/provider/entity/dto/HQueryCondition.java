package com.bid.mc.provider.entity.dto;

import com.bid.mc.provider.entity.vo.Where;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

@Data
public class HQueryCondition {


    public static final String OS_NUMBER_ANDROID="1";
    public static final String OS_NUMBER_IOS="0";

    private String country;

    private String os;

    private String appId;

    private String dataName;

    private List<HWhereObject>  whereList;

    //兼容之前的解析逻辑
    private List<Where>  whereLists;

    //用于记录taskId,给到mc的查询
    @JsonSerialize(using= ToStringSerializer.class)
    private Long hbaseSearchResultId;  //数据库的主键





}
