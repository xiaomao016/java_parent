package com.bid.mc.provider.entity.vo;

import com.bid.mc.provider.enums.LogicalOperatorEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zx
 * @description:
 * @date 2022/4/28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Where {
    private LogicalOperatorEnum operatorEnum;//运算符默认都是AND
    private List<WhereBean> whereBeanList;
}
