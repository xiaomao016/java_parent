package com.bid.mc.provider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2022-05-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tb_mc_search")
public class McSearch extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String country;

    private String os;

    private String status;

    private String tags;

    private String appId;

    /**
     * 当前查询数据量
     */
    private Long count;

    private String changeV;


}
