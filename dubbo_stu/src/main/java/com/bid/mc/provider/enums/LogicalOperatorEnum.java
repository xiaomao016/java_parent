package com.bid.mc.provider.enums;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public enum LogicalOperatorEnum {
    AND("AND"),
    OR("OR"),
    NOT("NOT");

    private String value;
    public String getValue() {
        return this.value;
    }
}
