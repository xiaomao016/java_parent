package com.bid.mc.provider.entity.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TagVO {

    private String logicalOperator;

    private String tagName;

    private String arithmeticOperator;

    private String operator;   //临时过渡用的

    private String tagValue;

    private String tagUnit; //单位：默认为空，当tagName=recentDays时，要指明时间单位



    public static void main(String[] args) {

        List<TagVO> tagVOList = new ArrayList<>();
        for (int i=0; i < 15; i++){
            TagVO tag = new TagVO();

            if(i%3==0){
                tag.setLogicalOperator("or");
            }else{
                tag.setLogicalOperator("and"+i);
            }
            tagVOList.add(tag);
        }


        //将tag 按照 or分割
        ArrayList<TagVO> tagVOS = new ArrayList<>();
        List<List<TagVO>> lists= new ArrayList<>();
        for (TagVO tag : tagVOList){
            String logicalOperator = tag.getLogicalOperator();
            if(logicalOperator.equalsIgnoreCase("or")){
                    if(!tagVOS.isEmpty()){
                        lists.add(tagVOS);
                    }

                    tagVOS = new ArrayList<>();
                    tagVOS.add(tag);

            }else{
                tagVOS.add(tag);
            }
        }
        if(!tagVOS.isEmpty()){
            lists.add(tagVOS);
        }
        for (List<TagVO> list : lists) {
             System.out.println(list.toString());
        }
    }
}
