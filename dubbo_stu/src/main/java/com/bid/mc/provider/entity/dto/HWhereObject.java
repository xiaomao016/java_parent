package com.bid.mc.provider.entity.dto;

import lombok.Data;

import java.util.List;

@Data
public class HWhereObject {

    private String logicalOperator;

    private List<TagVO> whereChildList;

}
