package com.bid.mc.provider.enums;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * EQ:等于
 * GT:大于
 * LT:小于
 * NE: 不等于
 * GE:大于等于
 * LE:小于等于
 */
@AllArgsConstructor
@NoArgsConstructor
public enum ArithmeticOperatorEnum {
    GR(">"),
    EQ("="),
    LT("<"),
    NE("<>"),
    GE(">="),
    LE("<=");

    private String value;
    public String getValue() {
        return this.value;
    }
}
