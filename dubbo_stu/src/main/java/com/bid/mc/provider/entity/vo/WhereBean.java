package com.bid.mc.provider.entity.vo;

import com.bid.mc.provider.enums.ArithmeticOperatorEnum;
import com.bid.mc.provider.enums.LogicalOperatorEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zx
 * @description:
 * @date 2022/4/28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WhereBean {
    private LogicalOperatorEnum operatorEnum;//连接符号：AND OR NOT
    private ArithmeticOperatorEnum compareEnum;//条件符号: > < =
    private String tag;//条件字段
    private String tagValue;//条件值
}
