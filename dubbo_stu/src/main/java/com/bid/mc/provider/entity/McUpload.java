package com.bid.mc.provider.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bid.mc.provider.entity.dto.HQueryCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2022-05-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tb_mc_upload")
public class McUpload extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String country;

    private String os;

    private Long deviceCount;

    /**
     * 选择的标签
     */
    private String tags;

    /**
     * 给这个数据包起的名字
     */
    private String deviceName;

    /**
     * 保存在oss上的路径
     */
    private String ossSavedUrl;

    private String isUpdate;

    private String remark;

    private String status;

    private String appId;

    /**
     * 一天的预估量
     */
    private Long countDay;

    private String changeV;

    @TableField(exist = false)
    private HQueryCondition hQueryCondition;
}
