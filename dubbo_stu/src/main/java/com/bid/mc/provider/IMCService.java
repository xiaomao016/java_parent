package com.bid.mc.provider;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bid.mc.provider.entity.McSearch;
import com.bid.mc.provider.entity.McUpload;
import com.bid.mc.provider.entity.dto.HQueryCondition;

import java.io.IOException;

/**
 * 数据包刷选、查询相关服务接口
 *
 */
public interface IMCService {

    //添加
    McSearch addSearchTask(HQueryCondition hQueryCondition) throws IOException;

    void addUploadTask(HQueryCondition hQueryCondition, String dataName) throws IOException;

    //查询+列表
    IPage pageSearch(Integer pageNum, Integer pageSize, String os, String country, String v);

    IPage pageUpload(Integer pageNum, Integer pageSize, String os, String country, String v);

    //删除
    boolean deleteSearch(Long id);

    boolean deleteUpload(Long id);

    //详情
    McUpload getById(Long id);

    String test();
}
