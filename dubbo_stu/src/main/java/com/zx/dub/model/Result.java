package com.zx.dub.model;

import lombok.Data;
import org.apache.http.HttpStatus;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {

    /**
     * 成功标志
     */
    private boolean success;

    /**
     * 返回处理消息
     */
    private String message;

    private String sys;   //click

    /**
     * 返回代码
     */
    private Integer code;

    /**
     * 返回数据对象 data
     */
    private T result;

    public Result() {
        this.sys = "click";
    }

    public Result<T> success(String message) {
        this.message = message;
        this.code = HttpStatus.SC_OK;
        this.success = true;
        return this;
    }


    public static Result insert() {
        Result r = new Result();
        r.setSuccess(true);
        r.setCode(HttpStatus.SC_OK);
        r.setMessage("add success");
        return r;
    }

    public static Result update() {
        Result r = new Result();
        r.setSuccess(true);
        r.setCode(HttpStatus.SC_OK);
        r.setMessage("update success");
        return r;
    }

    public static Result delete() {
        Result r = new Result();
        r.setSuccess(true);
        r.setCode(HttpStatus.SC_OK);
        r.setMessage("delete success");
        return r;
    }

    public static<T> Result<T> add(T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(HttpStatus.SC_OK);
        r.setResult(data);
        return r;
    }

    public static<T> Result<T> OK(T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(HttpStatus.SC_OK);
        r.setResult(data);
        return r;
    }

    public static<T> Result<T> OK(String msg, T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(HttpStatus.SC_OK);
        r.setMessage(msg);
        r.setResult(data);
        return r;
    }

    public static Result<Object> error(String msg) {
        return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
    }

    public static<T> Result<T> error(int code, String msg) {
        Result r = new Result();
        r.setCode(code);
        r.setMessage(msg);
        r.setSuccess(false);
        return r;
    }

}
