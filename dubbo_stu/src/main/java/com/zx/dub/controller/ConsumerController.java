package com.zx.dub.controller;

import com.bid.mc.provider.IMCService;
import com.zx.dub.model.Result;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zx
 * @description:
 * @date 2022/5/31
 */
@RestController
public class ConsumerController {

    @DubboReference(version = "${bid-dubbo.service.version}", timeout = 1000, connections = 300)
    private IMCService mcService;

    @GetMapping("/api/test1")
    public Result test() {
//        String testMsg = mcService.test();
        return Result.OK("aa");
    }

    @GetMapping("/api/t")
    public Result test2() {
//        String testMsg = mcService.test();
        return Result.OK("aa");
    }

}
