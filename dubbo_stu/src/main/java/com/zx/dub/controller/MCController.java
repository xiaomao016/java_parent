package com.zx.dub.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bid.mc.provider.IMCService;
import com.bid.mc.provider.entity.McSearch;
import com.bid.mc.provider.entity.McUpload;
import com.bid.mc.provider.entity.dto.HQueryCondition;
import com.zx.dub.model.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author zx
 * @description: 用于统一测试，实际调用走 Dubbo 服务
 * @date 2022/5/30
 */
@RestController
@RequestMapping
public class MCController {

    @DubboReference(version = "${bid-dubbo.service.version}", timeout = 1000, connections = 300)
    private IMCService service;

    @GetMapping("/api/test")
    public Result test() {
        String testMsg = service.test();
        return Result.OK(testMsg);
    }

    //==============添加查询或上传任务=============

    /**
     * 添加一个查询任务
     * @param hQueryCondition
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/admin/mc/search/task/add")
    public Result addReportTask(@RequestBody @Validated HQueryCondition hQueryCondition) throws IOException {
        McSearch mcSearch = service.addSearchTask(hQueryCondition);
        return Result.OK("查询任务创建成功 请在查询记录中等待完成 任务ID="+mcSearch.getId());
    }

    /**
     * 添加一个上传任务
     * @param hQueryCondition
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/admin/mc/upload/task/add")
    public Result addUploadTask(@RequestBody @Validated HQueryCondition hQueryCondition) throws IOException {
        service.addUploadTask(hQueryCondition, hQueryCondition.getDataName());
        return Result.OK("上传任务已经添加到上传记录中，正在执行请等待！") ;
    }


    //===============删除记录接口==================
    /**
     * 删除查询记录
     * @param id
     * @return
     */
    @GetMapping(value = "/admin/mc/search/delete")
    public Result deleteSearch(@RequestParam(value = "id") Long id) {
        service.deleteSearch(id);
        return Result.delete();
    }

    /**
     * 删除上传记录：要判断是否有任务在使用
     * @param id
     * @return
     */
    @GetMapping(value = "/admin/mc/upload/result/delete")
    public Result deleteUpload(@RequestParam(value = "id") Long id){
        boolean delete = service.deleteUpload(id);
        if(delete){
            return Result.delete();
        }else{
            return Result.error(100,"数据在使用中");
        }
    }

    //===============查询列表接口===================

    /**
     * 列表分页查询
     * @param pageNum
     * @param pageSize
     * @param country
     * @param os
     * @return
     */
    @GetMapping("/admin/mc/search/page")
    public Result pageSearch(@RequestParam Integer pageNum,
                                          @RequestParam Integer pageSize,
                                          @RequestParam(required = false) String country,
                                          @RequestParam(required = false) String os) {
        IPage page = service.pageSearch(pageNum, pageSize, os, country, "1");
        return Result.OK(page);
    }

    //===============上传记录接口===================
    /**
     * 列表分页查询
     * @param pageNum
     * @param pageSize
     * @param country
     * @param os
     * @return
     */
    @GetMapping("/admin/mc/upload/result/page")
    public Result pageUpload(@RequestParam Integer pageNum,
                                        @RequestParam Integer pageSize,
                                        @RequestParam(required = false) String country,
                                        @RequestParam(required = false) String os) {
        IPage page = service.pageUpload(pageNum, pageSize, os, country, "1");
        return Result.OK(page);
    }


    //=================详情：只有上传记录有=========================
    /**
     * 查询某个上传记录详情
     * @param id
     * @return
     */
    @GetMapping(value = "/admin/mc/upload/result/get")
    public Result getUploadResultById(@RequestParam(value = "id") String id) {
        if(StringUtils.isEmpty(id) || !StringUtils.isNumeric(id)){
            return Result.error("param type error!");
        }
        McUpload uploadResult = service.getById(Long.parseLong(id));
        String tags = uploadResult.getTags();
        HQueryCondition hQueryCondition = JSON.parseObject(tags, HQueryCondition.class);
        String os = hQueryCondition.getOs();
        if(os.equals("1") || os.equalsIgnoreCase("Android")){
            hQueryCondition.setOs("Android");
        }else{
            hQueryCondition.setOs("iOS");
        }
        uploadResult.setHQueryCondition(hQueryCondition);
        return Result.OK(uploadResult);
    }



}
