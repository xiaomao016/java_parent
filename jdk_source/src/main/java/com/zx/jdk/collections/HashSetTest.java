package com.zx.jdk.collections;

import java.util.*;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/2/20 0020 14:39
 * @description:
 */
public class HashSetTest {
    public static void main(String[] args) {
        HashSet<String> s = new HashSet<>();

        Hashtable<String,Integer> ht = new Hashtable<>();
        ht.put("a",1);

        Collections.synchronizedMap(new HashMap<>());

        TreeMap<String,Integer> tm = new TreeMap<>();
        tm.put("a",1);

        LinkedHashMap<String ,Integer> lm =new LinkedHashMap<>();
        lm.put("a",1);
    }
}
