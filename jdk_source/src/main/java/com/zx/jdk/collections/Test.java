package com.zx.jdk.collections;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/2/20 0020 09:49
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Stu[] ss = new Stu[2];
        Stu s1 = new Stu("a");
        Stu s2 = new Stu("b");
        ss[0]=s1;
        ss[1]=s2;

        Stu e = ss[0];
        s1.setName("change");

        ss[0]=null;
        System.out.println(e.getName());
        System.out.println(s1.getName());


        System.out.println("=======");
        Stu sc = new Stu("sc");
        change(sc);
        System.out.println(sc.getName());
    }
    public static void change(Stu s){
        s.setName("change了");
        System.out.println(s.getName());
    }
}

class Stu {
    private String name;

    Stu(String name){
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}