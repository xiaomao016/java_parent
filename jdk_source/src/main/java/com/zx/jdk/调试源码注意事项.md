#### 注意事项

debug时，代码执行过程为：
debug程序本身-》我们自己的代码
jdk源码的端点在运行到自己写的代码之前，先去掉源码中的断点，
因为在idea的debug程序本身初始化过程中可能会调用相关数据结构
为了避免和自己代码区分，此时不在jdk源码中打断点，等运行到自己代码
时再放开jdk中的断点。

#### 集合类



扩容机制：https://blog.csdn.net/u010890358/article/details/80496144

> **20、LinkedHashMap 和 TreeMap 排序的区别？**
> LinkedHashMap 和 TreeMap 都是提供了排序支持的 Map，区别在于支持的排序方式不同：
>
> LinkedHashMap：保存了数据的插入顺序，也可以通过参数设置，保存数据的访问顺序。
>
> TreeMap：底层是红黑树实现。可以指定比较器（Comparator 比较器），通过重写 compare 方法来自定义排序；如果没有指定比较器，TreeMap 默认是按 Key 的升序排序（如果 key 没有实现 Comparable接口，则会抛异常）。
>
> **21、HashMap 和 Hashtable 的区别？**
> HashMap 允许 key 和 value 为 null，Hashtable 不允许。
>
> HashMap 的默认初始容量为 16，Hashtable 为 11。
>
> HashMap 的扩容为原来的 2 倍，Hashtable 的扩容为原来的 2 倍加 1。
>
> HashMap 是非线程安全的，Hashtable 是线程安全的，使用 synchronized 修饰方法实现线程安全。
>
> HashMap 的 hash 值重新计算过，Hashtable 直接使用 hashCode。
>
> HashMap 去掉了 Hashtable 中的 contains 方法。
>
> HashMap 继承自 AbstractMap 类，Hashtable 继承自 Dictionary 类。
>
> HashMap 的性能比 Hashtable 高，因为 Hashtable 使用 synchronized 实现线程安全，还有就是 HashMap 1.8 之后底层数据结构优化成 “数组+链表+红黑树”，在极端情况下也能提升性能。

##### HashMap

1.HashMap的初始容量时多少？

2.HashMap的hash如何计算，为何是这样计算？

3.HashMap如何计算Node在数组中的下标？为何要采用这样的方式？

4.HashMap有哪些常用的属性？作用是什么？

5.HashMap如何扩容？扩容时分哪几种情况？

6.LinkedHashMap和HashMap的区别？

7.TreeMap，HashSet

8.并发情况下的Map？ 扩展到sychronize关键字，扩展到CAS锁机制，扩展到juc，扩展到分布式，扩展到jvm，扩展到布隆过滤器

```java
//这里其实叫 “扰动函数”，因为在计算元素的位置下标时，用的是
// hash & (capacity-1)  而capacity是2的n次方，减一就相当以一个“掩码的作用”
//hash的计算时用hashcode，而整数范围高大40亿，这样一个空间，按说映射比较均匀，但是根据上面公式，则大部分只会有
//hash的低位参与到运算。
// 为了增加hash的随机度，用右移16位和其或运算，相当于将高位信息也保留到了hash中。
 //计算hash
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

//根据参数确定table数组容量，默认是16 = 1<<4 
/**
     * Returns a power of two size for the given target capacity.
     */
    static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

```



```java
//put过程：
// resize :先进行初始化 table数组
final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
               boolean evict) {
    HashMap.Node<K,V>[] tab; HashMap.Node<K,V> p; int n, i;
    if ((tab = table) == null || (n = tab.length) == 0)
        //resize进行table数组初始化
        n = (tab = resize()).length;
    if ((p = tab[i = (n - 1) & hash]) == null)
        //如果对应hash的位置没有值，直接新建Node
        tab[i] = newNode(hash, key, value, null);
    else {
        HashMap.Node<K,V> e; K k;
        //如果有hash冲突并且是key一致，则直接覆盖
        if (p.hash == hash &&
            ((k = p.key) == key || (key != null && key.equals(k))))
            e = p;
        else if (p instanceof HashMap.TreeNode)
            e = ((HashMap.TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
        else {
            //如果是hash冲突，但是key不一致（小概率），此时进行拉链存储（同时判断是否要进行树化操作）
            for (int binCount = 0; ; ++binCount) {
                if ((e = p.next) == null) {
                    //将新节点插入链表尾部
                    p.next = newNode(hash, key, value, null);
                    if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                        treeifyBin(tab, hash);
                    break;
                }
                //如果拉链过程发现key重复，也进入覆盖逻辑
                if (e.hash == hash &&
                    ((k = e.key) == key || (key != null && key.equals(k))))
                    break;
                p = e;
            }
        }
        //key已经存在时覆盖原有值
        if (e != null) { // existing mapping for key
            V oldValue = e.value;
            if (!onlyIfAbsent || oldValue == null)
                e.value = value;
            afterNodeAccess(e);
            return oldValue;
        }
    }
    //更新 Map被操作次数
    ++modCount;
    //判断是否需要扩容
    if (++size > threshold)
        resize();
    afterNodeInsertion(evict);
    return null;
}
```

```java
//树化过程
final void treeifyBin(Node<K,V>[] tab, int hash) {
    int n, index; Node<K,V> e;
	//当当前table列表容量不足64时，只进行扩容，而不进行树化
    if (tab == null || (n = tab.length) < MIN_TREEIFY_CAPACITY)
        resize();
    else if ((e = tab[index = (n - 1) & hash]) != null) {
        TreeNode<K,V> hd = null, tl = null;
        do {
            TreeNode<K,V> p = replacementTreeNode(e, null);
            if (tl == null)
                hd = p;
            else {
                p.prev = tl;
                tl.next = p;
            }
            tl = p;
        } while ((e = e.next) != null);
        if ((tab[index] = hd) != null)
            hd.treeify(tab);
    }
}
```

```java
//扩容过程： 
final Node<K,V>[] resize() {
        Node<K,V>[] oldTab = table;
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        int oldThr = threshold;
        int newCap, newThr = 0;
    	//如果是不是第一次put，即Node数组有值
        if (oldCap > 0) {
            if (oldCap >= MAXIMUM_CAPACITY) {
                threshold = Integer.MAX_VALUE;
                return oldTab;
            }
            //如果之前数组容量大于16，扩容到原来的两倍，并且将threshold页置为原来的两倍
            else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                     oldCap >= DEFAULT_INITIAL_CAPACITY)
                newThr = oldThr << 1; // double threshold
        }
    	//如果是第一次put，即初始化table数组，则只需要将构造hashmap时计算的值赋值给数组容量即可
    	//因为初次new hasmap时，只设置了threshold。
        else if (oldThr > 0) // initial capacity was placed in threshold
            newCap = oldThr;
        else {               // zero initial threshold signifies using defaults
            //如果创建hashmap是传的是0，则使用默认长度
            newCap = DEFAULT_INITIAL_CAPACITY;
            newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
        }
        if (newThr == 0) {
            float ft = (float)newCap * loadFactor;
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
                      (int)ft : Integer.MAX_VALUE);
        }
        threshold = newThr;
        @SuppressWarnings({"rawtypes","unchecked"})
            Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
        table = newTab;
        if (oldTab != null) {
            //进行数据拷贝
            for (int j = 0; j < oldCap; ++j) {
                Node<K,V> e;
                if ((e = oldTab[j]) != null) {
                    oldTab[j] = null;
                    if (e.next == null)
                        //如果没有发生hash冲突，直接放入新的数组
                        newTab[e.hash & (newCap - 1)] = e;
                    else if (e instanceof TreeNode)
                        //当前节点是采用红黑树情况
                        ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
                    else { // preserve order
                        // 把当前index对应的链表分成两个链表，减少扩容的迁移量
                        Node<K,V> loHead = null, loTail = null;
                        Node<K,V> hiHead = null, hiTail = null;
                        Node<K,V> next;
                        do {
                            next = e.next;
                            //不需要移动的链表
                            if ((e.hash & oldCap) == 0) {
                                if (loTail == null)
                                    loHead = e;
                                else
                                    loTail.next = e;
                                loTail = e;
                            }
                            //需要移动的链表
                            else {
                                if (hiTail == null)
                                    hiHead = e;
                                else
                                    hiTail.next = e;
                                hiTail = e;
                            }
                        } while ((e = next) != null);
                        if (loTail != null) {
                            loTail.next = null;
                            newTab[j] = loHead;
                        }
                        if (hiTail != null) {
                            hiTail.next = null;
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
            }
        }
        return newTab;
    }
```

##### LinkedHashMap

继承了HashMap，并重写了 hashmap的部分回调方法，进而保持了一个双向链表，记录了元素插入的顺序。

```java
 void afterNodeAccess(Node<K,V> p) { }
void afterNodeInsertion(boolean evict) { }
void afterNodeRemoval(Node<K,V> p) { }
```

##### TreeMap

基于红黑树，默认安装key的字典顺序排序，可以传入一个比较器。

##### ArrayList

1.初始容量时多少？如何扩容

2.迭代器模式，快照如何处理？

3.扩展到设计模式，扩展到并发版ArrayList



#### 思考：

##### 1.移位符号

```java
java中有三种移位运算符

<<      :     左移运算符，num << 1,相当于num乘以2

>>      :     右移运算符，num >> 1,相当于num除以2

>>>    :     无符号右移，忽略符号位，空位都以0补齐
```



