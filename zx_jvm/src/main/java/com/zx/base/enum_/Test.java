package com.zx.base.enum_;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 14:50
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(ColorEnum.RED);
        System.out.println(ColorEnum.RED.name());
        System.out.println(ColorEnum.RED.ordinal());

        String jsonss = "{\n" +
                "            \"offer_id\":10508597,\n" +
                "            \"name\":\"Wajeez Wajeez SA/AE/US/QA/DZ IOS \",\n" +
                "            \"os\":[\n" +
                "                \"ios\"\n" +
                "            ],\n" +
                "            \"geo\":[\n" +
                "                \"qa\",\n" +
                "                \"sa\",\n" +
                "                \"ae\",\n" +
                "                \"us\",\n" +
                "                \"dz\"\n" +
                "            ],\n" +
                "            \"payout\":2.4,\n" +
                "            \"payout_type\":\"CPA\",\n" +
                "            \"icon\":\"\",\n" +
                "            \"url_preview\":\"https://apps.apple.com/us/app/wajeez-audiobooks-podcasts/id1483042035\",\n" +
                "            \"bundle\":\"id1483042035\",\n" +
                "            \"descr\":\"Подписка: \\n1 месяц - 2,4$ \\n6 месяцев - 4.8$\\n12 месяцев - 14$\",\n" +
                "            \"comment\":\"CPA = purchase\\n\\nNo fraud No bot\",\n" +
                "            \"end_date\":\"2023-02-28T00:00:00Z\",\n" +
                "            \"publisher_daily_cap\":0,\n" +
                "            \"publisher_total_cap\":0,\n" +
                "            \"publisher_month_cap\":0,\n" +
                "            \"goal_id\":\"purchase\",\n" +
                "            \"tracking_link\":\"https://trk.surfer.media/track?pubid=7258&oid=10508597\",\n" +
                "            \"currency\":\"USD\",\n" +
                "            \"required_appname\":false,\n" +
                "            \"required_device_id\":false,\n" +
                "            \"incent_allowed\":false,\n" +
                "            \"city_id\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"city_name\":null,\n" +
                "            \"city_exclude\":false,\n" +
                "            \"mmp_link\":\"\",\n" +
                "            \"mmp_links\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"is_mmp\":false\n" +
                "        }";

        JSONObject offer = JSON.parseObject(jsonss);

        List<String> countries = offer.getJSONArray("geo").toJavaList(String.class).stream()
                .map(code -> code.toUpperCase())
                .collect(Collectors.toList());

        System.out.println(JSONObject.toJSONString(countries));

        List<String> platforms = Lists.newArrayList("ios", "android");

        String platform = String.join(",", platforms);
        System.out.println(platform);

        System.out.println("=======");
        Set<String> events = new HashSet<>();
        boolean flag = false;

        events.add("regist");
        for (int i = 0; i < 10; ++i) {
            if (i == 5) {
                if (events.contains("regist")) {
                    flag = true;
                    break;
                }
                events.add("regist");
            } else {
                events.add("aa");
            }
        }
        System.out.println(flag);

        System.out.println("========");
        List<String> items = Arrays.asList("apple", "apple", "banana", "apple", "orange",
                "banana", "papaya", "Hello", "word", "Hello", "beijing", "shanghai", "china");

        Map<String, Long> wordCount = items
                .stream()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting())
                );

        Map<String, Long> finalMap = new LinkedHashMap<>();
        wordCount.entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue()
                        .reversed())
                .forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

        // 有序结果
        System.out.println(finalMap);
        AtomicBoolean flag2 = new AtomicBoolean(false);

        wordCount.entrySet().stream().forEach(e -> {
            if (e.getValue() > 1) {
                flag2.set(true);
            }
        });

        System.out.println(flag2.get());
        String bundleId = "1293013u1";
        System.out.println(bundleId.replace("id", ""));

        System.out.println("=======");

        String eventCapDaily = "[\n" +
                "    {\n" +
                "        \"revenue\":\"1\",\n" +
                "        \"payout\":\"0.2\",\n" +
                "        \"eventName\":\"event1\",\n" +
                "        \"currency\":\"USD\",\n" +
                "        \"capDaily\":\"200\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"revenue\":\"2\",\n" +
                "        \"payout\":\"1\",\n" +
                "        \"eventName\":\"event1\",\n" +
                "        \"currency\":\"USD\",\n" +
                "        \"capDaily\":\"1000\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"revenue\":\"1\",\n" +
                "        \"payout\":\"0.1\",\n" +
                "        \"eventName\":\"zhangxu111\",\n" +
                "        \"currency\":\"USD\",\n" +
                "        \"capDaily\":\"100\"\n" +
                "    }\n" +
                "]";
        List<String> eventNames = JSONArray.parseArray(eventCapDaily, JSONObject.class)
                .stream().map(j->j.getString("eventName")).collect(Collectors.toList());
        System.out.println(JSON.toJSONString(eventNames));

//        System.out.println(eventJsons.size());
        eventNames.stream()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()))
                .entrySet().stream()
                .forEach(e -> {
            System.out.println(e.getValue());
        });

    }

    /**
     *  List<AffOfferWhiteListPost> result = new ArrayList<>();
     *         Long offerId = o.getOfferIds().get(0);
     *         OfferPayout offerPayout = offerPayoutMapper.findByOfferId(o.getOfferIds().get(0));
     *         List<JSONObject> eventJsons = JSONArray.parseArray(offerPayout.getEventCapDaily(), JSONObject.class);
     *         //判断是否有重复event_name
     *         AtomicBoolean flag = new AtomicBoolean(false);
     *         eventJsons.stream()
     *                 .collect(Collectors.groupingBy(i->i,Collectors.counting()))
     *                 .entrySet()
     *                 .stream()
     *                 .forEach(wc->{
     *                     if(wc.getValue()>1) {
     *                         flag.set(true);
     *                     }
     *                 });
     */
}
