package com.zx.base.enum_;


/**
 *
 *而且，Java自动给按照枚举值出现的顺序，从0开始分配了编号。
 * 通过name()可以获得枚举值的名称，通过ordinal()可以获得枚举值的编号。
 *
 * enum修饰的类默认集成Enum类，
 *
 *
 枚举类在经过编译后确实是生成了一个扩展了java.lang.Enum的类
 枚举类是final的，因此我们无法再继承它了
 我们定义的每个枚举值都是该类中的一个成员，且成员的类型仍然是Color类型
 枚举类中被默认增加了许多静态方法，例如values()等
 *
 */
public enum ColorEnum {
    RED,GREEN,BLUE
}
