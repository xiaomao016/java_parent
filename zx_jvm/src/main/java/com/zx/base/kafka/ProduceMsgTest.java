package com.zx.base.kafka;

import com.alibaba.fastjson.JSONObject;

public class ProduceMsgTest {
    public static void main(String[] args) {
        KafkaInternalProducer kafkaInternalProducer = new KafkaInternalProducer();
        kafkaInternalProducer.initKafkaInternalProducer();
        JSONObject obj = new JSONObject();
        obj.put("name","zx");
        obj.put("age","1");
        kafkaInternalProducer.sendMessage("test-vb",obj);
    }
}
