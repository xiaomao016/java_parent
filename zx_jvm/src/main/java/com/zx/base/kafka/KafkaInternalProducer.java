package com.zx.base.kafka;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;

import java.util.Properties;

@Slf4j
public class KafkaInternalProducer {


    @Value("${aliyun.kafka2.vpc.bootstrap.servers}")
    private String bootstrapServers;


    private KafkaProducer<String, String> producerClickTopic04;


    public void initKafkaInternalProducer() {
        //TODO "172.232.28.242:9092,172.232.28.243:9092,172.232.28.244:9092,172.234.215.188:9092,172.234.215.210:9092"
        String bootstrapServers = "172.232.28.242:9092,172.232.28.243:9092,172.232.28.244:9092,172.234.215.188:9092,172.234.215.210:9092";
        Properties props = new Properties();
        //设置接入点，请通过控制台获取对应Topic的接入点。
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        //消息队列Kafka版消息的序列化方式。
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        //请求的最长等待时间。
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 30 * 1000);
        //设置客户端内部重试次数。
        props.put(ProducerConfig.RETRIES_CONFIG, 5);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 32768); //32K
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1200); //1.2秒*/
        //设置客户端内部重试间隔。
        props.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, 3000);
        // 设置压缩类型为LZ4
        props.put("compression.type", "lz4");
        //构造Producer对象，注意，该对象是线程安全的，一般来说，一个进程内一个Producer对象即可。
        //如果想提高性能，可以多构造几个对象，但不要太多，最好不要超过5个。
        producerClickTopic04 = new KafkaProducer<String, String>(props);
        //构造一个消息队列Kafka版消息。
        //String topic = "1.0_BidAllLogEntity"; //消息所属的Topic，请在控制台申请之后，填写在这里。
        //String value = "this is the message's value"; //消息的内容。


    }

    public void sendMessage(String topic, JSONObject msg) {
        try {
            ProducerRecord<String, String> kafkaMessage = new ProducerRecord<String, String>(topic, JSON.toJSONString(msg));
            producerClickTopic04.send(kafkaMessage);
            /*log.info("kafkaInternalProducer.sendMessage");
            RecordMetadata recordMetadata = metadataFuture.get();
            log.info("Produce ok:" + recordMetadata.toString());*/
        } catch (Exception e) {
            if(e instanceof InterruptedException){
                log.error("kafka sendMessage InterruptedException offerId is ");
            }else{
                log.error(e.getMessage(), e);
            }
        }
    }


}
