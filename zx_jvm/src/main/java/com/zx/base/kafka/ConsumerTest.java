package com.zx.base.kafka;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConsumerTest {
    public static void main(String[] args) {
        try {
            KafkaInternalConsumer consumer = new KafkaInternalConsumer();
            consumer.consumerData();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }
}
