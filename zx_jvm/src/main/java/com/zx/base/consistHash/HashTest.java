package com.zx.base.consistHash;

import com.google.common.collect.Lists;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class HashTest {


    public static void main(String[] args) {
        List<String> jobs = getJobList();
        List<String> address = Lists.newArrayList("node001","node002","node003","node004");

        int node01 = 0;
        int node02 = 0;
        int node03 = 0;
        int node04 = 0;
        for(String job : jobs){
            String node = hashJob(job, address);
            if("node001".equals(node)){
                node01++;
            }else if("node002".equals(node)){
                node02++;
            }else if("node003".equals(node)){
                node03++;
            }else if("node004".equals(node)){
                node04++;
            }
            System.out.println(job+"->"+hashJob(job,address));
        }

        System.out.println("node01:"+node01);
        System.out.println("node02:"+node02);
        System.out.println("node03:"+node03);
        System.out.println("node04:"+node04);

    }

    public static List<String> getJobList(){
        List<String> list = new ArrayList<>();
        list.add("1718934160734162946");
        list.add("1737381682483974146");
        list.add("1739921485880532994");
        list.add("1744595804191653889");
        list.add("1745644486034931714");
        list.add("1751103595383259137");
        list.add("1752659484384948225");
        list.add("1752659630434807809");
        list.add("1753337230136176642");
        list.add("1767164927517802498");
        list.add("1767167956111474690");
        list.add("1767525846445867010");
        list.add("1767525871968206850");
        list.add("1767758328239898626");
        list.add("1767792354958917634");
        list.add("1767851745720549377");
        list.add("1768602926227013633");
        list.add("1771017448271261697");
        list.add("1772237617169580034");
        list.add("1775058521859739650");
        list.add("1775059621727883265");
        list.add("1775061870566887426");
        list.add("1777649040620179457");
        list.add("1777650144296751106");
        list.add("1777650635537829890");
        list.add("1777650711421177857");
        list.add("1777652513680052225");
        list.add("1777653103395000321");
        list.add("1777653678744457217");
        list.add("1777653787523731457");
        list.add("1777957524985270274");
        list.add("1778007639053750274");
        list.add("1780414655772631041");
        list.add("1782588137650458625");
        list.add("1783094428227506178");
        list.add("1784422926221238273");
        list.add("1784506860904833027");
        list.add("1784507179982315521");
        list.add("1784520335387451393");
        list.add("1784520496431947777");
        list.add("1785264239804059649");
        list.add("1787657740802056194");
        list.add("1788022592041115650");
        list.add("1788042076852314113");
        list.add("1788103564287795201");
        list.add("1788103633812578305");
        list.add("1788103763211051009");
        list.add("1788478795963686914");
        list.add("1788478876108447746");
        list.add("1788894245201633282");
        list.add("1790558476812554241");
        list.add("1790558673898704898");
        list.add("1790558767180025858");
        list.add("1790558845693202433");
        list.add("1790558946482327553");
        list.add("1790559068167475201");
        list.add("1792454067289280513");
        list.add("1793248960144191490");
        list.add("1793249267574091778");
        list.add("1793249344526987266");
        list.add("1793555018754449410");
        list.add("1793556237820518402");
        list.add("1795751467797876738");
        list.add("1796507064075980802");
        list.add("1797591823954907138");
        list.add("1797591966108258305");
        list.add("1798241622106566658");
        list.add("1798241716394520577");
        list.add("1798241821994512385");
        list.add("1798302703797547009");
        list.add("1798302913676324865");
        list.add("1798551164480532481");
        list.add("1798551279505125377");
        list.add("1798622130749452289");
        list.add("1798674354859028482");
        list.add("1798674791561572354");
        list.add("1798674887099428865");
        list.add("1798675096336478210");
        list.add("1798677180842651649");
        list.add("1798677335390171138");
        list.add("1801089530673668097");
        list.add("1801133225817055234");
        list.add("1801173750829981698");
        list.add("1801173830676946946");
        list.add("1801449815829868546");
        list.add("1801556594140082178");
        list.add("1801557118369361921");
        list.add("1801557214913851394");
        list.add("1801558979931500545");
        list.add("1801559065696628738");
        list.add("1801559207292137474");
        list.add("1801559300678316034");
        list.add("1801560549838516225");
        list.add("1802533891877859330");
        list.add("1802534120102522882");
        list.add("1802546167863275522");
        list.add("1802546362617393154");
        list.add("1802588580581380097");
        list.add("1803384600743546881");
        list.add("1804110154375548929");
        list.add("1804112642763837441");
        list.add("1804120465828663297");
        list.add("1804121238813724673");
        list.add("1804127393564774402");
        list.add("1804127676466384897");
        list.add("1804155452590313474");
        list.add("1804155565744246785");
        list.add("1804155949393039362");
        list.add("1805202958254264322");
        list.add("1805508844113809409");
        list.add("1805583045982081026");
        list.add("1805784172228956161");
        list.add("1805784250087821313");
        list.add("1807976271569412099");
        list.add("1807976512045637634");
        list.add("1807976943392055298");
        list.add("1807977079782432769");
        list.add("1807977192743428098");
        list.add("1807977295105417217");
        list.add("1807977400021737474");
        list.add("1807977898980335618");
        list.add("1807978030186553345");
        list.add("1807978117025423362");
        list.add("1807978241474617345");
        list.add("1807978603422081026");
        list.add("1807979190339428354");
        list.add("1807979350196936705");
        list.add("1808013312587915265");
        list.add("1808013697021042690");
        list.add("1808013888415522818");
        list.add("1808014047585165313");
        list.add("1808014205504905217");
        list.add("1808014342738337793");
        list.add("1808014488830140418");
        list.add("1808014663455793153");
        list.add("1808014780741115905");
        list.add("1808014918301704194");
        list.add("1808015052703981569");
        list.add("1808015171092406274");
        list.add("1808015318106955778");
        list.add("1808015442543566850");
        list.add("1808021680132120578");
        list.add("1808021807508938754");
        list.add("1808021920268607490");
        list.add("1808022099050815489");
        list.add("1808022243859161090");
        list.add("1808022352386777089");
        list.add("1808022463175122945");
        list.add("1808022587037114369");
        list.add("1808022695233380354");
        list.add("1808022794692911106");
        list.add("1808022937613819905");
        list.add("1808023056035799042");
        list.add("1808023121634713602");
        list.add("1808023270087909377");
        list.add("1551895568215707649");
        list.add("1559449275887312897");
        list.add("1587063579918520321");
        list.add("1589862757816127489");
        list.add("1592110160648654849");
        list.add("1619306388129894402");
        list.add("1620254051381858306");
        list.add("1621037626498797569");
        list.add("1639160690417147906");
        list.add("1640993584651509761");
        list.add("1642788085949370369");
        list.add("1642788198205722626");
        list.add("1643933820254261250");
        list.add("1643933998591873025");
        list.add("1643934155291070465");
        list.add("1645354544136790017");
        list.add("1651890017512529922");
        list.add("1658734410605371393");
        list.add("1658807533828657153");
        list.add("1659395125573885954");
        list.add("1660475193556062210");
        list.add("1660909754531774465");
        list.add("1663066181357129730");
        list.add("1663834053643141122");
        list.add("1663882645850357762");
        list.add("1666765484136660993");
        list.add("1671351819845550082");
        list.add("1674747457060782082");
        list.add("1676529304076304385");
        list.add("1676858834926190594");
        list.add("1676858911157665794");
        list.add("1676859354529153026");
        list.add("1676859469243367426");
        list.add("1676867734031319042");
        list.add("1676870408172748802");
        list.add("1676870527412617218");
        list.add("1677267058260770818");
        list.add("1678328892866789378");
        list.add("1679102062104248321");
        list.add("1679109797789138945");
        list.add("1679109824854982657");
        list.add("1679337330554474497");
        list.add("1682297305136402434");
        list.add("1682297486712016898");
        list.add("1682297629301575681");
        list.add("1683789144712581121");
        list.add("1683811465537544193");
        list.add("1683811494855729154");
        list.add("1683811570080571393");
        list.add("1683811623092379649");
        list.add("1684536971601928194");
        list.add("1684537100828434433");
        return list;
    }
    private static int VIRTUAL_NODE_NUM = 100;

    private static long hash(String key) {

        // md5 byte
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 not supported", e);
        }
        md5.reset();
        byte[] keyBytes = null;
        try {
            keyBytes = key.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unknown string :" + key, e);
        }

        md5.update(keyBytes);
        byte[] digest = md5.digest();

        // hash code, Truncate to 32-bits
        long hashCode = ((long) (digest[3] & 0xFF) << 24)
                | ((long) (digest[2] & 0xFF) << 16)
                | ((long) (digest[1] & 0xFF) << 8)
                | (digest[0] & 0xFF);

        long truncateHashCode = hashCode & 0xffffffffL;
        return truncateHashCode;
    }

    public static String hashJob(String jobId, List<String> addressList) {

        // ------A1------A2-------A3------
        // -----------J1------------------
        TreeMap<Long, String> addressRing = new TreeMap<Long, String>();
        for (String address: addressList) {
            for (int i = 0; i < VIRTUAL_NODE_NUM; i++) {
                long addressHash = hash("SHARD-" + address + "-NODE-" + i);
                addressRing.put(addressHash, address);
            }
        }

        long jobHash = hash(jobId);
        SortedMap<Long, String> lastRing = addressRing.tailMap(jobHash);
        if (!lastRing.isEmpty()) {
            return lastRing.get(lastRing.firstKey());
        }
        return addressRing.firstEntry().getValue();
    }

}
