package com.zx.base.consistHash;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class HashTest2 {


    /**
     * 0xffffffff 转换为二进制是：
     *
     * 1111 1111 1111 1111 1111 1111 1111 1111
     *
     * 这是一个32位的二进制数，所有位都是1。由于 L 表示这是一个长整型，它将被扩展到64位。扩展的方式通常是在高位补0（如果是无符号扩展）或补符号位（如果是有符号扩展）。
     *
     * 对于 0xffffffffL，它将被扩展为64位，结果是：
     *
     * 0000 0000 0000 0000 0000 0000 0000 0000 1111 1111 1111 1111 1111 1111 1111 1111
     *
     * 进行按位与运算时，只有低32位会保留原值，高32位会被清零
     *
     * 通过这个操作，我们确保了 hashCode 的值在内部表示中是 0x0000000087654321，即一个无符号的32位整数。
     *
     * 假设我们有一个32位的整数 0x87654321，这个值在内部表示中是一个有符号的32位整数。在Java中，如果我们直接将其赋值给一个long类型的变量，Java会进行符号扩展，将其扩展为64位。
     *
     * 具体来说，0x87654321 的最高位（第32位）是1，表示这是一个负数。因此，Java会将其扩展为 0xFFFFFFFF87654321。
     *
     * 现在，我们希望确保这个值被解释为一个无符号的32位整数。为此，我们将其与 0xffffffffL 进行按位与（&）运算：
     *
     *
     * @param args
     */

    public static void main(String[] args) {
//        List<String> jobs = getJobList();
        List<String> jobs = getJobList2();
        //,"node002","node003","node004","node005","node006"
        List<String> address = Lists.newArrayList("node001","node002");

        List<String> node01 = new ArrayList<>();
        List<String> node02 = new ArrayList<>();
        List<String> node03 = new ArrayList<>();
        List<String> node04 = new ArrayList<>();
        List<String> node05 = new ArrayList<>();
        List<String> node06 = new ArrayList<>();
        for(String job : jobs){
            String node = hashJob(job, address);
            if("node001".equals(node)){
                node01.add(job);
            }else if("node002".equals(node)){
                node02.add(job);
            }else if("node003".equals(node)){
                node03.add(job);
            }else if("node004".equals(node)){
                node04.add(job);
            }else if("node005".equals(node)){
                node05.add(job);
            }else if("node006".equals(node)){
                node06.add(job);
            }
            System.out.println(job+"->"+hashJob(job,address));
        }

        System.out.println("node01:"+ JSON.toJSONString(node01));
        System.out.println("node02:"+ JSON.toJSONString(node02));
        System.out.println("node03:"+ JSON.toJSONString(node03));
        System.out.println("node04:"+ JSON.toJSONString(node04));
        System.out.println("node05:"+ JSON.toJSONString(node05));
        System.out.println("node06:"+ JSON.toJSONString(node06));

    }

    public static List<String> getJobList(){
        List<String> list = new ArrayList<>();
        //1810858719416692737
        //1810858845380030465
        //1810858975403454465
        list.add("1810858719416692737");
        list.add("1810858845380030465");
        list.add("1810858975403454465");
//        list.add("4");
//        list.add("5");
//        list.add("6");

        return list;
    }

    public static List<String> getJobList2() {
        List<String> list = new ArrayList<>();
        list.add("1718934160734162946");
        list.add("1737381682483974146");
        list.add("1739921485880532994");
        list.add("1744595804191653889");
        list.add("1745644486034931714");
        list.add("1751103595383259137");
        list.add("1752659484384948225");
        list.add("1752659630434807809");
        list.add("1753337230136176642");
        list.add("1767164927517802498");
        list.add("1767167956111474690");
        list.add("1767525846445867010");
        list.add("1767525871968206850");
        list.add("1767758328239898626");
        list.add("1767792354958917634");
        return list;
    }
    private static int VIRTUAL_NODE_NUM = 100;

    private static long hash(String key) {

        // md5 byte
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 not supported", e);
        }
        md5.reset();
        byte[] keyBytes = null;
        keyBytes = key.getBytes(StandardCharsets.UTF_8);

        md5.update(keyBytes);
        byte[] digest = md5.digest();

        // hash code, Truncate to 32-bits
        long hashCode = ((long) (digest[3] & 0xFF) << 24)
                | ((long) (digest[2] & 0xFF) << 16)
                | ((long) (digest[1] & 0xFF) << 8)
                | (digest[0] & 0xFF);

        long truncateHashCode = hashCode & 0xffffffffL;
        return truncateHashCode;
    }

    public static String hashJob(String jobId, List<String> addressList) {

        // ------A1------A2-------A3------
        // -----------J1------------------
        TreeMap<Long, String> addressRing = new TreeMap<Long, String>();
        for (String address: addressList) {
            for (int i = 0; i < VIRTUAL_NODE_NUM; i++) {
                long addressHash = hash("SHARD-" + address + "-NODE-" + i);
                addressRing.put(addressHash, address);
            }
        }

        long jobHash = hash(jobId);
        SortedMap<Long, String> lastRing = addressRing.tailMap(jobHash);
        if (!lastRing.isEmpty()) {
            return lastRing.get(lastRing.firstKey());
        }
        return addressRing.firstEntry().getValue();
    }

}
