package com.zx.base.File;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author zx
 * @description:
 * @date 2022/8/12
 */
public class FileReadWriteLine {
    public static void main(String[] args) {
        rw01();
    }



    public static void rw01() {
        try {
            String output = "E:\\back\\test\\output.csv";
            String input = "E:\\back\\test\\itau-cartoes-whitelist.csv";
            //write
            createFileIfNotExist(output);
            Path outPath = Paths.get("/opt/soft/output.csv");
            System.out.println(outPath.toAbsolutePath().toString());
            FileWriter fw = new FileWriter(output, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fw);
            //read
            Path readPath = Paths.get(input);
            BufferedReader reader = Files.newBufferedReader(readPath);
            int i = 0;
            String line = reader.readLine();
            System.out.println("line:{}"+line);
            while (line != null) {
                System.out.println(line);
                line = reader.readLine();
                bufferedWriter.append(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();
                i++;
                if(i>10){
                    break;
                }
            }
            reader.close();
            bufferedWriter.close();
            System.out.println("finish..");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }


    private static void createFileIfNotExist(String filePath) {
        try {
            Path targetFile = Paths.get(filePath);
            if (!Files.exists(targetFile)) {
                Files.createFile(targetFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
