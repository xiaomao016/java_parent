package com.zx.base.polymorphic.demo02;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/14 0014 07:39
 * @description: 动态绑定机制，JVM在执行对象的成员方法时，会将这个方法和对象的实际内存进行绑定，然后调用。
 *
 * 测试1：不注释任何代码，运行结果 40 。实际访问的是BB的方法和成员变量。
 * 测试2：去掉BB的成员变量  30  会访问BB的方法，但是当发现i变量没有，会向父类寻找。所以是20+10
 * 测试3：去掉BB的成员方法  20   会调用AA的方法，此时会绑定AA的内存空间，变量i也会用AA的10 。所以是10+10
 * 测试4：去掉BB的成员变量和方法  20 此时和上面一致。
 *
 *
 */
public class Test {

    public static void main(String[] args) {
        //多态，父类引用指向子类对象。
        AA a = new BB();

        System.out.println(a.get());
    }
}

class AA {
    public int i = 10;

    public int get() {
        return i + 10;
    }
}

class BB extends AA {

    public int i = 20;

    @Override
    public int get(){
        return i+20;
    }
}


