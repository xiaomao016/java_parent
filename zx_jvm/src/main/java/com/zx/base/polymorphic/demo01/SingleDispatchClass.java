package com.zx.base.polymorphic.demo01;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 14:38
 * @description:
 */
public class SingleDispatchClass {

    public void polymorphicMethod(ParentClass c){
        c.f();
    }


    public void OverLoadMethod(ParentClass c){
        System.out.println("重载方法 参数 parent");
    }

    public void OverLoadMethod(ChildClass c){
        System.out.println("重载方法 参数 child");
    }

}
