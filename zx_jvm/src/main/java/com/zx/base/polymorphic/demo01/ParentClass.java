package com.zx.base.polymorphic.demo01;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 14:37
 * @description:
 */
public class ParentClass {

    public void f(){
        System.out.println("parent class.. ");
    }
}
