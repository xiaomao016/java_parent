package com.zx.base.polymorphic.demo01;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 14:40
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        ChildClass c1 = new ChildClass();
        ParentClass p1 = new ParentClass();

        SingleDispatchClass s = new SingleDispatchClass();
        s.OverLoadMethod(c1);// -- 重载方法 参数 child
        s.OverLoadMethod(p1);// -- 重载方法 参数 parent
        System.out.println("===============");

        ParentClass p2 = new ChildClass(); //父类引用指向子类对象
        p2.f(); // -- Child class..  多态机制，会指向实际的类型。
        s.polymorphicMethod(p2); // -- Child class..  多态机制，会指向实际的类型。
        //虚拟机（或者准确地说是编译器）在重载时是通过参数的静态类型而不是实际类型作为判定依据的。
        //所以下面这句调用，会调用父类的方法。
        s.OverLoadMethod(p2); // -- 重载方法 参数 parent

    }

}
