package com.zx.base.javacollection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author zx
 * @description:
 * @date 2022/6/8
 */
public class SortList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("device/filter/code=BR/os=ios/task=1479329191239766018/day=20220607/.odps/.meta");
        list.add("device/filter/code=BR/os=ios/task=1479329191239766018/day=20220607/.odps/20220607055355611gapfmisd/R2_1_0_0-0_TableSink1-0-.tsv.gz");
        list.add("device/filter/code=BR/os=ios/task=1479329191239766018/day=20220607/.odps/20220607055814217gyqm5dk1/R2_1_0_0-0_TableSink1-0-.tsv.gz");
        list.add("device/filter/code=BR/os=ios/task=1479329191239766018/day=20220607/.odps/20220607055814217gyqm5dk1/R2_1_0_0-0_TableSink1-0-.tsv.gz");
        list.sort(Comparator.reverseOrder());

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));

    }
}
