package com.zx.base.javacollection;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/2/19 0019 12:01
 * @description:
 */
public class MapDemo {
    public static void main(String[] args) {
        //hash原理，冲突，散列冲突解决方案
        Map<String,Integer> map = new HashMap<>(2);
        map.put("a",12);
        map.put("c",12);
        map.put("d",12);
        System.out.println("finish");
    }

}
