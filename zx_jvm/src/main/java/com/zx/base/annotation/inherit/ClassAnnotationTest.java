package com.zx.base.annotation.inherit;

public class ClassAnnotationTest {

    @UninheritedAnnotationType
    static class A {

    }

    @InheritedAnnotationType
    static class B extends A {

    }

    static class C extends B {

    }

    public static void main(String[] args) {
        System.out.println(new A().getClass().getAnnotation(InheritedAnnotationType.class));
        System.out.println(new B().getClass().getAnnotation(InheritedAnnotationType.class));
        System.out.println(new C().getClass().getAnnotation(InheritedAnnotationType.class));
        System.out.println("_________________________________");
        System.out.println(new A().getClass().getAnnotation(UninheritedAnnotationType.class));
        System.out.println(new B().getClass().getAnnotation(UninheritedAnnotationType.class));
        System.out.println(new C().getClass().getAnnotation(UninheritedAnnotationType.class));
    }
}
