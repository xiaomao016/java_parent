package com.zx.base.annotation;

import java.lang.annotation.*;






@ClassAnnotation
public class AnnotationTest {

    @MultipleElementTypeAnnotation
    public void myMethod() {}

    // Main drive method
    public static void main(String[] args) throws Exception {
        AnnotationTest obj = new AnnotationTest();

        // Accessing the annotations used to annotate the
        // class and storing them in an array of Annotation
        // type since only one annotation is used to
        // annotate our class therefore we print a[0]
        Annotation a[] = obj.getClass().getAnnotations();

        System.out.println(a[0]);

        // Accessing the annotations used to annotate the
        // method and storing them in an array of Annotation
        // type
        Class<?> className = Class.forName("GFG").getDeclaringClass();
        Annotation b[] = className.getMethod("myMethod")
                .getAnnotations();
        System.out.println(b[0]);
    }
}
