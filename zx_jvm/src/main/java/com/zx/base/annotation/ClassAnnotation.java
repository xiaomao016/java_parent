package com.zx.base.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ClassAnnotation {
    String value() default "Can annotate a class";
    String init() default "";
    String destroy() default "";
}
