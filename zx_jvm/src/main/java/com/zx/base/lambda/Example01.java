package com.zx.base.lambda;

import com.zx.base.lambda.bean.TopicPartition;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zx
 * @description:
 * @date 2022/8/19
 */
@Slf4j
public class Example01 {
    public static void main(String[] args) {
        List<TopicPartition> list = getPartitions(10);
        // 假设分区对象列表变量名是list
        Set<String> topics = list.stream()
                .filter(tp -> tp.getTopic().startsWith("test_"))
                .collect(Collectors.groupingBy(TopicPartition::getTopic, Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 3)
                .map(entry -> entry.getKey()).collect(Collectors.toSet());

        log.info("topic size:{}",topics.size());
        for(String s:topics){
            log.info(s);
        }
    }

    private static List<TopicPartition> getPartitions(int num) {
        List<TopicPartition> list = new ArrayList<>(num);
        list.add(new TopicPartition(12,"topic_first"));
        for(int i=0;i<num;++i){
            list.add(new TopicPartition(i,"test_"+i));
            list.add(new TopicPartition(i+1,"test_"+i));
            list.add(new TopicPartition(i+2,"test_"+i));
            list.add(new TopicPartition(i+3,"test_"+i));
        }
        return list;
    }


}
