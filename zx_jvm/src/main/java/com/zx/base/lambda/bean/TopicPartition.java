package com.zx.base.lambda.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zx
 * @description:
 * @date 2022/8/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public  class TopicPartition implements Serializable {
    private  int partition;
    private  String topic;
    // 其他字段和方法......
}
