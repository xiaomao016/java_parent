package com.zx.base.basic;

import java.util.SortedMap;
import java.util.TreeMap;

public class TreeMapTest {
    public static void main(String[] args) {
        TreeMap<String, Integer> map = new TreeMap<>();
        map.put("Alice", 25);
        map.put("Bob", 30);
        map.put("Charlie", 35);
        map.put("David", 40);
        map.put("Eve", 45);

        // 获取从 "Charlie" 开始到末尾的子映射
        SortedMap<String, Integer> tailMap = map.tailMap("FF");

        // 输出子映射的内容
        for (String key : tailMap.keySet()) {
            System.out.println(key + " : " + tailMap.get(key));
        }
    }
}
