package com.zx.base.basic;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zx
 * @description:
 * @date 2022/4/25
 */
@Data
public class Student {

    private String email;
    private String name = "张三";
    private static int age;
    private Map<String,String> resultPaths;

    private void init(){
        resultPaths = new HashMap<>();
    }

    private AtomicInteger downloadedFileCount;

    private List<String> kvs = new ArrayList<>();

    public void start(){
        init();
        System.out.println(name);
        System.out.println(age);
        kvs.add("a");
        resultPaths  = compress();
        System.out.println(resultPaths.size());
    }

    private Map<String, String> compress() {
        Map<String,String> m = new HashMap<>();
        m.put("a","a");
        return m;
    }
}
