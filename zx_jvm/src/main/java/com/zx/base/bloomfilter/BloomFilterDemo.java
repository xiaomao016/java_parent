package com.zx.base.bloomfilter;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class BloomFilterDemo {
    public static void main(String[] args) {
        try {
            // 创建布隆过滤器，预计插入大小为3亿，误报率为0.01(1%)
            BloomFilter<CharSequence> filter = BloomFilter.create(Funnels.stringFunnel(StandardCharsets.UTF_8), 400_000_000, 0.01);
            // 读取文件A，并将每行加入到布隆过滤器中
            try (BufferedReader brA = new BufferedReader(new FileReader("E:\\test\\test_blomm\\fileA.txt"))) {
                String line;
                while ((line = brA.readLine()) != null) {
                    filter.put(line);
                }
            }

            // 读取文件B，并检查每行是否可能在布隆过滤器中（即文件A中）
            try (BufferedReader brB = new BufferedReader(new FileReader("E:\\test\\test_blomm\\fileB.txt"))) {
                String line;
                while ((line = brB.readLine()) != null) {
                    if (filter.mightContain(line)) {
                        System.out.println(line + " 可能存在于文件A中");
                    } else {
                        System.out.println(line + " 不存在于文件A中");
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
