package com.zx.jvm.jconsole;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/7 0007 09:54
 * @description:
 *
 *
 * VM args : -Xms100M -Xmx100M -XX:+UseSerialGC
 * 测试内存监控
 */
public class JConsoleTest {

    static class OOMObject {
        public byte[] placeholder = new byte[64 * 1024];
    }

    public static void fillHeap(int num) throws InterruptedException {

        List<OOMObject> list = new ArrayList<>();
        for (int i = 0; i < num; ++i) {
            //稍作延时，让曲线有波折
            TimeUnit.MILLISECONDS.sleep(500);
            list.add(new OOMObject());
        }

        System.gc();

        TimeUnit.SECONDS.sleep(500);
    }

    public static void main(String[] args) throws InterruptedException {
        fillHeap(1000);
    }

}
