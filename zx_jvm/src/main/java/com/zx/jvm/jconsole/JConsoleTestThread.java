package com.zx.jvm.jconsole;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/7 0007 09:54
 * @description:
 *
 *
 * VM args : -Xms100M -Xmx100M -XX:+UseSerialGC
 * 测试内存监控
 */
public class JConsoleTestThread {


    /**
     * 线程死循环演示
     */
    public static void createBusyThread() throws InterruptedException {

        new Thread(()->{while(true);},"testBusyThread").start();

    }

    /**
     *
     * 线程锁等待演示
     *
     */
    public static void createLockThread(final Object lock){

        new Thread(()->{
            synchronized (lock){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"testLockThread").start();

    }



    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();

        createBusyThread();

        br.readLine();

        Object obj = new Object();
        createLockThread(obj);

    }

}
