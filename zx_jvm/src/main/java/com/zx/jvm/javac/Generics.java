package com.zx.jvm.javac;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/18 0018 16:26
 * @description: 把这段Java代码编译成Class文件，然后再用字节码反编译工具进行反编译后，
 * 将会发现泛型都不见了，程序又变回了Java泛型出现之前的写法，泛型类型都变回了裸类型
 */
public class Generics {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("hello", "你好");
        map.put("how are you?", "吃了吗");

        System.out.println(map.get("hello"));
        System.out.println(map.get("how are you?"));
    }


    public static String method(List<String> list) {
        System.out.println("invoke method String");
        return "";
    }


    //    public static void method(List<Integer> list){
//        System.out.println("invoke method Integer");
//
//    }
//    public static int method(List<Integer> list) {
//        System.out.println("invoke method Integer");
//        return 1;
//    }
}
