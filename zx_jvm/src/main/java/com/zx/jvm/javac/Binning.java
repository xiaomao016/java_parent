package com.zx.jvm.javac;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/19 0019 08:00
 * @description:
 *
 * 我们知道Integer是int的包装类，在jdk1.5以上，可以实现自动装箱拆箱，就是jdk里面会自动帮我们转换，不需要我们手动去强转，所以我们经常在这两种类型中随意写，平时也没什么注意
 *
 * 但Integer他是对象，我们知道 == 比较的是堆中的地址，但有个奇怪的事是， 如果 Integer a = 123, Integer b = 123，可以返回true，但如果Integer a = 12345, Integer b = 12345,返回false，这就是jdk的东西，我们看下Integer的源码
 *
 * public static Integer valueOf(int i) {
 * if (i >= IntegerCache.low && i <= IntegerCache.high)
 *
 * return IntegerCache.cache[i + (-IntegerCache.low)];
 *
 * return new Integer(i);
 *
 * }
 *
 * 默认IntegerCache.low 是-127，Integer.high是128，如果在这个区间内，他就会把变量i当做一个变量，放到内存中；但如果不在这个范围内，就会去new一个Integer对象，
 *
 * 而我在代码中，两个Integer值都不在这个范围内，所以jdk帮我new了两个实例，这样在用==，肯定是false。
 *
 * 所以如果要比较Integer的值，比较靠谱的是通过Integer.intValue();这样出来的就是int值，就可以直接比较了；或者equals()比较
 *
 */
public class Binning {
    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 2;
        Integer c = 3;
        Integer d = 3;
        Integer e = 126;
        Integer f = 126;
        Long g = 3L;
        System.out.println(c==d);
        System.out.println(e==f);
        System.out.println(c==(a+b));
        System.out.println(c.equals(a+b));
        System.out.println(g==(a+b));
        System.out.println(g.equals(a+b));
    }
}

/**
 * @author zx
 * @date 2021/8/19 001
 * 去掉语法糖后的结果。
 * public class Binning
 * {
 *   public static void main(String[] args)
 *   {
 *     Integer a = Integer.valueOf(1);
 *     Integer b = Integer.valueOf(2);
 *     Integer c = Integer.valueOf(3);
 *     Integer d = Integer.valueOf(3);
 *     Integer e = Integer.valueOf(321);
 *     Integer f = Integer.valueOf(321);
 *     Long g = Long.valueOf(3L);
 *     System.out.println(c == d);
 *     System.out.println(e == f);
 *     System.out.println(c.intValue() == a.intValue() + b.intValue());
 *     System.out.println(c.equals(Integer.valueOf(a.intValue() + b.intValue())));
 *     System.out.println(g.longValue() == a.intValue() + b.intValue());
 *     System.out.println(g.equals(Integer.valueOf(a.intValue() + b.intValue())));
 *   }
 * }
 *
 *
 */

