package com.zx.jvm.classload;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/10 0010 10:49
 * @description:
 * 非主动引用下，类的初始化情况演示.
 *
 * 对于静态字段，只有直接定义这个字段的类才会被初始化，因此通过其子类来引用父类中定义的静态字段，
 * 只会触发父类的初始化而不会触发子类的初始化。至于是否要触发子类的加载和验证阶段，在《Java虚拟机规范》
 * 中并未明确规定，所以这点取决于虚拟机的具体实现。对于HotSpot虚拟机来说，可通过-XX：+TraceClassLoading
 * 参数观察到此操作是会导致子类加载的。
 *
 */
public class NotInitialization {
    public static void main(String[] args) {
        //1.子类引用父类的静态字段，不会触发子类的初始化
        System.out.println(SubClass.value);
        //2.通过数组定义引用类，不会触发该类的初始化
//        SuperClass[] arr = new SuperClass[10];
    }

}

class SuperClass{
    static {
        System.out.print("SuperClass Init!");
    }

    public static int value = 123;
}

class SubClass extends SuperClass{
    static {
        System.out.println("SubClass init");
    }
}
