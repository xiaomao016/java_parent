package com.zx.jvm.classload;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/12 0012 08:23
 * @description:
 * <clinit>()方法是由编译器自动收集类中的所有类变量的赋值动作和静态语句块（static{}块）中的语句合并产生的，
 * 编译器收集的顺序是由语句在源文件中出现的顺序决定的，静态语句块中只能访问到定义在静态语句块之前的变量，定义
 * 在它之后的变量，在前面的静态语句块可以赋值，但是不能访问，
 */
public class InitStage {

    static {
        i=0; //给变量赋值可以通过
//        System.out.println(i); //编译器会提示向前引用
    }
    static int i = 1;

    public static void main(String[] args) {

    }
}
