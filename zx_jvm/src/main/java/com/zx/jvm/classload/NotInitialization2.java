package com.zx.jvm.classload;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/10 0010 11:25
 * @description:
 *
 * 上述代码运行之后，也没有输出“ConstClass init！”，
 * 这是因为虽然在Java源码中确实引用了ConstClass类的常量HELLOWORLD，
 * 但其实在编译阶段通过常量传播优化，已经将此常量的值“hello world”直接
 * 存储在NotInitialization类的常量池中，以后NotInitialization对常量
 * ConstClass.HELLOWORLD的引用，实际都被转化为NotInitialization类对自身常量池的引用了。
 * 也就是说，实际上NotInitialization的Class文件之中并没有ConstClass类的符号引用入口，这
 * 两个类在编译成Class文件后就已不存在任何联系了。
 *
 */
public class NotInitialization2 {

    public static void main(String[] args) {
        System.out.println(ConstClass.HELLOWORLD);
    }
}

class ConstClass{
    static{
        System.out.println("ConstClass init!");
    }

    public static final String HELLOWORLD = "helloWorld";
}

