package com.zx.jvm.classload;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/12 0012 08:51
 * @description:
 */
public class DeadLoopClass {
    static{
        //不加if会报Initializer must be able to complete normally
        if(true) {
            System.out.println(Thread.currentThread() + "init DeadLoopClass");
            while(true){

            }
        }

    }
}
