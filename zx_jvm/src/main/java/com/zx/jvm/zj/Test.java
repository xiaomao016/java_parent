package com.zx.jvm.zj;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/13 0013 11:10
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        //====测试1=============
        String c1_2 =new String("abc");
        String c1_3 =new String("abc");
//        String c1_1 = "abc";

        //返回true
        System.out.println(c1_2 == c1_3.intern());
//        System.out.println(c1_1==c1_2);
    }
}
