package com.zx.jvm.zj;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/30 0030 10:30
 * @description:
 *
 * 通过String.intern()方法，总结JVM内存区域
 *
 * jdk6中是两个false。jdk7以后是一个true一个false。
 *      *
 *      * 在jdk6中，intern会将首次遇到的字符串实例复制到永久代，返回的是永久代实例的引用。而
 *      * StringBuilder是在堆上创建实例。所以必然不可能是同一个引用。
 *      *
 *      * 在7以后，字符串常量池已经移到堆中，只需要在常量池记录一下首次出现的引用即可，因此
 *      * intern返回的和StringBuild创建的实例是同一个。而java因为是关键词已经出现过，不符合intern
 *      * 首次遇到原则，所以是两个不同的对象。
 *
 * 最终是一条指令if_acmpne
 */
public class StringWithMemory {
    public static void main(String[] args) {
        //====测试1=============
        String c1_1 = "abc";
        String c1_2 =new String("abc");

        //返回true
        System.out.println(c1_1 == c1_2.intern());
        System.out.println(c1_1==c1_2);


        //===========测试2==============
        String c2_1 = new String("abc");
        String c2_2 = new String("abc");

        //返回true
        System.out.println(c2_1.intern() == c2_2.intern());
        //返回false
        System.out.println(c2_1 == c2_2);


        //===========测试3===========
        String c3_1 = new StringBuilder("计算机").append("软件").toString();
        System.out.println(c3_1.intern()== c3_1);

        String c3_2 = new StringBuilder("ja").append("va").toString();
        System.out.println(c3_2.intern()== c3_2);



    }
}
