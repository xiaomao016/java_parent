package com.zx.jvm.classinfo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 14:11
 * @description:
 */
public class Student {
    private int age;
    private int score;
    public Student(int age,int score){
        this.age = age;
        this.score = score;
    }
}
