package com.zx.jvm.extution;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/14 0014 11:24
 * @description:
 */
public class DynamicLinkDemo {


    public static void sayHello(){
        System.out.println("Hello world!");
    }

    public static void main(String[] args) {
        DynamicLinkDemo.sayHello();
    }

}
