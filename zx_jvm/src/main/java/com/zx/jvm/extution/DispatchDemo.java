package com.zx.jvm.extution;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/16 0016 15:40
 * @description: 方法重载
 * 但虚拟机（或者准确地说是编译器）在重载时是通过参数的静态类型而不是
 * 实际类型作为判定依据的。
 */
public class DispatchDemo {
    static abstract class Human{}

    static class Man extends Human{

    }

    static class WoMan extends Human{

    }

    public void sayHello(Human guy){
        System.out.println("hello,guy");
    }

    public void sayHello(Man guy){
        System.out.println("hello,man");
    }
    public void sayHello(WoMan guy){
        System.out.println("hello,woman");
    }

    public static void main(String[] args) {
        Human man = new Man();
        Human woman = new WoMan();

        DispatchDemo dispatchDemo = new DispatchDemo();
        dispatchDemo.sayHello(man);
        dispatchDemo.sayHello(woman);
    }

}
