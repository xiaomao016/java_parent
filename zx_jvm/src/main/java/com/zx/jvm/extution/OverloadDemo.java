package com.zx.jvm.extution;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/16 0016 16:13
 * @description:
 *
 * 代码中没有写其他的类型如float、double等的重载，不过实际上自动转型还能继续发生多次，
 * 按照char>int>long>float>double的顺序转型进行匹配，但不会匹配到byte和short类型的重载，因为char到byte或short的转型是不安全的
 *
 * 这个输出可能会让人摸不着头脑，一个字符或数字与序列化有什么关系？出现hello Serializable，
 * 是因为java.lang.Serializable是java.lang.Character类实现的一个接口，当自动装箱之后发现
 * 还是找不到装箱类，但是找到了装箱类所实现的接口类型，所以紧接着又发生一次自动转型。char可以转型成int，
 * 但是Character是绝对不会转型为Integer的，它只能安全地转型为它实现的接口或父类。
 *
 *
 * java数据类型：
 *
 * byte : 1
 * short: 2
 * int : 4
 * long : 8
 *
 * float : 4
 * double: 8
 *
 * char: 2
 *
 *
 * boolean : 1字节， 理论只需要1bit
 *
 *
 *
 */
public class OverloadDemo {

    public static void sayHello(Object arg){
        System.out.println("hello Object");
    }

    public static void sayHello(int arg){
        System.out.println("hello int");
    }

    public static void sayHello(long arg){
        System.out.println("hello long");
    }

    public static void sayHello(Character arg){
        System.out.println("hello Character");
    }

//    public static void sayHello(char arg){
//        System.out.println("hello char");
//    }

    public static void sayHello(char ... arg){
        System.out.println("hello char ...");
    }

    public static void sayHello(short arg){
        System.out.println("hello short");
    }

    public static void sayHello(byte arg){
        System.out.println("hello byte");
    }

    public static void main(String[] args) {
        sayHello('a');
    }

}
