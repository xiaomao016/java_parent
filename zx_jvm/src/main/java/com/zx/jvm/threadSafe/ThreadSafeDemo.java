package com.zx.jvm.threadSafe;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/25 0025 10:21
 * @description:
 */
public class ThreadSafeDemo {

    public static int count = 0;

    public static void main(String[] args) {


        new Thread(()->{
            for(int i=0;i<20000;++i){
                m();
            }
        },"A").start();
        new Thread(()->{
            for(int i=0;i<20000;++i){
                m();
            }
        },"B").start();
        new Thread(()->{
            for(int i=0;i<10000;++i){
                m();
            }
        },"C").start();


        while(Thread.activeCount()>2){
            Thread.yield();
        }

        System.out.println(count);

    }

    public static void m(){
        synchronized (ThreadSafeDemo.class){
            count ++;
        }
    }

}
