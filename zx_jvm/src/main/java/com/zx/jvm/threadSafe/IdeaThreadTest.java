package com.zx.jvm.threadSafe;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/25 0025 09:59
 * @description:
 *
 * 为了得到多线程操作下的最终结果，我们可能会在主线程写这样的代码：
 *
 * while(Thread.activeCount()>1){
 *  			Thread.yield();
 *  }
 * System.out.println(结果);
 *
 *
 *
 *
 * 如研究多线程下i++的线程不安全问题时：
 *
 *
 * 但是当使用idea的时候，往往得不到我们想要的结果，在在使用java命令行执行或者eclipse执行却没有这个问题。
 *
 * 经过测试发现，原来在执行main方法时，idea会启动两条线程，测试如下：
 *
 */
public class IdeaThreadTest {
    public static void main(String[] args) {
        System.out.println(Thread.activeCount());//输出2
        //列出线程
        //java.lang.ThreadGroup[name=main,maxpri=10]
        //    Thread[main,5,main]
        //    Thread[Monitor Ctrl-Break,5,main]
        Thread.currentThread().getThreadGroup().list();
    }
}
