package com.zx.jvm.threadSafe;

import java.util.Vector;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/25 0025 10:25
 * @description:
 */
public class VectorSafeTest {

    private static Vector<Integer> vector = new Vector<>();
    public static void main(String[] args) {
        while(true){
            for(int i=0;i< 10;i++){
                vector.add(i);
            }

            Thread removeThread = new Thread(()->{
                for(int i=0;i<vector.size();i++){
                    vector.remove(i);
                }
            });

            Thread printThread = new Thread(()->{
                for(int i=0;i<vector.size();i++){
                    System.out.println(vector.get(i));
                }
            });

            removeThread.start();
            printThread.start();

            while(Thread.activeCount()>20);

        }
    }

}
