package com.zx.jvm.gc;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/5 0005 18:33
 * @description:
 * 空方法启动后，eden space会有部分内存占用，原因是执行Main方法，要将Class对象加载等
 * 具体有待验证。
 * 当分配第一和第二个2M时，会在eden中分配，
 * 但到第三个2M时，eden区域已经有6M多，不够放下。发生一次MinorGC，即新生代的GC。
 * 此时回收了一部分内存，并会将eden中还存活的对象拷贝到survivor from中，但是survivor
 * 内存只有1M，所以根据内存担保规则，直接拷贝到老年代。
 * 随后清空eden区域和survivor to区域。此时eden可以重新分配内存。
 */
public class MemAllocation {


    public static void main(String[] args) {
//        testAllocation();
//        testPretenureSizeThreshold();
        testTenuringThreshold();
    }


    private static final int _1MB = 1024*1024;
    /**
     * VM args : -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     *
     * 参数解读：堆内存最大最小为20M ，新生代10M ，eden和survivor比例为8:1
     */
    public static void testAllocation(){
        byte[]allocation1,allocation2,allocation3,allocation4;
        allocation1 = new byte[2*_1MB];
        allocation2 = new byte[2*_1MB];
        allocation3 = new byte[2*_1MB];
        allocation4 = new byte[2*_1MB];
    }

    /**
     *
     * HotSpot虚拟机提供了-XX：PretenureSizeThreshold参数，
     * 指定大于该设置值的对象直接在老年代分配，这样做的目的就是避免在
     * Eden区及两个Survivor区之间来回复制，产生大量的内存复制操作。
     *
     * VM args: -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     *  -XX:PretenureSizeThreshold=3145728
     *
     *  -XX：PretenureSizeThreshold参数只对Serial和ParNew两款新生代收集器有效，
     *  HotSpot的其他新生代收集器，如Parallel Scavenge并不支持这个参数。
     *  如果必须使用此参数进行调优，可考虑ParNew加CMS的收集器组合。
     *
     * -XX:+UseConcMarkSweepGC
     */
    public static void testPretenureSizeThreshold(){
        byte[] allocation;
        allocation = new byte[4 * _1MB];
    }


    /**
     *
     * 增加参数-XX：MaxTenuringThreshold=15 设置晋升老年代的年龄线。
     * VM args: -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails -XX:SurvivorRatio=8
     * -XX:MaxTenuringThreshold=1 -XX:+PrintTenuringDistribution
     * -XX:+UseConcMarkSweepGC
     */
    public static void testTenuringThreshold(){
        byte[]allocation1,allocation2,allocation3;
        allocation1 = new byte[_1MB / 4];
        allocation2 = new byte[4 * _1MB];
        allocation3 = new byte[4 * _1MB];
    }


}
