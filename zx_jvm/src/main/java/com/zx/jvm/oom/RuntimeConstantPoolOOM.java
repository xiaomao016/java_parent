package com.zx.jvm.oom;

import java.util.HashSet;
import java.util.Set;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/3 0003 08:19
 * @description:
 * String::intern()是一个本地方法，作用是如果字符串常量池中已经包含一个等于此String对象的字符串，
 * 则返回代表池中这个字符串的String对象的引用，否则会将此String对象包含的字符串添加到常量池，并返回此String对象的引用。
 *
 * jdk1.6中，运行时常量池都分配在永久代
 * VM Args: -XX:PermSize=6M
 * 可以出现OOM
 *
 * jdk1.8之后，永久代的字符串常量池放入了堆中。所以可以
 * 限制堆为6M就可以出现OOM
 * VM args: -Xmx6M
 *
 */
public class RuntimeConstantPoolOOM {
    public static void main(String[] args) {
        //使用Set保持常量池引用，避免Full GC回收常量池行为
        Set<String> set = new HashSet<>();
        //在short范围内足以让6MB的PermSize产生OOM
        short i = 0 ;
        while(true){
            set.add(String.valueOf(i++).intern());
        }
    }
}
