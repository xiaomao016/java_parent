package com.zx.jvm.oom;

import java.util.HashSet;
import java.util.Set;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/3 0003 08:19
 * @description:
 * String::intern()是一个本地方法，作用是如果字符串常量池中已经包含一个等于此String对象的字符串，
 * 则返回代表池中这个字符串的String对象的引用，否则会将此String对象包含的字符串添加到常量池，并返回此String对象的引用。
 *
 * jdk1.6中，运行时常量池都分配在永久代
 * VM Args: -XX:PermSize=6M
 * 可以出现OOM
 *
 * jdk1.7之后，永久代的字符串常量池放入了堆中。所以可以
 * 限制堆为6M就可以出现OOM
 * VM args: -Xmx6M
 *
 *
 * 这段代码在JDK 6中运行，会得到两个false，而在JDK 7中运行，会得到一个true和一个false。
 * 产生差异的原因是，在JDK 6中，intern()方法会把首次遇到的字符串实例复制到永久代的字符串常量池中存储，返回的也是永久代里面这个字符串实例的引用，
 * 而由StringBuilder创建的字符串对象实例在Java堆上，所以必然不可能是同一个引用，结果将返回false。而JDK 7（以及部分其他虚拟机，例如JRockit）的
 * intern()方法实现就不需要再拷贝字符串的实例到永久代了，既然字符串常量池已经移到Java堆中，那只需要在常量池里记录一下首次出现的实例引用即可，因此
 * intern()返回的引用和由StringBuilder创建的那个字符串实例就是同一个。而对str2比较返回false，这是因为“java”[插图]这个字符串在执行
 * String-Builder.toString()之前就已经出现过了，字符串常量池中已经有它的引用，不符合intern()方法要求“首次遇到”的原则，“计算机软件”这个字符串则是
 * 首次出现的，因此结果返回true。
 *
 */
public class RuntimeConstantPoolOOM2 {
    public static void main(String[] args) {
        test();
    }

    /**
     * jdk6中是两个false。jdk7以后是一个true一个false。
     *
     * 在jdk6中，intern会将首次遇到的字符串实例复制到永久代，返回的是永久代实例的引用。而
     * StringBuilder是在堆上创建实例。所以必然不可能是同一个引用。
     *
     * 在7以后，字符串常量池已经移到堆中，只需要在常量池记录一下首次出现的引用即可，因此
     * intern返回的和StringBuild创建的实例是同一个。而java因为是关键词已经出现过，不符合intern
     * 首次遇到原则，所以是两个不同的对象。
     *
     *
     */
    public static void test(){
        //
        String str1 = new StringBuilder("计算机").append("软件").toString();
        System.out.println(str1.intern()== str1);

        String str2 = new StringBuilder("ja").append("va").toString();
        System.out.println(str2.intern()== str2);


    }
}
