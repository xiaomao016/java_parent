package com.zx.jvm.oom;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/2 0002 13:54
 * @description:
 *
 * VM Args :-Xss180k
 *
 *
 */
public class JavaVMStackSOF {


    private int stackLength = 1;
    public void stackLeak(){
        stackLength++;
        stackLeak();
    }

    public static void main(String[] args) throws Throwable{

        JavaVMStackSOF oom = new JavaVMStackSOF();

        try {
            oom.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack length:"+oom.stackLength);
            throw e;
        }
    }

}
