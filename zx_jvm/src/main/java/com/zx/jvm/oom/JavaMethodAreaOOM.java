package com.zx.jvm.oom;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/3 0003 08:46
 * @description:
 * jdk1.8之后，永久代彻底退出了历史舞台，元空间作为其替代登场。
 *
 * 针对元空间的一些设置：
 * -XX:MaxMataspaceSize   设置元空间最大值默认是-1，即不限制，只受限于本地内存大小。
 * -XX:MataspaceSize   设置元空间的初始空间大小以字节为单位，达到该值就会触发垃圾收集进行类型卸载。
 *                      同时垃圾收集器会对该值进行调整：如果释放了大量空间，就适当降低该值，如果
 *                      释放了很少的空间，则会适当提高该值（但不超MaxMataspaceSize设置的值）
 *
 *  -XX:MinMetaspaceFreeRatio
 *  -XX:MaxMetaspaceFreeRatio
 *  用于控制垃圾回收之后元空间剩余容量的百分比，控制收集的频率。比如提高该值，可以避免频繁GC。
 *
 *
 *  1.6中：方法区用永久代实现。方法区主要用来存放类型相关的信息，如类名，访问修饰符，常量池，
 *  字段描述，方法描述符等。可以通过产生大量的类去填满方法区。但在1.8后，这招很难造成OOM。
 *  -XX:PermSize=10M -XX:MaxPermSize=10M
 *
 */
public class JavaMethodAreaOOM {
    public static void main(String[] args) {
        while(true){
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(OOMObject.class);
            enhancer.setUseCache(false);
//            enhancer.setCallback((MethodInterceptor) (o, method, objects, methodProxy) -> methodProxy.invokeSuper(o,objects));
            enhancer.setCallback(new MethodInterceptor() {
                @Override
                public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                    return methodProxy.invokeSuper(o,objects);
                }
            });
            enhancer.create();
        }
    }
    static class OOMObject{}
}
