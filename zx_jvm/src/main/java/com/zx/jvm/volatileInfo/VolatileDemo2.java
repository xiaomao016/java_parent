package com.zx.jvm.volatileInfo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/23 0023 09:22
 * @description: if(null = = instance) {    // 线程二检测到instance不为空
 * synchronized (AtomicDemo.class) {
 * if(null == instance) {
 * instance = new AtomicDemo(); // 线程一执行这条语句并不是原子性的，所以会出现instance不为空，但其实对象并没有初始化的情况
 * }
 * }
 * }
 * return instance;    // 后面线程二执行时将引发：对象尚未初始化错误
 * <p>
 * 我们看instance这个变量，在执行new Singleton()语句，即创建Singleton对象的时候，其实在jvm虚拟机中是进行了下面这三件事：
 * <p>
 * 1、先在内存中开辟一个空间分配给Singleton对象；
 * <p>
 * 2、执行构造方法，初始化成员变量；
 * <p>
 * 3、将instance对象指向分配的内存空间。
 * <p>
 * 如果这三件事按顺序执行倒也无妨，但是jvm通常会给指令进行重排序，所以就有可能出现这种情况：
 * <p>
 * a、先在内存中开辟一个空间分配给Singleton对象；
 * <p>
 * b、将instance对象指向分配的内存空间；
 * <p>
 * c、执行构造方法，初始化成员变量。
 * <p>
 * 所以这个时候如果A线程执行到了b，另外一个B线程来看instance已经非空了，于是就返回instance了，
 * 但其实这个时候对象还没有完全创建完成，接着就会报对象尚未初始化的错误。
 * <p>
 * 而如果我们使用volatile关键字修饰instance变量，这种情况就可以避免，因为被volatile修饰的变量
 * 不会被jvm进行指令重排序，所以也就不会出现先执行3，后执行2的情况发生。所以instance只要是非空，就一定是已经初始化完毕的。
 */
public class VolatileDemo2 {

    public static AtomicInteger race = new AtomicInteger(0);
    private static final int THREAD_COUNT = 20;

    public static void increase() {
        race.incrementAndGet();
    }


    public static void main(String[] args) {

        Thread[] threads = new Thread[THREAD_COUNT];

        for (int i = 0; i < THREAD_COUNT; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 1000; i++) {
                        increase();
                    }
                }
            });
            threads[i].start();
        }

        //这里的意思是，不去打印，类似递归，如果当前还有线程活跃，即有可能修改最终的值，就要让出当前线程，
        //这样就可以打印最终的结果，而不是一个中间结果
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }


        System.out.println(race);

    }


}
