package com.zx.jvm.volatileInfo;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/8/23 0023 08:32
 * @description:
 *
 *
 * 关于volatile变量的可见性，经常会被开发人员误解，他们会误以为下面的描述是正确的：“volatile变量对所有线程是立即可见的，
 * 对volatile变量所有的写操作都能立刻反映到其他线程之中。换句话说，volatile变量在各个线程中是一致的，所以基于volatile变
 * 量的运算在并发下是线程安全的”。这句话的论据部分并没有错，但是由其论据并不能得出“基于volatile变量的运算在并发下是线程安
 * 全的”这样的结论。volatile变量在各个线程的工作内存中是不存在一致性问题的（从物理存储的角度看，各个线程的工作内存中volat
 * ile变量也可以存在不一致的情况，但由于每次使用之前都要先刷新，执行引擎看不到不一致的情况，因此可以认为不存在一致性问题），
 * 但是Java里面的运算操作符并非原子操作，这导致volatile变量的运算在并发下一样是不安全的，我们可以通过一段简单的演示来说明原因，请
 *
 *
 * 这段代码发起了20个线程，每个线程对race变量进行10000次自增操作，如果这段代码能够正确并发的话，最后输出的结果应该是200000。
 * 读者运行完这段代码之后，并不会获得期望的结果，而且会发现每次运行程序，输出的结果都不一样，都是一个小于200000的数字。这是为什么呢？
 *
 *
 * 问题就出在自增运算“race++”之中，我们用Javap反编译这段代码后会得到代码清单12-2所示，发现只有一行代码的increase()方法在Class文件
 * 中是由4条字节码指令构成（return指令不是由race++产生的，这条指令可以不计算），从字节码层面上已经很容易分析出并发失败的原因了：当
 * getstatic指令把race的值取到操作栈顶时，volatile关键字保证了race的值在此时是正确的，但是在执行iconst_1、iadd这些指令的时候，其
 * 他线程可能已经把race的值改变了，而操作栈顶的值就变成了过期的数据，所以putstatic指令执行后就可能把较小的race值同步回主内存之中。
 *
 *
 * 实事求是地说，笔者使用字节码来分析并发问题仍然是不严谨的，因为即使编译出来只有一条字节码指令，也并不意味执行这条指令就是一个原子操作。
 * 一条字节码指令在解释执行时，解释器要运行许多行代码才能实现它的语义
 *
 * 由于volatile变量只能保证可见性，在不符合以下两条规则的运算场景中，我们仍然要通过加锁（使用synchronized、java.util.concurrent中的
 * 锁或原子类）来保证原子性：
 * ·运算结果并不依赖变量的当前值，或者能够确保只有单一的线程修改变量的值。
 * ·变量不需要与其他的状态变量共同参与不变约束。
 * 下面这个示例，就适合用Volatile控制并发，当shutdown()方法被调用时，能保证所有线程中执行的doWork()方法都立即停下来。
 * volatile boolean shutDownRequest;
 * public void shutdown(){
 *     shutDownRequest = true;
 * }
 *
 * public void doWork(){
 *     while(!shutDownRequest){
 *         //代码逻辑
 *     }
 * }
 *
 */
public class VolatileDemo {
    public static volatile int race = 0;

    public static void increase(){
        race++;
    }

    private static final int THREAD_COUNT = 20;

    public static void main(String[]args){

        Thread[] threads = new Thread[THREAD_COUNT];

        for(int i=0;i<THREAD_COUNT;i++){
            threads[i] = new Thread(()->{
                for(int j=0;j<10000;++j){
                    increase();
                }
            });
            threads[i].start();
        }

//        while(Thread.activeCount()>1)
//            Thread.yield();


        System.out.println(race);

    }

}
