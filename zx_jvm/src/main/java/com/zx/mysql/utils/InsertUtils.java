package com.zx.mysql.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/11/30 0030 10:10
 * @description:
 */
public class InsertUtils {


    public static void main(String[] args) {

        //调用工具类
        Connection connection = JDBCUtil.getConnection();

        System.out.println("连接成功");

        //执行增删改操作
        PreparedStatement statement = null;
        try {

            //=============插入数据==============
            String sql = "insert into people(city,name,age,addr) values(?,?,?,?);";
            statement = connection.prepareStatement(sql);

            String[]citys = {"杭州","西安","兰州"};
            String[]names = {
                    "阿娇","段泽宁","寇丹","周泽","李晖","闵亚楠","朱朝阳","钱波","田伟厚","刘荣昌","万孟",
                    "何星","党小伟","张富强","薛若梅","胡美艳","涂勤瑶","岐娜","王园","张瑞瑞","刘娇","沙涛",
                    "姬小华","李阳","王栋","纪晨阳","刘永涛","向超","杨勇","张云云","周卡","邹博鹏","孙志勇",
                    "任铁兵","张文","李哲","张峰","严肖","马绍","王文鹏","范俊生","荆晓雯","卢峰","李洋波",
                    "刘鹏举","李鑫","李华","贾源","曹丹","周萌","袁瑞琦","胡延军","刘九宏","曹丹","李超",
                    "李苗苗", "秦思佳","杨昊天","张茜","周千恒","张云娇","左京伟","闫应飞","杨青","王浩天","廉春雨",
                    "张晨晨", "辛友强","王晓敏","乔鹏"};
            String[]adds = {"峨眉","华山","丐帮","少林","昆仑","武当","嵩山"};
            for(int i=0;i<1000;++i) {
                statement.setString(1,citys[i%3]);
                statement.setString(2,names[i%70]);
                statement.setInt(3,6+(i%30));
                statement.setString(4,adds[i%7]);
                int re = statement.executeUpdate();
                System.out.println(re);
            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            //调用工具类，关闭资源
            JDBCUtil.close(connection,statement,null);
        }


    }

}
