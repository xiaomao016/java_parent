package com.zx.mysql.utils;

import java.sql.*;

public class JDBCUtil {

    private static final String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    private static final String MYSQL_URL = "jdbc:mysql://192.168.100.9:3306/test?useUnicode=true&characterEncoding=UTF-8";
    private static final String MYSQL_USERNAME = "root";
    private static final String MYSQL_PASSWORD = "root@123A";

    /**
     * 功能：用于获取连接
     * @return Connection连接对象
     * @ throws Exception
     *
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(MYSQL_DRIVER_CLASS);
            conn = DriverManager.getConnection(MYSQL_URL, MYSQL_USERNAME, MYSQL_PASSWORD);
        } catch ( Exception e ) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     * 功能：释放资源
     * 通用的释放资源方法，无用参数留null
     * @param connection
     * @param statement PreparedStatement是其子类，使用多态，也可引用
     * @param resultSet
     */
    public static void close(Connection connection, Statement statement, ResultSet resultSet){
        //使用try-catch方式处理异常，免去调用者再次处理
        try {
            if(connection != null){
                connection.close();
            }
            if(statement != null){
                statement.close();
            }
            if(resultSet != null){
                resultSet.close();
            }
        } catch (SQLException e) {
            //编译时异常转为运行时异常
            throw new RuntimeException(e);
        }
    }



}
