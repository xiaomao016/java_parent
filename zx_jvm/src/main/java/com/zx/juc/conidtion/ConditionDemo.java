package com.zx.juc.conidtion;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/24 0024 17:33
 * @description:
 * 三个线程：要求
 * AA打印5次，BB打印10次，CC打印15次
 * 接着
 * AA打印5次，BB打印10次，CC打印15次
 *
 * 来10轮
 *
 */
public class ConditionDemo {


    static class ShareData{
        public static int flag = 1;//1.A打印5次 2.B打印10次 3.C打印15次

        Lock lock = new ReentrantLock();
        Condition a = lock.newCondition();
        Condition b = lock.newCondition();
        Condition c = lock.newCondition();



        public  void printA() throws InterruptedException {
            lock.lock();
            try {
                //1.判断
                while(flag!=1){
                    a.await();
                }
                //2.干活
                printTimes(5,"A");

                //3.通知
                flag = 2;
                b.signal();
            }finally {
                lock.unlock();
            }
        }

        public  void printB() throws InterruptedException {
            lock.lock();
            try {
                //1.判断
                while(flag!=2){
                   b.await();
                }
                //2.干活
                printTimes(10,"B");

                //3.通知
                flag = 3;
                c.signal();
            }finally {
                lock.unlock();
            }
        }

        public  void printC() throws InterruptedException {
            lock.lock();
            try {
                //1.判断
                while(flag!=3){
                    c.await();
                }
                //2.干活
                printTimes(15,"C");

                //3.通知
                flag = 1;
                a.signal();
            }finally {
                lock.unlock();
            }
        }

        public  void printTimes(int n,String name){
            for(int i = 0;i<n;++i){
                System.out.println(name);
            }
        }
    }

    public static void main(String[] args) {

        ShareData shareData = new ShareData();

        for(int i = 0;i<10;++i){
            new Thread(()->{
                try {
                    shareData.printA();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },Thread.currentThread().getName()).start();

            new Thread(()->{
                try {
                    shareData.printB();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },Thread.currentThread().getName()).start();

            new Thread(()->{
                try {
                    shareData.printC();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },Thread.currentThread().getName()).start();

        }

    }
}
