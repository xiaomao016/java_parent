package com.zx.juc.conidtion;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/24 0024 14:39
 * @description:
 * 一.
 * 多线程的编程，如何进行横向的交互。
 * wait() ,notifyAll
 * 通信判断，用while，即要重新检查。
 * 好比：大家过安检，坐上飞机，突然广播说有突发情况，可能有定时炸弹，所有人员下飞机。结果排查后，所有人员都要重新过安检。
 *
 * 如果在里边转悠等待的线程大部分是增加的线程，一旦减线程操作完，notifyAll，这些转悠的线程会立马上飞机，而不会再过安检，导致加操作被执行多次。
 *
 * 小结：多线程+while判断+新版写法。
 * synchronized - wait - notify
 * lock - Condition(await) - Condition(signal)
 *
 *
 *
 *
 */
public class ThreadNotify {

    //synchronized版本
    static class  Conditioner{
        public int number = 0;

        public synchronized void increment() throws InterruptedException {
            while(number!=0) {
                this.wait();
            }

            number++;
            System.out.println(number);
            this.notifyAll();
        }

        public synchronized void decrement() throws InterruptedException {
            while(number == 0 ){
                this.wait();
            }

            number--;
            System.out.println(number);
            this.notifyAll();
        }

    }
    //lock版本:定点通知
    static class AirConditioner{

        public int number = 0;

        final Lock lock = new ReentrantLock();
        final Condition incre  = lock.newCondition();
        final Condition decre = lock.newCondition();


        public  void increment() throws InterruptedException {
            lock.lock();
            try {
                //1.判断
                while (number != 0) {
                    incre.await();
                }
                //2.干活
                number++;
                System.out.println(number);
                //3.通知
                decre.signal();
            }finally {
                lock.unlock();
            }
        }

        public  void decrement() throws InterruptedException {
            lock.lock();
            try {
                while (number == 0) {
                    decre.await();
                }

                number--;
                System.out.println(number);
                incre.signal();
            }finally {
                lock.unlock();
            }
        }


    }

    //lock版本2：只用一个condition
    static class AirConditioner2{

        public int number = 0;

        Lock lock = new ReentrantLock();
        Condition condition  = lock.newCondition();


        public  void increment() throws InterruptedException {
            lock.lock();
            try {
                //1.判断
                while (number != 0) {
                    condition.await();
                }
                //2.干活
                number++;
                System.out.println(number);
                //3.通知
                condition.signal();
            }finally {
                lock.unlock();
            }
        }

        public  void decrement() throws InterruptedException {
            lock.lock();
            try {
                while (number == 0) {
                    condition.await();
                }

                number--;
                System.out.println(number);
                condition.signal();
            }finally {
                lock.unlock();
            }
        }


    }

    /**
     * 要求：
     * 一个变量，要求一个线程对其进行减操作，一个线程对其进行加操作。
     * 轮翻循环10轮。
     *
     * 思路：操作前判断，增加同步锁。
     *
     */
    public static void main(String[] args) throws Exception {

//        Conditioner c = new Conditioner();
        AirConditioner c = new AirConditioner();

        for (int i = 0 ;i<10;++i){
            new Thread(()->{
                try {
                    c.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },Thread.currentThread().getName()).start();




            new Thread(()->{
                try {
                    //此睡眠，会导致先产生大量等待的加线程，而这些先产生的线程优先级高，一旦他们拿到锁，必须通过while让他们继续等，如果是if，他们会
                    //如饿狼一样，使劲加。导致异常。
                    Thread.sleep(100);
                    c.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },Thread.currentThread().getName()).start();



        }

    }
}
