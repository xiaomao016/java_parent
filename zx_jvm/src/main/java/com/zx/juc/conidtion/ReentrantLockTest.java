package com.zx.juc.conidtion;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zx
 * @description:
 * @date 2022/6/17
 */

class ReentrantLockTest {
    private final Lock rtl =
            new ReentrantLock();
    int value;
    public int get() {
        // 获取锁
        rtl.lock();
        try {
            return value;
        } finally {
            // 保证锁能释放
            rtl.unlock();
        }
    }
    public void addOne() {
        // 获取锁
        rtl.lock();
        try {
            value = 1 + get();
        } finally {
            // 保证锁能释放
            rtl.unlock();
        }
    }
}
