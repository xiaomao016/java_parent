package com.zx.juc.interrupt;

/**
 * @author zx
 * @description:
 * @date 2022/8/1
 */
public class ThreadLocalTest {

    static ThreadLocal<String> localVariable = new ThreadLocal<>();

    static void print(String str){
        //打印当前线程本地内存中的变量localVariable的值
        System.out.println(str + ":" + localVariable.get());
        //清除当前内存中的localVariable
        localVariable.remove();
    }

    public static void main(String[] args) {
        Thread threadOne = new Thread(() -> {
            //设置线程One中本地变量localVariable的值
            localVariable.set("threadOne local variable");
            print("threadOne");
            System.out.println("threadOne remove after :"+localVariable.get());
        });

        Thread threadTwo = new Thread(()->{
            //设置线程One中本地变量localVariable的值
            localVariable.set("threadTwo local variable");
            print("threadTwo");
            System.out.println("threadTwo remove after :"+localVariable.get());
        });

        threadOne.start();
        threadTwo.start();
    }
}
