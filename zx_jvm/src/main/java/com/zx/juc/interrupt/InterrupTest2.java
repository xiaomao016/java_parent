package com.zx.juc.interrupt;

/**
 * @author zx
 * @description:
 * @date 2022/8/1
 */
public class InterrupTest2 {
    public static void main(String[] args) throws InterruptedException {
        Thread threadOne = new  Thread(new Runnable() {
            @Override
            public void run() {
                while(!Thread.currentThread().interrupted()){

                }
                System.out.println("threadOne isInterrupted " + Thread.currentThread().isInterrupted());
            }
        });

        //启动线程
        threadOne.start();
        //由输出结果可知，调用interrupted（）方法后中断标志被清除了。
        threadOne.interrupt();
        threadOne.join();
        System.out.println("main thread is over!");
    }
}
