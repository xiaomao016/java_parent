package com.zx.juc.interrupt;

import java.sql.SQLOutput;

/**
 * @author zx
 * @description:
 * @date 2022/7/27
 */
public class InterruptTest {

    public static void main(String[] args) throws InterruptedException {
        Thread threadOne = new Thread(new Runnable() {
            @Override
            public void run() {
                for (;;){

                }
            }
        });

        //启动线程
        threadOne.start();
        //设置中断标志
        threadOne.interrupt();

        //获取中断标志:true
        System.out.println("isInterrupted:" + threadOne.isInterrupted());
        //获取中断标识并重置:false  另外从下面的代码可以知道，在interrupted（）内部是获取当前调用线程的中断标志而不是调用interrupted（）方法的实例对象的中断标志。
        System.out.println("isInterrupted:" + threadOne.interrupted());
        //获取中断标识并重置:false
        System.out.println("isInterrupted:" + Thread.interrupted());
        //获取中断标志:true
        System.out.println("isInterrupted:" + threadOne.isInterrupted());

        threadOne.join();
        System.out.println("main thread is over!");

    }

}
