package com.zx.juc.interrupt;

/**
 * @author zx
 * @description:
 * @date 2022/8/1
 */
public class DaemonThreadTest {
    public static void main(String[] args) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                    for(;;){

                    }
            }
        });

        //设置位守护线程
        thread.setDaemon(true);

        thread.start();

        System.out.println("main thread is over");

    }
}
