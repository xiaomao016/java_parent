package com.zx.juc.collectionsafe;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/22 0022 09:44
 * @description:
 *
 * 故障现象：java.util.ConcurrentModificationException
 * 导致原因：并发修改异常，好比一张名单，同学们争先去签名，拉扯中，笔在纸上划了一道，这就是异常。
 * 解决办法：加锁，更改为同步方法。java集合工具类Collections、CopyOnWriteArrayList
 * 优化建议：采用current并发工具包中已经提供的支持并发的类
 *
 *
 */
public class CollectionNotSafe {

    public static void main(String[] args) {
//        listNotSafe();
//        listSafe01();
//        listSafe02();
//        listSafeFinal();
        
//        setSafeFinal();
        mapSafeFinal();

    }

    private static void mapSafeFinal() {
        Map<String,Integer> map = new ConcurrentHashMap<>();
//        Collections.synchronizedMap(new HashMap<>());
//        Map<String,Integer> map = new HashMap<>();
        for (int i = 1;i<50;++i){
            new Thread(()->{
                map.put(UUID.randomUUID().toString().substring(0,8),1);
                System.out.println(map);
                },Thread.currentThread().getName()).start();
        }
    }

    private static void setSafeFinal() {
//        Set<String> set = Collections.synchronizedSet(new HashSet<>());
        Set<String> set = new CopyOnWriteArraySet<>();
        for(int i=1;i<50;++i){
           new Thread(()->{
               set.add(UUID.randomUUID().toString().substring(0,8));
               System.out.println(set);
            },Thread.currentThread().getName()).start();
        }
    }

    private static void listSafeFinal() {
        List<String> list = new CopyOnWriteArrayList<>();
//        Collections.synchronizedList(new ArrayList_<>());
        for(int i = 1;i<300;++i){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },Thread.currentThread().getName()).start();
        }
    }

    private static void listSafe01() {
        List<String> list = new Vector<>();

        for(int i = 1;i<300;++i){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },Thread.currentThread().getName()).start();
        }
    }

    private static void listSafe02() {

        List<String> list = Collections.synchronizedList(new ArrayList<>());

        for(int i = 1;i<300;++i){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },Thread.currentThread().getName()).start();
        }

    }

    /**
     * 功能描述
     * @author zx
     * @date 2021/7/22 0022
     * @param
     * @return void
     * List线程不安全
     * 报错：CollectionNotSafe
     */
    private static void listNotSafe() {
        List<String> list = new ArrayList<>();

        for(int i=1;i<300;++i){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            },Thread.currentThread().getName()).start();

        }
    }

}
