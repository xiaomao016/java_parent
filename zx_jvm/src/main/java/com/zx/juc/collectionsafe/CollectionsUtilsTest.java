package com.zx.juc.collectionsafe;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author zx
 * @description:
 * @date 2022/6/23
 */
public class CollectionsUtilsTest {
    public static void main(String[] args) {
        //1.同步容器：基于sychronized关键字.外加 Vector,Stack,HashTable.
        List list = Collections.synchronizedList(new ArrayList());
        Set set = Collections.synchronizedSet(new HashSet());
        Map map = Collections.synchronizedMap(new HashMap());

        //2.并发容器
        //2.1 CopyOnWriteArrayList
        //一个是应用场景，CopyOnWriteArrayList 仅适用于写操作非常少的场景，而且能够容忍读写的短暂不一致。例如上面的例子中，写入的新元素并不能立刻被遍历到。
        List<String> list1 = new CopyOnWriteArrayList<>();

        //2.2.ConcurrentHashMap 和 ConcurrentSkipListMap
        Map<String,Integer> map2 = new ConcurrentHashMap<>();

        //2.3.CopyOnWriteArraySet 和 ConcurrentSkipListSet
        Set<String> set2 = new CopyOnWriteArraySet<>();

        //2.4.Queue
        //2.4.1.单端阻塞队列
        // ArrayBlockingQueue、LinkedBlockingQueue、SynchronousQueue、
        // LinkedTransferQueue、PriorityBlockingQueue 和 DelayQueue

        //2.4.2.单端非阻塞队列  ConcurrentLinkedQueue

        //2.4.3.双端非阻塞队列  ConcurrentLinkedDeque




    }
}
