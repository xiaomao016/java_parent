package com.zx.juc.collectionsafe;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zx
 * @description:
 * @date 2022/4/26
 * 结论：当10000个线程并发访问时
 * 最终list集合的数量不足10000，
 * 所以当多线程写入list时，需要用线程安全的list
 */
public class ListTest {
    List<String> downLoadPaths ;
    private AtomicInteger downloadedFileCount;
    private void init(){
        downLoadPaths = new CopyOnWriteArrayList<>();
        downloadedFileCount = new AtomicInteger(0);
    }

    private void dowload(){
        for(int i=0;i<10000;i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    downLoadPaths.add("aa");
                    finish();
                }
            }).start();
        }
    }

    private void finish() {
        synchronized (ListTest.class) { // 此处为类级别的锁
            int downLoadCount = downloadedFileCount.incrementAndGet();
            if(downLoadCount==10000){
                System.out.println(downLoadPaths.size());
            }
        }
    }

    public static void main(String[] args) {
        ListTest test = new ListTest();
        test.init();
        test.dowload();

    }
}
