package com.zx.juc.threadpool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author zx
 * @description:
 * @date 2022/6/24
 * 创建线程的几种方式
 */
public class NewThread {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //1.1.创建线程第一种方式
        new MyThread().start();
        //1.2.创建线程第二种方式，传递Runnable接口匿名类.相比第一种，这种方式可以让多个线程复用runnable的逻辑。
        new Thread(() -> {
            //....
        }).start();
        //1.3.创建线程第三种方式callable
        FutureTask<Integer> futureTask = new FutureTask<>(new CallableDemo.MyCallable());
        FutureTask<Integer> futureTask2 = new FutureTask(new CallableDemo.MyRunnable(),10);
        new Thread(futureTask,"A").start();
        int result = futureTask.get();
        //1.4.线程池
        //不建议使用 Executors 的最重要的原因是：
        // Executors 提供的很多方法默认使用的都是无界的
        // LinkedBlockingQueue，高负载情境下，无界队列很容易导致 OOM，
        // 而 OOM 会导致所有请求都无法处理，这是致命问题。所以强烈建议使用有界队列。

    }
}

//public 修饰的class可以当前package和其他package的类访问，而非public只能在当前包访问
class MyThread extends Thread {
    @Override
    public void run() {
        //...
    }
}