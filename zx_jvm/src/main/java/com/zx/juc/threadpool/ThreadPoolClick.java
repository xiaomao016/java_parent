package com.zx.juc.threadpool;

/**
 * @author admin
 * @Date Created in 下午2:59 2021/5/19
 *
 */

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolClick {
	/**
	 * 根据cpu的数量动态的配置核心线程数和最大线程数
	 */
	private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
	/**
	 * 核心线程数 = CPU核心数 + 1
	 */
	private static final int CORE_POOL_SIZE = 2000;
	/**
	 * 线程池最大线程数 = CPU核心数 * 2 + 1
	 */
	private static final int MAXIMUM_POOL_SIZE = 3000;
	/**
	 * 非核心线程闲置时超时60s
	 */
	private static final int KEEP_ALIVE = 60;

	private static ThreadPoolExecutor threadPool;

	/**
	 * 无返回值直接执行
	 * @param runnable
	 */
	public static void execute(Runnable runnable) {
		getThreadPool().execute(runnable);
	}

	/**
	 * 返回值直接执行
	 * @param callable
	 */
	public static <T> Future<T> submit(Callable<T> callable) {
		return getThreadPool().submit(callable);
	}

	/**
	 * dcs获取线程池
	 * @return 线程池对象
	 */
	private static ThreadPoolExecutor getThreadPool() {
		if (threadPool != null) {
			return threadPool;
		} else {

			synchronized (ThreadPoolClick.class) {
				if (threadPool == null) {
					threadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS,
							new LinkedBlockingQueue<>(2000), new DefaultThreadFactoryA(), new ThreadPoolExecutor.CallerRunsPolicy());
				}
				return threadPool;
			}
		}
	}

	static class DefaultThreadFactoryA implements ThreadFactory {
		private static final AtomicInteger poolNumber = new AtomicInteger(1);
		private final ThreadGroup group;
		private final AtomicInteger threadNumber = new AtomicInteger(1);
		private final String namePrefix;

		DefaultThreadFactoryA() {
			SecurityManager s = System.getSecurityManager();
			group = (s != null) ? s.getThreadGroup() :
					Thread.currentThread().getThreadGroup();
			namePrefix = "My-ThreadPool-" +
					poolNumber.getAndIncrement() +
					"-thread-";
		}

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(group, r,
					namePrefix + threadNumber.getAndIncrement(),
					0);
			if (t.isDaemon()) {
				t.setDaemon(false);
			}
			if (t.getPriority() != Thread.NORM_PRIORITY) {
				t.setPriority(Thread.NORM_PRIORITY);
			}
			return t;
		}
	}


}