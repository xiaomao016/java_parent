package com.zx.juc.threadpool;

import java.util.concurrent.*;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 09:49
 * @description: 线程池示例
 *
 *
 */
public class ExecutorDemo {

    public static void main(String[] args) {
        //用线程池执行任务，有两种提交方式，executor和submit 后者有返回值（future对象）。
        //1.1.创建固定数量线程池
        Executor executor = Executors.newFixedThreadPool(5);
        executor.execute(()->{
            System.out.println("bbb");
        });
        //1.2.用工厂类创建固定大小的一个线程池
        ExecutorService service = Executors.newFixedThreadPool(5);
        service.submit(()->{
            System.out.println("任务A");
        });
        service.submit(()->{
            System.out.println("任务B");
        });
        service.submit(new CallableDemo.MyCallable());


        //1.3.自定义一些参数
//        ExecutorService service1 = Executors.newCachedThreadPool();
//        service1.execute(()->{
//            System.out.println("ccc");
//        });

        //用Executors创建的线程池，等待队列默认是无界队列，可能会出现OOM
        //1.4.自定义一些参数
//        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS,
//                        new LinkedBlockingQueue<>(2000), new ThreadPoolClick.DefaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());



    }

}
