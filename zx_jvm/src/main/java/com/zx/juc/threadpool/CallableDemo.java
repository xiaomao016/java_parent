package com.zx.juc.threadpool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/28 0028 09:05
 * @description: callable 和runnable类似，是用futureTask封装了一层，
 * 这样就可以获取线程处理结果。
 */
public class CallableDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask<Integer> futureTask = new FutureTask<>(new MyCallable());
        FutureTask<Integer> futureTask2 = new FutureTask(new MyRunnable(),10);

        new Thread(futureTask,"A").start();
        int result = futureTask.get();

    }



    static class MyCallable implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            int sum = 0;
            for(int i = 0;i<10;++i){
                sum += i;
            }
            return sum;
        }
    }

    static class MyRunnable implements Runnable{

        @Override
        public void run() {
            int sum = 0;
            for(int i = 0;i<10;++i){
                sum += i;
            }
        }
    }
}
