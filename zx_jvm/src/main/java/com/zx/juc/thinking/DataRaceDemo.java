package com.zx.juc.thinking;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zx
 * @description:  封装共享变量
 * @date 2022/6/15
 */
public class DataRaceDemo {

        // 库存上限
        private final AtomicLong upper =
                new AtomicLong(0);
        // 库存下限
        private final AtomicLong lower =
                new AtomicLong(0);

        final Lock lock = new ReentrantLock();
        final Condition upperCondition = lock.newCondition();
        final Condition lowerCondition = lock.newCondition();


        // 设置库存上限
        void setUpper(long v){
            lock.lock();
            try {
                // 检查参数合法性
                while(v<lower.get()){
                    upperCondition.await();
                }
                upper.set(v);
                lowerCondition.signal();
            }catch (InterruptedException e){
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        // 设置库存下限
        void setLower(long v){
            lock.lock();
            try {
                // 检查参数合法性
                while (v > upper.get()) {
                    lowerCondition.await();
                }
                lower.set(v);
                upperCondition.signal();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }

}
