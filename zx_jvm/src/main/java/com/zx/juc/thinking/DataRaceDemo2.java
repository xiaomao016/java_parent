package com.zx.juc.thinking;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author zx
 * @description: 封装共享变量
 * @date 2022/6/15
 */
public class DataRaceDemo2 {

    // 库存上限
    private final AtomicLong upper =
            new AtomicLong(0);
    // 库存下限
    private final AtomicLong lower =
            new AtomicLong(0);


    // 设置库存上限
    synchronized void setUpper(long v) throws InterruptedException {
        // 检查参数合法性
        while (v < lower.get()) {
            this.wait();
        }
        upper.set(v);
        this.notifyAll();
    }

    // 设置库存下限
    synchronized void setLower(long v) throws InterruptedException {
        // 检查参数合法性
        while (v > upper.get()) {
            this.wait();
        }
        lower.set(v);
        this.notifyAll();
    }

}
