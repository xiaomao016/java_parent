package com.zx.juc.thinking;

/**
 * @author zx
 * @description:
 * @date 2022/6/13
 *
 * 1：非静态内部类，必须有一个外部类的引用才能创建。
 *
 * 2：在外部类的非静态方法中，因为有隐含的外部类引用this，所以可以直接创建非静态内部类。
 *
 * 3：在外部类的静态方法中，因为没有this，所以必须先获得外部类引用，然后创建非静态内部类。
 *
 * 4：静态内部类，不需要外部类引用就可以直接创建。
 *
 * 5：同时静态的内部类，也不能直接访问外部类的非静态方法。
 *
 * 6：由此可以推测，非静态内部类之所以可以直接访问外部类的方法，是因为创建非静态内部类时，有一个隐含的外部类引用被传递进来。
 *
 */
public class ThreadLifeCycle {

    public static void main(String[] args) {

        AddContainer addContainer = new AddContainer();


       Thread thread1 =  new Thread(()->{
          Thread th = Thread.currentThread();

          while(true){
              if(th.isInterrupted()){
                  break;
              }

              //...

              try {
                  Thread.sleep(1000);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }
        });

       Thread thread2 = new Thread(()->{
            thread1.interrupt();
       });

    }




    static class AddContainer{
        int num;

        public synchronized void add(int a) throws InterruptedException{

            while(num!=0){
                this.wait();
            }

            num+=a;
            this.notifyAll();
        }

        public synchronized void sub() throws InterruptedException{
            while(num<=0){
                this.wait();
            }
            num--;
            this.notifyAll();
        }

    }






}
