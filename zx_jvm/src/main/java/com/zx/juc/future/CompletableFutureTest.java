package com.zx.juc.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author zx
 * @description:
 * @date 2022/6/27
 * <p>
 * // 一次执行结果：
 * T1:洗水壶...
 * T2:洗茶壶...
 * T1:烧开水...
 * T2:洗茶杯...
 * T2:拿茶叶...
 * T1:拿到茶叶:龙井
 * T1:泡茶...
 * 上茶:龙井
 *
 *
    //使用默认线程池
    static CompletableFuture<Void>
    runAsync(Runnable runnable)
    static <U> CompletableFuture<U>
    supplyAsync(Supplier<U> supplier)
    //可以指定线程池
    static CompletableFuture<Void>
    runAsync(Runnable runnable, Executor executor)
    static <U> CompletableFuture<U>
    supplyAsync(Supplier<U> supplier, Executor executor)

    不是在调用方法的线程中执行的，这样是不是更容易理解

    对于简单的并行任务，你可以通过“线程池 +Future”的方案来解决；
    如果任务之间有聚合关系，无论是 AND 聚合还是 OR 聚合，都可以通过 CompletableFuture 来解决；
    而批量的并行任务，则可以通过 CompletionService 来解决。
 */
public class CompletableFutureTest {

    public static void main(String[] args) {
        m();
    }

    public static void m() {
        //任务1：洗水壶->烧开水
        CompletableFuture<Void> f1 =
                CompletableFuture.runAsync(() -> {
                    System.out.println("T1:洗水壶...");
                    sleep(1, TimeUnit.SECONDS);

                    System.out.println("T1:烧开水...");
                    sleep(10, TimeUnit.SECONDS);
                });
        //任务2：洗茶壶->洗茶杯->拿茶叶
        CompletableFuture<String> f2 =
                CompletableFuture.supplyAsync(() -> {
                    System.out.println("T2:洗茶壶...");
                    sleep(1, TimeUnit.SECONDS);

                    System.out.println("T2:洗茶杯...");
                    sleep(2, TimeUnit.SECONDS);

                    System.out.println("T2:拿茶叶...");
                    sleep(1, TimeUnit.SECONDS);
                    return "龙井";
                });
        //任务3：任务1和任务2完成后执行：泡茶
        CompletableFuture<String> f3 =
                f1.thenCombine(f2, (aa, tf) -> {
                    System.out.println("T1:拿到茶叶:" + tf);
                    System.out.println("T1:泡茶...");
                    return "上茶:" + tf;
                });
        //等待任务3执行结果
        System.out.println(f3.join());
    }

    public static void testAPI01(){

        CompletableFuture<String> f0 =
                CompletableFuture.supplyAsync(
                                () -> "Hello World")      //①
                        .thenApply(s -> s + " QQ")  //②
                        .thenApply(String::toUpperCase);//③

        System.out.println(f0.join());
        //输出结果 HELLO WORLD QQ
    }

    public static void testAPI02(){

        CompletableFuture<String> f1 =
                CompletableFuture.supplyAsync(()->{
                    int t = getRandom(5, 10);
                    sleep(t, TimeUnit.SECONDS);
                    return String.valueOf(t);
                });

        CompletableFuture<String> f2 =
                CompletableFuture.supplyAsync(()->{
                    int t = getRandom(5, 10);
                    sleep(t, TimeUnit.SECONDS);
                    return String.valueOf(t);
                });

        CompletableFuture<String> f3 =
                f1.applyToEither(f2,s -> s);

        System.out.println(f3.join());

        //异常处理
        CompletableFuture<Integer>
                f0 = CompletableFuture
                .supplyAsync(()->(7/0))
                .thenApply(r->r*10)
                .exceptionally(e->0);
        System.out.println(f0.join());
    }

    private static int getRandom(int i, int i1) {
        return 0;
    }

    private static void sleep(int i, TimeUnit seconds) {
        try {
            seconds.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
