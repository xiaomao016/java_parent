package com.zx.juc.future;

import java.util.concurrent.*;

/**
 * @author zx
 * @description:
 * @date 2022/6/29
 */
public class CompletionServiceTest {

    /**
     * ExecutorCompletionService(Executor executor)；
     * ExecutorCompletionService(Executor executor, BlockingQueue<Future<V>> completionQueue)。
     * 这两个构造方法都需要传入一个线程池，如果不指定 completionQueue，那么默认会使用无界的 LinkedBlockingQueue。
     * 任务执行结果的 Future 对象就是加入到 completionQueue 中。
     */

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        // 创建线程池
        ExecutorService executor =
                Executors.newFixedThreadPool(3);
        // 创建CompletionService
        CompletionService<Integer> cs = new
                ExecutorCompletionService<>(executor);
        // 异步向电商S1询价
        cs.submit(()->getPriceByS1());
        // 异步向电商S2询价
        cs.submit(()->getPriceByS2());
        // 异步向电商S3询价
        cs.submit(()->getPriceByS3());
        // 将询价结果异步保存到数据库
        for (int i=0; i<3; i++) {
            Integer r = cs.take().get();
            executor.execute(()->save(r));
        }
    }

    private static Integer getPriceByS1() {
        return 0;
    }

    private static Integer getPriceByS2() {
        return 0;
    }

    private static Integer getPriceByS3() {
        return 0;
    }

    private static void save(Integer r) {
    }

}
