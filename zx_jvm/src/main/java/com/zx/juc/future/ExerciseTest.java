package com.zx.juc.future;

import com.zx.design.strategy.demo.version02.sorters.ISortFile;

import java.util.concurrent.*;

/**
 * @author zx
 * @description:
 * @date 2022/6/27
 * 因为线程池，jvm进程不退出
 */
public class ExerciseTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        M1();
//        M2();
        M3();
    }

    //串行处理
    static void M1() throws InterruptedException {
        long start = System.currentTimeMillis();
        // 向电商S1询价，并保存
        int r1 = getPriceByS1();
        save(r1);
        // 向电商S2询价，并保存
        int r2 = getPriceByS2();
        save(r2);
        // 向电商S3询价，并保存
        int r3 = getPriceByS3();
        save(r3);
        long end = System.currentTimeMillis();
        //用时：3005
        System.out.println("time:" + (end - start));
    }

    //存在问题：
    //1.save操作也可以放在子线程，并发执行。
    //2.如果三个任务完成的时间不同，会阻塞到r11.get
    //可以通过一个阻塞队列，将执行完的结果放入队列，然后消费队列，这样先执行完的结果就可以先保存。
    static void M2() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        //优化
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer> r11 = executorService.submit(new Task1());
        Future<Integer> r12 = executorService.submit(new Task2());
        Future<Integer> r13 = executorService.submit(new Task3());

        save(r11.get());
        save(r12.get());
        save(r13.get());
        long end = System.currentTimeMillis();
        //用时：1015
        System.out.println("time:" + (end - start));
    }

    /**
     * 1，读数据库属于io操作，应该放在单独线程池，避免线程饥饿
     * 2，异常未处理
     */
    static void M3(){
        //采购订单
        Object po = new Object();
        CompletableFuture<Boolean> cf =
                CompletableFuture.supplyAsync(()->{
                    //在数据库中查询规则
                    return findRuleByJdbc();
                }).thenApply(r -> {
                    //规则校验
                    return check(po, r);
                });
        Boolean isOk = cf.join();
        System.out.println(isOk);
    }

    static boolean check(Object po,Integer r){
        return false;
    }

    private static Integer findRuleByJdbc() {
        return 0;
    }


    static class Task1 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            return getPriceByS1();
        }
    }

    static class Task2 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            return getPriceByS2();
        }
    }

    static class Task3 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            return getPriceByS3();
        }
    }

    static void save(int a) {
        System.out.println("保存：" + a);
    }

    private static int getPriceByS1() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return 1;
    }

    private static int getPriceByS2() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return 2;
    }

    private static int getPriceByS3() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return 3;
    }
}
