package com.zx.juc.future;

import java.util.concurrent.*;

/**
 * @author zx
 * @description:
 * @date 2022/6/27
 */
public class FutureTaskTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        testAPI();
    }

    private static void testAPI() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(1);

        TaskRunnable task1 = new TaskRunnable();

        executorService.submit(task1);
        Future<Integer> future = executorService.submit(task1, 12);
        System.out.println(future.get());

        TaskCallable task2 = new TaskCallable();
        Future future1 = executorService.submit(task2);
        Integer re = (Integer) future1.get();
        System.out.println(re);

    }

    static class TaskRunnable implements Runnable {

        @Override
        public void run() {
            System.out.println("taskRun");
        }
    }

    static class TaskCallable implements Callable{

        @Override
        public Integer call() throws Exception {
            System.out.println("call");
            return 12;
        }
    }


}
