package com.zx.juc.example.tickets;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/14 0014 07:39
 * @description: 动态绑定机制，JVM在执行对象的成员方法时，会将这个方法和对象的实际内存进行绑定，然后调用。
 */
public class KafkaLearn01 {

    public static void main(String[] args) {
        //多态，父类引用指向子类对象。
        AA a = new BB();

        System.out.println(a.get());
    }
}

class AA {
    public int i = 10;

    public int get() {
        return i + 10;
    }
}

class BB extends AA {

    public int i = 20;

    @Override
    public int get(){
        return i+20;
    }
}


