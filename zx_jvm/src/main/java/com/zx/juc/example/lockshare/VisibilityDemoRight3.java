package com.zx.juc.example.lockshare;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zx
 * @description:
 * @date 2022/5/24
 */
public class VisibilityDemoRight3 {


    private static long count ;

    private void add10K() {
        int idx = 0;
        while (idx++ < 1000000) {
            count +=1;
        }
    }

    public static long calc() throws InterruptedException {
        final VisibilityDemoRight3 test = new VisibilityDemoRight3();

        //锁
        final Lock lock = new ReentrantLock();
        //条件变量

        final Condition condition = lock.newCondition();

        // 创建两个线程，执行add()操作
        Thread th1 = new Thread(() -> {
            lock.lock();
            try {
                test.add10K();
                condition.signalAll();
            }finally {
                lock.unlock();
            }
        });
        Thread th2 = new Thread(() -> {
            lock.lock();
            try {
                test.add10K();
                condition.signalAll();
            }finally {
                lock.unlock();
            }
        });
        // 启动两个线程
        th1.start();
        th2.start();
        // 等待两个线程执行结束
        th1.join();
        th2.join();
        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println(calc());
    }

}
