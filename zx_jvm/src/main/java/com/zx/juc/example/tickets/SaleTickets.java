package com.zx.juc.example.tickets;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/14 0014 19:40
 * @description: 1.复制小括号，写死右箭头，落地大括号
 * 2.@FunctionalInterface
 * 3.default
 * 4.static
 * 5.Lock: 线程 操作 资源类
 */
public class SaleTickets {

    public static void main(String[] args) {
        //1.1.创建线程第一种方式
        //new MyThread().start();
        //1.2.创建线程第二种方式，传递Runnable接口匿名类.相比第一种，这种方式可以让多个线程复用runnable的逻辑。
        new Thread(() -> {
            //....
        }).start();
        //1.3.创建线程第三种方式callable

        //1.4.线程池

        //售票
        Tickets tickets = new Tickets();
        new Thread(() -> {
            for (int i = 1; i < 40; ++i) tickets.sale();
        }, "A").start();
        new Thread(() -> {
            for (int i = 1; i < 40; ++i) tickets.sale();
        }, "B").start();
        new Thread(() -> {
            for (int i = 1; i < 40; ++i) tickets.sale();
        }, "C").start();

    }
}

class Tickets {
    public int count = 40;
    Lock l = new ReentrantLock();

    public void sale() {
        l.lock();
        try { // access the resource protected by this lock
            if (count > 0) {
                System.out.println(Thread.currentThread().getName() + "售出第：" + count-- + "张票，还剩" + count + "张");
            }
        } finally {
            l.unlock();
        }
    }
}

class MyThread extends Thread {
    @Override
    public void run() {
        //...
    }
}

