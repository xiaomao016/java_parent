package com.zx.juc.example.price;

import com.zx.juc.future.ExerciseTest;
import com.zx.juc.threadpool.ThreadPoolClick;
import org.checkerframework.checker.units.qual.C;

import java.util.concurrent.*;

/**
 * @author zx
 * @description:
 * @date 2022/6/29
 */
public class FutureExamplePrice {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
//        M1();
//        M2();
//        M3();
        M4();
    }


    //串行处理:
    static void M1() throws InterruptedException {
        long start = System.currentTimeMillis();
        // 向电商S1询价，并保存
        int r1 = getPriceByS1();
        save(r1);
        // 向电商S2询价，并保存
        int r2 = getPriceByS2();
        save(r2);
        // 向电商S3询价，并保存
        int r3 = getPriceByS3();
        save(r3);
        long end = System.currentTimeMillis();
        //用时： 6039
        System.out.println("time:" + (end - start));
    }

    //利用Future对象
    static void M2() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        //优化
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer> r11 = executorService.submit(new Task1());
        Future<Integer> r12 = executorService.submit(new Task2());
        Future<Integer> r13 = executorService.submit(new Task3());

        save(r11.get());
        save(r12.get());
        save(r13.get());
        long end = System.currentTimeMillis();
        //用时：4039
        System.out.println("time:" + (end - start));
    }

    //优化save串行
    static void M3() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        //优化
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer> r11 = executorService.submit(new Task1());
        Future<Integer> r12 = executorService.submit(new Task2());
        Future<Integer> r13 = executorService.submit(new Task3());
        //创建一个计数器
        CountDownLatch latch = new CountDownLatch(3);
        int re1 = r11.get();
        executorService.execute(() -> {
            save(re1);
            latch.countDown();
        });
        int re2 = r12.get();
        executorService.execute(() -> {
            save(re2);
            latch.countDown();

        });
        int re3 = r13.get();
        executorService.execute(() -> {
            save(re3);
            latch.countDown();
        });
        latch.await();
        long end = System.currentTimeMillis();
        //用时：2047
        System.out.println("time:" + (end - start));
    }

    //优化 r11.get可能阻塞问题，将执行结果放入阻塞队列，先执行完的先进行保存
    static void M4() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        //优化
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer> r11 = executorService.submit(new Task1());
        Future<Integer> r12 = executorService.submit(new Task2());
        Future<Integer> r13 = executorService.submit(new Task3());

        BlockingQueue<Integer> queue = new LinkedBlockingQueue<>(10);

        executorService.execute(() -> {
            try {
                queue.put(r11.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });

        executorService.execute(() -> {
            try {
                queue.put(r12.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });

        executorService.execute(() -> {
            try {
                queue.put(r13.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
        CountDownLatch latch = new CountDownLatch(3);
        for (int i = 0; i < 3; ++i) {
            //从队列取出结果消费保存：谁先执行完，谁就会被先消费
            Integer r = queue.take();
            executorService.execute(() -> {
                save(r);
                latch.countDown();
            });
        }
        latch.await();
        long end = System.currentTimeMillis();
        //用时：1015
        System.out.println("time:" + (end - start));
    }

    //用completionService完成异步任务，并且它自带保存结果的阻塞队列。使用更加简介。
    //cs有两个构造方法，如果只传线程池，则结果默认放入一个无界队列，也可以通过构造方法传入一个队列用来保存结果。
    static void M5() throws InterruptedException, ExecutionException {
        //创建线程池
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2000, 2000, 60, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(2000), Executors.defaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());
        //创建cs：完成的任务默认放入completionQueue 中，一个无界队列
        CompletionService<Integer> cs = new ExecutorCompletionService<Integer>(poolExecutor);

        //向平台1询价
        cs.submit(()->getPriceByS1());
        //向平台2询价
        cs.submit(()->getPriceByS2());
        //向平台3询价
        cs.submit(()->getPriceByS3());

        //从结果队列中消费，异步保存
        for(int i=0;i<3;++i){
            Integer r = cs.take().get();
            poolExecutor.execute(()->save(r));
        }
    }


    /************业务方法******************/
    static boolean check(Object po, Integer r) {
        return false;
    }

    private static Integer findRuleByJdbc() {
        return 0;
    }

    static class Task1 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            TimeUnit.SECONDS.sleep(2);
            return getPriceByS1();
        }
    }

    static class Task2 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            return getPriceByS2();
        }
    }

    static class Task3 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            return getPriceByS3();
        }
    }

    static void save(int a) {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            //重置中断标识，后续如果需要使用，可以直接使用
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
        System.out.println("保存：" + a);
    }

    private static int getPriceByS1() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return 1;
    }

    private static int getPriceByS2() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return 2;
    }

    private static int getPriceByS3() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return 3;
    }
}
