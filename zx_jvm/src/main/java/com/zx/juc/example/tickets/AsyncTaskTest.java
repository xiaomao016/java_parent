package com.zx.juc.example.tickets;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author zx
 * @description:
 * @date 2022/4/22
 */
public class AsyncTaskTest {
    private static ExecutorService executor = Executors.newFixedThreadPool(10);
    private static LinkedBlockingQueue<String> userInfoQueue = new LinkedBlockingQueue(10000 * 50);
    public static void main(String[] args) {
        String jsonStr;
        for(int i=1;i<10000;i++){
            System.out.println("开始执行读取操作：");
            jsonStr = i+"";
            asyncTask(jsonStr);
            System.out.println("读取下一条数据");
        }

        System.out.println(new Date() + " 主线程执行结束");
    }

    /**
     * 异步执行的方法
     */
    public static void asyncTask(String i) {
        executor.execute(new Runnable() {
            public void run() {
                try {
                    System.out.println("redis去重操作");
                    // 模拟异步方法执行的过程，耗时10秒
                    Thread.sleep(10);
                    System.out.println(i + " 异步方法执行结束");
                    userInfoQueue.put(i);
                    System.out.println(userInfoQueue.size());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 同步执行的方法
     */
    public static void synTask() {
        System.out.println(new Date() + " 同步方法执行结束");
    }
}
