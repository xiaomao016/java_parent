package com.zx.juc.eightLocks;

import java.util.concurrent.Semaphore;

/**
 * @author zx
 * @description:
 * @date 2022/6/20
 */
public class SemaphoreTest {

    public static void main(String[] args) {

    }


    static class SemaphoreCount{

        static int count;
        //初始化信号量
        static final Semaphore s
                = new Semaphore(1);
        //用信号量保证互斥
        static void addOne() throws InterruptedException {
            s.acquire();
            try {
                count+=1;
            } finally {
                s.release();
            }
        }
    }

}
