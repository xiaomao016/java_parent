package com.zx.juc.eightLocks;

import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.StampedLock;

/**
 * @author zx
 * @description:
 * @date 2022/6/22
 */
public class StampedLockTest {


    static class Point {
        private int x, y;
        final StampedLock sl = new StampedLock();

        //计算到原点的距离
        double distanceFromOrigin() {
            // 乐观读
            long stamp = sl.tryOptimisticRead();
            // 读入局部变量，
            // 读的过程数据可能被修改
            int curX = x, curY = y;
            //判断执行读操作期间，
            //是否存在写操作，如果存在，
            //则sl.validate返回false
            if (!sl.validate(stamp)) {
                // 升级为悲观读锁
                stamp = sl.readLock();
                try {
                    curX = x;
                    curY = y;
                } finally {
                    //释放悲观读锁
                    sl.unlockRead(stamp);
                }
            }
            return Math.sqrt(curX * curX + curY * curY);
        }

    }


    /**
     * 使用 StampedLock 一定不要调用中断操作，如果需要支持中断功能，一定使用可中断的悲观读锁
     * readLockInterruptibly() 和写锁 writeLockInterruptibly()
     */
    static class InterruptTest {

        final StampedLock lock = new StampedLock();

        void m() throws InterruptedException {
            Thread T1 = new Thread(() -> {
                // 获取写锁
                lock.writeLock();
                // 永远阻塞在此处，不释放写锁
                LockSupport.park();
            });
            T1.start();
            // 保证T1获取写锁
            Thread.sleep(100);
            Thread T2 = new Thread(() ->
                    //阻塞在悲观读锁
                    lock.readLock()
            );
            T2.start();
            // 保证T2阻塞在读锁
            Thread.sleep(100);
            //中断线程T2
            //会导致线程T2所在CPU飙升
            T2.interrupt();
            T2.join();
        }
    }

    //课后思考
    static class Exercise {
        private double x, y;
        final StampedLock sl = new StampedLock();

        // 存在问题的方法
        void moveIfAtOrigin(double newX, double newY) {
            long stamp = sl.readLock();

            try {
                while (x == 0.0 && y == 0.0) {
                    long ws = sl.tryConvertToWriteLock(stamp);
                    if (ws != 0L) {
                        stamp = ws;
                        x = newX;
                        y = newY;
                        break;
                    } else {
                        sl.unlockRead(stamp);
                        stamp = sl.writeLock();
                    }
                }
            } finally {
                sl.unlock(stamp);
            }
        }
    }


}
