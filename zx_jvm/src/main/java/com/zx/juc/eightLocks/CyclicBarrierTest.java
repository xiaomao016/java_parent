package com.zx.juc.eightLocks;

import com.zx.juc.thinking.vo.D;
import com.zx.juc.thinking.vo.P;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

/**
 * @author zx
 * @description:
 * @date 2022/6/22
 *
 * 线程 T1 负责查询订单，当查出一条时，调用 barrier.await() 来将计数器减 1，同时等待计数器变成 0；
 * 线程 T2 负责查询派送单，当查出一条时，也调用 barrier.await() 来将计数器减 1，同时等待计数器变成 0；
 * 当 T1 和 T2 都调用 barrier.await() 的时候，计数器会减到 0，此时 T1 和 T2 就可以执行下一条语句了，
 * 同时会调用 barrier 的回调函数来执行对账操作。
 */
public class CyclicBarrierTest {


    static class Bill{


        // 订单队列
        Vector<P> pos;
        // 派送单队列
        Vector<D> dos;
        // 执行回调的线程池
        Executor executor =
                Executors.newFixedThreadPool(1);
        final CyclicBarrier barrier =
                new CyclicBarrier(2, ()->{
                    executor.execute(()->check());
                });
        boolean isDone = false;

        boolean diff = false;
        void check(){
            P p = pos.remove(0);
            D d = dos.remove(0);
            // 执行对账操作
            diff = checkPD(p, d);
            // 差异写入差异库
            save(diff);
        }
        void checkAll(){
            // 循环查询订单库
            Thread T1 = new Thread(()->{
                while(isDone){
                    // 查询订单库
                    pos.addAll(getPOrders());
                    // 等待
                    try {
                        barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }
            });
            T1.start();
            // 循环查询运单库
            Thread T2 = new Thread(()->{
                while(isDone){
                    // 查询运单库
                    dos.addAll(getDOrders());
                    // 等待
                    try {
                        barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }
            });
            T2.start();
        }

        //思考题
        void t(){
            CompletableFuture<List> pOrderFuture = CompletableFuture.supplyAsync(this::getPOrders);
            CompletableFuture<List> dOrderFuture = CompletableFuture.supplyAsync(this::getDOrders);
//            pOrderFuture.thenCombine(dOrderFuture, this::check).thenAccept(this::save)
        }

        //===========业务方法===========
        private boolean checkPD(P p, D d) {
            return false;
        }
        private List<D> getDOrders() {
            return null;
        }
        private List<P> getPOrders() {
            return null;
        }
        private void save(boolean diff) {
        }
    }

}
