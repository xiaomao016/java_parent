package com.zx.juc.eightLocks;

import java.util.concurrent.TimeUnit;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/7/23 0023 20:32
 * @description:
 *
 * 1.当两个方法都是普通方法，多线程调用时，要根据cpu的调度情况。
 * 2.当一个方法是普通方法，一个是synchronized方法时，两个方法互不影响。好比synchronized锁的是手机，而普通方法是充电器，可以各使用各的。
 * 3.当两个方法都是synchronized方法时，第一个拿到锁的方法，会锁住整个this对象 ,所以另一个只能等待。
 * 4.如果是静态方法，加上synchronized，则锁住的是Class。 就好比锁住的是学校大门，而this是一间教室。
 *
 * 也就是说：锁有两种，一种是当前对象this，另一种是Class。
 *          当大家需要获取相同锁时，就需要等待，否则互不影响。
 *
 *
 */
public class EightLock {

    static class Phone {

        public  synchronized void sendEmail() {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("SendEmail");
        }

//        public  synchronized void sendSMS() {
//            System.out.println("SendSMS");
//        }

        /**
         * 如果同步代码块中锁对象是this,则同步代码块和同步方法锁的是一把锁，之前的结论依然成立，如果是
         * 同步代码块中的对象不是this，则不是同一把锁，也就不存在等待之说。
         *
         */
        public   void sendSMS() {
            synchronized(new Object()) {
                System.out.println("SendSMS");
            }
        }
    }

    public static void main(String[] args) {

        Phone phone = new Phone();
        Phone phone2 = new Phone();


        new Thread(() -> {
            phone.sendEmail();
        }, Thread.currentThread().getName()).start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            phone.sendSMS();
        }, Thread.currentThread().getName()).start();

    }

}
