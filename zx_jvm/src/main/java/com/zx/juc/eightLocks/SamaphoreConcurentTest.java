package com.zx.juc.eightLocks;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static jnr.ffi.provider.jffi.JNINativeInterface.Throw;

@Slf4j
public class SamaphoreConcurentTest {

    private static final Integer downloadFileThreadCount = 300;
    private static final Semaphore downloadFileSemaphore = new Semaphore(downloadFileThreadCount);

    public static void main(String[] args) {

        for(int i=0;i<3;++i){
            int finalI = i;
            Thread thread = new Thread(()->{
                downloadFile(finalI);
            });
            thread.start();
        }

    }

    public static void downloadFile(int i) {
        try {
            try {
//                downloadFileSemaphore.acquire();
                int i1 = ThreadLocalRandom.current().nextInt(3);
                TimeUnit.SECONDS.sleep(i1);
//                if(i % 2==0){
                    throw new RuntimeException("下载文件失败");
//                }
            } finally {
                log.info("final time:{}",System.currentTimeMillis());
                TimeUnit.MILLISECONDS.sleep(8);
                downloadFileSemaphore.release();
                int d = downloadFileThreadCount - downloadFileSemaphore.availablePermits();
                log.info("当前下载文件的线程数量:{}, offerId is :{}",d , i);
                if(d<0){
                    log.info("============================:{}",d);
                }
            }
        } catch (Exception e) {
            log.info("error time:{}",System.currentTimeMillis());
        }
    }


}
