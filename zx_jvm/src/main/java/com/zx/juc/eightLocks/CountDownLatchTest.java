package com.zx.juc.eightLocks;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author zx
 * @description:
 * @date 2022/6/22
 */
public class CountDownLatchTest {

    void t() throws InterruptedException {

        // 创建2个线程的线程池
        Executor executor = Executors.newFixedThreadPool(2);

        boolean isDone = false;
        while (isDone) {
            // 计数器初始化为2
            CountDownLatch latch = new CountDownLatch(2);
            // 查询未对账订单
            executor.execute(() -> {
                List<String> pos = getPOrders();
                latch.countDown();
            });
            // 查询派送单
            executor.execute(() -> {
                List<String> dos = getDOrders();
                latch.countDown();
            });

            // 等待两个查询操作结束
            latch.await();

            // 执行对账操作
            //diff = check(pos, dos);
            // 差异写入差异库
            //save(diff);
        }
    }

    private List<String> getDOrders() {
        return null;
    }

    private List<String> getPOrders() {
        return null;
    }

}
