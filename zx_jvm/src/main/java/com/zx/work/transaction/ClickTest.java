package com.zx.work.transaction;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.zip.Inflater;

/**
 * @author li
 * @version 1.0
 * @description:
 * @date 2022/3/23 18:20
 */
public class ClickTest {


    public static void main(String[] args) {
        Base64.Decoder urlDecoder  = Base64.getUrlDecoder();
        String click = "QngBDce9CsIwFEDhF7qB-99kEQSXLhUcXOUmTSaHopOQh7fD4eO4sSqpWgmdhESI6C9ndZOsSx_arQ86a5HSBUgA4ji-4_3rH3gyeYbrdnvc1xuQC56fMxYW8aKLAlRfsnJt6bQlHVZTQYvUahRytSF9nzuLVS4RYqJFKNiMSiBC55Beqc91m3-HuSyE";
        byte[] decode = urlDecoder.decode(click);
        byte[] uncompress = uncompress(decode);
        /**
         * 0: click_time + offer_id
         * 1: aff
         * 2: aff_click_id
         * 3: country + [mix_aff]
         * [4: screen_source_id]
         */
        String[] params = new String(uncompress).split("\\|");


        Long offerId;
        // 解析渠道
        int affiliateId;
        String affSub = null;
        Long clickTime;
        int affSplit = params[1].indexOf("_");
        if (affSplit > -1) {
            affiliateId = Integer.parseInt(params[1].substring(0, affSplit));
            affSub = params[1].substring(affSplit + 1);
        } else {
            affiliateId = Integer.parseInt(params[1]);
        }

        if (affiliateId < 1000) {
            offerId = Long.valueOf(params[0]);
            clickTime = Long.valueOf(params[4]);
            //log.info("old platform post back aff -> {}, offer -> {}, url -> {}", affiliateId, offerId, request.getUrl());
        } else {
            offerId = Long.valueOf(params[0].substring(8), 16);
            clickTime = Long.valueOf(params[0].substring(0, 8), 16);
        }
        System.out.println(offerId );
    }


    public static byte[] uncompress(byte[] decode) {
        int num = (decode.length - 1) / 128 + 1;
        byte[] data = new byte[decode.length - num];
        int remainder = decode.length % 128;
        remainder = remainder == 0 ? 128 : remainder;

        for(int i = 0; i < num; ++i) {
            int length = i < num - 1 ? 127 : remainder - 1;
            System.arraycopy(decode, i * 128 + 1, data, i * 127, length);
        }

        Inflater decompressor = new Inflater();

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Throwable var6 = null;

            try {
                decompressor.setInput(data);
                byte[] buf = new byte[128];
                int loop = 0;

                while(!decompressor.finished() && loop < 10) {
                    ++loop;
                    int count = decompressor.inflate(buf);
                    bos.write(buf, 0, count);
                }

                byte[] var35;
                if (loop >= 10) {
                    var35 = null;
                    return var35;
                }

                var35 = bos.toByteArray();
                return var35;
            } catch (Throwable var29) {
                var6 = var29;
                throw var29;
            } finally {
                if (bos != null) {
                    if (var6 != null) {
                        try {
                            bos.close();
                        } catch (Throwable var28) {
                            var6.addSuppressed(var28);
                        }
                    } else {
                        bos.close();
                    }
                }

            }
        } catch (Exception var31) {
            var31.printStackTrace();
        } finally {
            decompressor.end();
        }

        return null;
    }

}
