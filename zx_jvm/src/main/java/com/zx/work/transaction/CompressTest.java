package com.zx.work.transaction;

import java.io.UnsupportedEncodingException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressTest {


    public static void main(String[] args) {
        String aa = "123134|100008311|UA|android|name";
        String b = new String(compress(aa));
        System.out.println(b);
    }

    public static byte[] compress(String data){
        byte[] input = new byte[0];
        try {
            input = data.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        byte[] output = new byte[input.length];

        Deflater deflater = new Deflater();
        deflater.setInput(input);
        deflater.finish();
        int compressedDataLength = deflater.deflate(output);

        return output;
    }

    public static String decompress(byte[] data){
        try {
            byte[] result = new byte[data.length * 3];
            Inflater inflater = new Inflater();
            inflater.setInput(data);
            int resultLength = inflater.inflate(result);
            inflater.end();
            return new String(result, 0, resultLength, "UTF-8");
        } catch (DataFormatException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
