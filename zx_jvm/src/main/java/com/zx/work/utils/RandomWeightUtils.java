package com.zx.work.utils;

import java.util.*;

public class RandomWeightUtils {
    public RandomWeightUtils() {
    }

    public static <K> K random(Map<K, Integer> map) {
        if (map != null && !map.isEmpty()) {
            List<K> list = new LinkedList();
            Iterator var2 = map.entrySet().iterator();

            while(var2.hasNext()) {
                Map.Entry<K, Integer> entry = (Map.Entry)var2.next();

                for(int i = 0; i < (Integer)entry.getValue(); ++i) {
                    list.add(entry.getKey());
                }
            }
            Collections.shuffle(list);
            return list.get(0);
        } else {
            return null;
        }
    }
}
