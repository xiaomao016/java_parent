package com.zx.work.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author zx
 * @description:
 * @date 2022/6/28
 */
public class DateUtilFinal {

    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat daydf = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat dayNumberdf = new SimpleDateFormat("dd");
    private static final DateFormat hhmmss = new SimpleDateFormat("HH:mm:ss");
    private static final DateFormat hh = new SimpleDateFormat("HH");
    private static final DateFormat mm = new SimpleDateFormat("mm");
    private static final DateFormat yyyymmddhhmmss = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final DateFormat yyyymmdd = new SimpleDateFormat("yyyyMMdd");
    private static final DateFormat yyyymmddhhmm = new SimpleDateFormat("yyyyMMddHHmm");

    /*********format to string 系列*********/
    //获取格式化后的日期字符串
    public static String getDateFormatyyyyMMdd(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date newDate = new Date(date);
        String dateStr = format.format(newDate);
        return dateStr;
    }

    public static String getDateFormatyyyyMMddHHmmssDense(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Date newDate = new Date(date);
        String dateStr = format.format(newDate);
        return dateStr;
    }

    public static String getDateFormatyyyyMMddHHmmss(long date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = new Date(date);
        return df.format(newDate);
    }


    public static String getDateFormatyyyyMMdd(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String dateStr = format.format(date);
        return dateStr;
    }

    public static String getDateFormatyyyy(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        String dateStr = format.format(date);
        return dateStr;
    }

    public static String getDateFormatMM(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MM");
        String dateStr = format.format(date);
        return dateStr;
    }

    public static String getDateFormatdd(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        String dateStr = format.format(date);
        return dateStr;
    }

    public static String getDateFormatHH(Date date){
        return hh.format(date);
    }

    public static String getDateFormatmm(Date date){
        return mm.format(date);
    }

    public static String getDateFormatyyyyMMddHHmmss() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar curcal = Calendar.getInstance();
        return sdf.format(curcal.getTime());
    }

    public static String getDateFormatyyyyMMddHHmmss(Date d) {
        DateFormat yyyymmddhhmmss = new SimpleDateFormat("yyyyMMddHHmmss");
        return yyyymmddhhmmss.format(d);
    }

    public static String getDateFormatMMdd(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MMdd");
        return format.format(date);
    }

    public static String getDateFormatyyyMMddHHmm(Date d){
        return yyyymmddhhmm.format(d);
    }

    public static String formatWithLocalDate(LocalDateTime date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return date.format(formatter);
    }

    public static String getDateBeforeDaysyyyyMMddHHmmss(){
        // current_time
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime fromDate = currentDateTime.minusDays(3);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String from = fromDate.format(formatter);
        return from;
    }

    public static String getPreviousHourTime() {
        LocalDateTime now = LocalDateTime.now();  // 获取当前时间
        LocalDateTime previousHourTime = now.minusHours(1);  // 减去一个小时
        LocalDateTime formattedTime = previousHourTime.withMinute(0).withSecond(0);  // 设置分钟和秒设置为0
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  // 定义日期时间格式
        return formatter.format(formattedTime);  // 格式化时间
    }

    public static String getHourTime() {
        LocalDateTime now = LocalDateTime.now();  // 获取当前时间
        LocalDateTime formattedTime = now.withMinute(0).withSecond(0);  // 设置分钟和秒设置为0
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  // 定义日期时间格式
        return formatter.format(formattedTime);  // 格式化时间
    }

    /***********format to Date***************/

    /**
     * 格式化字符串为日期
     *
     * @param time 字符串格式为 yyyy-MM-dd HH:mm:ss
     * @return 日期 Date对象
     */
    public static Date formatTime(String time) {
        try {
            return df.parse(time);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        }
    }

    /**
     * 格式化字符串为日期
     *
     * @param day 字符串格式为yyyy-MM-dd
     * @return 日期 Date对象
     */
    public static Date formatDayTime(String day) {
        try {
            return daydf.parse(day);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        }
    }

    public static Date formatDayByPattern(String format, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);
        Date date = null;

        try {
            date = sdf.parse(format);
        } catch (Exception var5) {
        }

        return date;
    }


    /********Date 加减系列********************/

    public static Date addSeconds(Date date, int seconds) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(13, seconds);
        return c.getTime();
    }

    public static Date addMinutes(Date date, int minutes) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(12, minutes);
        return c.getTime();
    }

    public static Date addHours(Date date, int hours) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(11, hours);
        return c.getTime();
    }

    public static Date addDays(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(5, days);
        return c.getTime();
    }

    public static Date addMonths(Date date, int months) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(2, months);
        return c.getTime();
    }

    //计算该时间n天前的时间戳
    public static Long getDayTimeBefore(Long cur,int n) {
        return cur - n*24*60*60*1000;
    }
    //计算该时间n小时前的时间戳
    public static Long getHoursTimeBefore(Long cur,int n) {
        return cur - n*60*60*1000;
    }
    //计算该时间n分钟前的时间戳
    public static Long getMinutesTimeBefore(Long cur,int n) {
        return cur - n*60*1000;
    }

    /**
     * 两个日期相减，返回差值（单位天）
     *
     * @param date1 被减数
     * @param date2 减数
     * @return
     */
    public static int subDate1ToDate2(String date1, String date2) {
        int days = 0;
        try {
            Date d1 = formatDayTime(date1);
            Date d2 = formatDayTime(date2);
            long result = (d1.getTime() - d2.getTime()) / (1000 * 24 * 60 * 60);
            days = (int) result;
        } catch (Exception e) {
            return days;
        }
        return days;
    }





    /**
     * 两个日期相减，返回差值（单位秒）
     *
     * @param date1 被减数
     * @param date2 减数
     * @return
     */
    public static Long subDate1ToDate2(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return 0L;
        }
        long result = (date1.getTime() - date2.getTime()) / 1000;
        if (result < 0) {
            result = Math.abs(result);
        }
        return result;
    }

    /*******************时间点计算*******************/

    //计算该时间所在天零点的时间戳
    public static Long getZeroPointTimeMillis(Long cur) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    //计算该时间所在天24点的时间戳
    public static Long get24PointTimeMillis(Long cur) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    /**
     * 当前日期 + 整点时间
     *
     * @return yyyy-MM-dd格式的当前日期
     */
    public static String getFixedHourDate() {
        String format = daydf.format(new Date());
        String todayTime = hh.format(new Date());
        return format + " " + todayTime + ":00:00";
    }

    /**
     * 獲取Date对应的整点日期
     * @param date
     * @return
     */
    public static Date getFixedHourDate(Date date) {
        String format = daydf.format(date);
        String todayTime = hh.format(date);
        String reStr =  format + " " + todayTime + ":00:00";
        Date re = null;
        try {
            re = df.parse(reStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 当前日期 + 整点时间
     *
     * @return yyyy-MM-dd格式的当前日期
     */
    public static String getFixedMinuteDate() {
        Date date = new Date();
        String format = daydf.format(date);
        String hourTime = hh.format(date);
        String minute = mm.format(date);
        return format + " " + hourTime + ":"+minute +":00";
    }

    public static String getFixedMinuteDate2(){
        LocalDateTime now = LocalDateTime.now().withSecond(0).withNano(0);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        return formattedDateTime;
    }

    /**
     * 整小时开始
     * @param date
     * @return
     */
    public static Date milliSecondFormat(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }


    //===========业务格式化============

    /**
     * 获取时间所在的5min的起始格式化字符串 00,05,10,15,20,25,30,35,40,45,50,55
     * 比如：2023-04-23 10:24:12 格式化后：2023-04-23 10:20:00
     * 比如：2023-04-23 10:20:12 格式化后：2023-04-23 10:20:00
     * 比如：2023-04-23 10:19:12 格式化后：2023-04-23 10:15:00
     * @param time
     * @return
     */
    public static String getSectionOfFiveMinute(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = null;
        try {
            now = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        int min = c.get(Calendar.MINUTE);
        int section_min = (min/5)*5;
        c.set(Calendar.MINUTE, section_min);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return sdf.format(c.getTime());
    }

    public static Long getTimeFromDateStr(String dateStr){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 日期格式

        try {
            Date date = formatter.parse(dateStr); // 解析日期字符串
            long timestamp = date.getTime(); // 获取时间戳
            return timestamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static Date getDateFromTimeStamp(long timestamp){
        Instant instant = Instant.ofEpochMilli(timestamp);
        Date date = Date.from(instant);
        return date;
    }



}
