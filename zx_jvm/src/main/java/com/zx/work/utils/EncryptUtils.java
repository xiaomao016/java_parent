package com.zx.work.utils;


import org.apache.commons.codec.digest.DigestUtils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;

/**
 * @author zhangxu
 * @date 2021/8/13 0013
 */
public class EncryptUtils {

    private static final String CHARS_ALL = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final String CHARS_NUM = "0123456789";

    /**
     * 获取uuid: 它保证对在同一时空中的所有机器都是唯一的，用到太网卡地址、纳秒级时间、芯片ID码
     */
    public static String genUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * 获取长度为num的随机数
     * @param num 生成的字符串长度
     */
    public static String genRandomNUM(final int num) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= num; i++) {
            stringBuilder.append(CHARS_NUM.charAt(new Random().nextInt(CHARS_NUM.length())));
        }
        return stringBuilder.toString();
    }

    /**
     * @author zhangxu
     * @date 2021/8/13 0013
     * 生成模板编码
     */
    public static String genTemplateEncode(){
        return "GWMB"+genRandomNUM(8);
    }

    /**
     * 重设密码-如有需要:方便测试
     * @author zhangxu
     * @date 2021/8/25 0025
     */
    public static String md5Password(String strSrc) {
        try {
            char hexChars[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
                    '9', 'a', 'b', 'c', 'd', 'e', 'f' };
            byte[] bytes = strSrc.getBytes();
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(bytes);
            bytes = md.digest();
            int j = bytes.length;
            char[] chars = new char[j * 2];
            int k = 0;
            for (int i = 0; i < bytes.length; i++) {
                byte b = bytes[i];
                chars[k++] = hexChars[b >>> 4 & 0xf];
                chars[k++] = hexChars[b & 0xf];
            }
            return new String(chars);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("MD5加密出错！！+" + e);
        }
    }

    public static String md5(String data){
        return DigestUtils.md5Hex(data);
    }

    public static String getMD5SubString(String str) {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(str.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            //一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方）
            String md5Result =  new BigInteger(1, md.digest()).toString(16);
            return md5Result.substring(md5Result.length()-8);
        } catch (Exception e) {
            return null;
        }
    }

}
