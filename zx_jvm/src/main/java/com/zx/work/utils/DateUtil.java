package com.zx.work.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author zx
 * @date 2021/8/25 0025
 *
 */
public class DateUtil {

    private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";


    public static String getSysYear() {
        Calendar date = Calendar.getInstance();
        String year = String.valueOf(date.get(Calendar.YEAR));
        return year;
    }


    /**
     * 将Date转换为LocalDate
     * @return java.time.LocalDate
     */
    public static LocalDate date2LocalDate(Date date) {
        Instant instant = date.toInstant();
        ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
        LocalDate localDate = zdt.toLocalDate();
        return localDate;
    }

    /**
     * 将LocalDate转换为Date
     * @return java.util.Date
     */
    public static Date localDate2Date(LocalDate localDate) {
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        Instant instant1 = zonedDateTime.toInstant();
        Date from = Date.from(instant1);
        return from;
    }


    /**
     * 返回当前日期的LocalDate
     * @return java.time.LocalDate
     */
    public static LocalDate currentDate(){
        return LocalDate.now();
    }

    /**
     * 返回当前日期字符串（格式化表达式：yyyy-MM-dd）
     * @return java.lang.String
     */
    public static String currentDateStr(){
        return toDateStr(currentDate());
    }

    /**
     * 返回当前日期字符串（格式化表达式：yyyy-MM-dd）
     * @return java.lang.String
     */
    public static String currentDateStr(String pattern){
        DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime time = LocalDateTime.now();
        return df.format(time);
    }

    /**
     * 格式化LocalDate
     * @param date LocalDate
     * @return java.lang.String
     */
    public static String toDateStr(LocalDate date){
        return toDateStr(date, DEFAULT_DATE_FORMAT);
    }

    /**
     * 格式化LocalDate
     * @param date LocalDate
     * @param pattern 指定日期格式化表达式
     * @return java.lang.String
     */
    public static String toDateStr(LocalDate date, String pattern){
        return date.format(DateTimeFormatter.ofPattern(pattern, Locale.SIMPLIFIED_CHINESE));
    }

    /**
     * 小写的hh采用12小时制，大写的HH采用24小时制
     * @param dateTime
     * @param pattern
     * @return
     */
    public static String toDateTimeStr(LocalDateTime dateTime, String pattern){
        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String re = dtf2.format(dateTime);
        return re;
    }

    public static String getDateFormatHH(Date date){
        DateFormat hh = new SimpleDateFormat("HH");
        return hh.format(date);
    }

    public static String getDateFormatMMdd(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MMdd");
        return format.format(date);
    }
    public static String getDateFormatmm(Date date){
        DateFormat mm = new SimpleDateFormat("mm");
        return mm.format(date);
    }
    public static String getDateFormatyyyyMMdd(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        return format.format(date);
    }


    /**
     * 返回几天之后的时间（精确到秒的时间戳）
     * @param days 天数
     * @param zoneOffset 时区，不填默认为+8
     * @return java.lang.Long
     */
    public static Long nextDaysSecond(Long days, ZoneOffset zoneOffset){
        LocalDateTime dateTime = nextDays(days);

        if(zoneOffset == null){
            return dateTime.toEpochSecond(ZoneOffset.ofHours(8));
        }else{
            return dateTime.toEpochSecond(zoneOffset);
        }
    }

    /**
     * 将天数转化为秒数
     * @param days 天数
     * @return java.lang.Integer
     */
    public static Long dayToSecond(Long days){
        return days * 86400;
    }

    /**
     * 返回几天之后的时间
     * @param days 天数
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime nextDays(Long days){
        return now().plusDays(days);
    }

    /**
     * 返回当前的LocalDateTime
     * @return java.time.LocalDateTime
     */
    public static LocalDateTime now(){
        return LocalDateTime.now();
    }



    public static Date addSeconds(Date date, int seconds) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(13, seconds);
        return c.getTime();
    }

    public static Date addMinutes(Date date, int minutes) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(12, minutes);
        return c.getTime();
    }

    public static Date addHours(Date date, int hours) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(11, hours);
        return c.getTime();
    }

    public static Date addDays(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(5, days);
        return c.getTime();
    }

    public static Date addMonths(Date date, int months) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(2, months);
        return c.getTime();
    }
}
