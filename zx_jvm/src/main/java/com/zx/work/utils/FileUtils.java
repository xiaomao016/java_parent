package com.zx.work.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author zx
 * @description:
 * @date 2022/4/19
 */
public class FileUtils {

    /**
     * 递归删除文件目录及文件
     *
     * @param path
     */
    public static void delDirRecursion(String path){
        File dir=new File(path);
        if(dir.exists()){
            File[] tmp=dir.listFiles();
            //如果是空文件夹或者文件，直接删除即可
            if(tmp==null || tmp.length==0){
                dir.delete();
                return ;
            }
            for(int i=0; i<tmp.length; i++){
                if(tmp[i].isDirectory()){
                    delDirRecursion(path+"/"+tmp[i].getName());
                }else{
                    tmp[i].delete();
                }
            }
            dir.delete();
        }
    }


    public static void createDirectoryIfNotExist(String path) {
        try {
            Path p = Paths.get(path);
            Files.createDirectories(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createFileIfNotExist(String filePath) {
        try {
            Path targetFile = Paths.get(filePath);
            if (!Files.exists(targetFile)) {
                Files.createFile(targetFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isExistFile(String filePath){
        try {
            Path targetFile = Paths.get(filePath);
            if (!Files.exists(targetFile)) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
