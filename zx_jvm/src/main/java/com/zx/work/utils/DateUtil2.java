package com.zx.work.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil2 {

    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat daydf = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat dayNumberdf = new SimpleDateFormat("dd");
    private static final DateFormat hhmmss = new SimpleDateFormat("HH:mm:ss");
    private static final DateFormat hh = new SimpleDateFormat("HH");
    private static final DateFormat yyyymmddhhmmss = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final DateFormat yyyymmdd = new SimpleDateFormat("yyyyMMdd");


    public static Date getDateFromLong(Long value) {
        return new Date(value);
    }

    /**
     * 格式化字符串为日期
     *
     * @param time 字符串格式为 yyyy-MM-dd HH:mm:ss
     * @return 日期 Date对象
     */
    public static Date formatTime(String time) {
        try {
            return df.parse(time);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        }
    }

    /**
     * 格式化字符串为日期
     *
     * @param day 字符串格式为yyyy-MM-dd
     * @return 日期 Date对象
     */
    public static Date formatDayTime(String day) {
        try {
            return daydf.parse(day);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        }
    }


    /**
     * 格式字符串为某年某月的第一天。
     *
     * @param yearmonth 格式为2008-10
     * @return 某年某月的第一天
     */
    public static Date formatMonthTime(String yearmonth) {
        try {
            return daydf.parse(yearmonth + "-01");
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date();
        }
    }

    /**
     * 返回自1970年1月1日00:00:00GMT以来此日期对象表示的毫秒数
     *
     * @param str 格式为yyyy-MM-dd
     */
    public static long parseDayByYYYYMMDD(String str) {
        try {
            return daydf.parse(str).getTime();
        } catch (Exception ex) {
            return 0L;
        }
    }

    /**
     * 返回自1970年1月1日00:00:00GMT以来此时间对象表示的秒数
     *
     * @param str 格式为yyyy-MM-dd HH:mm:ss
     */
    public static int parseTimeByYYYYMMDDHHMMSS(String str) {
        if (str == null || str.length() != 19) {
            return 0;
        }
        try {
            return (int) (df.parse(str).getTime() / 1000L);
        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     * 得到 yyyy-MM-dd 格式的指定日期的前一天
     */
    public static String foreDay(String day) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(parseDayByYYYYMMDD(day));
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return daydf.format(cal.getTime());
    }

    /**
     * 根据时间值构造日期
     */
    public static String parseDay(int time) {
        return daydf.format(new Date(time * 1000L));
    }

    /**
     * 显示时间
     *
     * @param millseconds 毫秒数
     * @return 时间显示
     */
    public static String displayTime(long millseconds) {
        if (millseconds < 1000) {
            return millseconds + " 毫秒";
        }
        int seconds = (int) (millseconds / 1000);
        if (seconds < 60) {
            return seconds + " 秒";
        }
        if (seconds < 60 * 60) {
            return seconds / 60 + "分" + seconds % 60 + "秒";
        }
        int m = seconds - (seconds / 3600) * 3600;
        if (seconds < 24 * 60 * 60) {
            return seconds / 3600 + "小时" + m / 60 + "分" + m % 60 + "秒";
        }
        return millseconds + " 毫秒";
    }


    /**
     * 转换成yyyy-MM-dd格式的日期字符串
     *
     * @param d Date对象
     */
    public static String formatDay(Date d) {
        return daydf.format(d);
    }

    public static String formatDayTime(Date d) {
        return yyyymmddhhmmss.format(d);
    }
    /**
     * 转换成yyyy-MM-dd格式的日期字符串
     *
     * @param d Calendar对象
     */
    public static String formatDay(Calendar d) {
        return daydf.format(d.getTime());
    }

    /**
     * 两个日期相减，返回差值（单位天）
     *
     * @param date1 被减数
     * @param date2 减数
     * @return
     */
    public static int subDate1ToDate2(String date1, String date2) {
        int days = 0;
        try {
            Date d1 = formatDayTime(date1);
            Date d2 = formatDayTime(date2);
            long result = (d1.getTime() - d2.getTime()) / (1000 * 24 * 60 * 60);
            days = (int) result;
        } catch (Exception e) {
            return days;
        }
        return days;
    }



    /**
     * 两个日期相减，返回差值（单位秒）
     *
     * @param date1 被减数
     * @param date2 减数
     * @return
     */
    public static Long subDate1ToDate2(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return 0L;
        }
        long result = (date1.getTime() - date2.getTime()) / 1000;
        if (result < 0) {
            result = Math.abs(result);
        }
        return result;
    }


    /**
     * 转换成yyyy-MM-dd HH:mm:ss格式的时间
     *
     * @param time 毫秒数
     */
    public static String formatyyyyMMddHHmmss(long time) {
        return df.format(new Date(time));
    }

    /**
     * 将时间转换成yyyy-MM-dd HH:mm:ss的格式字符串。
     *
     * @param time 时间对象
     * @return 格式化后的字符串, 当输入为null时输出为""
     */
    public static String formatyyyyMMddHHmmss(Date time) {
        if (time == null) {
            return "";
        }
        try {
            return df.format(time);
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * 当前日期
     *
     * @return yyyy-MM-dd格式的当前日期
     */
    public static String today() {
        return daydf.format(new Date());
    }

    /**
     * 当前日期 + 整点时间
     *
     * @return yyyy-MM-dd格式的当前日期
     */
    public static String nowDate() {
        String format = daydf.format(new Date());
        String todayTime = hh.format(new Date());
        return format + " " + todayTime + "-00-00";
    }

    /**
     * 当前日期 + 整点时间
     *
     * @return yyyy-MM-dd格式的当前日期
     */
    public static String nowDate2() {
        String format = daydf.format(new Date());
        String todayTime = hh.format(new Date());
        return format + " " + todayTime + ":00:00";
    }


    /**
     * 当前日期的前一天
     *
     * @return 当前日期的前一天
     */
    public static String yesterday() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return daydf.format(cal.getTime());
    }

    /**
     * 当前日期的前一天
     *
     * @return 当前日期的前一天
     */
    public static String yesterdayyyymmdd() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return yyyymmdd.format(cal.getTime());
    }

    //当前时间的前一个小时
    public static String yesterdayyyymmdd(String dateTime) throws ParseException {
        Calendar cal = Calendar.getInstance();
        long time = df.parse(dateTime).getTime();
        cal.setTimeInMillis(time);
        cal.add(Calendar.HOUR_OF_DAY, -1);
        return df.format(cal.getTime());
    }





    /**
     * 当前日期的下一天
     *
     * @return 当前日期的下一天
     */
    public static String tomorrow() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        return daydf.format(cal.getTime());
    }

    /**
     * 返回本月1号
     *
     * @return 返回本月1号
     */
    public static String currmonth1day() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return daydf.format(cal.getTime());
    }

    /**
     * 返回本周第一天日期
     *
     * @return
     */
    public static String currweek1day() {
        Calendar cal = Calendar.getInstance();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int mondayPlus = 0;
        if (dayOfWeek != 1) {
            mondayPlus = 1 - dayOfWeek;
        }
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(GregorianCalendar.DATE, mondayPlus);
        return daydf.format(currentDate.getTime());
    }

    /**
     * 返回当前日期周的第一天
     *
     * @return
     */
    public static String currweek1day(String day) {
        Calendar cal = Calendar.getInstance();

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int mondayPlus = 0;
        if (dayOfWeek != 1) {
            mondayPlus = 1 - dayOfWeek;
        }
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(GregorianCalendar.DATE, mondayPlus);
        return daydf.format(currentDate.getTime());
    }

    /**
     * 返回本月最后一天
     */
    public static String lastdayofmonth() {
        Calendar calendar = Calendar.getInstance();
        Calendar cpcalendar = (Calendar) calendar.clone();
        cpcalendar.set(Calendar.DAY_OF_MONTH, 1);
        cpcalendar.add(Calendar.MONTH, 1);
        cpcalendar.add(Calendar.DATE, -1);
        String date = daydf.format(new Date(cpcalendar.getTimeInMillis()));
        return date;
    }

    /**
     * 返回本年第一天
     */
    public static String firstdayofyear() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String year = sdf.format(new Date());
        return year + "-01-01";
    }

    /**
     * 返回本年最后一天
     */
    public static String lastdayofyear() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String year = sdf.format(new Date());
        return year + "-12-31";
    }

    /**
     * 给指定时间加上一个数值
     *
     * @param time1   要加上一数值的时间，为null即为当前时间，格式为yyyy-MM-dd HH:mm:ss
     * @param addpart 要加的部分：年月日时分秒分别为：YMDHFS
     * @param num     要加的数值
     * @return 新时间，格式为yyyy-MM-dd HH:mm:ss
     */
    public static String addTime(String time1, String addpart, int num) {
        try {
            String now = df.format(new Date());
            time1 = (time1 == null) ? now : time1;
            if (time1.length() < 19) {
                time1 += " 00:00:00";
            }
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(df.parse(time1));
            if (addpart.equalsIgnoreCase("Y")) {
                cal.add(Calendar.YEAR, num);
            } else if (addpart.equalsIgnoreCase("M")) {
                cal.add(Calendar.MONTH, num);
            } else if (addpart.equalsIgnoreCase("D")) {
                cal.add(Calendar.DATE, num);
            } else if (addpart.equalsIgnoreCase("H")) {
                cal.add(Calendar.HOUR, num);
            } else if (addpart.equalsIgnoreCase("F")) {
                cal.add(Calendar.MINUTE, num);
            } else if (addpart.equalsIgnoreCase("S")) {
                cal.add(Calendar.SECOND, num);
            }
            return df.format(cal.getTime());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 给指定日期加上一个数值
     *
     * @param date1   要加上一数值的日期，为null即为当前日期，格式为yyyy-MM-dd
     * @param addpart 要加的部分：年月日分别为：YMD
     * @param num     要加的数值
     * @return 新日期，格式为yyyy-MM-dd
     */
    public static String addDate(String date1, String addpart, int num) {
        return addTime(date1, addpart, num).substring(0, 10);
    }

    /**
     * 获取当月第一天的日期
     *
     * @return 当月第一天的日期，格式为yyyy-MM-dd
     */
    public static String getMonthFirstDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar
                .getActualMinimum(Calendar.DAY_OF_MONTH));

        return daydf.format(calendar.getTime());
    }

    /**
     * 当前日期
     *
     * @return yyyy-MM-dd HH:mm:ss格式的当前日期
     */
    public static String now() {
        return df.format(new Date());
    }

    /**
     * 当前时间前一天的 yyyy-MM-dd HH:mm:ss格式的
     *
     * @return
     */
    public static String getBeforeOneDay() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        return df.format(date);
    }

    public static String getBeforeDay3() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -3);
        date = calendar.getTime();
        return df.format(date);
    }

    public static String getDayNumber() {
        String todayStr = dayNumberdf.format(new Date());
        return todayStr;
    }


    /**
     * 返回时间HH:mm:ss
     *
     * @return
     */
    public static String getDayTime() {
        String todayTime = hhmmss.format(new Date());
        return todayTime;
    }

    /**
     * 返回当前时间
     */
    public static String timeofnow() {
        Calendar curcal = Calendar.getInstance();
        return df.format(curcal.getTime());
    }

    /**
     * 返回yyyyMMddHHmmss格式的当前时间
     */
    public static String othertimeofnow() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar curcal = Calendar.getInstance();
        return sdf.format(curcal.getTime());
    }


    public static String getDateStr(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = new Date(date);
        String dateStr = format.format(newDate);
        return dateStr;
    }


    public static Long getGMT0DateLong24() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("Etc/GMT+0"));
        Date newDate = new Date(System.currentTimeMillis());
        String dateStr = format.format(newDate);
        long nowDateLong = format.parse(dateStr).getTime();
        return nowDateLong;
    }

    public static Long getGMT0DateLong12() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("Etc/GMT+0"));
        Date newDate = new Date(System.currentTimeMillis());
        String dateStr = format.format(newDate);
        long nowDateLong = format.parse(dateStr).getTime();
        return nowDateLong;
    }

    public static boolean isCurrentGMT0Match(Long timestamp) throws ParseException {
        //24小时制判断
        long nowDate = System.currentTimeMillis();
        long apiDate = timestamp;
        if ((nowDate - apiDate) < (60000 * 60 * 24 * 30l) && (nowDate - apiDate) >= -(60000 * 60 * 24 * 30l)) {
            return true;
        }
        return false;
    }


    // 获得本周一0点时间
    public static String getTimesWeekmorning() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return df.format(cal.getTime());
    }

    public static String getWeekOfMonday() {  //1  获得这周周一的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    public static String getWeekOfTuesday() { //2  获得这周周二的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    public static String getWeekOfWednesday() { //3  获得这周周三的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    public static String getWeekOfThursday() { //4   获得这周周四的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    public static String getWeekOfFriday() { //5   获得这周周五的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    public static String getWeekOfSaturday() { //6  获得这周周六的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    public static String getWeekOfSunday() { // 日        一个周的第一天      获得这周周天的时间
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        String moday = df.format(date);
        return moday;
    }

    /**
     * @return 返回1是星期日、2是星期一、3是星期二、4是星期三、5是星期四、6是星期五、7是星期六
     */
    public static int getWeekOfDate() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    public static Date getNowTime() {
        SimpleDateFormat timedf = new SimpleDateFormat("HH:mm:ss");
        String nowTiemStr = timedf.format(new Date());
        Date nowTime = null;
        try {
            nowTime = timedf.parse(nowTiemStr);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return nowTime;
    }

    public static List<String> getMouthList() {
        List<String> mouthList = new ArrayList<>();
        for (int i = 1; i <= 30; i++) {
            String dateStr = "2019-09-";
            if (i < 10) {
                dateStr = dateStr + "0" + i;
            } else {
                dateStr = dateStr + i;
            }
            mouthList.add(dateStr);
        }
        return mouthList;
    }

    //获得一个月之前的时间
    public static String getBeforOneMonthDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        Date m = c.getTime();
        String mon = df.format(m);
        return mon;
    }




    //返回距离0点的秒数
    public static Long getSecondsToZero() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Calendar curCal = Calendar.getInstance();

        long calInMillis = cal.getTimeInMillis();
        long diff = calInMillis - curCal.getTimeInMillis();
        long diffSeconds = diff / 1000;
        return diffSeconds;
    }

    //计算该时间所在天零点的时间戳
    public static Long getZeroPointTimeMillis(Long cur) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    //计算该时间n天前的时间戳
    public static Long getDayTimeBefore(Long cur,int n) {
        return cur - n*24*60*60*1000;
    }



}
