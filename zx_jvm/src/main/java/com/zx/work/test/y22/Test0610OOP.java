package com.zx.work.test.y22;

import com.zx.work.test.mc.ArithmeticOperatorEnum;
import com.zx.work.test.mc.LogicalOperatorEnum;

/**
 * @author zx
 * @description:
 * @date 2022/6/10
 */
public class Test0610OOP {

    public static void main(String[] args) {
        Domain bean  = new Domain();
        bean.setArithmeticOperatorEnum(ArithmeticOperatorEnum.LT);
//        bean.setLogicalOperatorEnum(LogicalOperatorEnum.);
        ArithmeticOperatorEnum arithmeticOperator = bean.getArithmeticOperatorEnum();
        LogicalOperatorEnum logicalOperator = bean.getLogicalOperatorEnum();
        //处理第一个子条件
        if (logicalOperator == null) {
            //算术运算符取反
            if (arithmeticOperator == ArithmeticOperatorEnum.LT) {
                arithmeticOperator = ArithmeticOperatorEnum.GE;
            } else if (arithmeticOperator == ArithmeticOperatorEnum.GR) {
                arithmeticOperator = ArithmeticOperatorEnum.LE;
            } else if (arithmeticOperator == ArithmeticOperatorEnum.EQ) {
                arithmeticOperator = ArithmeticOperatorEnum.NE;
            } else if (arithmeticOperator == ArithmeticOperatorEnum.NE) {
                arithmeticOperator = ArithmeticOperatorEnum.EQ;
            }
            bean.setArithmeticOperatorEnum(arithmeticOperator);
        }

        System.out.println(bean.getArithmeticOperatorEnum().getValue());
    }

}
