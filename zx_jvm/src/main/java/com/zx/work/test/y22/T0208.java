package com.zx.work.test.y22;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/2/8 0008 13:52
 * @description:
 */
public class T0208 {
    public static void main(String[] args) {

        String[] names = new String[]{"aa","bb","cc","dd","ee"};
        List<Stu> list = new ArrayList<>();
        for(int i=0;i<5;++i){
            Stu s ;
            s = new Stu(names[i]);
            list.add(s);
        }

        for(Stu s:list){
            System.out.println(s.getName());
        }

        String s1 = "002";
        String s2 = "001";
        int a = s1.compareTo(s2);
        System.out.println(a);
    }
}

class Stu {
    private String name;

    Stu(String name){
        this.name = name;
    }

    String getName(){
        return this.name;
    }
}
