package com.zx.work.test.ctit;

public class TestDoubleFormat {
    public static void main(String[] args) {
        double value = 3.141592653589793;
        double roundedValue = roundToNDecimalPlaces(value, 3);

        System.out.println(roundedValue); // 输出: 3.142
    }

    public static double roundToNDecimalPlaces(double value, int decimalPlaces) {
        double scale = Math.pow(10, decimalPlaces);
        System.out.println(scale);
        return Math.round(value * scale) / scale;
    }
}
