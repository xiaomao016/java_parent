package com.zx.work.test.string;

import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;

/**
 * @author zx
 * @description:
 * @date 2022/9/15
 */
public class T0915 {
    public static void main(String[] args) {
        System.out.println(getSizeOfMB("3.2G"));
        System.out.println(getSizeOfMB("494M"));
        System.out.println(getSizeOfMB("123K"));
        String fileName = "/opt/oss_temp_device/middle/android/RU/output__0";
        String cmdFormat = String.format("du -sh %s | awk '{print$1}'",fileName);
        System.out.println(cmdFormat);
//        double s = 1024.5;
//        int spits = 1;
//        spits = (int) (s/100);
//        System.out.println(spits);


    }

    public static double getSizeOfMB(String s) {
        if(StringUtils.isEmpty(s)){
            return 0L;
        }
        String suffix = "K";
        float factor = 0.001f;
        if(s.endsWith("M")){
            suffix = "M";
            factor = 1f;
        }else if(s.endsWith("G")){
            suffix = "G";
            factor = 1024f;
        }
        String n = s.substring(0,s.indexOf(suffix));
        return Double.valueOf(n) * factor;
    }
}
