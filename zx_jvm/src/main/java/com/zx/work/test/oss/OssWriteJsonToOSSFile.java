package com.zx.work.test.oss;

import com.alibaba.fastjson.JSONObject;
import com.zx.design.iterator.demo.ArrayList_;

import java.util.ArrayList;
import java.util.List;

public class OssWriteJsonToOSSFile {
    public static void main(String[] args) {

        OSSClientUtils ossClientUtils = new OSSClientUtils();
        ossClientUtils.init();
        List<String> listJson = new ArrayList<>();
        for(int i=0;i<10;++i){
            JSONObject obj = new JSONObject();
            obj.put("event_name","install");
            obj.put("country","BR");
            obj.put("gaid","afaf-sfag");
            listJson.add(obj.toJSONString()+"\n");
        }
        String objName = "test/josn.txt";
        ossClientUtils.appendJsonToOSSFile(objName,listJson);

    }
}
