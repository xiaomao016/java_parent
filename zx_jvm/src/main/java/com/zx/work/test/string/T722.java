package com.zx.work.test.string;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author zx
 * @description:
 * @date 2022/7/22
 */
public class T722 {
    public static void main(String[] args) {
        String s = "1547148457271967746";
        String s2 = "";
        System.out.println(s.length());
        boolean re = NumberUtils.isParsable(s);
        System.out.println(re);
    }
}
