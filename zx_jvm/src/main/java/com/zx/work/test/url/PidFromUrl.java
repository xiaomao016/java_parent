package com.zx.work.test.url;

import com.google.common.base.Strings;

public class PidFromUrl {


    public static void main(String[] args) {
        //     "preview_url":"https://apps.apple.com/us/app/id957122272",
        String preView = "https://apps.apple.com/us/app/id957122272";
        System.out.println(getPkgName(preView));
    }

    public static String getPkgName(String previewLink) {
        String pkgName = null;
        if (!Strings.isNullOrEmpty(previewLink)) {
            try {
                if (previewLink.indexOf("/id") > 0) {
                    pkgName = previewLink.substring(previewLink.indexOf("/id") + 3, previewLink.indexOf("?") > 0 ? previewLink.indexOf("?") : previewLink.length());
                    if (pkgName.indexOf("/id") != -1) {// 防止preview_link有/id/../id123这种
                        pkgName = pkgName.substring(pkgName.indexOf("/id") + 3);
                    }
                } else if (previewLink.indexOf("id=") > 0) {
                    pkgName = previewLink.substring(previewLink.indexOf("id=") + 3);
                }else if(previewLink.indexOf("-id") > 0){
                    pkgName = previewLink.substring(previewLink.indexOf("-id") + 3);
                }
                if (!Strings.isNullOrEmpty(pkgName) && pkgName.indexOf("&") >= 0) {
                    pkgName = pkgName.substring(0, pkgName.indexOf("&"));
                }
                if (!Strings.isNullOrEmpty(pkgName) && pkgName.indexOf("?") >= 0) {
                    pkgName = pkgName.substring(0, pkgName.indexOf("?"));
                }
            } catch (Exception e) {
            }
        }
        return pkgName;
    }
}
