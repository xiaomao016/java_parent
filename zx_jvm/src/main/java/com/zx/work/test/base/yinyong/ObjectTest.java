package com.zx.work.test.base.yinyong;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

public class ObjectTest {
    public static void main(String[] args) {
        //================1================
//        HQueryCondition hQueryCondition = new HQueryCondition();
//        hQueryCondition.setWhereList(getWheres());
//        System.out.println(JSON.toJSONString(hQueryCondition));
//        //替换
//        List<HWhereObject> list = hQueryCondition.getWhereList();
//        HWhereObject firstParams = list.get(0);
//        List<TagVO> childList = firstParams.getWhereChildList();
//        TagVO eventTag =  TagVO.builder()
//                .tagName("eventName")
//                .tagValue("install")
//                .arithmeticOperator("=")
//                .logicalOperator("AND")
//                .build();
//        childList.add(eventTag);
//        System.out.println(JSON.toJSONString(hQueryCondition));
        System.out.println("===========");
        McUpload mc = new McUpload();
        String tag = "{\"country\":\"AE\",\"dataName\":\"AE_IOS_定时更新\",\"dataSourceType\":\"0\",\"homologous\":true,\"isDataLack\":\"0\",\"mcSearchResultId\":1633712150423457794,\"os\":\"ios\",\"time\":1,\"unit\":\"hour\",\"whereList\":[{\"logicalOperator\":\"AND\",\"whereChildList\":[{\"arithmeticOperator\":\"=\",\"tagName\":\"source\",\"tagValue\":\"appsflyer\"}]}],\"whereLists\":[{\"operatorEnum\":\"AND\",\"whereBeanList\":[{\"arithmeticOperatorEnum\":\"EQ\",\"tag\":\"source\",\"tagValue\":\"appsflyer\"}]}]}";
        mc.setTags(tag);
        mc.setDataSourceType("1");
        List<McUpload> taskList = Lists.newArrayList(mc);
        System.out.println(JSON.toJSONString(taskList.get(0)));
        taskList.stream().forEach(m->{
            if(mc.getDataSourceType().equalsIgnoreCase("1")){
                HQueryCondition hQueryCondition = JSONObject.parseObject(mc.getTags(), HQueryCondition.class);
                HWhereObject f1 = hQueryCondition.getWhereList().get(0);
                List<TagVO> childList = f1.getWhereChildList();
                TagVO eventTag = new TagVO();
                eventTag.setTagName("eventName");
                eventTag.setTagValue("install");
                eventTag.setArithmeticOperator("=");
                if(childList.size()>0) {
                    eventTag.setLogicalOperator("AND");
                }
                childList.add(eventTag);
                mc.setTags(JSON.toJSONString(hQueryCondition));
            }
        });

        System.out.println(JSON.toJSONString(taskList.get(0)));


    }

    public static List<TagVO> getTags(){
        List<TagVO> vos = new ArrayList<>();
        TagVO vo1 = TagVO.builder()
                .arithmeticOperator("=")
                .tagName("source")
                .tagValue("appsflyer")
                .build();

        TagVO vo2 = TagVO.builder()
                .arithmeticOperator(">")
                .tagName("os_version")
                .tagValue("11")
                .logicalOperator("AND")
                .build();

        vos.add(vo1);
        vos.add(vo2);
        return vos;
    }

    public static List<HWhereObject> getWheres(){
        List<HWhereObject> whereObjects = new ArrayList<>();

        HWhereObject w1 = HWhereObject.builder()
                .logicalOperator("AND")
                .whereChildList(getTags())
                .build();

        whereObjects.add(w1);

        return whereObjects;
    }

}
