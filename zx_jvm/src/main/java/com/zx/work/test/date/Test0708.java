package com.zx.work.test.date;

import com.alibaba.fastjson.JSON;
import com.zx.work.utils.DateUtilFinal;

import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/7/8
 */
public class Test0708 {
    public static void main(String[] args) {

        String start = "2024-08-17 18:26:00";
        String end = "2024-08-19 10:00:00";

        Long startTimeStamp = DateUtilFinal.getTimeFromDateStr(start);
        Long endTimeStamp = DateUtilFinal.getTimeFromDateStr(start);




        Date date = DateUtilFinal.getDateFromTimeStamp(startTimeStamp);
        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(date));

    }

    public static Date getDateFromTimeStamp(long timestamp){
        Instant instant = Instant.ofEpochMilli(timestamp);
        Date date = Date.from(instant);
        return date;
    }
    public static String getDateYYYYMMDDFromTime(long timestamp){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd"); // 日期格式

        Date date = new Date(timestamp); // 将时间戳转换为Date对象
        String dateString = formatter.format(date); // 格式化Date对象为字符串
        return dateString;
    }
    public static Long getTimeFromYYYYMMDD(String dateStr){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // 日期格式

        try {
            Date date = formatter.parse(dateStr); // 解析日期字符串
            long timestamp = date.getTime(); // 获取时间戳
            return timestamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public static String getDateYYYYMMDDFromTime2(long timestamp){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss"); // 日期格式

        Date date = new Date(timestamp); // 将时间戳转换为Date对象
        String dateString = formatter.format(date); // 格式化Date对象为字符串
        return dateString;
    }

}
