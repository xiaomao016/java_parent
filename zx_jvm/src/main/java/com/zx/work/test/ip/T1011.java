package com.zx.work.test.ip;

import com.zx.work.test.ip.util.IPV4;

/**
 * @author zx
 * @description:
 * @date 2022/10/11
 */
public class T1011 {
    public static void main(String[] args) {

//        String ipv4 = "1.0.8.0/21";
//        String ipv4 = "172.168.16.10/25";
//        String ipv4 = "192.168.161.125/30";
//        String ipv4 = "103.4.156.0/22";
//        String ipv4 = "152.169.118.0/23";
        String ipv4 = "186.89.220.0/24";
//        String ipv4 = "213.219.240.0/21";

        System.out.println(RangeCal.getIPRange(ipv4)); //2896695306~2896695433
        //2896695297
        //2896695422

        IPV4 v = new IPV4(ipv4);
        System.out.println(v.getFirstIpNumeric());
        System.out.println(v.getLastIpNumeric());


    }
}
