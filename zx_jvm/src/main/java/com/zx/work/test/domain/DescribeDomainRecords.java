package com.zx.work.test.domain;


import com.alibaba.fastjson.JSON;
import com.aliyun.alidns20150109.models.DescribeDomainRecordsResponse;
import com.aliyun.tea.*;
/**
 * 获取域名解析记录
 */
public class DescribeDomainRecords {


        /**
         * 使用AK&SK初始化账号Client
         * @param accessKeyId
         * @param accessKeySecret
         * @return Client
         * @throws Exception
         */
        public static com.aliyun.alidns20150109.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
            com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                    // 必填，您的 AccessKey ID
                    .setAccessKeyId(accessKeyId)
                    // 必填，您的 AccessKey Secret
                    .setAccessKeySecret(accessKeySecret);
            // 访问的域名
            config.endpoint = "alidns.cn-hangzhou.aliyuncs.com";
            return new com.aliyun.alidns20150109.Client(config);
        }

        public static void main(String[] args_) throws Exception {
            java.util.List<String> args = java.util.Arrays.asList(args_);
            // 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html
            com.aliyun.alidns20150109.Client client = DescribeDomainRecords.createClient(Credentials.ACCESS_KEY_ID, Credentials.ACCESS_KEY_SECRET);
            com.aliyun.alidns20150109.models.DescribeDomainRecordsRequest describeDomainRecordsRequest = new com.aliyun.alidns20150109.models.DescribeDomainRecordsRequest()
                    .setLang("en")
                    .setDomainName("linaigood.com");
            com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
            try {
                // 复制代码运行请自行打印 API 的返回值
                DescribeDomainRecordsResponse reps = client.describeDomainRecordsWithOptions(describeDomainRecordsRequest, runtime);
                System.out.println(JSON.toJSONString(reps));
            } catch (TeaException error) {
                // 如有需要，请打印 error
                com.aliyun.teautil.Common.assertAsString(error.message);
                error.printStackTrace();
            } catch (Exception _error) {
                TeaException error = new TeaException(_error.getMessage(), _error);
                // 如有需要，请打印 error
                com.aliyun.teautil.Common.assertAsString(error.message);
                _error.printStackTrace();
            }
        }

}
