package com.zx.work.test.date;

import com.zx.juc.thinking.vo.D;
import com.zx.work.utils.DateUtil;
import com.zx.work.utils.DateUtil2;

import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/6/28
 */
public class T628 {

    public static void main(String[] args) {
        //30天前的Date,30天+1hour前的Date
        Date currentDate = new Date();
        Date de = DateUtil.addDays(currentDate,-30);
        System.out.println(DateUtil2.formatDayTime(de));

        Date ds = DateUtil.addHours(DateUtil.addDays(currentDate,-30),-1);
        System.out.println(DateUtil2.formatDayTime(ds));
    }
}
