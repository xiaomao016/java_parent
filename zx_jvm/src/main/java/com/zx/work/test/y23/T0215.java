package com.zx.work.test.y23;

import com.zx.work.utils.RandomWeightUtils;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class T0215 {


        public static void main(String[] args) {
            int clickWeight = 1;
            int impressionWeight = 10;
            AtomicInteger click = new AtomicInteger();
            AtomicInteger impression = new AtomicInteger();
            Random random = new Random();
            for (int i = 0; i < 1000000000; i++) {
                int index =  random.nextInt(clickWeight+impressionWeight);
                if (index==0) {
                    click.incrementAndGet();
                } else {
                    impression.incrementAndGet();
                }
            }
            int impressionCount = impression.get();
            int clickCount = click.get();
            log.info("clickCount:{},impression:{}", clickCount, impressionCount);
        }


}
