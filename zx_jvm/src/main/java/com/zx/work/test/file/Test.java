package com.zx.work.test.file;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.zx.work.utils.DateUtil;
import com.zx.work.utils.DateUtilFinal;
import com.zx.work.utils.EncryptUtils;
import jnr.ffi.annotations.In;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class Test {

    public static void main(String[] args) {
      try {
          Integer a = getINt();
          a.intValue();
      }catch (Exception e){
          log.error("save pb to db fail. error:",e.getMessage(), e);
//          log.error(e.getMessage(),e);
          throw new RuntimeException(e);
      }
    }

    private static Integer getINt() {
        return null;
    }

    public static String convertToCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '_') {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String getPreviousHourTime() {
        LocalDateTime now = LocalDateTime.now();  // 获取当前时间
        LocalDateTime previousHourTime = now.minusHours(1);  // 减去一个小时
        LocalDateTime formattedTime = previousHourTime.withMinute(0).withSecond(0);  // 设置分钟和秒设置为0
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  // 定义日期时间格式
        return formatter.format(formattedTime);  // 格式化时间
    }

    public static String getHourTime() {
        LocalDateTime now = LocalDateTime.now();  // 获取当前时间
        LocalDateTime formattedTime = now.withMinute(0).withSecond(0);  // 设置分钟和秒设置为0
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  // 定义日期时间格式
        return formatter.format(formattedTime);  // 格式化时间
    }

    public static int getTimeSub(){
        String dateStr1 = "2024-01-31 10:15:00";
        String dateStr2 = "2024-01-31 01:15:00";

        LocalDateTime dateTime1 = LocalDateTime.parse(dateStr1, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime dateTime2 = LocalDateTime.parse(dateStr2, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        long diffInMinutes = ChronoUnit.MINUTES.between(dateTime1, dateTime2);
        System.out.println("The difference in minutes is: " + diffInMinutes);
        return -1;
    }


    public static int minuteBetweenTowDate(String dateStr1,String dateStr2){
        LocalDateTime dateTime1 = LocalDateTime.parse(dateStr1, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime dateTime2 = LocalDateTime.parse(dateStr2, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        long diffInMinutes = ChronoUnit.MINUTES.between(dateTime1, dateTime2);

        return (int) Math.abs(diffInMinutes);
    }


    private static String buildArrayStringAlia(List<String> list){
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for(int i=0;i<list.size();++i){
            sb.append("\"");
            sb.append(list.get(i));
            sb.append("\"");
            if(i<(list.size()-1)){
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    public static Date getNowTime() {
        SimpleDateFormat timedf = new SimpleDateFormat("HH:mm:ss");
        String nowTiemStr = timedf.format(new Date());
        System.out.println(nowTiemStr);
        Date nowTime = null;
        try {
            nowTime = timedf.parse(nowTiemStr);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return nowTime;
    }

    public static void loadCodeMap() {
        try {
            InputStream inputStream = com.zx.work.test.y23.Test.class.getResourceAsStream("/source.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            Map<String,Integer> cm = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                // 按逗号分割每一行内容
                String nl = line.replace("{","").replace("}","");
                String[] apps = nl.split(",");
                for(String app:apps) {
                    // 使用分割后的内容
                    if (cm.containsKey(app)) {
                        // 如果存在，则将对应的值加一
                        cm.put(app, cm.get(app) + 1);
                    } else {
                        // 如果不存在，则将其作为一个新的键，并将对应的值设为1
                        cm.put(app, 1);
                    }
                }
            }

            List<Map.Entry<String, Integer>> entryList = new ArrayList<>(cm.entrySet());

            Collections.sort(entryList, new Comparator<Map.Entry<String, Integer>>() {
                public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
                    return entry2.getValue().compareTo(entry1.getValue()); // 降序排序
                }
            });

            // 打印排序后的结果
            for (Map.Entry<String, Integer> entry : entryList) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
