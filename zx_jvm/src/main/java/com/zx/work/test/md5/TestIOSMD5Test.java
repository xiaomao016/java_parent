package com.zx.work.test.md5;

import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.security.MessageDigest;

@Slf4j
public class TestIOSMD5Test {
    public static void main(String[] args) {
        //d57ee90237055a722eda1382f69a8de7
        String ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
        String ip = "72.89.234.0";

        String md = getMD5(ip+ua);
        log.info("md:{}",md);
        System.out.println(md.length());
        String sub = md.substring(md.length()-16);
        System.out.println(sub);
        String deviceId = "0ios" + md.substring(md.length() - 16);
        System.out.println(deviceId.length());
        System.out.println(deviceId.toUpperCase());
        System.out.println(sub.length());
    }

    public static String getMD5(String str) {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(str.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8位字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            //一个byte是八位二进制，也就是2位十六进制字符（2的8次方等于16的2次方）
            String md5Result =  new BigInteger(1, md.digest()).toString(16);
            return md5Result;
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            return null;
        }
    }
}
