package com.zx.work.test.staticField;

import java.util.HashMap;
import java.util.Map;

public class TestClickSpeed {
    public static void main(String[] args) {

        ClickSpeed c = new ClickSpeed();
        if(c.getIsAvg()!=null && c.getIsAvg()) {
            System.out.println(c.getIsAvg());
        }

        Map<String,Double> d = new HashMap<>();
    }

    public static boolean isPercentageAbnormal(Map<String, Double> hourPercentage) {
        //1. Whether all percentage sum up to 1
        //2. Whether more than half of percentage is 0
        int zero_count = 0;
        double total = 0.0d;
        for(double value:hourPercentage.values()){
            if(value<=0.0d){
                zero_count++;
            }
            total+=value;
        }

        if(zero_count>=12){
            return false;
        }

        if(Math.abs(total-1D)>=0.0001){
            return false;
        }
        return true;
    }
}
