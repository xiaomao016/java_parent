package com.zx.work.test.ip.ipgeo;

import com.maxmind.db.MaxMindDbConstructor;
import com.maxmind.db.MaxMindDbParameter;
import com.maxmind.db.Reader;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

public class IPGeoCountryTest {
    public static void main(String[] args) throws IOException {
        File database = new File("C:\\Users\\dell\\Desktop\\临时\\9.3\\na_35_ipv4.db");
//        File database = new File("static/asn.mmdb");
        try (Reader reader = new Reader(database)) {
            InetAddress address = InetAddress.getByName("51.195.253.93");

            // get() returns just the data for the associated record
            LookupResult result = reader.get(address, LookupResult.class);
            System.out.println(result.country); // Output → Google LLC
            System.out.println(result.country_name); // Output → Google LLC
            System.out.println(result.continent); // Output → Google LLC

        }
    }

    public static class LookupResult {
        private final String continent;
        private final String continent_name;
        private final String country;
        private final String country_name;

        @MaxMindDbConstructor
        public LookupResult(
                @MaxMindDbParameter(name = "continent") String continent,
                @MaxMindDbParameter(name = "continent_name") String continent_name,
                @MaxMindDbParameter(name = "country") String country,
                @MaxMindDbParameter(name = "country_name") String country_name
        ){
            this.continent = continent;
            this.continent_name = continent_name;
            this.country = country;
            this.country_name = country_name;
        }
    }
}


