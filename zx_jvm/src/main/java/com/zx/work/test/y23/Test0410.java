package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.Collectors;

public class Test0410 {
    public static void main(String[] args) {


        List<Long> offerIdsNow = Lists.newArrayList(12l,33l,10394l,98239334L);

        List<Long> triggerTwiceIds = Lists.newArrayList();
        List<Long> lastReplaceOffers = Lists.newArrayList(12l,88l,3294L,943l,98239334L);
        lastReplaceOffers.stream().forEach(o->{
            if(offerIdsNow.contains(o)){
                triggerTwiceIds.add(o);
            }
        });

        triggerTwiceIds.stream().map(o->o+"aaa").collect(Collectors.toList());

        System.out.println(JSON.toJSONString(triggerTwiceIds));
        triggerTwiceIds.clear();
        System.out.println(triggerTwiceIds.size());
        System.out.println(JSON.toJSONString(triggerTwiceIds));
    }
}
