package com.zx.work.test.base.yinyong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HWhereObject {

    private String logicalOperator;

    private List<TagVO> whereChildList;

}
