package com.zx.work.test.ua;

import com.alibaba.fastjson.JSON;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * DeviceClass = Phone
 * DeviceName = SM-A215U
 * DeviceBrand = Samsung
 * OperatingSystemName = Android
 * OperatingSystemVersion = 11
 * OperatingSystemNameVersion = Android 11
 * OperatingSystemNameVersionMajor = Android 11
 * AgentName = Chrome Webview
 * AgentVersion = 87.0.4280.141
 * AgentNameVersion = Chrome Webview 87.0.4280.141
 * AgentInformationEmail = Unknown
 * AgentInformationUrl = Unknown
 *
 * 参考链接：
 * https://yauaa.basjes.nl/using/
 */
public class UaParseTest {

    //指定特定的字段可以增加解析速度
    //如果是在java8中使用，为了避免未知错误，在依赖中排除caffeine ，并且缓存使用useJava8CompatibleCaching
    //UserAgentAnalyzer 是线程安全的，如果负载较高，可以初始化多个实例
    //withCache 是缓存的结果数量，如果出现重复ua，会加快解析
    //So be prepared to receive a null, Unknown or another field specific default.
    public static UserAgentAnalyzer uaa = UserAgentAnalyzer
            .newBuilder()
            .useJava8CompatibleCaching()
            .hideMatcherLoadStats()
            .withField("DeviceBrand")
            .withField("DeviceName")
            .withField("OperatingSystemName")
            .withField("OperatingSystemVersion")
            .withField("OperatingSystemNameVersionMajor")
            .withCache(1000)
            .build();

    public static void main(String[] args) {
//        String userAgent = "Mozilla/5.0 (Linux; Android 11; SM-A215U Build/RP1A.200720.012; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/87.0.4280.141 Mobile Safari/537.36";
        String userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 17_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";

        UserAgent agent = uaa.parse(userAgent);

        for (String fieldName: agent.getAvailableFieldNamesSorted()) {
            System.out.println(fieldName + " = " + agent.getValue(fieldName));
        }

        System.out.println("===========");

        String os = agent.getValue("OperatingSystemName");
        String brand = agent.getValue("DeviceBrand");
        String model = agent.getValue("DeviceName");
        String browser = agent.getValue("AgentNameVersion");
        String ip = agent.getValue("AgentVersion");
        String osv = agent.getValue("OperatingSystemVersion");


        System.out.println(os);
        System.out.println(browser);
        System.out.println(brand);
        System.out.println(model);
        System.out.println(formatOsv(osv));
        System.out.println(ip);
        System.out.println("===============");



        Map<String, String> map = new HashMap<>();
        map.put("os",os);
        map.put("brand",brand);
        map.put("model",model);
        map.put("osv",null);
        if(map.get("osv")==null){
            System.out.println("is null");
        }

        System.out.println(map.get("osv"));
    }

    public static String formatOsv(String osv) {
        if (StringUtils.isEmpty(osv)) {
            return "0";
        }
        String[] split = osv.split("\\.");
        if (split.length >= 3) {
            return split[0] + "." + split[1];
        } else {
            return osv;
        }
    }



}
