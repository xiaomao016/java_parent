package com.zx.work.test.ip.util;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.AsnResponse;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * 54.81.101.125：Amazon.com
 *
 */
public class ASNMaxMindTest {
    public static void main(String[] args) {
        String code3 = queryIpCountryCode("54.81.101.125");
        String code2 = queryIpCountryCode("128.101.101.101");
        String code1 = queryIpCountryCode("68.183.144.165");
        String code0 = queryIpCountryCode("73.34.185.167");
        System.out.println(code3);
        System.out.println(code2);
        System.out.println(code1);
        System.out.println(code0);
    }

    public static String queryIpCountryCode(String ip) {
        try {
            File ASNDatabase = new File("E:\\ip\\GeoLite2-ASN.mmdb");

            // This reader object should be reused across lookups as creation of it is
            // expensive.
            DatabaseReader reader = new DatabaseReader.Builder(ASNDatabase).build();

//            InetAddress ipAddress = InetAddress.getByName("128.101.101.101");
            InetAddress ipAddress = InetAddress.getByName(ip);

            AsnResponse response = reader.asn(ipAddress);

//            System.out.println(response.getAutonomousSystemNumber());       // 217
//            System.out.println(response.getAutonomousSystemOrganization()); // 'University of Minnesota'
            return response.getAutonomousSystemOrganization();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeoIp2Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
