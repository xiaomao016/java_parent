package com.zx.work.test.y23;

import lombok.Data;

@Data
public class RecordNumber {
    int totalNum ;
    int syncNum;
    int repeatNum;
    int blackNum;
    int errorIPNum;

    public RecordNumber(){
        this.totalNum = 0;
        this.syncNum = 0;
        this.repeatNum = 0;
        this.blackNum = 0;
        this.errorIPNum = 0;
    }

    public void incrementTotal() {
        totalNum++;
    }

    public void incrementRepeatNum() {
        repeatNum++;
    }

    public void incrementBlackNum() {
        blackNum++;
    }

    public void incrementErrorIPNum() {
        errorIPNum++;
    }

    public void incrementSyncNum(int num) {
        this.syncNum += num;
    }

}
