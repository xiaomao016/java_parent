package com.zx.work.test.date;

import com.zx.work.utils.DateUtilFinal;
import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;
import java.util.Date;

@Slf4j
public class DateFormatRound {
    public static void main(String[] args) {

        getRoundDate(new Date());

        String dateFormatyyyyMMddHHmmss = DateUtilFinal.getDateFormatyyyyMMddHHmmss(1698249600000L);
        log.info(dateFormatyyyyMMddHHmmss);
    }

    private static Date getRoundDate(Date currentDate){
        // 保证时分秒为0
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date formattedDate = calendar.getTime();

        log.info("date:{}", DateUtilFinal.getDateFormatyyyyMMddHHmmss(formattedDate));
        return formattedDate;
    }
}
