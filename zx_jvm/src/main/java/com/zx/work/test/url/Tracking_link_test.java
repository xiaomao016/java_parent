package com.zx.work.test.url;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;

import java.util.Map;

public class Tracking_link_test {

    static Map<String, String> requiredParameters = Maps.newLinkedHashMap();

    static {
        requiredParameters.put("affiliate_id", "{affiliate_sub_id}");
        requiredParameters.put("clickid", "{transaction_id}");
        requiredParameters.put("gaid", "{gaid}");
        requiredParameters.put("uagent", "{ua}");
        requiredParameters.put("ip", "{ip}");
        requiredParameters.put("country_code", "{country}");
        requiredParameters.put("idfa", "{idfa}");
    }
    public static void main(String[] args) {

        String tracking = "http://exping.rqmob.com/click?creative_name={creative_name}&device_model={device_model}&affiliate_id={affiliate_sub_id}&gaid={gaid}&advid=30&pkg=com.novapontocom.casasbahia&platform=P_API_BidFuture&creative_id={creative_id}&mac={mac}&oaid={oaid}&uagent={ua}&ext={ext}&campid=1403686&idfa={idfa}&os_platform={os_platform}&os_version={os_version}&ip={ip}&rta_token={rta_token}&clickid={transaction_id}&lzd_cid=lzd_im_200040003976021&offer_name={offer_name}&country_code={country}&adset_id=13982&imei={imei}&android_id={android_id}";
        Map<String, String> stringStringMap = parseUrlParams(tracking, 1000);
        requiredParameters.forEach(stringStringMap::put);
        String join = Joiner.on("&")
                // 用指定符号代替空值,key 或者value 为null都会被替换
                .withKeyValueSeparator("=")
                .join(stringStringMap);
        String[] split = tracking.split("\\?");
        String rr = split[0].concat("?").concat(join);

        System.out.println(rr);
    }


    public static Map<String, String> parseUrlParams(String url, int maxLimit) {
        Map<String, String> params = Maps.newHashMap();
        String[] urlParts = url.split("\\?");
        //没有参数
        if (urlParts.length == 1) {
            return params;
        }
        //有参数
        String[] strParams = urlParts[1].split("&");
        int limit = 0;
        for (String strParam : strParams) {
            if (limit > maxLimit) {
                break;
            }
            limit++;
            String[] keyValue = strParam.split("=");
            if (keyValue.length == 1) {
                params.put(keyValue[0], "");
            } else {
                params.put(keyValue[0], keyValue[1]);
            }
        }
        return params;
    }

}

