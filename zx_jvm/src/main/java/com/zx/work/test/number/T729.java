package com.zx.work.test.number;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author zx
 * @description:
 * @date 2022/7/29
 */
public class T729 {

    public static void main(String[] args) {
        long a = 1551472798566715394L;
        long b = 1551473000446955521L;
        long c = 1473940642533359619L;

        System.out.println(isOdd(a));
        System.out.println(isOdd(b));

        System.out.println(a % 3);
        System.out.println(b % 3);
        System.out.println(c % 3);
    }

    public static boolean isOdd(long data){
        if(data%2==0){
            return true;
        }else{
            return false;
        }
    }
}
