package com.zx.work.test.url;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringJoin {
    public static void main(String[] args) {

        List<Map<String, Object>> defaultPayout = newEventPayout("install");
        List<Map<String, Object>> defaultPayout2 = newEventPayout("install");
        List<Map<String,Object>> df = Lists.newArrayList();
        df.addAll(defaultPayout);
        df.addAll(defaultPayout2);

        String settleEvent = df.stream()
                .map(x -> x.get("eventName").toString().trim())
                .distinct()
                .collect(Collectors.joining(","));

        System.out.println(settleEvent);
        JSONObject off = new JSONObject();
        off.put("payout","0.60");
        String previewLink = off.getString("preview_url");
        System.out.println(off.getBigDecimal("payout"));
//        BigDecimal revenue = BigDecimal.valueOf().setScale(3, BigDecimal.ROUND_HALF_UP);
    }


    public static List<Map<String,Object>> newEventPayout(String event){
        List<Map<String, Object>> result = Lists.newLinkedList();
        Map<String, Object> payOutInfo = new HashMap<>();
        payOutInfo.put("eventName", event);
        payOutInfo.put("revenue", 12);
        payOutInfo.put("payout", 2);
        payOutInfo.put("capDaily", 166);
        payOutInfo.put("currency", "USD");
        payOutInfo.put("payoutType", "CPI");
        result.add(payOutInfo);
        return result;
    }
}
