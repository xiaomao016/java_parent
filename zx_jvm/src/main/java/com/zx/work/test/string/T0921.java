package com.zx.work.test.string;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zx
 * @description:
 * @date 2022/9/21
 */
public class T0921 {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("AU AE AF AG AI AL AM AO AQ AR AS AT AW AX AZ ");
        sb.append("BA BB BD BE BF BG BH BI BJ BL BM BN BO BQ BR BS BT BW BY BZ ");
        sb.append("CA CC CD CF CG CH CI CK CL CM CN CO CR CU CV CW CX CY CZ ");
        sb.append("DE DJ DK DM DO DZ ");
        sb.append("EC EE EG EH ER ES ET EU ");
        sb.append("FI FJ FK FM FO FR ");
        sb.append("GA GB GD GE GF GG GH GI GL GM GN GP GQ GR GS GT GU GW GY ");
        sb.append("HK HM HN HR HT HU ");
        sb.append("ID IE IL IM IN IO IQ IR IS IT ");
        sb.append("JE JM JO JP ");
        sb.append("KE KG KH KI KM KN KO KP KR KW KY KZ ");
        sb.append("LA LB LC LI LK LR LS LT LU LV LY ");
        sb.append("MA MC MD ME MF MG MH MK ML MM MN MO MP MQ MR MS MT MU MV MW MX MY MZ ");
        sb.append("NA NC NE NF NG NI NL NO NONE NP NR NU NZ ");
        sb.append("OM ");
        sb.append("PA PE PF PG PH PK PL PM PN PR PS PT PW PY ");
        sb.append("QA ");
        sb.append("RE RO RS RU RW ");
        sb.append("SA SB SC SD SE SG SH SI SJ SK SL SM SN SO SR SS ST SV SX SY SZ ");
        sb.append("TC TD TF TG TH TJ TK TL TM TN TO TR TT TV TW TZ ");
        sb.append("UA UG UM US UY UZ ");
        sb.append("VA VC VE VG VI VN VU ");
        sb.append("WF WS ");
        sb.append("XK ");
        sb.append("YE YT ");
        sb.append("ZA ZM ZW ZZ");

        String[] s = sb.toString().split(" ");

        Set<String> codesSet = new HashSet<>();


        for(String code :s){
            if(codesSet.contains(code)){
                System.out.println("已存在："+code);
                continue;
            }
            codesSet.add(code);
            System.out.println(code);
        }


        System.out.println(s.length);
        System.out.println(codesSet.size());

    }
}
