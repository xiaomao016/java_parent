package com.zx.work.test.base;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginPlatformParse {
    public static void main(String[] args) {
        String url = "http://47.253.41.138/";
        String url2 = "https://mng.bidmatrixcorp.com/#/login";
        String[] ss = getPlatform(url);
        String[] s2 = getPlatform(url2);
        log.info("test:{}", JSON.toJSONString(ss));
        log.info("prod:{}", JSON.toJSONString(s2));

        log.info("domain:{}",s2[s2.length - 2]);

        //select id,platform, logo
        //        from sys_platform
        //        where `enable` = 1 and console_domain = #{domain}


//                SysPlatform platform = sysPlatformMapper.findPlatform(platforms[0]);
//                if (platform == null) {
//                    platform = sysPlatformMapper.findPlatform(platforms[1]);
//                }
//                if (platform == null) {
//                    List<SysPlatform> domains = sysPlatformMapper.findDomain(platforms[platforms.length - 2]);
//                    if (CollectionUtils.isEmpty(domains) || domains.size() > 1) {
//                        return Result.error(403, "error platform");
//                    }
//                    platform = domains.get(0);
//                }
    }


    public static String[] getPlatform(String url){
        String domain = url.substring(url.indexOf("://") + 3);
        domain = domain.substring(0, domain.indexOf("/"));
        return domain.split("\\.");
    }

}
