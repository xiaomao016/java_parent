package com.zx.work.test.y22;

import com.zx.work.test.mc.ArithmeticOperatorEnum;
import com.zx.work.test.mc.LogicalOperatorEnum;
import com.zx.work.test.vo.Stu610;

/**
 * @author zx
 * @description:
 * @date 2022/6/8
 */

public class Domain {
    private Integer recentActiveDays;
    private ArithmeticOperatorEnum arithmeticOperatorEnum;
    private LogicalOperatorEnum logicalOperatorEnum;

    public LogicalOperatorEnum getLogicalOperatorEnum() {
        return logicalOperatorEnum;
    }

    public void setLogicalOperatorEnum(LogicalOperatorEnum logicalOperatorEnum) {
        this.logicalOperatorEnum = logicalOperatorEnum;
    }

    public ArithmeticOperatorEnum getArithmeticOperatorEnum() {
        return arithmeticOperatorEnum;
    }

    public void setArithmeticOperatorEnum(ArithmeticOperatorEnum arithmeticOperatorEnum) {
        this.arithmeticOperatorEnum = arithmeticOperatorEnum;
    }

    public Integer getRecentActiveDays() {
        return recentActiveDays;
    }

    public void setRecentActiveDays(Integer recentActiveDays) {
        this.recentActiveDays = recentActiveDays;
    }
}
