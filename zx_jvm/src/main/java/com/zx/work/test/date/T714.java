package com.zx.work.test.date;

/**
 * @author zx
 * @description:
 * @date 2022/7/14
 */
public class T714 {

    public static void main(String[] args) {
        Long todsy = System.currentTimeMillis();
        System.out.println(todsy);
        Long bf = getDayTimeBefore(todsy,30);
        System.out.println(bf);//1659492334726

        System.out.println(Integer.MAX_VALUE);
    }

    //计算该时间n天前的时间戳
    public static Long getDayTimeBefore(Long cur,int n) {
        long dis = n*24*60*60*1000L;
        return cur - dis;
    }
}
