package com.zx.work.test.y23;

import com.zx.work.utils.DateUtilFinal;

import java.util.Calendar;
import java.util.Date;

public class T0814 {
    public static void main(String[] args) {
        long timeStamp = 1691643686420L;
        long timeStamp2 = 1691603216359L;

        String dateFormatyyyyMMddHHmmss = DateUtilFinal.getDateFormatyyyyMMddHHmmss(timeStamp);
        String dateFormatyyyyMMddHHmmss2 = DateUtilFinal.getDateFormatyyyyMMddHHmmss(timeStamp2);
        System.out.println(dateFormatyyyyMMddHHmmss);
        System.out.println(dateFormatyyyyMMddHHmmss2);

        String fromDate = "2023-08-15";
        Date start = DateUtilFinal.formatTime("2023-08-15");
        start = addHours(start,2);



    }

    public static Date addHours(Date date, int hours) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(11, hours);
        return c.getTime();
    }


    public static enum DatePattern {
        yyyyMM("yyyyMM"),
        yyyyMMdd("yyyyMMdd"),
        yyyyMMddHH("yyyyMMddHH"),
        yyMMddHH("yyMMddHH"),
        yyyyMMddHHmmss("yyyyMMddHHmmss"),
        yyyy_MM("yyyy-MM"),
        yyyy_MM_dd("yyyy-MM-dd"),
        yyyy_MM_dd_HH("yyyy-MM-dd-HH"),
        yyyy_MM_dd_HH_mm_ss("yyyy-MM-dd HH:mm:ss");

        private String pat;

        private DatePattern(String pat) {
            this.pat = pat;
        }
    }


}
