package com.zx.work.test.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Iterator;
import java.util.List;

public class JSONTest {

    public static void main(String[] args) {
        // 创建 JSONObject 对象
        JSONObject jsonObject = new JSONObject();

        // 添加键值对
        jsonObject.put("key1", "value1");
        jsonObject.put("key2", "value2");
        jsonObject.put("key3", "value3");

        System.out.println("original json: "+JSON.toJSONString(jsonObject));
        // 遍历 JSONObject 中的键值对
        Iterator<String> iterator = jsonObject.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Object value = jsonObject.get(key);
            System.out.println("Key: " + key + ", Value: " + value);

            // 判断特定的键并删除键值对
            if ("key1".equals(key)) {
                iterator.remove();
            }
        }
        System.out.println("process json: "+JSON.toJSONString(jsonObject));
    }
}
