package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.*;

public class T0216 {
    public static void main(String[] args) {
        String link = "http://pb.bidamericangame.com/pb/lsr?transaction_id={transaction_id}";
        replaceDomainAndPort("pb.new.com",null,link);
        System.out.println("===============");

        List<String> list = Lists.newArrayList("下列offer有问题：","123");

        String r = String.join(",",list);
        System.out.println(r);

        System.out.println("==========");

        Map<String, String> linkMap = new LinkedHashMap<>();
        for(int i=0;i<3;++i) {
            linkMap.put("campaign","rammslg");
        }

        System.out.println("=====");

        String trackingLink = "https://api.nealvwillpams.com/api/advert/4570bbe56e00421e82482b3120d8c2b2?gaid=__GAID__&clickId=__CLICK_ID__";
        trackingLink = trackingLink.substring(0,trackingLink.indexOf("?"));
        System.out.println(trackingLink);

        System.out.println("========");
        Set<String> hs  = new HashSet<>();
        hs.add("zhangxu");
        hs.add("zhangxu");
        hs.add("lisi");
        System.out.println(String.join(",",hs));

        System.out.println("==========");

        BigDecimal rate = new BigDecimal(-100);

        System.out.println(rate);

        System.out.println("=========");
        Set<Integer> s1 = new HashSet<>();
        s1.add(1110);
        s1.add(1111);
        s1.add(1112);
        Set<Integer> s2 = new HashSet<>();
        s2.add(1110);
        s2.add(1111);
        s2.add(1112);
        s2.add(1113);

        Set<Integer> re = new HashSet<>();
        re.addAll(s1);
        re.removeAll(s2);

        System.out.println(JSON.toJSONString(re));

        System.out.println(re.size());









    }

    public static String replaceDomainAndPort(String domain, String port, String url) {
        String url_bak = "";
        if (url.indexOf("//") != -1) {
            String[] splitTemp = url.split("//");
            url_bak = splitTemp[0] + "//";
            if (port != null) {
                url_bak = url_bak + domain + ":" + port;
            } else {
                url_bak = url_bak + domain;
            }
            if (splitTemp.length >1 && splitTemp[1].indexOf("/") != -1) {
                String[] urlTemp2 = splitTemp[1].split("/");
                if (urlTemp2.length > 1) {
                    for (int i = 1; i < urlTemp2.length; i++) {
                        url_bak = url_bak + "/" + urlTemp2[i];
                    }
                }
            }
        }
        return url_bak;
    }
}
