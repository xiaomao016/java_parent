package com.zx.work.test.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class InterupteTest {
    public static final boolean[] f = {true};
    public static void main(String[] args) {

        LinkedBlockingQueue<String> q = new LinkedBlockingQueue(10);
        Thread threadOne = new  Thread(new Runnable() {
            @Override
            public void run() {
                    try {
                        q.poll(1,TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        System.out.println("interrupt");
                    }

                System.out.println("threadOne isInterrupted " + Thread.currentThread().isInterrupted());
            }
        });

        //启动线程
        threadOne.start();
        //由输出结果可知，调用interrupted（）方法后中断标志被清除了。
        threadOne.interrupt();
    }

    private static int readFile(){
        while(f[0]){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
//                System.out.println("被置为中断:"+Thread.currentThread().interrupted());
                f[0] = false;
            }finally {
                System.out.println("finally");
            }
        }
        return 0;
    }
}
