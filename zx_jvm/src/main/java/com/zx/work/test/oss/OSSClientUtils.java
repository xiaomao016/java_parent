package com.zx.work.test.oss;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.internal.OSSHeaders;
import com.aliyun.oss.model.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * 类说明
 *
 * @author zx
 * @date 2022/4/14
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class OSSClientUtils {

    private String bucketName;
    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;

    public void init() {
        this.bucketName = OSSCommInfo.bucketName;
        this.endpoint = OSSCommInfo.endpoint;
        this.accessKeyId = OSSCommInfo.key;
        this.accessKeySecret = OSSCommInfo.secret;
    }

    public String appendJsonToOSSFile(String objectName,List<String> jsons){
        try {
            ObjectMetadata meta = new ObjectMetadata();
            // 指定上传的内容类型。
            meta.setContentType("text/plain");

            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            // 构造追加写入请求
            AppendObjectRequest appendRequest = new AppendObjectRequest(bucketName, objectName, new ByteArrayInputStream(new byte[0]),meta);
            appendRequest.setPosition(0L);
            // 执行追加写入操作
            AppendObjectResult appendObjectResult = ossClient.appendObject(appendRequest);

            for(String json:jsons){
                appendRequest.setPosition(appendObjectResult.getNextPosition());
                appendRequest.setInputStream(new ByteArrayInputStream(json.getBytes()));
                appendObjectResult = ossClient.appendObject(appendRequest);
            }
            ossClient.shutdown();
        } catch (OSSException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String genURL(String objectName) {
        try {
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            Date expiration = new Date(System.currentTimeMillis() + 3600 * 1000);
            String url = ossClient.generatePresignedUrl(bucketName, objectName, expiration).toString();
            GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName,accessKeyId);
            String url2 = ossClient.generatePresignedUrl(request).toString();

            ossClient.shutdown();
            return url;
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定下OSS的对象列表
     * 此种方式会列举目录本身
     * @param prefix
     * @return
     */
    public List<String> listObjectFromPrefix(String prefix) {
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        List<String> result = new ArrayList<>();
        try {
            // 列举文件。如果不设置keyPrefix，则列举存储空间下的所有文件。如果设置keyPrefix，则列举包含指定前缀的文件。
            ObjectListing objectListing = ossClient.listObjects(bucketName, prefix);
            List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
            for (OSSObjectSummary s : sums) {
                result.add(s.getKey());
            }
        } catch (OSSException oe) {
            log.info("OSSException,ErrorMessage:{},Error Code{},Request ID:{},Host ID:{}",
                    oe.getErrorMessage(), oe.getErrorCode(), oe.getRequestId(), oe.getHostId());
        } catch (ClientException ce) {
            log.error("listObjectFromPrefix ClientException ,Error Message:{},prefix:{}", ce.getMessage(), prefix);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return result;
    }

    /**
     * 此种方式不会列举目录，只会列举文件对象
     * @return
     */
    public List<OSSObjectSummary> listObjects(){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        List<OSSObjectSummary> oneObjectSummaryList = new ArrayList<>();
        ObjectListing objectListing;
        String nextMarker = null;
        do {
            ListObjectsRequest objectsRequest = new ListObjectsRequest(bucketName);
            // 设置最大个数。
            final int maxKeys = 200;
            // 设置查询前缀
            objectsRequest.setPrefix("dwd/android/IN/20230612/16/47");
            objectsRequest.withMarker(nextMarker);
            objectsRequest.setMaxKeys(maxKeys);
            objectListing = ossClient.listObjects(objectsRequest);
            List<OSSObjectSummary> objectSummaryList = objectListing.getObjectSummaries();
            oneObjectSummaryList.addAll(objectSummaryList);
            nextMarker = objectListing.getNextMarker();
        } while (objectListing.isTruncated());
        return oneObjectSummaryList;
    }

    /**
     * 下载oss文件到本地
     *
     * @param objectName
     * @param destFilePath
     */
    public void downloadFile(String objectName, String destFilePath) {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {
            // 下载Object到本地文件，并保存到指定的本地路径中。如果指定的本地文件存在会覆盖，不存在则新建。
            File downLoadFile = new File(destFilePath);
            log.info("-------准备下载文件：{}", downLoadFile.getAbsolutePath());
            if (downLoadFile.exists()) {
                log.info("--------文件已存在，跳过下载-----");
                return;
            }
            ossClient.getObject(new GetObjectRequest(bucketName, objectName), downLoadFile);
        } catch (OSSException oe) {
            log.error("OSSException,ErrorMessage:{},Error Code{},Request ID:{},Host ID:{}",
                    oe.getErrorMessage(), oe.getErrorCode(), oe.getRequestId(), oe.getHostId());
        } catch (ClientException ce) {
            log.error("downloadFile ClientException ,doError Message:{},param:{},{}", ce.getMessage(), objectName, destFilePath);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 上传文件
     *
     * @param objectName
     * @param uploadFilePath
     */
    public void uploadFile(String objectName, String uploadFilePath) {

        // 创建OSSClient实例。
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, new File(uploadFilePath));
            // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
             ObjectMetadata metadata = new ObjectMetadata();
             metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
             metadata.setObjectAcl(CannedAccessControlList.PublicRead);
             putObjectRequest.setMetadata(metadata);

            // 上传文件。
            ossClient.putObject(putObjectRequest);
        } catch (OSSException oe) {
            log.error("OSSException,ErrorMessage:{},Error Code{},Request ID:{},Host ID:{}",
                    oe.getErrorMessage(), oe.getErrorCode(), oe.getRequestId(), oe.getHostId());
        } catch (ClientException ce) {
            log.error("uploadFile ClientException ,Error Message:{},param:{},{}", ce.getMessage(), objectName, uploadFilePath);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 读取文件
     */
    public  void readFile(){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        GetObjectRequest request = new GetObjectRequest(bucketName,"temp/test/ttt02101027/aliyun-filter-AD-android-20220803-0910.gz");
        OSSObject object = ossClient.getObject(request);
        GZIPInputStream in;
        try {
            in = new GZIPInputStream(object.getObjectContent());
            InputStreamReader reader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(reader);
            int itemNum = 0;
            int syncNum = 0;
            String str;
            while((str = bufferedReader.readLine()) != null){
                itemNum = itemNum + 1;
                System.out.println(str);
            }
            System.out.println(itemNum);
            bufferedReader.close();
            reader.close();
            in.close();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取文件
     */
    public  void readFile2(){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        GetObjectRequest request = new GetObjectRequest(bucketName,"temp/test/ttt02101027/aliyun-filter-AD-android-20220803-0910.gz");
//        OSSObject object = ossClient.getObject(bucketName,"temp/test/ttt02101027/aliyun-filter-AD-android-20220803-0910.gz");
        OSSObject object = ossClient.getObject(bucketName,"temp/test/ttt02101027/");
        GZIPInputStream in;
        try {
            in = new GZIPInputStream(object.getObjectContent());
            InputStreamReader reader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(reader);
            int itemNum = 0;
            int syncNum = 0;
            String str;
            while((str = bufferedReader.readLine()) != null){
                itemNum = itemNum + 1;
                System.out.println(str);
            }
            System.out.println(itemNum);
            bufferedReader.close();
            reader.close();
            in.close();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
