package com.zx.work.test.staticField;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class Test {
    public static void main(String[] args) throws InterruptedException {
        LocalDateTime currentDateTime = LocalDateTime.now();
        // Move the current date forward 7 days
        LocalDate fromDate = currentDateTime.toLocalDate().minusDays(7);
        LocalDate toDate = currentDateTime.toLocalDate();
        LocalDateTime fromDateZeroClock = LocalDateTime.of(fromDate, LocalTime.MIDNIGHT);
        LocalDateTime toDateZeroClock = LocalDateTime.of(toDate, LocalTime.MIDNIGHT);


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String from = fromDateZeroClock.format(formatter);
        String to = toDateZeroClock.format(formatter);
        log.info("格式化后的前一天凌晨0点时间：{}" , from);
        log.info("格式化后的前一天最后一秒时间：{}",  to);
    }
}
