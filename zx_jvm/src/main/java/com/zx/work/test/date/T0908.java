package com.zx.work.test.date;

import com.zx.work.utils.DateUtil;
import com.zx.work.utils.DateUtilFinal;

import java.util.Calendar;
import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/9/8
 */
public class T0908 {
    public static void main(String[] args) {
        Date delDate = addHours(new Date(),~(30*24)+1);
        String dateStr = DateUtilFinal.getDateFormatyyyyMMdd(delDate);
        System.out.println(dateStr);
    }

    public static Date addHours(Date date, int hours) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(11, hours);
        return c.getTime();
    }
}
