package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.zx.work.utils.DateUtilFinal;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class TeasAkamai {
    public static void main(String[] args) {
        long currentDate     = System.currentTimeMillis();
        long startDate       = 1719220501269L;
        long endDate         = 1719306901269L;
        long ONE_DAY         = 86400000L;
        long offsetFromStart = (currentDate - startDate)/ONE_DAY + 1;
        long offsetFromEnd   = (currentDate - endDate)/ONE_DAY;
        System.out.println(offsetFromStart);
        System.out.println(offsetFromEnd);

        List<String> indexList = new ArrayList<>();
        for(; offsetFromStart >= offsetFromEnd; offsetFromStart--){
            indexList.add("ks_kafka_group_metric" + "_" + getFormatDayByOffset((int)offsetFromStart));
        }

        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(startDate));
        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(endDate));
        System.out.println(JSON.toJSONString(indexList));
    }

    public static String getFormatDayByOffset(int offset) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return dateTimeFormatter.format( ZonedDateTime.now().minus(offset, ChronoUnit.DAYS));
    }
}
