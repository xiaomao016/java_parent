package com.zx.work.test.date;

import com.zx.work.utils.DateUtilFinal;

import java.text.MessageFormat;
import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/8/4
 */
public class T0804 {
    public static void main(String[] args) {
        Date date = new Date();
        Date d_2 = DateUtilFinal.addMinutes(date,-900);
        String hh  = DateUtilFinal.getDateFormatHH(d_2);
        String mm  = DateUtilFinal.getDateFormatmm(d_2);
        String yyyMMdd = DateUtilFinal.getDateFormatyyyyMMdd(d_2);
        String ossPath = "dwd/ios/{0}/{1}/{2}/{3}/";
        System.out.println(MessageFormat.format(ossPath,"BR",yyyMMdd,hh,mm));
    }
}
