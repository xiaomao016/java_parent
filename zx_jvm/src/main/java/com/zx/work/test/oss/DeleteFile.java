package com.zx.work.test.oss;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.*;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zx
 * @description:
 * @date 2022/9/2
 */
@Slf4j
public class DeleteFile {


    public static void main(String[] args) {
        //获取ios国家编码集合

        //拼接国家相关前缀

        //获取android国家编码集合

        //拼接国家相关前缀

        //循环删除

        listObjectFromPrefix("dwd/ios");

    }


    /**
     * 删除文件
     * @param objectName
     */
    public static void deleteObject(String objectName){
        String accessKeyId = "LTAI5tSVHkTXX6KKQbkfc39k";
        String accessKeySecret = "IOiyNAZiZWDAPXoUloGx8B3aCCyiEp";
        String endpoint = "https://oss-us-east-1.aliyuncs.com";
        String bucketName = "click-data-filter-mc";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            // 删除文件或目录。如果要删除目录，目录必须为空。
            ossClient.deleteObject(bucketName, objectName);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 获取指定下OSS的对象列表
     * @param prefix
     * @return
     */
    public static List<String> listObjectFromPrefix(String prefix){
        String accessKeyId = "LTAI5tSVHkTXX6KKQbkfc39k";
        String accessKeySecret = "IOiyNAZiZWDAPXoUloGx8B3aCCyiEp";
        String endpoint = "https://oss-us-east-1.aliyuncs.com";
        String bucketName = "click-data-filter-mc";
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        List<String> result = new ArrayList<>();
        try {
            // 列举文件。如果不设置keyPrefix，则列举存储空间下的所有文件。如果设置keyPrefix，则列举包含指定前缀的文件。
            ObjectListing objectListing = ossClient.listObjects(bucketName, prefix);
            List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
            for (OSSObjectSummary s : sums) {
                result.add(s.getKey());
            }
        } catch (OSSException oe) {
            log.info("OSSException,ErrorMessage:{},Error Code{},Request ID:{},Host ID:{}",
                    oe.getErrorMessage(),oe.getErrorCode(),oe.getRequestId(),oe.getHostId());
        } catch (ClientException ce) {
            log.error("listObjectFromPrefix ClientException ,Error Message:{},prefix:{}",ce.getMessage(),prefix);
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return result;
    }


    public static void deletePrefix(String prefix){
        String accessKeyId = "LTAI5tSVHkTXX6KKQbkfc39k";
        String accessKeySecret = "IOiyNAZiZWDAPXoUloGx8B3aCCyiEp";
        String endpoint = "https://oss-us-east-1.aliyuncs.com";
        String bucketName = "click-data-filter-mc";
        // 如果您仅需要删除src目录及目录下的所有文件，则prefix设置为src/。

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            // 列举所有包含指定前缀的文件并删除。
            String nextMarker = null;
            ObjectListing objectListing = null;
            do {
                ListObjectsRequest listObjectsRequest = new ListObjectsRequest(bucketName)
                        .withPrefix(prefix)
                        .withMarker(nextMarker);

                objectListing = ossClient.listObjects(listObjectsRequest);
                if (objectListing.getObjectSummaries().size() > 0) {
                    List<String> keys = new ArrayList<String>();
                    for (OSSObjectSummary s : objectListing.getObjectSummaries()) {
                        System.out.println("key name: " + s.getKey());
                        keys.add(s.getKey());
                    }
                    DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest(bucketName).withKeys(keys).withEncodingType("url");
                    DeleteObjectsResult deleteObjectsResult = ossClient.deleteObjects(deleteObjectsRequest);
                    List<String> deletedObjects = deleteObjectsResult.getDeletedObjects();
                    try {
                        for(String obj : deletedObjects) {
                            String deleteObj =  URLDecoder.decode(obj, "UTF-8");
                            System.out.println(deleteObj);
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                nextMarker = objectListing.getNextMarker();
            } while (objectListing.isTruncated());
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

}
