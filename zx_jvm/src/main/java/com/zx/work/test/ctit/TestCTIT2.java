package com.zx.work.test.ctit;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TestCTIT2 {
    public static void main(String[] args) {
        List<CTITProportionReportBean> reportList = new ArrayList<>();

        // 添加示例数据
        reportList.add(new CTITProportionReportBean(1, 1L, "Offer1", 1, "Affiliate1", "AffSub1", "Country1", 10L, 20L, "Prod1", parseDate("2023-10-24 10:00:01")));
        reportList.add(new CTITProportionReportBean(1, 1L, "Offer1", 1, "Affiliate1", "AffSub1", "Country1", 5L, 20L, "Prod1", parseDate("2023-10-24 10:00:01")));
        reportList.add(new CTITProportionReportBean(1, 2L, "Offer2", 1, "Affiliate1", "AffSub3", "Country2", 8L, 15L, "Prod1", parseDate("2023-10-24 12:00:00")));
        reportList.add(new CTITProportionReportBean(2, 3L, "Offer3", 1, "Affiliate2", "AffSub4", "Country1", 12L, 30L, "Prod2", parseDate("2023-10-25 09:00:00")));
        reportList.add(new CTITProportionReportBean(2, 3L, "Offer3", 1, "Affiliate2", "AffSub5", "Country1", 18L, 30L, "Prod2", parseDate("2023-10-25 09:00:00")));


        Map<String, List<CTITProportionReportBean>> groupedMap = reportList.stream()
                .collect(Collectors.groupingBy(bean ->
                        bean.getOfferId() + "-" +
                                bean.getAffiliateId() + "-" +
                                bean.getAffSub() + "-" +
                                bean.getCountry() + "-" +
                                formatDate(bean.getCreateTime())));

        groupedMap.values().forEach(group -> {
            long totalInstallCount = group.stream().mapToLong(CTITProportionReportBean::getInstallCount).sum();
            group.forEach(bean -> bean.setCtitProportion((double) bean.getInstallCount() / totalInstallCount));
        });

        List<CTITProportionReportBean> updatedList = groupedMap.values().stream()
                .flatMap(List::stream)
                .sorted(Comparator.comparingDouble(CTITProportionReportBean::getCtitProportion).reversed())
                .collect(Collectors.toList());
    }

    private static Date parseDate(String dateString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
