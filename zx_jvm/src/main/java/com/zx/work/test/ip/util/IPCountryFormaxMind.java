package com.zx.work.test.ip.util;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

public class IPCountryFormaxMind {
    public static void main(String[] args) {
        String code = queryIpCountryCode("45.114.152.116");
        String code2 = queryIpCountryCode("2401:fb00:ffff:fffc:0:b:ac11:3466");
        String code3 = queryIpCountryCode("10.140.121.133");
        System.out.println(code);
        System.out.println(code2);
        System.out.println(code3);
    }

    public static String queryIpCountryCode(String ip) {
        try {
            String ipv6Str = "::/64";
            File database = new File("C:\\Users\\dell\\Desktop\\临时\\1.11_ip\\GeoLite2-City.mmdb");

            // This reader object should be reused across lookups as creation of it is
            // expensive.
            DatabaseReader reader = new DatabaseReader.Builder(database).build();

            // If you want to use caching at the cost of a small (~2MB) memory overhead:
            // new DatabaseReader.Builder(file).withCache(new CHMCache()).build();

            // InetAddress ipAddress = InetAddress.getByName("2a01:4b00:e019:e800:1a89:24ff:fec9:bdd8");
            InetAddress ipAddress = InetAddress.getByName(ip);

            CityResponse response = reader.city(ipAddress);

            Country country = response.getCountry();
            return country.getIsoCode();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeoIp2Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
