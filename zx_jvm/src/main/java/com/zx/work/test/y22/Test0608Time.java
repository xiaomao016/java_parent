package com.zx.work.test.y22;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/6/8
 */
public class Test0608Time {

    public static void main(String[] args) {

        Long curMi = System.currentTimeMillis();
        System.out.println(curMi);
        long curmm = 1654682459281L;

        System.out.println(getDateStr(curmm));
        //计算
        System.out.println("===========");

        long zt = getZeroPointTimeMillis(curmm);
        System.out.println(zt);
        System.out.println(getDateStr(zt));

        System.out.println("===================");
        System.out.println(getDateStr(getDayTimeBefore(curmm,1)));

        System.out.println(getDateStrYMD(curMi));

        System.out.println(getDateStr(get24PointTimeMillis(curmm)));
    }

    public static Long getSecondsToZero() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Calendar curCal = Calendar.getInstance();

        long calInMillis = cal.getTimeInMillis();
        long diff = calInMillis - curCal.getTimeInMillis();
        long diffSeconds = diff / 1000;
        return diffSeconds;
    }

    //计算该时间所在天零点的时间戳
    public static Long getZeroPointTimeMillis(Long cur) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    //计算该时间所在天24点的时间戳
    public static Long get24PointTimeMillis(Long cur) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 1);

        return cal.getTimeInMillis();
    }

    //计算该时间n天前的时间戳
    public static Long getDayTimeBefore(Long cur,int n) {
       return cur - n*24*60*60*1000;
    }

    public static String getDateStr(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = new Date(date);
        String dateStr = format.format(newDate);
        return dateStr;
    }
    public static String getDateStrYMD(long date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date newDate = new Date(date);
        String dateStr = format.format(newDate);
        return dateStr;
    }

}
