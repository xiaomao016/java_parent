package com.zx.work.test.string;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class T1215 {

    public static void main(String[] args) {
        try {
            String trackingLink = "http://com.zx.cc?app_name={app_name}&type={type}&adv_name={adv_name} ";
            String bundle = "123456789";
            trackingLink = trackingLink.replace("{app_name}", URLEncoder.encode(bundle,"UTF-8"));

            trackingLink = trackingLink.replace("{", "%7b");
            trackingLink = trackingLink.replace("}", "%7d");
            trackingLink = trackingLink.trim();
            trackingLink = trackingLink.replace(" ", "%20");


            System.out.println(trackingLink);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
