package com.zx.work.test.date;

import com.zx.work.utils.DateUtilFinal;

import java.text.MessageFormat;
import java.util.*;

/**
 * @author zx
 * @description:
 * @date 2022/8/11
 */
public class T0811 {
    public static void main(String[] args) {
        Date now = DateUtilFinal.formatTime("2022-08-11 11:26:01");

        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(now));

        String ossPathFormat = "dwd/android/{0}/{1}/{2}/{3}/";

        List<String> ossPaths = new ArrayList<>();
        Map<String, String> remoteToLocalMap = new HashMap<>();

        for (int i = 0; i < 15; ++i) {
            int pre = i + 3;
            Date dPre = DateUtilFinal.addMinutes(now, ~pre + 1);
            String hh = DateUtilFinal.getDateFormatHH(dPre);
            String mm = DateUtilFinal.getDateFormatmm(dPre);
            String yyyMMdd = DateUtilFinal.getDateFormatyyyyMMdd(dPre);
            String ossPath = MessageFormat.format(ossPathFormat, "AU", yyyMMdd, hh, mm);
            ossPaths.add(ossPath);
//            String downloadPath = MessageFormat.format(downloadPathFormat, code, yyyMMdd, hh, mm);
//            createDirectoryIfNotExist(downloadPath);
//            remoteToLocalMap.put(ossPath, downloadPath);
        }

        for(String os:ossPaths){
            System.out.println(os);
        }

        for (int i = 0; i < 15; ++i) {
            System.out.println(~(i+3)+1);
        }
    }
}
