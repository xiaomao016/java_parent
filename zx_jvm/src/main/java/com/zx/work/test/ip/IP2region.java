package com.zx.work.test.ip;

import org.lionsoul.ip2region.xdb.Searcher;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.alibaba.nacos.api.cmdb.pojo.PreservedEntityTypes.ip;

/**
 * @author zx
 * @description:
 * @date 2022/9/29
 */
public class IP2region {

    public static void main(String[] args) {
        // 1、创建 searcher 对象
        String dbPath = "F:\\DM\\Github\\ip2region-master\\data\\ip2region.xdb";
        Searcher searcher = null;
        try {
            searcher = Searcher.newWithFileOnly(dbPath);
        } catch (IOException e) {
            System.out.printf("failed to create searcher with `%s`: %s\n", dbPath, e);
            return;
        }

        // 2、查询
        try {
            String ip = "103.165.108.72";
//            String ip = "103.168.201.45";
//            String ip = "44.197.131.0";
//            String ip = "34.24.64.228";
            long sTime = System.nanoTime();
            String region = searcher.search(ip);
            long cost = TimeUnit.NANOSECONDS.toMicros((long) (System.nanoTime() - sTime));
            System.out.printf("{region: %s, ioCount: %d, took: %d μs}\n", region, searcher.getIOCount(), cost);
            // 3、关闭资源
            searcher.close();
            // 备注：并发使用，每个线程需要创建一个独立的 searcher 对象单独使用。
        } catch (Exception e) {
            System.out.printf("failed to search(%s): %s\n", ip, e);
        }


    }
}
