package com.zx.work.test.y23;

import com.zx.work.utils.DateUtilFinal;

import java.time.LocalDateTime;
import java.time.Month;

public class Test1107 {

    public static void main(String[] args) {
        LocalDateTime localDateTime1 = LocalDateTime.now();
        int yearNow = localDateTime1.getYear();
        Month monthNow = localDateTime1.getMonth();
        int dayOfMonthNow = localDateTime1.getDayOfMonth();

        LocalDateTime localDateTime2 = localDateTime1.plusDays(-7);
        int year2 = localDateTime2.getYear();
        Month month2 = localDateTime2.getMonth();
        int dayOfMonth2 = localDateTime2.getDayOfMonth();
        LocalDateTime minTime = LocalDateTime.of(year2, month2, dayOfMonth2, 0, 0, 0);
        LocalDateTime maxTime = LocalDateTime.of(yearNow, monthNow, dayOfMonthNow, 23, 59, 59);

        System.out.println(minTime);
        System.out.println(maxTime);
    }
}
