package com.zx.work.test.string;

/**
 * @author zx
 * @description:
 * @date 2022/7/14
 */
public class T114 {

    public static void main(String[] args) {
        String queryBaseSQLSegment = "with temp_t0 as (\n" +
                "  select\n" +
                "  get_json_object(tb_cs.click_log,'$.country_code') as country_code,\n" +
                "  get_json_object(tb_cs.click_log,'$.exchange') as `exchange`,\n" +
                "  get_json_object(tb_cs.click_log,'$.os_version') as os_version,\n" +
                "  get_json_object(tb_cs.click_log,'$.brand') as `brand`,\n" +
                "  get_json_object(tb_cs.click_log,'$.language') as `language`,\n" +
                "  get_json_object(tb_cs.click_log,'$.timestamp') as `timestamp`,\n" +
                "  get_json_object(tb_cs.click_log,'$.app_id') as `app_id`,\n" +
                "  get_json_object(tb_cs.click_log,'$.deviceId') as `deviceId`\n" +
                "  from dwd_click_data as tb_cs\n" +
                "  where code='%s' AND DAY='%s' AND TOLOWER(get_json_object(tb_cs.click_log,'$.platform'))='%s' %s " +
                "AND get_json_object(tb_cs.click_log,'$.timestamp')>%s AND get_json_object(tb_cs.click_log,'$.timestamp')<%s)";


        String APPID_SQL = "AND app_id='%s'";
        APPID_SQL = String.format(APPID_SQL,"AAAAAA");
        queryBaseSQLSegment = String.format(queryBaseSQLSegment,"p1","p2","p3",APPID_SQL,"p5","p6");
        System.out.println(queryBaseSQLSegment);
    }
}
