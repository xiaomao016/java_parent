package com.zx.work.test.number;

import org.apache.commons.lang3.math.NumberUtils;

public class HourNumbDemo {
    public static void main(String[] args) {

        String fromHourStr = "09";
        boolean parsable = NumberUtils.isParsable(fromHourStr);
        boolean createble = NumberUtils.isCreatable(fromHourStr);
        System.out.println(Integer.parseInt(fromHourStr));
        System.out.println(parsable);
        System.out.println(createble);
        if (NumberUtils.isCreatable(fromHourStr)) {
            fromHourStr = Integer.valueOf(fromHourStr) + "";
        } else {
            fromHourStr = "0";
        }
        System.out.println(fromHourStr);

        System.out.println("==============");
        int match = match(fromHourStr);
        System.out.println(match);
    }

    public static int match(String timeStr){
        if (timeStr.matches("\\d{2}")) {
            int timeInt = Integer.parseInt(timeStr);
            return timeInt;
        } else {
            return 0;
        }
    }
}
