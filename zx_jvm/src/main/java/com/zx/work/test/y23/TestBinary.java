package com.zx.work.test.y23;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TestBinary {
    public static void main(String[] args) {
        for(int i=0;i<100;i++) {
            List<Double> taskDataList = Lists.newArrayList(0.5, 0.5,0.5);
            double randomValue = ThreadLocalRandom.current().nextDouble();
             // 生成一个[0, 1)范围内的随机数
            double[] cumulativePercentages = getCumulativePercentages(taskDataList);
            int selectedIndex = binarySearchForCumulativePercent(cumulativePercentages, randomValue);
            System.out.println(selectedIndex);
        }

    }

    public static double[] getCumulativePercentages(List<Double> taskDataList){
        double[] cumulativePercentages = new double[taskDataList.size()];
        double sum = 0;
        for (int i = 0; i < taskDataList.size(); i++) {
            sum += Double.valueOf(taskDataList.get(i));
            cumulativePercentages[i] = sum;
        }
        return cumulativePercentages;
    }
    private static int binarySearchForCumulativePercent(double[] cumulativePercents, double randomValue) {
        int low = 0;
        int high = cumulativePercents.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (cumulativePercents[mid] > randomValue) {
                if (mid == 0 || cumulativePercents[mid - 1] <= randomValue) {
                    return mid;
                }
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return -1; // 应该不会发生，因为randomValue的范围应保证在0到1之间
    }
}
