package com.zx.work.test.ctit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestUA {

    public static void main(String[] args) {
        String userAgent = "Mozilla/5.0 (Linux; Android 9)faf,; SM-G975F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.96 Mobile Safari/537.36";
        String osVersion = extractOSVersion(userAgent);
        System.out.println("操作系统版本：" + osVersion);
    }

    public static String extractOSVersion(String userAgent) {
        String osVersion = "";
        // 定义匹配操作系统版本的正则表达式
//        Pattern pattern = Pattern.compile("Android (\\d+\\.\\d+)");
        Pattern pattern = Pattern.compile("Android (\\d+(\\.\\d*)?)");
//        Pattern pattern = Pattern.compile("Android (\\d+)");
        Matcher matcher = pattern.matcher(userAgent);

        // 使用正则表达式进行匹配
        if (matcher.find()) {
            osVersion = matcher.group(1);
        }

        return osVersion;
    }
}
