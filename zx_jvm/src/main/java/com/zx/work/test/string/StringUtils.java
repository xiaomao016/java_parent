package com.zx.work.test.string;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;

import java.util.Map;

public class StringUtils {

    public static void toHexString() {
        String join = String.join(",", "a", "b", "c");
        System.out.println(join);

        String hs = Long.toHexString(10000);
        System.out.println(hs);

        long v = Long.valueOf(hs, 8);
        System.out.println(v);

    }

    public static void guava(){
        Map<String, String> stringStringMap = Maps.newHashMap();
        String join = Joiner.on("&")
                // 用指定符号代替空值,key 或者value 为null都会被替换
                .withKeyValueSeparator("=")
                .join(stringStringMap);
    }
}
