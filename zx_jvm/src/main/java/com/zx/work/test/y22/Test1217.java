package com.zx.work.test.y22;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zx.design.iterator.demo.ArrayList_;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/12/17 0017 17:49
 * @description:
 */
public class Test1217 {
    public static void main(String[] args) {
        List<Long> list = new ArrayList();

        list.add(1471771478470955008L);
        String re = JSON.toJSONString(list);

        JSONArray a = JSON.parseArray(re);

        List<Long> nlist = new ArrayList<>();


        Iterator<Object> iter = (Iterator<Object>) a.iterator();

        while(iter.hasNext()){
            nlist.add((Long) iter.next());
        }

        System.out.println(nlist.get(0));

        System.out.println(a.get(0));

        System.out.println(re);
    }
}
