package com.zx.work.test.vo;

/**
 * @author zx
 * @description:
 * @date 2022/6/10
 */

public class Stu610 {
    private String name;

    public Stu610(String name) {
        this.name = name;
    }

    public Stu610() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
