package com.zx.work.test.date;

import com.zx.work.utils.DateUtil2;
import com.zx.work.utils.DateUtilFinal;

import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/6/29
 */
public class T629 {
    public static void main(String[] args) {
        String dd = DateUtil2.nowDate2();
        System.out.println(dd);
        Date d = DateUtilFinal.getFixedHourDate(new Date());
        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(d));
    }

}
