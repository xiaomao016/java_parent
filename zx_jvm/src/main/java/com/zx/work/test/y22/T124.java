package com.zx.work.test.y22;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/1/24 0024 11:25
 * @description:
 */
public class T124 {

    //测试一个对象传入一个方法后，设置某些属性原来的对象是否改变
    public static void main(String[] args) {

        Domain d = new Domain();
        d.setRecentActiveDays(12);

        T124 t = new T124();
        t.addSearchTask(d);




    }

    public void addSearchTask(Domain d){
        setTimeRange(d);
        System.out.println(d.getRecentActiveDays());
    }

    public void setTimeRange(Domain d){
        d.setRecentActiveDays(1);
    }


}
