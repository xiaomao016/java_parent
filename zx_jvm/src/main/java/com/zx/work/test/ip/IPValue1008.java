package com.zx.work.test.ip;

/**
 * @author zx
 * @description:
 * @date 2022/9/28
 */
public class IPValue1008 {


    public static void main(String[] args) {
        System.out.println(parseIp("65.141.104.0")); //3232276861
        System.out.println(parseIp("192.168.161.125")); //3232276861

    }

    public static Long parseIp(String ipv4Address) {
        String[] ipStr = ipv4Address.trim()
                .split("\\.");
        if (ipStr.length != 4) {
            return 0L;
        }
        return Long.valueOf(ipStr[0]) * (1 << 24)
                + Long.valueOf(ipStr[1]) * (1 << 16)
                + Long.valueOf(ipStr[2]) * (1 << 8)
                + Long.valueOf(ipStr[3]);
    }


}


