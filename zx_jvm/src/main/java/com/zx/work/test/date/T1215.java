package com.zx.work.test.date;

import com.zx.work.utils.DateUtil;
import com.zx.work.utils.DateUtilFinal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class T1215 {
    public static void main(String[] args) {
        Date d = new Date();

        String f = DateUtilFinal.getFixedMinuteDate();
        System.out.println(f);

        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(DateUtilFinal.formatTime(f)));

        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(getFixedMinuteDate(new Date())));
        System.out.println("===========");
        Date now = getFixedMinuteDate(new Date());
        System.out.println(~2+1);
        Date endTime = DateUtilFinal.addMinutes(now,~2+1);
        Date startTime = DateUtilFinal.addMinutes(now,~7+1);

        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(endTime));
        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(startTime));

    }

    public static Date getFixedMinuteDate(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String fixFormat = dayFormat.format(date)+":00";

        try {
            return df.parse(fixFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
