package com.zx.work.test.y23;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class T0724 {
    public static void main(String[] args) {
        String input = "f555a0e0-5bfa-4386-bd2a-eb7745049fa3";
        String sha256Hash = getSHA256Hash(input);
        System.out.println("SHA-256 加密后的结果：" + sha256Hash);

        boolean re = true && true || true && true;
        boolean re2 = false && true || false && true;
        boolean re3 = false && true || false && false;
        System.out.println(re);
        System.out.println(re2);
        System.out.println(re3);
        Map<String,Double> map = new HashMap<>();
        Double a = map.get("score_min");

        System.out.println(a);

    }

    public static String getSHA256Hash(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedHash = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (byte b : encodedHash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
