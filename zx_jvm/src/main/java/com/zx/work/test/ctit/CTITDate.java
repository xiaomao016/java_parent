package com.zx.work.test.ctit;

import com.zx.juc.thinking.vo.D;
import com.zx.work.utils.DateUtilFinal;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class CTITDate {
    public static void main(String[] args) {
//        String ds = "2023-11-05 04:00:00";
        String ds = "2023-11-05 04:01:10";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(ds, formatter);

        LocalDateTime truncatedDateTime = dateTime.withMinute(0).withSecond(0);
        String result = truncatedDateTime.format(formatter);

        System.out.println(result);

        System.out.println("================");
        Date syncTime = DateUtilFinal.formatTime("2023-11-09 08:10:00");

        String year = DateUtilFinal.getDateFormatyyyy(syncTime);
        String month = DateUtilFinal.getDateFormatMM(syncTime);
        String day = DateUtilFinal.getDateFormatdd(syncTime);
        String hour = DateUtilFinal.getDateFormatHH(syncTime);
        String minute = DateUtilFinal.getDateFormatmm(syncTime);

        String re = String.join("/",year,month,day,hour,minute);
        System.out.println(re);




    }
}
