package com.zx.work.test.oss;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ListObjectsV2Request;
import com.aliyun.oss.model.ListObjectsV2Result;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;

import java.util.ArrayList;
import java.util.List;

public class TestOSSSG {

//    static String endpoint = "https://oss-ap-southeast-1.aliyuncs.com";
    static String endpoint = "https://oss-us-east-1.aliyuncs.com";
    static String accessKeyId = "LTAI5t767H4ioUA5LVFXZXha";
    static String accessKeySecret = "JqIweGRBfNHDekhsucr4Bovpr8MYSL";
    /**
     *  oss-key-id: LTAI5tAXW35fEVkFxhkRJUTt
     *   oss-key-secret: rWpwbWgP5GXprtueBkBohHmn06bLIv
     * @param args
     */
    public static void main(String[] args) {


//        listObjectFromPrefix("device/request/20240112/18/51/");
//        listObjectFromPrefix("pb_hbase/20240119/");
        List<String> strings = queryOssFileByBucket(null, "sorting/sorted_pkg_test/android/ZA/20240427/package=com.zhiliaoapp.musically/");
        System.out.println(JSON.toJSONString(strings));

        System.out.println(doesExist("sorting/sorted_pkg_test/android/ZA/20240427/package=com.zhiliaoapp.musically/part-00048.gz"));
    }

    /**
     * 获取指定下OSS的对象列表
     * @param prefix
     * @return
     */
    public static List<String> listObjectFromPrefix(String prefix){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        List<String> result = new ArrayList<>();
        try {
            // 列举文件。如果不设置keyPrefix，则列举存储空间下的所有文件。如果设置keyPrefix，则列举包含指定前缀的文件。
            ObjectListing objectListing = ossClient.listObjects("click-data-filter-mc", prefix);
            List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
            for (OSSObjectSummary s : sums) {
                result.add(s.getKey());
            }
            System.out.println(JSON.toJSONString(result));
        } catch (OSSException oe) {
//            log.info("OSSException,ErrorMessage:{},Error Code{},Request ID:{},Host ID:{}",
//                    oe.getErrorMessage(),oe.getErrorCode(),oe.getRequestId(),oe.getHostId());
            System.out.println(oe.getMessage());
        } catch (ClientException ce) {
//            log.error("listObjectFromPrefix ClientException ,Error Message:{},prefix:{}",ce.getMessage(),prefix);
            System.out.println(ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
        return result;
    }

    public static List<String> queryOssFileByBucket(String nextMarker, String ossPath) {
        OSSClientBuilder ossClientBuilder = new OSSClientBuilder();
        OSS ossClient = ossClientBuilder.build(endpoint, accessKeyId, accessKeySecret);

        ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request("bid-click-conversion");
        listObjectsV2Request.setPrefix(ossPath);
        listObjectsV2Request.setMaxKeys(100);
        ListObjectsV2Result result = ossClient.listObjectsV2(listObjectsV2Request);
        List<OSSObjectSummary> ossObjectSummaries = result.getObjectSummaries();

        List<String> keys = new ArrayList<>();
        for (OSSObjectSummary s : ossObjectSummaries) {
            if(s.getKey().endsWith("/")){
                continue;
            }
            keys.add(s.getKey());
        }
        return keys;
    }

    public static boolean doesExist(String ossPath) {
        OSSClientBuilder ossClientBuilder = new OSSClientBuilder();
        OSS ossClient = ossClientBuilder.build(endpoint, accessKeyId, accessKeySecret);

//        ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request("bid-click-conversion");
//        listObjectsV2Request.setPrefix(ossPath);
//        listObjectsV2Request.setMaxKeys(100);
        return ossClient.doesObjectExist("bid-click-conversion",ossPath);

    }
}
