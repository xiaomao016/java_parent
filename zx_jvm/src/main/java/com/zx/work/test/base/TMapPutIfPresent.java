package com.zx.work.test.base;

import org.checkerframework.checker.units.qual.C;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zx
 * @description:
 * @date 2022/8/5
 */
public class TMapPutIfPresent {
    public static void main(String[] args) throws InterruptedException {
        // 创建一个 HashMap
        Map<String, Long> deviceCountMap = new ConcurrentHashMap<>();

        deviceCountMap.put("1556530522857574402",0L);
        CountDownLatch countDownLatch = new CountDownLatch(1500);

        for(int i=0;i<1500;++i){
            new Thread(()->{
                for(int j=0;j<10000;++j) {
                      deviceCountMap.computeIfPresent("1556530522857574402", (k, v) -> v + 1);
                }
                countDownLatch.countDown();

            }).start();
        }

        countDownLatch.await();
        System.out.println(deviceCountMap.get("1556530522857574402"));


    }
}
