package com.zx.work.test.string;

import com.alibaba.fastjson.JSONObject;

/**
 * @author zx
 * @description:
 * @date 2022/7/27
 */
public class T27 {

    public static void main(String[] args) {
        String json = "{\"ad_type\":\"banner\",\"app_id\":\"com.opera.mini.native\",\"brand\":\"xiaomi\",\"country_code\":\"UK\",\"deviceId\":\"629df839-9d10-45b1-b4f4-471cdbd13b03\",\"deviceType\":4,\"exchange\":\"Epom-new\",\"extra1\":\"62d8dfe47df9b317fdfbbbf2nx\",\"extra2\":\"com.opera.mini.native\",\"extra3\":\"dvucnup2cabi\",\"ip\":\"79.67.112.153\",\"language\":\"en\",\"level\":\"info\",\"model\":\"M2004J19C\",\"msg\":\"\",\"network_type\":2,\"os_version\":\"10\",\"platform\":\"android\",\"pos_id\":0,\"price\":0.156,\"pub_id\":{\"id\":\"chvtb17xdn\"},\"size\":\"320x480\",\"time\":\"2022-07-21T13:11:00+08:00\",\"timestamp\":1658380260,\"user_agent\":\"Mozilla/5.0 (Linux; Android 10; M2004J19C Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/96.0.4664.104 Mobile Safari/537.36\"}";


        System.out.println(json);
        JSONObject vo = JSONObject.parseObject(json);
        //判断国家编码是否为UK，如果是UK，将UK替换为GB
        String isUk =  vo.getString("country_code");
        System.out.println(isUk);
        if(isUk.equalsIgnoreCase("UK")){
            vo.put("country_code","GB");
        }
        json = vo.toJSONString();
        System.out.println(json);

    }
}
