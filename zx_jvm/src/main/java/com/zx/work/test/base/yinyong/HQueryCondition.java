package com.zx.work.test.base.yinyong;

import lombok.Data;

import java.util.List;

@Data
public class HQueryCondition {
    private List<HWhereObject> whereList;
}
