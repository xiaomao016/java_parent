package com.zx.work.test.string;

import java.text.MessageFormat;

/**
 * @author zx
 * @description:
 * @date 2022/8/4
 */
public class T0804 {
    public static void main(String[] args) {
        String format = "oss/{0}/{1}";
        format = MessageFormat.format(format,"RU");
        System.out.println(format);
    }
}
