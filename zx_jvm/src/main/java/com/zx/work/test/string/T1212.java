package com.zx.work.test.string;

import akka.actor.FSM;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class T1212 {
    public static void main(String[] args) {

        List<ReportBeanDTO> list = Lists.newArrayList();

        setDayList("2022-12-11",list,null);

        System.out.println(JSON.toJSONString(list));

    }

    private static void setDayList(String fromDate, List<ReportBeanDTO> list, List<Integer> affiliateIds) {
        Map<String, Map<String, ReportBeanDTO>> dashboardDayList = getDataList();
        for (Map.Entry<String, Map<String, ReportBeanDTO>> dateMap : dashboardDayList.entrySet()) {
            if (dateMap.getKey().compareTo(fromDate) >= 0) {
                ReportBeanDTO reportBean = new ReportBeanDTO();
                reportBean.setDate(dateMap.getKey());
                for (Map.Entry<String, ReportBeanDTO> entry : dateMap.getValue().entrySet()) {
                    ReportBeanDTO bean = entry.getValue();
                    if ((affiliateIds == null || affiliateIds.contains(bean.getAffiliateId()))) {
                        setReport(bean, reportBean);
                    }
                }
                list.add(reportBean);
            }
        }
    }

    private static Map<String,Map<String,ReportBeanDTO>> getDataList(){
        String path = "C:\\Users\\dell\\Desktop\\临时\\12.12\\dashDay.txt";
        HashMap<String, Map<String, ReportBeanDTO>> result = new HashMap<>();
        try (BufferedReader buffer = new BufferedReader(new FileReader(path))) {
            String firstLine = buffer.readLine();
            System.out.println(firstLine);

            JSONObject jsonObject = JSONObject.parseObject(firstLine);
            for (String day : jsonObject.keySet()) {
                Map<String,JSONObject> value = (Map<String, JSONObject>) jsonObject.get(day);
                HashMap<String, ReportBeanDTO> map = new HashMap<>();
                for (String affiliateId : value.keySet()) {
                    JSONObject jsonObject1 = value.get(affiliateId);
                    ReportBeanDTO reportBean = JSONObject.parseObject(jsonObject1.toJSONString(), ReportBeanDTO.class);
                    map.put(affiliateId,reportBean);
                }
                result.put(day,map);
            }
            System.out.println(result.size());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private static void setReport(ReportBeanDTO source, ReportBeanDTO target) {
        target.setClickCount(addLong(source.getClickCount(), target.getClickCount()));
        target.setConversionCount(addInt(source.getConversionCount(), target.getConversionCount()));
        target.setPayout(addDecimal(source.getPayout(), target.getPayout()));
        target.setRevenue(addDecimal(source.getRevenue(), target.getRevenue()));
        target.setProfit(addDecimal(source.getProfit(), target.getProfit()));
    }

    private static  int addInt(Integer x, Integer y) {
        if (x == null && y == null) {
            return 0;
        } else if (x == null) {
            return y;
        } else if (y == null) {
            return x;
        } else {
            return x + y;
        }
    }

    private static long addLong(Long x, Long y) {
        if (x == null && y == null) {
            return 0;
        } else if (x == null) {
            return y;
        } else if (y == null) {
            return x;
        } else {
            return x + y;
        }
    }

    private static BigDecimal addDecimal(BigDecimal a, BigDecimal b) {
        if (a == null && b == null) {
            return BigDecimal.ZERO;
        } else if (a == null) {
            return b;
        } else if (b == null) {
            return a;
        } else {
            return a.add(b);
        }
    }

}
