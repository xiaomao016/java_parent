package com.zx.work.test.sql;

import com.zx.work.utils.DateUtilFinal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Date;

public class PBSQLCreate {
    public static void main(String[] args) {
        //数据库中最后一个分区的range值：(将时间戳转为秒 s)
        long timeStart = 1735833600L;
        long oneDaySeconds = 86400;
        String sqlHeader = "ALTER TABLE log_postback_bill ADD PARTITION (";
        String sqlFormat = "PARTITION {0} VALUES LESS THAN ({1})";
        String sqlEnd = ");";
        //起始分区日期字符串：20230101
        Date dStart = new Date(timeStart * 1000);
        System.out.println("======开始计算=====");

        String filePath = "E:\\back\\sql\\partitionSQL25.sql";
        long s = System.currentTimeMillis();
        createFileIfNotExist(filePath);
        try {
            FileWriter fw = new FileWriter(filePath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fw, 1024 * 1024);
            //写入sql前缀
            bufferedWriter.append(sqlHeader);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            int count =0;
            //写入分区sql
            for (int i = 0; i < 366; ++i) {
                Date dfu = DateUtilFinal.addDays(dStart, i);
                String param0 = "p" + DateUtilFinal.getDateFormatyyyyMMdd(dfu);
                String param1 = (timeStart + oneDaySeconds * (i+1)) + "";
                String sql = MessageFormat.format(sqlFormat, param0, param1);
                if(i<365){
                    sql = sql+",";
                }
                bufferedWriter.append(sql);
                bufferedWriter.newLine();
                bufferedWriter.flush();
                System.out.println(sql);
                count++;
            }
            //写入sql结尾
            bufferedWriter.append(sqlEnd);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            //关闭流
            fw.flush();
            fw.close();

            System.out.println(count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createFileIfNotExist(String filePath) {
        try {
            Path targetFile = Paths.get(filePath);
            if (!Files.exists(targetFile)) {
                Files.createFile(targetFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
