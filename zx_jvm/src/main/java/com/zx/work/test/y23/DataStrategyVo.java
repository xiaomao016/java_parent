package com.zx.work.test.y23;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataStrategyVo {

    private String dataId;  //如果是策略是S0定时，并且是多个的，就是逗号隔开的。  如果是S0 实时，就是一个

    private String dataStrategyCode;

    private Float percentage;

    private String dataSourceType;  //0是 定时更新的数据包  1 是ams实时。只有S0 有实时的情况


}
