package com.zx.work.test.ip;

import com.alibaba.fastjson.JSONObject;

import java.math.BigDecimal;

/**
 * @author zx
 * @description:
 * @date 2022/9/29
 */
public class IPQueryTest {

    private final static long MIX = 16777216L;
    private final static long MAX = 3758096383L;
    private final static String rangeKey = "ip-locations-range";
    private final static String GEONAMEID_KEY = "ip-geoname-id";

    private final static String[] keys = {"ip-country-locations-001", "ip-country-locations-002", "ip-country-locations-003"};
    private final static int nodes = 3;

    private final static long RANGE_SIZE = 1000000L;

    public static void main(String[] args) {

        System.out.println(parseIp("2601:701:c281:f680:99d6:4101:d09f:1635"));
        System.out.println(computeRangeKey(1746724839L));
        System.out.println(getCacheKey(1746698240L));
        System.out.println(getCacheKey("89.107.61.104"));//150w
        System.out.println(getCacheKey("111.239.160.0"));//200w
        System.out.println(getCacheKey("186.89.176.0"));//280w
        System.out.println(getCacheKey("196.60.78.0"));//300w
    }

    public static void samplefind(){
        //1.计算ip值
        String ipv4Address = "100.134.104.94";
        Long midNumber = parseIp(ipv4Address);
        System.out.println("ipv4长整型:" + midNumber); //1686530142
        //2.查看区间范围字符串:根据区间key和ip值  redisOperation.zrangeByScore(range_key, rangeValue)
        String range_key = computeRangeKey(midNumber);
        System.out.println(range_key); //ip-locations-range-1669
        //redis命令： ZRANGEBYSCORE ip-locations-range-481 1746724839 1000000000000000000000 LIMIT 0 1
        //1745611776~1745612287
        //在范围内,根据区间拿到ipFrom=1745611776

        //3.查询IPEntity实体
        //先根据ipFrom查询cacheKey(即哈希表的key，这里做了分区)
        String cacheKey = getCacheKey(1745611776L);
        System.out.println("cacheKey:"+cacheKey); //ip-country-locations-002

        //根据key=ip-country-locations-002 ,field=1745611776 去redis查询对应的
        //redis命令：hget ip-country-locations-002 1745611776
        //结果：{"ipFrom":1745611776,"ipTo":1745612287,"countryCode":"US","country":"United States","regionCode":"CA","region":"California","city":"Fullerton"}

    }

    //线上环境
    public static void sample(){
        //1.计算ip值
        String ipv4Address = "103.165.108.72";
        Long midNumber = parseIp(ipv4Address);
        System.out.println("ipv4长整型:" + midNumber); //1738894408
        //2.查看区间范围字符串:根据区间key和ip值  redisOperation.zrangeByScore(range_key, rangeValue)
        String range_key = computeRangeKey(midNumber);
        System.out.println(range_key); //ip-locations-range-1722
        //redis命令： ZRANGEBYSCORE ip-locations-range-1722 1738894408 1000000000000000000000 LIMIT 0 1
        //1739136768~1739136895
        //不在范围内，所以没查到
    }

    //测试环境
    public static void sample2(){
        //1.计算ip值
        String ipv4Address = "103.165.108.72";
        Long midNumber = parseIp(ipv4Address);
        System.out.println("ipv4长整型:" + midNumber); //1738894408
        //2.查看区间范围字符串:根据区间key和ip值  redisOperation.zrangeByScore(range_key, rangeValue)
        String range_key = computeRangeKey(midNumber);
        System.out.println(range_key); //ip-locations-range-1722
        //redis命令： ZRANGEBYSCORE ip-locations-range-1722 1738894408 1000000000000000000000 LIMIT 0 1
        //1738894336~1738894846
        //在范围：ipFrom=1738894336
        //cacheKey: ip-country-locations-002

    }

    public static String getCacheKey(Long ipFrom) {
        int index = (int) ((ipFrom - MIX) * nodes / (MAX - MIX));
        return keys[index];
    }

    public static String getCacheKey(String ipaddr) {
        long ipFrom = parseIp(ipaddr);
        int index = (int) ((ipFrom - MIX) * nodes / (MAX - MIX));
        return keys[index];
    }


    public static void selectByIp(String ipv4Address) {
        Long midNumber = parseIp(ipv4Address);
        System.out.println("ipv4长整型:" + midNumber); //2675341522
        selectOne(midNumber);
    }

    public static void selectOne(Long midNumber) {
        // 先拿到midNumber所在的区间
        String range = zrangeByScore(null, midNumber.doubleValue());
        System.out.println("range:" + range);
        if (range == null) {
            return;
        }
        String[] rangeSn = range.split("~");
        if (rangeSn.length != 2) {
            return;
        }
        // 验证是否在范围内，要验证的原因请看：IP2LocationRedisOperation#zrangeByScore方法的注释
        // [0] <= midNumber <= [1]
        if (midNumber.compareTo(Long.valueOf(rangeSn[0])) < 0
                || midNumber.compareTo(Long.valueOf(rangeSn[1])) > 0) {
            return;
        }
        // 再根据区间拿到记录的key
        String recordKey = rangeSn[0];
        System.out.println("recordKey:" + recordKey);
        // 根据key获取记录
        selectByKey(recordKey);
    }

    public static Long parseIp(String ipv4Address) {
        String[] ipStr = ipv4Address.trim()
                .split("\\.");
        if (ipStr.length != 4) {
            return 0L;
        }
        return Long.valueOf(ipStr[0]) * (1 << 24)
                + Long.valueOf(ipStr[1]) * (1 << 16)
                + Long.valueOf(ipStr[2]) * (1 << 8)
                + Long.valueOf(ipStr[3]);
    }

    private static String computeRangeKey(double value) {
        int index = (int) ((BigDecimal.valueOf(value).longValue() - MIX) / RANGE_SIZE);
        return rangeKey + "-" + index;
    }

    private static String zrangeByScore(String key, Double rangeValue) {
        String range_key = computeRangeKey(rangeValue);
        System.out.println("range_key:" + range_key); //ip-locations-range-2658
        System.out.println("range_value:" + rangeValue); //2675341522
//        String range_value = redisOperation.zrangeByScore(range_key, rangeValue); //2675341312~2675341567
        String range_value = "2675341312~2675341567";
//        if (range_value == null) {
        // 可能落在下一个分区了，因为范围可能刚好跨两个分区的分界
//            range_value = redisOperation.zrangeByScore(computeRangeKey(rangeValue + RANGE_SIZE), rangeValue);
//        }
        return range_value;
//        return "待返回";
    }

    private static void selectByKey(String recordKey) {
        String key = getCacheKey(Long.parseLong(recordKey));
        System.out.println("key:" + key);
//        String value = redisOperation.hget(key, recordKey);

        return;
    }
}
