package com.zx.work.test.number;

/**
 * 只有两个相同符号的整数相加的时候才会溢出，一正一负相加不会溢出，溢出后得到的结果符号一定与原值的符号相反。
 * 异或的规则是相同为0，不同为1，利用这个原理，将用运算符算出来的r与x和y分别异或，然后看符号位。
 * 例如：假设两个数都为正数，则两个数的符号位都为0，假设这两个数相加以后，如果溢出，则两个异或运算得到的数值符号位都是1，
 * 再将两个数运算得到的值相与，如果符号位不同，那么两个括号得到的数值符号位都是1。
 * 而与运算又是只有1&1才是1，有一个为0都是0。所以只要溢出，无论是正溢出负溢出，(x ^ r) & (y ^ r)计算出的结果符号位都为1(负数小于0)。

 */
public class T1212 {
    public static void main(String[] args) {
        int c1 = 12 * 100000000;
        int c2 = 12 * 100000000;
        System.out.println(isOverflow(c1,c2));
    }

    public static int isOverflow(int x, int y) {
        int r = x + y;
        if (((x ^ r) & (y ^ r)) < 0) {
            return Integer.MAX_VALUE;
        }
        return r;
    }

}
