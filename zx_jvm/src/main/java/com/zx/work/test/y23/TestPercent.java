package com.zx.work.test.y23;
import  java.util.ArrayList;
import  java.util.Collections;
import  java.util.List;

public class TestPercent {
    public  static  void  main(String[]  args)  {
        //  创建10个Task对象，并分配权重
        List<Task>  tasks  =  new  ArrayList<>();
        for  (int  i  =  0;  i  <  1;  i++)  {
            tasks.add(new  Task((int)  (Math.random()  *  100)  +  1));
        }

        //  创建TaskAssigner对象并分配文件
        TaskAssigner  assigner  =  new  TaskAssigner(tasks,  56);
        assigner.assignFiles();

        //  输出每个Task分配到的文件
        for  (Task  task  :  assigner.getTasks())  {
            System.out.println("Task  with  weight  "  +  task.getWeight()  +  "  got  files:  "  +  task.getFiles());
        }
    }

}

class  Task  {
    private  int  weight;
    private  List<Integer>  files;

    public  Task(int  weight)  {
        this.weight  =  weight;
        this.files  =  new  ArrayList<>();
    }

    public  int  getWeight()  {
        return  weight;
    }

    public  void  addFile(int  fileNumber)  {
        files.add(fileNumber);
    }

    public  List<Integer>  getFiles()  {
        return  files;
    }
}

class  TaskAssigner  {
    private  List<Task>  tasks;
    private  int  numberOfFiles;

    public  TaskAssigner(List<Task>  tasks,  int  numberOfFiles)  {
        this.tasks  =  tasks;
        this.numberOfFiles  =  numberOfFiles;
    }

    public  void  assignFiles()  {
        //  根据权重值对任务进行降序排序
        Collections.sort(tasks,  (task1,  task2)  ->  Integer.compare(task2.getWeight(),  task1.getWeight()));

        //  分配文件
        int  filesPerTask  =  numberOfFiles  /  tasks.size();
        int  remainingFiles  =  numberOfFiles  %  tasks.size();
        int  fileNumber  =  0;

        for  (int  i  =  0;  i  <  tasks.size();  i++)  {
            //  每个任务分配基础数量的文件
            for  (int  j  =  0;  j  <  filesPerTask;  j++)  {
                tasks.get(i).addFile(fileNumber++);
            }
            //  分配剩余的文件
            if  (remainingFiles  >  0)  {
                tasks.get(i).addFile(fileNumber++);
                remainingFiles--;
            }
        }
    }

    public  List<Task>  getTasks()  {
        return  tasks;
    }
}

