package com.zx.work.test.y22;

import com.zx.work.utils.EncryptUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/11/5 0005 11:14
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        String  s = "da5ad168-8de4-4366-ae27-1e02eb9dc1e5.tenant.1428243778583924736";

        String[]arr = s.split(".");
        for(String a:arr){
            System.out.println(a);
        }

        LocalDateTime deadLine = LocalDateTime.of(2021,11,18,15,50);
        boolean re  = Duration.between(deadLine,LocalDateTime.now()).isNegative();
        System.out.println(re);

        System.out.println(EncryptUtils.md5Password("1"));
        List<Integer> list = new ArrayList<>();
        list.add(1);

        StringBuffer buffer = new StringBuffer();
        buffer.append("a");
        StringBuilder builder = new StringBuilder();
        builder.append("b");


        final String FEEDBACK_REMIDER_TEMPLATE="编号为%s的反馈收到催办消息，请您尽快处理";
        System.out.println("===============");
        String sss = String.format(FEEDBACK_REMIDER_TEMPLATE,"1211111");
        System.out.println(sss);

     }

}
