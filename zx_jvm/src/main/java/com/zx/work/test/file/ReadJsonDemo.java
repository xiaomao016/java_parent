package com.zx.work.test.file;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

@Slf4j
public class ReadJsonDemo {
    public static void main(String[] args) {

        int count = 0;
        File file = new File("C:\\Users\\dell\\Desktop\\py_test_file\\json_file\\allpb_json_file");
        try (FileInputStream fis = new FileInputStream(file);
             InputStreamReader isr = new InputStreamReader(fis);
             BufferedReader br = new BufferedReader(isr)) {
            String line = br.readLine();
            while (null != (line = br.readLine())) {
                JSONObject result = JSON.parseObject(line);
                if(result.getString("eventName").equalsIgnoreCase("af_app_opened")){
                    count++;
                }

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        log.info("count:{}",count);
    }
}
