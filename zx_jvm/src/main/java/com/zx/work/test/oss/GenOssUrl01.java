package com.zx.work.test.oss;

/**
 * @author zx
 * @description:
 * @date 2022/10/20
 */
public class GenOssUrl01 {

    public static void main(String[] args) throws Throwable {
       OSSClientUtils ossClientUtils = new OSSClientUtils();
       ossClientUtils.init();

      String url =  ossClientUtils.genURL("test/info-send-0908.log");
        System.out.println(url);
    }

}


