package com.zx.work.test.number;

/**
 * @author zx
 * @description:
 * @date 2022/8/5
 */
public class T0805 {
    public static void main(String[] args) {
        int nv = 8;
        int ov = 8;
        System.out.println(nv!=ov);

        String cn = "ab";
        String cn1 = "cb";

        System.out.println(cn.compareToIgnoreCase(cn1)<0);
        Long a = 111111111111111111L;
        System.out.println(a.toString());

    }
}
