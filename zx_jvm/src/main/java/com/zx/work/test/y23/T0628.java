package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class T0628 {
    public static int calculateIntervalIndex(float number) {
        // 计算区间下标
        int intervalIndex = (int) Math.ceil(number) / 10 + 1;
        return intervalIndex;
    }

    public static void main(String[] args) {
        float number = 1f;  // 指定要计算的数
        int intervalIndex = calculateIntervalIndex(number);
        System.out.println("该数属于第 " + intervalIndex + " 个区间");


        float number1 = 9.9f;  // 指定要计算的数
        int intervalIndex1 = calculateIntervalIndex(number1);
        System.out.println("该数属于第 " + intervalIndex1 + " 个区间");

        float number2 = 10.1f;  // 指定要计算的数
        int intervalIndex2 = calculateIntervalIndex(number2);
        System.out.println("该数属于第 " + intervalIndex2 + " 个区间");

        float number3 = 10.8f;  // 指定要计算的数
        int intervalIndex3 = calculateIntervalIndex(number3);
        System.out.println("该数属于第 " + intervalIndex3 + " 个区间");

        Map<Integer, Long> rangeCount = new ConcurrentHashMap<>();
        rangeCount.put(1,1L);
        for(int i=0;i<10;++i) {
            if (rangeCount.containsKey(i)) {
                rangeCount.computeIfPresent(i, (k, v) -> v + 1);
            } else {
                rangeCount.put(i, 1L);
            }

        }
        System.out.println(JSON.toJSONString(rangeCount));

        System.out.println("===========");
        long timeLeft = 7 * 24 * 60 * 60 * 1000;
        System.out.println(timeLeft);
        System.out.println(Long.MAX_VALUE);
    }
}
