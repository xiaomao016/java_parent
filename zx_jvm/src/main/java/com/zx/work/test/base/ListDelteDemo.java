package com.zx.work.test.base;

import com.zx.work.test.staticField.LogPostbackBillBlockReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ListDelteDemo {
    public static void main(String[] args) {

        List<LogPostbackBillBlockReport> allInstalls = new ArrayList<>();

        for (int i = 0; i < 1000000; i++) {
            LogPostbackBillBlockReport report = new LogPostbackBillBlockReport();
            report.setCountry("Country_" + i);
            report.setProd("Prod_" + i);
            report.setCreateTime(new Date());
            report.setInstalls((long) (Math.random() * 200)); // 随机生成 0~199 之间的整数
            allInstalls.add(report);
        }

        Iterator<LogPostbackBillBlockReport> iterator = allInstalls.iterator();
        while (iterator.hasNext()) {
            LogPostbackBillBlockReport report = iterator.next();
            if (report.getInstalls() < 100L) {
                iterator.remove();
            }
        }

        System.out.println(allInstalls.size());

    }
}
