package com.zx.work.test.ip.util;


import java.util.concurrent.ThreadLocalRandom;

/**
 * @author zx
 * @description: ipv4地址封装类
 * @date 2022/10/11
 * 根据CIDR格式构建ipv4对象
 * IP地址4字节
 * 示例：
 * 那么 从 172.168.16.10/25 可以得到的信息如以下 (地址数量：2^7 = 128)
 * Ipv4 地址 = 172.168.16.10
 * 子网掩码 = 255.255.255.128
 * 网络地址 = 172.168.16.0  （IP地址和子网掩码按位与可以得到网络地址，这也是子网掩码的作用）
 * 广播地址 = 172.168.16.127 (网络地址为网段的最大地址)
 * 地址范围 = 172.168.16.0 - 172.168.16.127
 * 可使用地址 = 172.168.16.1 - 172.168.16.126
 *
 *
 * 在Java中，Integer类型是一个32位的有符号整数，表示范围是从-2,147,483,648到2,147,483,647。
 * 一个IPv4地址是一个32位的数，但通常它是无符号的，范围是从0到4,294,967,295 (即0x00000000到0xFFFFFFFF).
 *
 * 如果你直接将一个IPv4地址转换成一个Integer，那么IPv4地址的高位如果是1（也就是说，如果这个IPv4地址大于或等于128.0.0.0），
 * 那么转换成的Integer会是一个负值，因为在Java中Integer是有符号的。然而，这并不是说Integer会溢出，因为IPv4地址的大小是适合放在32位数中的。
 * 只是说，当你将IP地址视为一个无符号数时，使用Integer来存储可能会导致理解上的混淆或错误，因为Integer会把高位的IP地址解释为负数。
 *
 * 这就是为什么在处理IP地址时通常使用long类型来避免符号问题，或者使用BigInteger来确保你可以作无符号的操作。
 * 在Java 8及以上版本，也可以使用InetAddress类来代表IP地址，这样可以避免直接处理整数转换的问题。
 *
 */
public class IPV4 {

    private int baseIPNumeric;
    private int netmaskNumeric;

    /**
     * 传入IP的CIDR格式：即Classless Inter-Domain Routing 无差别网段路由
     *
     * @param IPinCIDRFormat
     * @throws NumberFormatException
     */
    public IPV4(String IPinCIDRFormat) throws NumberFormatException {

        String[] st = IPinCIDRFormat.split("\\/");
        if (st.length != 2)

            throw new NumberFormatException("Invalid CIDR format '"
                    + IPinCIDRFormat + "', should be: xx.xx.xx.xx/xx");

        String symbolicIP = st[0];
        String symbolicCIDR = st[1];

        Integer numericCIDR = new Integer(symbolicCIDR);
        if (numericCIDR > 32)
            throw new NumberFormatException("CIDR can not be greater than 32");

        //计算IP
        st = symbolicIP.split("\\.");
        if (st.length != 4)
            throw new NumberFormatException("Invalid IP address: " + symbolicIP);

        int i = 24;
        baseIPNumeric = 0;
        for (int n = 0; n < st.length; n++) {
            int value = Integer.parseInt(st[n]);
            //确保在+0~255内
            if (value != (value & 0xff)) {
                throw new NumberFormatException("Invalid IP address: " + symbolicIP);
            }
            baseIPNumeric += value << i;
            i -= 8;
        }

        //计算掩码 netmask from CIDR A类地址网络号+前缀=8，所以掩码位数不会小于8
        if (numericCIDR < 8)
            throw new NumberFormatException("Netmask CIDR can not be less than 8");
        //用4字节全1左移计算
        netmaskNumeric = 0xffffffff;
        netmaskNumeric = netmaskNumeric << (32 - numericCIDR);
    }

    /**
     * 根据ip地址值获取点分10进制的写法表达式
     *
     * @param ip
     * @return
     */
    private String convertNumericIpToSymbolic(Integer ip) {
        StringBuffer sb = new StringBuffer(15);
        for (int shift = 24; shift > 0; shift -= 8) {
            // 处理3字节，从高往低
            sb.append((ip >>> shift) & 0xff);
            sb.append('.');
        }
        sb.append(ip & 0xff);
        return sb.toString();
    }

    private static String convertNumericIpToSymbolic(Long ip) {
        StringBuffer sb = new StringBuffer(15);
        for (int shift = 24; shift > 0; shift -= 8) {
            // 处理3字节，从高往低
            sb.append((ip >>> shift) & 0xff);
            sb.append('.');
        }
        sb.append(ip & 0xff);
        return sb.toString();
    }


    /**
     * 获取网段第一个可用ip （排除网络地址）
     * @return
     */
    public Long getFirstIpNumeric() {
        Integer baseIP = baseIPNumeric & netmaskNumeric;
        String firstIP = convertNumericIpToSymbolic(baseIP + 1);
        return parseIp(firstIP);
    }

    /**
     * 获取网段第一个可用ip （排除广播地址）
     * @return
     */
    public Long getLastIpNumeric() {
        int numberOfBits;
        for (numberOfBits = 0; numberOfBits < 32; numberOfBits++) {
            if ((netmaskNumeric << numberOfBits) == 0)
                break;
        }
        Integer numberOfIPs = 0;
        for (int n = 0; n < (32 - numberOfBits); n++) {
            numberOfIPs = numberOfIPs << 1;
            numberOfIPs = numberOfIPs | 0x01;
        }
        Integer baseIP = baseIPNumeric & netmaskNumeric;
        String lastIP = convertNumericIpToSymbolic(baseIP + numberOfIPs - 1);
        return parseIp(lastIP);
    }

    /**
     * 获取IP值范围字符串
     *
     * @return
     */
    public String getHostAddressDecimalRange() {
        int numberOfBits;
        for (numberOfBits = 0; numberOfBits < 32; numberOfBits++) {
            if ((netmaskNumeric << numberOfBits) == 0)
                break;
        }
        Integer numberOfIPs = 0;
        for (int n = 0; n < (32 - numberOfBits); n++) {
            numberOfIPs = numberOfIPs << 1;
            numberOfIPs = numberOfIPs | 0x01;
        }
        Integer baseIP = baseIPNumeric & netmaskNumeric;
        String firstIP = convertNumericIpToSymbolic(baseIP + 1);
        String lastIP = convertNumericIpToSymbolic(baseIP + numberOfIPs - 1);
        return parseIp(firstIP) + "~" + parseIp(lastIP);
    }

    public String getHostAddressRange() {

        int numberOfBits;
        for (numberOfBits = 0; numberOfBits < 32; numberOfBits++) {

            if ((netmaskNumeric << numberOfBits) == 0)
                break;
        }
        Integer numberOfIPs = 0;
        for (int n = 0; n < (32 - numberOfBits); n++) {

            numberOfIPs = numberOfIPs << 1;
            numberOfIPs = numberOfIPs | 0x01;

        }

        Integer baseIP = baseIPNumeric & netmaskNumeric;
        String firstIP = convertNumericIpToSymbolic(baseIP + 1);
        String lastIP = convertNumericIpToSymbolic(baseIP + numberOfIPs - 1);
        return firstIP + " - " + lastIP;
    }


    /**
     * 获取网段全部IP地址总数
     *
     * @return
     */
    public Long getNumberOfHosts() {
        //获取掩码位数
        int numberOfBits;
        for (numberOfBits = 0; numberOfBits < 32; numberOfBits++) {
            if ((netmaskNumeric << numberOfBits) == 0)
                break;
        }
        Double x = Math.pow(2, (32 - numberOfBits));
        if (x == -1)
            x = 1D;

        return x.longValue();
    }

    /**
     * 获取IP地址值
     *
     * @return
     */
    public String getIP() {
        return convertNumericIpToSymbolic(baseIPNumeric);
    }

    /**
     * 这里解析IP为long型，因为整形表示会有溢出情况。
     *
     * @param ipv4Address
     * @return
     */
    public static Long parseIp(String ipv4Address) {
        String[] ipStr = ipv4Address.trim()
                .split("\\.");
        if (ipStr.length != 4) {
            return 0L;
        }
        return Long.valueOf(ipStr[0]) * (1 << 24)
                + Long.valueOf(ipStr[1]) * (1 << 16)
                + Long.valueOf(ipStr[2]) * (1 << 8)
                + Long.valueOf(ipStr[3]);
    }

    private  String getAnotherRandomIPInSameRange(){
        String originIp = getIP();
        long firstIp = getFirstIpNumeric();
        int range = getLastIpNumeric().intValue() - getFirstIpNumeric().intValue() -2;
        String newIp = null;
        while (true){
            int random = ThreadLocalRandom.current().nextInt(range) +1;
            Long ip = firstIp + random;
            newIp = convertNumericIpToSymbolic(ip);
            if(newIp.equals(originIp)){
                continue;
            }
            break;
        }
        return  newIp;
    }

    public static void main(String[] args) {
        IPV4 ipv4 = new IPV4("116.23.227.95/22");
        System.out.println(ipv4.getNumberOfHosts());
        System.out.println(ipv4.getHostAddressDecimalRange());
        System.out.println(ipv4.getHostAddressRange());
        System.out.println(ipv4.getIP());
        System.out.println(ipv4.getFirstIpNumeric());
        System.out.println(ipv4.getLastIpNumeric());

        System.out.println("================");
        IPv4Origin iPv4Origin = new IPv4Origin("103.4.156.0/22");
        System.out.println(iPv4Origin.getNumberOfHosts());
        System.out.println(iPv4Origin.getHostAddressRange());
        System.out.println(iPv4Origin.getIP());

        System.out.println("==============");
        String newIp = ipv4.getAnotherRandomIPInSameRange();
        System.out.println(newIp);
    }


}

