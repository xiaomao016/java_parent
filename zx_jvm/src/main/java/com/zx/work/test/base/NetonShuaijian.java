package com.zx.work.test.base;

public class NetonShuaijian {


    /**
     * //30天的秒数
     * time_second = 30*24*60*60 = 2592000
     *
     * //将30天衰减值代入
     * 0.033 = e^(-α*time_second)=e^(-α*2592000)
     *
     * //两边同时取对数
     * lg(0.033) = -α*2592000
     * //计算可以得到α值
     * α = 2.6650*10e-6
     * @param args
     */
    public static void main(String[] args) {

        System.out.println(30*24*60*60);
        double speed = 1/1000D;
        System.out.println(speed);
        double alph = Math.log(speed) / 2592000;
        System.out.println(alph);

        double al = 1.3160677922514106e-6;
        System.out.println(al);

        double score = Math.exp(-2592000 * al);

        System.out.println(score);


    }


    //
    public static double f(long x, double oldScore) {
        double alpha = -0.00000266502904282;
        double pa = alpha * x;
        return oldScore * Math.exp(pa);
    }
}
