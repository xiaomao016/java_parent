package com.zx.work.test.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/6/27
 */
public class T0627 {
    public static void main(String[] args) {
        Date currentDate = new Date();
        String suffix = format(addMonths(currentDate, -1), DatePattern.yyyyMM);
        System.out.println(suffix);

        System.out.println(new Date(getZeroPointTimeMillis(System.currentTimeMillis())));
    }
    public static Date addMonths(Date date, int months) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(2, months);
        return c.getTime();
    }
    public static String format(Date date, DatePattern pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern.pat);
        return sdf.format(date);
    }

    //计算该时间所在天零点的时间戳
    public static Long getZeroPointTimeMillis(Long cur) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    public static enum DatePattern {
        yyyyMM("yyyyMM"),
        yyyyMMdd("yyyyMMdd"),
        yyyyMMddHH("yyyyMMddHH"),
        yyMMddHH("yyMMddHH"),
        yyyyMMddHHmmss("yyyyMMddHHmmss"),
        yyyy_MM("yyyy-MM"),
        yyyy_MM_dd("yyyy-MM-dd"),
        yyyy_MM_dd_HH("yyyy-MM-dd-HH"),
        yyyy_MM_dd_HH_mm_ss("yyyy-MM-dd HH:mm:ss");

        private String pat;

        private DatePattern(String pat) {
            this.pat = pat;
        }
    }
}
