package com.zx.work.test.file;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author zx
 * @description:
 * @date 2022/8/9
 */
public class TfileWrite {
    public static void main(String[] args) {

        String filePath = "E:\\back\\test\\aa.txt";
        long s = System.currentTimeMillis();
        createFileIfNotExist(filePath);
        try {
            FileWriter fw = new FileWriter(filePath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fw,1024*1024);
            for (int i = 0; i < 10000000; ++i) {
                String jsonStr = "this is a content:" + i;
                bufferedWriter.append(jsonStr);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("time cost:" + (System.currentTimeMillis() - s));


    }

    private static void createFileIfNotExist(String filePath) {
        try {
            Path targetFile = Paths.get(filePath);
            if (!Files.exists(targetFile)) {
                Files.createFile(targetFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
