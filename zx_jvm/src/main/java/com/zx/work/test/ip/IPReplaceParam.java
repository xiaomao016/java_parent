package com.zx.work.test.ip;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPReplaceParam {
    private static final Pattern IP_PATTERN = Pattern.compile("(ip=)([^&]*)");
    private static final Pattern COUNTRY_CODE_PATTERN = Pattern.compile("(country_code=)([^&]*)");

    private static String replaceSomeParameters(String sendUrl, String ip, String countryCode) {
        try {
            Matcher matcher = IP_PATTERN.matcher(sendUrl);
            String newUrl = matcher.replaceFirst("$1" + ip);
            matcher = COUNTRY_CODE_PATTERN.matcher(newUrl);
            newUrl = matcher.replaceFirst("$1" + countryCode);
            return newUrl;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendUrl;
    }

    public static void main(String[] args) {
        String originalUrl = "https://example.com/?ip=123.45.67.89&country_code=US";
        String replacedUrl = replaceSomeParameters(originalUrl, "127.0.0.1", "CN");
        System.out.println("Replaced URL: " + replacedUrl);
    }
}
