package com.zx.work.test.base.yinyong;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zx
 * @since 2022-05-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class McUpload extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String country;

    private String os;

    private Long deviceCount;

    /**
     * 选择的标签
     */
    private String tags;

    /**
     * 给这个数据包起的名字
     */
    private String deviceName;

    /**
     * 保存在oss上的路径
     */
    private String ossSavedUrl;

    private String isUpdate;

    private String remark;

    private String status;

    private String appId;

    /**
     * 一天的预估量
     */
    private Long countDay;

    private String changeV;

    /**
     * 定时任务id
     */
    private Integer xxlId;

    private String dataSourceType;//0：普通的定时更新的，1：实时的

    private String isDataLack;//0：正常数据，1：ams空ip数据

    private HQueryCondition hQueryCondition;
}
