package com.zx.work.test.y22;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/12/27 0027 18:31
 * @description:
 */
public class Test1227 {
    public static void main(String[] args) {

        Set<String>  ids = new HashSet<>();
        String pre = "14422121";
        String pre2 = "144444;155555";

        //测试分解
        String[]arr  = pre.split(";");
        int len = arr.length;
        System.out.println(len);
        Arrays.stream(arr)
                .forEach(a->ids.add(a));
        //测试组合

        ids.add("15555");

        StringBuilder sb = new StringBuilder();
        Iterator<String> iter = ids.iterator();
        while(iter.hasNext()){
            sb.append(iter.next()+";");
        }
        String handlerList = sb.toString().trim();

        handlerList = handlerList.substring(0,handlerList.lastIndexOf(";"));

        System.out.println(handlerList);

        double d = 18d/12d * 2;
        double d2 = (3d+4d/31d) * 3d;
        System.out.println(d);
        System.out.println(d2);

    }
}
