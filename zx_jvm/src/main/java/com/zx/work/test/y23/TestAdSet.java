package com.zx.work.test.y23;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class TestAdSet {
    public static void main(String[] args) {
        String offerId = "27626";
        String SPLIT = "|";
        String affId = "10110006";
        String affSub = "";
        String code = "SA";
        String pkg = "878577184";
        String adx = "";
        String osv= "14";
        String dsId = "";
        String afSub2 = "aa";
        StringBuilder sb = new StringBuilder();
        sb.append(offerId).append(SPLIT).append(affId).append("_")
                .append(affSub).append(SPLIT).append(code)
                .append(SPLIT).append(pkg).append(SPLIT).append(adx)
                .append(SPLIT).append(osv).append(SPLIT)
                .append(dsId)
                .append(SPLIT)
                .append(afSub2);
        Byte b = '*';
        Base64.Encoder urlEncoder = Base64.getUrlEncoder().withoutPadding();
        String afAdset = new String(urlEncoder.encode(compress(sb.toString().getBytes(), b)));
        //log.info("iosPrivacyGuard set,afAdset:{},affclickid:{}", afAdset, affiliateTrack.getAffClickId());
        System.out.println(afAdset);

        //new String(urlEncoder.encode(CodeUtils.compress(sb.toString().getBytes(), b)));
        String t = "KngBHckxCoAwDAXQK-U3-Wm76znEQacgRXEQcnjF5S1PIWj5ARHxRTt7pYCS05zNTJRWmOsY1x7PduZxRyRYC7ybmhdobXT88QIr4BXf";
        byte[] uncompress = uncompress(Base64.getUrlDecoder().decode(t));

        String[] params = new String(uncompress).split("\\|");
        System.out.println("aaa");
    }

    public static byte[] uncompress(byte[] decode) {
        int num = (decode.length - 1) / 128 + 1;
        byte[] data = new byte[decode.length - num];
        int remainder = decode.length % 128;
        remainder = remainder == 0 ? 128 : remainder;

        for(int i = 0; i < num; ++i) {
            int length = i < num - 1 ? 127 : remainder - 1;
            System.arraycopy(decode, i * 128 + 1, data, i * 127, length);
        }

        Inflater decompressor = new Inflater();

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Throwable var6 = null;

            try {
                decompressor.setInput(data);
                byte[] buf = new byte[128];
                int loop = 0;

                while(!decompressor.finished() && loop < 10) {
                    ++loop;
                    int count = decompressor.inflate(buf);
                    bos.write(buf, 0, count);
                }

                byte[] var35;
                if (loop >= 10) {
                    var35 = null;
                    return var35;
                }

                var35 = bos.toByteArray();
                return var35;
            } catch (Throwable var29) {
                var6 = var29;
                throw var29;
            } finally {
                if (bos != null) {
                    if (var6 != null) {
                        try {
                            bos.close();
                        } catch (Throwable var28) {
                            var6.addSuppressed(var28);
                        }
                    } else {
                        bos.close();
                    }
                }

            }
        } catch (Exception var31) {
            var31.printStackTrace();
        } finally {
            decompressor.end();
        }

        return null;
    }
    public static byte[] compress(byte[] data, Byte b) {
        Deflater compressor = new Deflater(1);

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Throwable var4 = null;

            try {
                compressor.setInput(data);
                compressor.finish();
                byte[] buf = new byte[128];

                while(!compressor.finished()) {
                    if (b == null) {
                        b = (byte)(ThreadLocalRandom.current().nextInt(256) - 128);
                    }

                    buf[0] = b;
                    int count = compressor.deflate(buf, 1, buf.length - 1);
                    bos.write(buf, 0, count + 1);
                }

                byte[] var28 = bos.toByteArray();
                return var28;
            } catch (Throwable var24) {
                var4 = var24;
                throw var24;
            } finally {
                if (bos != null) {
                    if (var4 != null) {
                        try {
                            bos.close();
                        } catch (Throwable var23) {
                            var4.addSuppressed(var23);
                        }
                    } else {
                        bos.close();
                    }
                }

            }
        } catch (IOException var26) {
            var26.printStackTrace();
        } finally {
            compressor.end();
        }

        return null;
    }

    public static byte[] intToByteArray(int number) {
        byte[] byteArray = new byte[4];

        byteArray[0] = (byte) (number >> 24);
        byteArray[1] = (byte) (number >> 16);
        byteArray[2] = (byte) (number >> 8);
        byteArray[3] = (byte) number;

        return byteArray;
    }
}
