package com.zx.work.test.date;

import com.zx.work.utils.DateUtilFinal;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class DateSubDemo {

    public static void main(String[]ags){

        Date c_t = DateUtilFinal.formatTime("2023-10-10 10:00:00");
        Date c_i = DateUtilFinal.formatTime("2023-10-12 10:11:01");

        log.info("c_t:{},c_i:{}",DateUtilFinal.getDateFormatyyyyMMddHHmmss(c_i),DateUtilFinal.getDateFormatyyyyMMddHHmmss(c_i));

        long l = calcCTIT(c_t, c_i);
        log.info("ctit:{}",l);


    }


    private static long calcCTIT(Date clickTime, Date createTime) {

        long C_30_SECONDS = 30L;
        long C_5_MINUTES = 5 * 60L;
        long C_2_HOURS = 2 * 60 * 60L;
        long C_48_HOURS = 48 * 60 * 60L;
        long C_72_HOURS = 72 * 60 * 60L;
        long C_MORE_THAN_72_HOURS = 72 * 60 * 60L + 1;

        // 计算两个日期之间的秒数差
        long diffMilliseconds = Math.abs(createTime.getTime() - clickTime.getTime());
        long diffSeconds = diffMilliseconds / 1000;

        log.info("diffMilliseconds:{},diffSeconds:{}",diffMilliseconds,diffSeconds);

        if(diffSeconds<=C_30_SECONDS){
            return C_30_SECONDS;
        }

        if(diffSeconds<=C_5_MINUTES){
            return C_5_MINUTES;
        }

        if(diffSeconds<=C_2_HOURS){
            return C_2_HOURS;
        }

        if(diffSeconds<=C_48_HOURS){
            return C_48_HOURS;
        }

        if(diffSeconds<=C_72_HOURS){
            return C_72_HOURS;
        }

        return C_MORE_THAN_72_HOURS;
    }

}
