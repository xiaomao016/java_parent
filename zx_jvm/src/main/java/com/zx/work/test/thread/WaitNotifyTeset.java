package com.zx.work.test.thread;

import java.util.concurrent.atomic.AtomicBoolean;

public class WaitNotifyTeset {

    private static AtomicBoolean flag = new AtomicBoolean(true);
    private static Object lockA = new Object();
    private static Object lockB = new Object();

    public static void main(String[] args) {


        Thread a = new Thread(() -> {
            something();
        }
        );
        a.start();

        Thread b = new Thread(() -> {
            waitTask();
        });
        b.start();


    }

    private static void something() {
        synchronized (lockA) {
            try {
                while(true) {
                    System.out.println("第一步");
                    Thread.sleep(1000 * 3);
                    if (!flag.get()) {
                        lockA.wait();
                    }
                    System.out.println("第二步");
                    Thread.sleep(1000 * 3);
                    System.out.println("最后一步");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private static void waitTask() {

        try {
            flag.set(false);
            System.out.println("B执行:"+flag.get());
            Thread.sleep(1000 * 4);
            flag.set(true);
            notifyTask();
            System.out.println("更新状态:"+flag.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void notifyTask() {
        Thread thread = new Thread(()->{
            synchronized (lockA) {
                lockA.notify();
                System.out.println("唤醒");
            }
        }, "notifyTaskToRunning");
        thread.start();
    }
}
