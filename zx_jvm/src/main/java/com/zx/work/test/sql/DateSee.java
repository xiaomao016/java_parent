package com.zx.work.test.sql;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zx.work.utils.DateUtilFinal;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
public class DateSee {
    public static void main(String[] args) {
        // 获取当前时间
        LocalDateTime currentTime = LocalDateTime.now();
        // 获取7天前的时间
        LocalDateTime sevenDaysAgo = currentTime.minusDays(7).withHour(0).withMinute(0).withSecond(0).withNano(0);

        String s = DateUtilFinal.formatWithLocalDate(sevenDaysAgo);

        System.out.println("7天前的那天的零点时间为：" + s);
    }
}

class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
