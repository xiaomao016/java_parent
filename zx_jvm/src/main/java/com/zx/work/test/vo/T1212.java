package com.zx.work.test.vo;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.BeanUtils;

public class T1212 {
    public static void main(String[] args) {
        ReportBean reportBean = new ReportBean();
        reportBean.setClickTime("2022");
        reportBean.setClickCount(122000);
        reportBean.setDate("data");


        ReportBeanDTO dto = new ReportBeanDTO();
        BeanUtils.copyProperties(reportBean,dto);

        dto.setClickCount(Long.valueOf(reportBean.getClickCount()));
        System.out.println(JSON.toJSONString(dto));


    }

    private static Long addInt(Integer x, Long y) {
        if (x == null && y == null) {
            return 0L;
        } else if (x == null) {
            return y;
        } else if (y == null) {
            return Long.valueOf(x);
        } else {
            return x + y;
        }
    }
}
