package com.bid.read.config;


import com.alibaba.fastjson.JSON;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import javax.annotation.PostConstruct;
import java.util.Properties;

/**
 *
 */
public class KafkaPublicProducer {
    @Value("${aliyun.kafka.bootstrap.public.servers}")
    private String bootstrapServers;
    @Value("${aliyun.kafka.sslTruststoreLocation}")
    private String sslTruststoreLocation;

    private static final Logger log = LoggerFactory.getLogger(KafkaPublicProducer.class);

    private KafkaProducer<String, String> producer;

    @PostConstruct
    public void initKafkaPublicProducer() {

        Properties props = new Properties();
        //设置接入点，请通过控制台获取对应Topic的接入点。
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        //* 与sasl路径类似，该文件也不能被打包到jar中。
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, sslTruststoreLocation);
        //* 根证书store的密码，保持不变。
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "KafkaOnsClient");
        //* 接入协议，目前支持使用SASL_SSL协议接入。
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
        // SASL鉴权方式，保持不变。
        props.put(SaslConfigs.SASL_MECHANISM, "PLAIN");

        //消息队列Kafka版消息的序列化方式。
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        //请求的最长等待时间。
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, 30 * 1000);
        //设置客户端内部重试次数。
        props.put(ProducerConfig.RETRIES_CONFIG, 5);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384); //64K
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1200); //1秒*/
        //设置客户端内部重试间隔。
        props.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, 3000);
        //构造Producer对象，注意，该对象是线程安全的，一般来说，一个进程内一个Producer对象即可。
        //如果想提高性能，可以多构造几个对象，但不要太多，最好不要超过5个。
        producer = new KafkaProducer<String, String>(props);
    }
    public void sendPublicMessage(String topic, Object message) {
        try {
            ProducerRecord<String, String> kafkaMessage = new ProducerRecord<String, String>(topic, JSON.toJSONString(message));
            producer.send(kafkaMessage);
        } catch (Exception e) {
            log.error("kafka sendMessage Exception");
            log.error(e.getMessage(), e);
        }

    }
}
