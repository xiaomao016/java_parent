package com.zx.work.test.y22;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Sets;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 类说明
 *
 * @author zx
 * @date 2022/4/11
 */
public class TestTime {
    public static void main(String[] args) {

        Map<String, Set<String>> map = new HashMap<>();
        map.put("zx", Sets.newHashSet("zhangxu"));

        if(map.containsKey("zx")){
            map.get("zx").add("xiaomao");
        }

        System.out.println(map.get("zx").size());
        System.out.println("".toLowerCase());

        System.out.println(getCount());

        List<String> list = new LinkedList<>();
        list.add("expertCVR");
        list.add("expertSpec");
        list.add("expert");

        String s  = JSON.toJSONString(list);
        System.out.println(s);

    }

    public static int getCount(){
        int dayCount = 15;
        int splits = dayCount;
        double mb = 860;
        if(mb>50) {
            int extraCount = (int) Math.pow(mb/50,2);
            splits =Math.min(dayCount+extraCount,300);
        }
        return splits;
    }

    public static void timeMillToDate(){
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        long millis  = 1648577340000L;
        long millis2 = 1649665935932L;
        Date date=new Date(millis);
        Date date2=new Date(millis2);
        System.out.println(sd.format(date));
        System.out.println(sd.format(date2));

    }
}
