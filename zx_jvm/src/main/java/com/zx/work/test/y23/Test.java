package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.lindorm.thirdparty.netty.util.internal.StringUtil;
import com.google.common.collect.Lists;
import com.zx.base.basic.Student;
import com.zx.work.utils.DateUtilFinal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
@Slf4j
public class Test {
    public static void main(String[] args) {


        // 创建一个示例的时间映射
        Map<String, Long> timeMap = new ConcurrentHashMap<>();
        timeMap.put("OrderA", 5L);
        timeMap.put("OrderB", 10L);
        timeMap.put("OrderC", 3L);
        timeMap.put("OrderD", 8L);
        String a = null;
        if (timeMap != null && timeMap.containsKey(a)) {
            System.out.println("aa");
        }
    }



    private static String getOrderListStr(Map<String, Long> timeMap) {
        String sortedKeys = timeMap.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(","));
        return sortedKeys;
    }

    public static int bytesToInt(byte[] bytes) {
        return ((bytes[3] & 255) << 24) + ((bytes[2] & 255) << 16) + ((bytes[1] & 255) << 8) + (bytes[0] & 255);
    }

    private static boolean isFieldNotEmpty(String field){
        if(!isNull(field) && !field.equals("none")){
            return true;
        }
        return false;
    }

    public static boolean isNull(String str) {
        if (str == null || "".equals(str) || "null".equalsIgnoreCase(str)) {
            return true;
        } else {
            return false;
        }
    }
    private static final String MD5_ALGORITHM = "MD5";
    public static String generateMD5(String input) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance(MD5_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        byte[] bytes = md.digest(input.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }

    private static String getLatestOne(Map<Long, String> sourceMap, List<String> sources) {
        long maxTimestamp = Long.MIN_VALUE;
        String result = null;
        for (Map.Entry<Long, String> entry : sourceMap.entrySet()) {
            Long timestamp = entry.getKey();
            String value = entry.getValue();

            if (sources.contains(value)) {
                if (timestamp > maxTimestamp) {
                    maxTimestamp = timestamp;
                    result = value;
                }
            }
        }
        return result;
    }

    private static String buildClause(List<String> list){
        StringBuilder inClause = new StringBuilder("(");
        for (int i = 0; i < list.size(); i++) {
            inClause.append("?");
            if (i < list.size() - 1) {
                inClause.append(",");
            }
        }
        inClause.append(")");
        return inClause.toString();
    }

    private static  String buildInSQLFrag(List<String> list){
        StringBuilder sb_app = new StringBuilder();
        for(int i=0;i<list.size();++i){
            sb_app.append("'");
            sb_app.append(list.get(i));
            sb_app.append("'");
            if(i<(list.size()-1)){
                sb_app.append(",");
            }
        }
        return sb_app.toString();
    }

    private static String checkSource(String adx) {
        List<String> adxList = new ArrayList<>(Arrays.asList(adx.split(",")));
        Set<String> af = new HashSet<>();
        af.add("appsflyer");
        af.add("mb");
        Map<String, Integer> typesMap = new HashMap<>();
        for (String s : adxList) {
            if (af.contains(s)) {
                typesMap.put("ams", 1);
            } else {
                typesMap.put("dsp", 1);
            }
        }
        int types = 0;
        for (Integer v : typesMap.values()) {
            types += v;
        }
        if (types == 1) {
            return typesMap.keySet().iterator().next();
        } else {
            return null;
        }
    }

    public static int convertToHour(int minutes) {
        if (minutes < 60) {
            return minutes;
        } else {
            return ((int) (minutes / 60)) * 60;
        }
    }

    public static void loadCodeMap() {
        try {
            InputStream inputStream = Test.class.getResourceAsStream("/app_basic_info.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // 按逗号分割每一行内容
                String[] parts = line.split(",");
                // 使用分割后的内容
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String removeSuffix(String input) {
        // 寻找最后一个下划线的位置
        int lastIndex = input.lastIndexOf("_");

        // 检查最后一个下划线是否存在且不在字符串末尾
        if (lastIndex != -1 && lastIndex != input.length() - 1) {
            // 从最后一个下划线位置开始截取字符串
            // 并检查截取结果是否以数字结尾
            String subString = input.substring(lastIndex + 1);
            if (subString.matches("\\d+")) {
                // 如果是数字结尾则移除最后一个下划线及其后面的部分
                input = input.substring(0, lastIndex);
            }
        }

        return input;
    }

    public static String[] mergeArrayForSource(String[] arr1, String[] arr2) {
        if (arr1 == null || arr2 == null) {
            return (arr1 == null) ? arr2 : arr1;
        }
        Set<String> uniqueElements = new HashSet<>();
        List<String> mergedList = new ArrayList<>();

        // 遍历arr2数组，将元素添加到uniqueElements集合中
        for (String element : arr2) {
            uniqueElements.add(element.toLowerCase());
        }
        // 遍历arr1数组，将不重复的元素添加到mergedList中
        for (String element : arr1) {
            if (!uniqueElements.contains(element.toLowerCase()) && !mergedList.contains(element.toLowerCase())) {
                mergedList.add(element.toLowerCase());
            }
        }
        // 将arr2数组中的元素添加到mergedList中
        for (String element : arr2) {
            mergedList.add(element.toLowerCase());
        }
        // 将mergedList转换为字符串数组并返回
        return mergedList.toArray(new String[0]);
    }

    public static String[] mergeArrays(String[] arr1, String[] arr2) {
        Set<String> uniqueElements = new HashSet<>();
        List<String> mergedList = new ArrayList<>();

        // 遍历arr2数组，将元素添加到uniqueElements集合中
        for (String element : arr2) {
            uniqueElements.add(element);
        }
        // 遍历arr1数组，将不重复的元素添加到mergedList中
        for (String element : arr1) {
            if (!uniqueElements.contains(element)) {
                mergedList.add(element);
            }
        }
        // 将arr2数组中的元素添加到mergedList中
        for (String element : arr2) {
            mergedList.add(element);
        }

        // 将mergedList转换为字符串数组并返回
        return mergedList.toArray(new String[0]);
    }
}
