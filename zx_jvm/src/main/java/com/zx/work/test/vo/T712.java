package com.zx.work.test.vo;

import java.util.regex.Pattern;

/**
 * @author zx
 * @description:
 * @date 2022/7/12
 */
public class T712 {
    public static void main(String[] args) {
        Stu610 s = new Stu610("s");
        zuti.set(s);
    }

    public static boolean canCastToNumber(String str){
        if(str==null)return false;
        boolean int_flag = Pattern.compile("^-?[1-9]\\d*$").matcher(str).find();
        boolean double_flag = Pattern.compile("^-?([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0)$").matcher(str).find();
        return int_flag || double_flag;
    }
}
