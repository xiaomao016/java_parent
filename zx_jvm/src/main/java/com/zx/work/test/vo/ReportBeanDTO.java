package com.zx.work.test.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReportBeanDTO {
    private Integer platformId;
    private Integer sourceId;
    private Integer affiliateId;
    private BigDecimal payout;
    private BigDecimal revenue;
    private Long clickCount;
    private Integer conversionCount;
    private String clickTime;
    private String date;
    private BigDecimal profit;
}
