package com.zx.work.test.y23;

import com.zx.juc.thinking.vo.D;
import com.zx.work.utils.DateUtilFinal;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class T0601 {
    private static Map<Long, LinkedBlockingQueue<String>> queueMap;
    public static void main(String[] args) {

        queueMap = new ConcurrentHashMap<>();
        long queenSize = 8;
        for (long i = 0; i < queenSize; ++i) {
            queueMap.put(i, new LinkedBlockingQueue<>(10 * 10000));
        }
        System.out.println(queueMap.get(0l));


        Put put1 = new Put(Bytes.toBytes("row_key"));
        Put put2 = new Put(Bytes.toBytes("row_key"));
        put1.setAttribute("flag", Bytes.toBytes("aa"));
        System.out.println(Bytes.toString(put1.getAttribute("flag")));
        System.out.println(Bytes.toString(put2.getAttribute("flag")));
        if(put2.getAttribute("flag")!=null && Bytes.toString(put2.getAttribute("flag")).equalsIgnoreCase("aa")){
            System.out.println("aabb" +
                    "");
        }
    }
}
