package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

public class TESTjson {
    public static void main(String[] args) {
        String json = "{\n" +
                "    \"ad_type\":\"banner\",\n" +
                "    \"app_id\":\"com.cleverside.differencegame\",\n" +
                "    \"brand\":\"General Mobile\",\n" +
                "    \"cat\":null,\n" +
                "    \"city\":\"Kosekoy\",\n" +
                "    \"country_code\":\"TR\",\n" +
                "    \"deviceId\":\"9ee20b36-c2a3-4b0e-8e4b-43b9310d96a9\",\n" +
                "    \"deviceType\":4,\n" +
                "    \"exchange\":\"Epom-new\",\n" +
                "    \"extra1\":\"64b4fc80d162b4061ea4119bnx\",\n" +
                "    \"extra2\":\"com.cleverside.differencegame\",\n" +
                "    \"extra3\":\"dbuigb6\",\n" +
                "    \"ip\":\"95.10.233.0\",\n" +
                "    \"language\":\"tr\",\n" +
                "    \"level\":\"info\",\n" +
                "    \"model\":\"G700\",\n" +
                "    \"msg\":\"\",\n" +
                "    \"network_type\":2,\n" +
                "    \"os_version\":\"12\",\n" +
                "    \"platform\":\"android\",\n" +
                "    \"pos_id\":0,\n" +
                "    \"price\":0.027,\n" +
                "    \"pub_id\":{\n" +
                "        \"id\":\"ecxmec1\",\n" +
                "        \"name\":\"Cleverside\",\n" +
                "        \"domain\":\"https://cleverside.com\"\n" +
                "    },\n" +
                "    \"region\":\"Kocaeli\",\n" +
                "    \"size\":\"320x50\",\n" +
                "    \"time\":\"2023-07-17T16:32:00+08:00\",\n" +
                "    \"timestamp\":1689582720,\n" +
                "    \"user_agent\":\"Mozilla/5.0 (Linux; Android 12; G700 Build/SP1A.210812.016; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/114.0.5735.196 Mobile Safari/537.36\"\n" +
                "}";


        JSONObject obj = JSON.parseObject(json);
        System.out.println(obj.toJSONString());

        JSONArray cat = obj.getJSONArray("cat");
        if(cat!=null) {
            List<String> list = JSON.parseObject(cat.toJSONString(), List.class);
            System.out.println(JSON.toJSONString(list));
        }else{
            System.out.println("cat is null");
        }

    }
}
