package com.zx.work.test.staticField;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zx.work.test.staticField.ClickSpeed;
import com.zx.work.test.staticField.LogPostbackBillBlockReport;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class Test1019ClickSpeed {
    private static String DEFAULT_PERCENTAGE_JSON = "{\"0\":\"0.0417\",\"1\":\"0.0417\",\"2\":\"0.0417\",\"3\":\"0.0417\",\"4\":\"0.0417\",\"5\":\"0.0417\",\"6\":\"0.0417\",\"7\":\"0.0417\",\"8\":\"0.0417\",\"9\":\"0.0417\",\"10\":\"0.0417\",\"11\":\"0.0417\",\"12\":\"0.0417\",\"13\":\"0.0417\",\"14\":\"0.0417\",\"15\":\"0.0417\",\"16\":\"0.0417\",\"17\":\"0.0417\",\"18\":\"0.0417\",\"19\":\"0.0417\",\"20\":\"0.0417\",\"21\":\"0.0417\",\"22\":\"0.0417\",\"23\":\"0.0417\"}";
    public static void main(String[] args) {


        String reportJons = "[{\"country\":\"AE\",\"createTime\":1697713200000,\"installs\":6,\"prod\":\"com.zx.testss\"},{\"country\":\"AE\",\"createTime\":1697709600000,\"installs\":8,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697709600000,\"installs\":8,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697695200000,\"installs\":9,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697695200000,\"installs\":8,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697691600000,\"installs\":22,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697536800000,\"installs\":20,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697533200000,\"installs\":0,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697529600000,\"installs\":0,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697526000000,\"installs\":92,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697526000000,\"installs\":100,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697522400000,\"installs\":0,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697454000000,\"installs\":2,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697454000000,\"installs\":0,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697709600000,\"installs\":0,\"prod\":\"com.zx.testss\"},{\"country\":\"AE\",\"createTime\":1697540400000,\"installs\":0,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697529600000,\"installs\":54,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697457600000,\"installs\":0,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697450400000,\"installs\":0,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697450400000,\"installs\":0,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697446800000,\"installs\":94,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697446800000,\"installs\":101,\"prod\":\"test.huawei.com\"},{\"country\":\"AE\",\"createTime\":1697439600000,\"installs\":6,\"prod\":\"test.huawei.sss\"},{\"country\":\"AE\",\"createTime\":1697162400000,\"installs\":10,\"prod\":\"test.huawei.com\"}]";

        List<LogPostbackBillBlockReport> installs = JSON.parseArray(reportJons, LogPostbackBillBlockReport.class);
        //构造数据:
//        List<LogPostbackBillBlockReport> installs = new ArrayList<>();

//        for(int i=0;i<24;++i){
//            LogPostbackBillBlockReport ins = new LogPostbackBillBlockReport();
//            ins.setCountry("BR");
//            ins.setProd("com.zd.test");
//            int hour = i;
//            String sh = hour+"";
//            if(hour<10){
//                sh = "0"+sh;
//            }
//            String dateStr = MessageFormat.format("2023-10-11 {0}:00:00",sh);
//            log.info("dateStr:{}",dateStr);
//            ins.setCreateTime(DateUtilFinal.formatTime(dateStr));
//            ins.setInstalls((long) i);
//            installs.add(ins);
//        }

        log.info("installs:{}", JSON.toJSONString(installs));



        // 统计不同prod+country维度在各个小时里总的installs数量
        Map<String, Map<String, Long>> hourInstallsByProdCountry = new HashMap<>();

        for (LogPostbackBillBlockReport install : installs) {
            String prodCountryKey = install.getCountry()+ "|" +install.getProd() ;
            String hour = getHour(install.getCreateTime())+"";
            log.error("prodCountryKey:{},hour:{}",prodCountryKey,hour);
            hourInstallsByProdCountry.putIfAbsent(prodCountryKey, new HashMap<>());
            Map<String, Long> hourInstalls = hourInstallsByProdCountry.get(prodCountryKey);
            hourInstalls.put(hour, hourInstalls.getOrDefault(hour, 0L) + install.getInstalls());
        }

        log.info("hourInstallsByProdCountry:{}",JSON.toJSONString(hourInstallsByProdCountry));

        // 计算在prod+country维度，各个小时的installs数量占比
        Map<String, Map<String, Double>> hourInstallsPercentageByProdCountry = new HashMap<>();

        for (Map.Entry<String, Map<String, Long>> entry : hourInstallsByProdCountry.entrySet()) {
            String prodCountryKey = entry.getKey();

            Map<String, Long> hourInstalls = entry.getValue();
            long totalInstalls = hourInstalls.values().stream().mapToLong(Long::longValue).sum();

            hourInstallsPercentageByProdCountry.putIfAbsent(prodCountryKey, new HashMap<>());
            Map<String, Double> hourInstallsPercentage = hourInstallsPercentageByProdCountry.get(prodCountryKey);

            for (int hour = 0; hour < 24; hour++) {
                long installsInHour = hourInstalls.getOrDefault(hour+"", 0L);
                double percentage = totalInstalls > 0 ? (installsInHour / (double) totalInstalls) : 0.0;
                hourInstallsPercentage.put(hour+"", percentage);
            }
        }
        log.info("percentage result size:{}",hourInstallsPercentageByProdCountry.size());
        log.info("percentage result info:{}",JSON.toJSONString(hourInstallsPercentageByProdCountry));
        log.info("result:{}",hourInstallsPercentageByProdCountry.get("BR|com.zd.test"));

        //构造入库对象
        List<ClickSpeed> speeds = new ArrayList<>();
        for (String prodCountryKey : hourInstallsPercentageByProdCountry.keySet()) {
            String[] parts = prodCountryKey.split("\\|");
            String countryCode = parts[0];
            String prod = parts[1];

//            Map<String, Double> hourPercentage = hourInstallsPercentageByProdCountry.get(prodCountryKey);
//            String speedConfig = JSON.toJSONString(hourPercentage);

            String speedConfig = null;
            Map<String, Double> hourPercentage = hourInstallsPercentageByProdCountry.get(prodCountryKey);
            if(isPercentageAbnormal(hourPercentage)){
                log.info("abnormal:{}",JSON.toJSONString(hourPercentage));
                speedConfig = DEFAULT_PERCENTAGE_JSON;
            }else{
                speedConfig = JSON.toJSONString(hourPercentage);
            }


            ClickSpeed clickSpeed = new ClickSpeed();
            clickSpeed.setCountryCode(countryCode);
            clickSpeed.setProd(prod);
            clickSpeed.setSpeedConfig(speedConfig);
            speeds.add(clickSpeed);
        }

        log.info("Speeds:{}",JSON.toJSONString(speeds));

        // 示例数据
        List<ClickSpeed> speeds2 = new ArrayList<>();
        speeds.add(new ClickSpeed("{\"1\":0.5,\"2\":0.3}", "country1", "prod1"));
        speeds.add(new ClickSpeed("{\"1\":0.2,\"2\":0.4}", "country2", "prod1"));
        speeds.add(new ClickSpeed("{\"1\":0.8,\"2\":0.7}", "country1", "prod2"));
        speeds.add(new ClickSpeed("{\"1\":0.4,\"2\":0.5}", "country2", "prod2"));

        // 将 List<ClickSpeed> 转换为 JSON 字符串
        String jsonString = JSONObject.toJSONString(speeds2);

        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(speeds2));
//        Map map = JSONObject.parseObject(jsonArray.toJSONString(), Map.class);
        // 输出 JSON 字符串
        System.out.println(jsonString);

    }

    private static int getHour(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour;
    }

    private static boolean isPercentageAbnormal(Map<String, Double> hourPercentage) {
        //1. Whether all percentage sum up to 1
        //2. Whether more than half of percentage is 0
        int zero_count = 0;
        double total = 0.0d;
        for(double value:hourPercentage.values()){
            if(value<=0.0d){
                zero_count++;
            }
            total+=value;
        }

        log.info("total:{},zero_count:{}",total,zero_count);

        if(zero_count>=12){
            return true;
        }

        if(Math.abs(total-1D)>=0.0001){
            return true;
        }
        return false;
    }
}
