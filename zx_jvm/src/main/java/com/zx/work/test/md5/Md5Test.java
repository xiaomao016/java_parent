package com.zx.work.test.md5;

import org.apache.commons.codec.digest.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Test {
    public static void main(String[] args) {

        String deviceId = "571A342E-B965-0D8D-7943-14D92145B61D";

        String re = md5("deviceId");
        String re2 = getSHA256Hash(deviceId);

        System.out.println(re);
        System.out.println(re2);
    }

    public static String md5(String data){
        return DigestUtils.md5Hex(data);
    }

    public static String getSHA256Hash(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedHash = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (byte b : encodedHash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
