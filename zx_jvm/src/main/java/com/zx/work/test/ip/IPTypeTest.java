package com.zx.work.test.ip;

import org.jcodings.util.Hash;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class IPTypeTest {
    public static void main(String[] args) throws UnknownHostException {
//        String ipAddress = "2001:0db8:85a3:0000:0000:8a2e:0370:7334";
        String ipAddress = "192.168.1.1";
        InetAddress inetAddress = InetAddress.getByName(ipAddress);

        if (inetAddress instanceof Inet4Address) {
            System.out.println("IPv4 Address");
        } else if (inetAddress instanceof Inet6Address) {
            System.out.println("IPv6 Address");
        } else {
            System.out.println("Unknown IP Address");
        }
        Map<Long,String> map = new HashMap<>();
        Long k = new Long(1231893131L);
        map.put(k,"aa");
        Long a = 123189311L;
        if(map.containsKey(a)){
            System.out.println("aafafa");
        }
    }
}
