package com.zx.work.test.ctit;

import lombok.Data;

import java.util.Date;

@Data
public class CTITProportionReportBean {
    private Integer platformId;
    private Long offerId;
    private String offerName;
    private Integer affiliateId;
    private String affiliateName;
    private String affSub;
    private String country;
    private Long ctit;
    private Long installCount;
    private String prod;
    private String date;
    private Date createTime;
    private Double ctitProportion;
    private Double ctitProportion_30;
    private Double ctitProportion_120;
    private Double ctitProportion_720;

    public CTITProportionReportBean(Integer platformId, Long offerId, String offerName, Integer affiliateId, String affiliateName, String affSub, String country, Long ctit, Long installCount, String prod, Date createTime) {
        this.platformId = platformId;
        this.offerId = offerId;
        this.offerName = offerName;
        this.affiliateId = affiliateId;
        this.affiliateName = affiliateName;
        this.affSub = affSub;
        this.country = country;
        this.ctit = ctit;
        this.installCount = installCount;
        this.prod = prod;
        this.createTime = createTime;
    }

    public CTITProportionReportBean(Integer platformId, Long offerId, String offerName, Integer affiliateId, String affiliateName, String affSub, String country, Long ctit, Long installCount, String prod, String date) {
        this.platformId = platformId;
        this.offerId = offerId;
        this.offerName = offerName;
        this.affiliateId = affiliateId;
        this.affiliateName = affiliateName;
        this.affSub = affSub;
        this.country = country;
        this.ctit = ctit;
        this.installCount = installCount;
        this.prod = prod;
        this.date = date;
    }
}
