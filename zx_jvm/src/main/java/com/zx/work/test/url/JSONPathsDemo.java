package com.zx.work.test.url;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;

import java.util.List;
import java.util.Objects;

public class JSONPathsDemo {
    public static void main(String[] args) {
        String json = "{\n" +
                "    \"status\":1,\n" +
                "    \"offers\":[\n" +
                "        {\n" +
                "            \"id\":66736,\n" +
                "            \"offer_id\":\"66736\",\n" +
                "            \"bundle_id\":\"\",\n" +
                "            \"title\":\"Stickpolo_CPR_IND_AND\",\n" +
                "            \"preview_url\":\"https://play.google.com/store/apps/details?id=com.stickpoolclub&hl=en_IN\",\n" +
                "            \"description_lang\":{\n" +
                "                \"en\":\"1) registration, purchase, Install to First Gameplay should be above 40%   install to registration >20%  Registration - purchase > 15%No Incent, No Pop up ads, No Free-Recharge apps traffic and No App Wall like Geenapp etc. No SMS, No Email, No Adult traffic and No Bot Traffic Only IOS 2) CVR within 8% The caps provided are network wide. Please ask AM for individual Caps by Mail and Skype. expiry date: TBA 3) The offer will stop converting once we hit the daily/monthly cap. 4) Usage of any other creatives apart from the mentioned on the dashboard needs explicit approval from AM, and needs to be shown to AM. Not adhering to this would be treated as fraud. 5) Non-Solicitation: For a period of six months from the start of the campaign, the Publisher agrees not to solicit, nor attempt to solicit any Advertiser of InMobi  6) No reborkering  7) Low MTTI installs (<20 sec) won't be paid 8)Install to First Gameplay should be above 40% 9) install to registration >20% 10) Registration - purchase > 15%\",\n" +
                "                \"es\":\"\",\n" +
                "                \"ka\":\"\",\n" +
                "                \"ru\":\"\",\n" +
                "                \"vi\":\"\"\n" +
                "            },\n" +
                "            \"cr\":0,\n" +
                "            \"epc\":0,\n" +
                "            \"affiliate_epc\":0,\n" +
                "            \"logo\":\"\",\n" +
                "            \"logo_source\":\"\",\n" +
                "            \"stop_at\":\"\",\n" +
                "            \"sources\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"categories\":[\n" +
                "                \"overall\"\n" +
                "            ],\n" +
                "            \"full_categories\":[\n" +
                "                {\n" +
                "                    \"id\":\"5cf8d5612d82f138a774e357\",\n" +
                "                    \"title\":\"overall\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"payments\":[\n" +
                "                {\n" +
                "                    \"countries\":[\n" +
                "                        \"IN\"\n" +
                "                    ],\n" +
                "                    \"cities\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"devices\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"os\":[\n" +
                "                        \"Android\"\n" +
                "                    ],\n" +
                "                    \"goal\":\"1\",\n" +
                "                    \"revenue\":0.21,\n" +
                "                    \"currency\":\"usd\",\n" +
                "                    \"title\":\"\",\n" +
                "                    \"type\":\"fixed\",\n" +
                "                    \"country_exclude\":false\n" +
                "                }\n" +
                "            ],\n" +
                "            \"caps\":[\n" +
                "                {\n" +
                "                    \"goals\":{\n" +
                "\n" +
                "                    },\n" +
                "                    \"period\":\"day\",\n" +
                "                    \"type\":\"budget\",\n" +
                "                    \"value\":30,\n" +
                "                    \"goal_type\":\"all\",\n" +
                "                    \"country_type\":\"all\",\n" +
                "                    \"country\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"sub_number\":null,\n" +
                "                    \"sub_value\":[\n" +
                "\n" +
                "                    ]\n" +
                "                },\n" +
                "                {\n" +
                "                    \"goals\":{\n" +
                "\n" +
                "                    },\n" +
                "                    \"period\":\"day\",\n" +
                "                    \"type\":\"conversions\",\n" +
                "                    \"value\":100,\n" +
                "                    \"goal_type\":\"all\",\n" +
                "                    \"country_type\":\"all\",\n" +
                "                    \"country\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"sub_number\":null,\n" +
                "                    \"sub_value\":[\n" +
                "\n" +
                "                    ]\n" +
                "                }\n" +
                "            ],\n" +
                "            \"caps_status\":[\n" +
                "                \"confirmed\",\n" +
                "                \"pending\",\n" +
                "                \"declined\",\n" +
                "                \"hold\"\n" +
                "            ],\n" +
                "            \"required_approval\":true,\n" +
                "            \"strictly_country\":1,\n" +
                "            \"strictly_os\":{\n" +
                "                \"items\":{\n" +
                "                    \"Android\":[\n" +
                "\n" +
                "                    ]\n" +
                "                }\n" +
                "            },\n" +
                "            \"strictly_brands\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"is_cpi\":false,\n" +
                "            \"creatives\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"creatives_zip\":null,\n" +
                "            \"impressions_link\":null,\n" +
                "            \"landings\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"links\":[\n" +
                "                {\n" +
                "                    \"id\":null,\n" +
                "                    \"title\":null,\n" +
                "                    \"hash\":null,\n" +
                "                    \"url\":\"https://kraken.g2afse.com/click?pid=1124&offer_id=66736\",\n" +
                "                    \"postbacks\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"created\":null\n" +
                "                }\n" +
                "            ],\n" +
                "            \"macro_url\":null,\n" +
                "            \"link\":\"https://kraken.g2afse.com/click?pid=1124&offer_id=66736\",\n" +
                "            \"use_https\":true,\n" +
                "            \"use_http\":false,\n" +
                "            \"hold_period\":0,\n" +
                "            \"hold_type\":\"days\",\n" +
                "            \"kpi\":{\n" +
                "                \"en\":\"\"\n" +
                "            },\n" +
                "            \"click_session\":\"1y\",\n" +
                "            \"minimal_click_session\":\"0s\",\n" +
                "            \"strictly_isp\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"restriction_isp\":[\n" +
                "\n" +
                "            ],\n" +
                "            \"targeting\":[\n" +
                "                {\n" +
                "                    \"country\":{\n" +
                "                        \"allow\":[\n" +
                "                            \"IN\"\n" +
                "                        ],\n" +
                "                        \"deny\":[\n" +
                "\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"region\":{\n" +
                "                        \"allow\":{\n" +
                "\n" +
                "                        },\n" +
                "                        \"deny\":{\n" +
                "\n" +
                "                        }\n" +
                "                    },\n" +
                "                    \"city\":{\n" +
                "                        \"allow\":{\n" +
                "\n" +
                "                        },\n" +
                "                        \"deny\":{\n" +
                "\n" +
                "                        }\n" +
                "                    },\n" +
                "                    \"os\":{\n" +
                "                        \"allow\":[\n" +
                "                            {\n" +
                "                                \"name\":\"Android\",\n" +
                "                                \"comparison\":\"EQ\",\n" +
                "                                \"version\":\"\"\n" +
                "                            }\n" +
                "                        ],\n" +
                "                        \"deny\":[\n" +
                "\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"isp\":{\n" +
                "                        \"allow\":{\n" +
                "\n" +
                "                        },\n" +
                "                        \"deny\":{\n" +
                "\n" +
                "                        }\n" +
                "                    },\n" +
                "                    \"ip\":{\n" +
                "                        \"allow\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"deny\":[\n" +
                "\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"browser\":{\n" +
                "                        \"allow\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"deny\":[\n" +
                "\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"brand\":{\n" +
                "                        \"allow\":[\n" +
                "\n" +
                "                        ],\n" +
                "                        \"deny\":[\n" +
                "\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"device_type\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"connection\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"id\":\"00000000-0000-0000-0000-000000000000\",\n" +
                "                    \"zip\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"sub_regexps\":[\n" +
                "\n" +
                "                    ],\n" +
                "                    \"block_proxy\":false,\n" +
                "                    \"sub\":{\n" +
                "                        \"allow\":{\n" +
                "\n" +
                "                        },\n" +
                "                        \"deny\":{\n" +
                "\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            ],\n" +
                "            \"consider_personal_targeting_only\":false,\n" +
                "            \"hosts_only\":false,\n" +
                "            \"duplicate_clicks_threshold\":0,\n" +
                "            \"countries\":[\n" +
                "                \"IN\"\n" +
                "            ],\n" +
                "            \"uniq_ip_only\":false,\n" +
                "            \"reject_not_uniq_ip\":0,\n" +
                "            \"sign_clicks_integration\":\"inherit\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"pagination\":{\n" +
                "        \"per_page\":5,\n" +
                "        \"total_count\":217,\n" +
                "        \"page\":1,\n" +
                "        \"next_page\":2\n" +
                "    }\n" +
                "}";

        JSONObject resp =JSONObject.parseObject(json);
        List<JSONObject> offers = resp.getJSONArray("offers").toJavaList(JSONObject.class);
        JSONObject offer = offers.get(0);
        parseOs(offer);

        System.out.println(offers.size());
    }

    public static String parseOs(JSONObject offer) {
        //取所有元素的name，
        Object eval = JSONPath.eval(offer, "$.targeting[0].os.allow.name");
        JSONArray osList;
        if (Objects.isNull(eval) || "[]".equals(eval.toString())) {
            osList = (JSONArray) JSONPath.eval(offer, "$.payments[0].os");
        } else {
            osList = (JSONArray) JSONPath.eval(offer, "$.targeting[0].os.allow.name");
        }
        return String.join(",", osList.toJavaList(String.class));
    }
}
