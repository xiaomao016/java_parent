package com.zx.work.test.y23;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class OriginalDeviceInfo {
    private String deviceId;
    private String country;
    private String os;
    private String osVersion;
    private String language;
    private String model;
    private String brand;
    private String ip;
    private String userAgent;
    private String appId;
//    private String cat;//IAB1,IAB2
    private String city;
    private String exchange;
    private String dataType;//代表什么类型的，比如access， impression， click， install， event
    private List<String> cat;

    public OriginalDeviceInfo() {
    }

    public OriginalDeviceInfo(String deviceId, String country, String os, String osVersion, String language, String model, String brand, String ip, String userAgent, String appId, String city, String exchange, String dataType, List<String> cat) {
        this.deviceId = deviceId;
        this.country = country;
        this.os = os;
        this.osVersion = osVersion;
        this.language = language;
        this.model = model;
        this.brand = brand;
        this.ip = ip;
        this.userAgent = userAgent;
        this.appId = appId;
        this.city = city;
        this.exchange = exchange;
        this.dataType = dataType;
        this.cat = cat;
    }
}
