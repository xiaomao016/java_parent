package com.zx.work.test.staticField;

import lombok.Data;

import java.util.Date;

@Data
public class LogPostbackBillBlockReport {
    private String country;
    private String prod;
    private Date createTime;
    private Long installs;
}
