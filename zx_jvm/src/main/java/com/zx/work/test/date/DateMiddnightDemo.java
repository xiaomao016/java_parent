package com.zx.work.test.date;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateMiddnightDemo {

    public static void main(String[] args) {
        // 获取当前日期时间
        LocalDateTime currentDateTime = LocalDateTime.now();
        // 获取当前日期的前一天
        LocalDate previousDate = currentDateTime.toLocalDate().minusDays(1);
        // 构造前一天的凌晨0点时间
        LocalDateTime previousMidnight = LocalDateTime.of(previousDate, LocalTime.MIDNIGHT);
        // 构造前一天的最后一秒时间
        LocalDateTime previousLastSecond = previousMidnight.withHour(23).withMinute(59).withSecond(59);

        // 格式化前一天凌晨0点时间和前一天最后一秒时间
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedMidnight = previousMidnight.format(formatter);
        String formattedLastSecond = previousLastSecond.format(formatter);
        System.out.println("格式化后的前一天凌晨0点时间：" + formattedMidnight);
        System.out.println("格式化后的前一天最后一秒时间：" + formattedLastSecond);


//        // 格式化前一天凌晨0点时间
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        String formattedDateTime = previousMidnight.format(formatter);
//        System.out.println("格式化后的前一天凌晨0点时间：" + formattedDateTime);

        System.out.println("当天0点");

        getToday();
    }

    private static void getToday() {
        // 获取当前日期
        LocalDate previousDate = LocalDateTime.now().toLocalDate();
        // 构造当天的凌晨0点时间
        LocalDateTime previousMidnight = LocalDateTime.of(previousDate, LocalTime.MIDNIGHT);
        // 构造当天的最后一秒时间
        LocalDateTime previousLastSecond = previousMidnight.withHour(23).withMinute(59).withSecond(59);

        // 格式化前一天凌晨0点时间和前一天最后一秒时间
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String from = previousMidnight.format(formatter);
        String to = previousLastSecond.format(formatter);
        System.out.println(from);
        System.out.println(to);

        String pid = MessageFormat.format("pid:{0} cap reach {1},offer blocked", "pid", 11);
        System.out.println(pid);
    }
}
