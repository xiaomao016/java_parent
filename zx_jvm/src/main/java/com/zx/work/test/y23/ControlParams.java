package com.zx.work.test.y23;

import lombok.Data;

@Data
public class ControlParams {
    private String isNotify;
    private String isClean;
    private Float osVersion;
}
