package com.zx.work.test.file;

import com.alibaba.fastjson.JSONObject;
import com.zx.work.test.base.TrackLinkUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.CharsetNames;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.UriUtils;

import java.io.*;

@Slf4j
public class ReadFile {

    public static void main(String[] args) {
        File file = new File("C:\\Users\\dell\\Desktop\\临时\\3.28_离线_peter\\in.startv.hotstar_postbacks_2023-03-06_2023-03-13_UTC.csv");
        try (FileInputStream fis = new FileInputStream(file);
             InputStreamReader isr = new InputStreamReader(fis);
             BufferedReader br = new BufferedReader(isr)) {
            String line = br.readLine();
            while (null != (line = br.readLine())) {
                log.info("line:{}", line);
                String[] item = line.split(",");
                JSONObject result = new JSONObject();
                //ip: 30
                //platform: 42
                //country_code:  25
                //deviceId: 37
                //user_agent: 54 / 56的url中
                //os_version: 43
                //eventName: 4
                //city:27
                result.put("ip", item[30]);
                result.put("platform", item[42]);
                result.put("country_code", item[25]);
                result.put("deviceId", item[37]);
                if (!StringUtils.isEmpty(item[54])) {
                    result.put("user_agent", item[54]);
                } else {
                    //从链接中解析
                    String originURL = item[56];
                    String ua = TrackLinkUtils.getParamByKey(originURL, "af_ua");
                    result.put("user_agent", UriUtils.decode(ua, CharsetNames.UTF_8));
                }
                result.put("city", item[27]);
                result.put("eventName", item[4]);
                String jsonStr = result.toJSONString();


            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
