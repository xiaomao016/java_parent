package com.zx.work.test.date;

import com.zx.work.utils.DateUtilFinal;

import java.util.Date;

/**
 * @author zx
 * @description:
 * @date 2022/8/10
 */
public class T0810 {

    public static void main(String[] args) {
        Date now = new Date();
        Date updateTime = DateUtilFinal.formatTime("2022-08-10 15:08:33");

        //计算时间差
        long diMinute = DateUtilFinal.subDate1ToDate2(now,updateTime)/60;
        System.out.println(diMinute);
    }
}
