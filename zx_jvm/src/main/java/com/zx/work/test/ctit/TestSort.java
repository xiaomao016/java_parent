package com.zx.work.test.ctit;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestSort {
    public static void main(String[] args) {
        // 创建几个 CTITProportionReportBean 对象
        CTITProportionReportBean report1 = new CTITProportionReportBean(1, 1L, "Offer 1", 1, "Affiliate 1", "Sub 1", "Country 1", 100L, 10L, "Product 1", "2023-11-13");
        CTITProportionReportBean report2 = new CTITProportionReportBean(2, 2L, "Offer 2", 2, "Affiliate 2", "Sub 2", "Country 2", 200L, 20L, "Product 2", "2023-11-14");
        CTITProportionReportBean report3 = new CTITProportionReportBean(3, 3L, "Offer 3", 3, "Affiliate 3", "Sub 3", "Country 3", 300L, 30L, "Product 3", "2023-11-16");

        // 将对象添加到 list 中
        List<CTITProportionReportBean> reportList = new ArrayList<>();
        reportList.add(report1);
        reportList.add(report3);
        reportList.add(report2);

        // 按 createTime 属性进行降序排序
        reportList.sort(Comparator.comparing(CTITProportionReportBean::getDate));

        // 打印排序后的结果
        for (CTITProportionReportBean report : reportList) {
            System.out.println(report.getOfferName() + " - " + report.getDate());
        }
    }
}
