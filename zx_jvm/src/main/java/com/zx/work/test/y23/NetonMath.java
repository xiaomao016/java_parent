package com.zx.work.test.y23;

import com.zx.work.utils.DateUtilFinal;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Date;

public class NetonMath {

    private static int Ta = 0;
    private static int To = 10;
    public static void main(String[] args) {
        /**
         * import time
         * from math import exp
         *
         * Ta = 0  # 环境温度
         * To = 100  # 初始温度
         * k = 0.01  # 热交换系数
         *
         * def update_score(timestamp, score):
         *     t = time.time() - timestamp
         *     score_new = Ta + (score - Ta) * exp(-k * t)
         *     return score_new
         */


        long time = DateUtilFinal.addDays(new Date(),-5).getTime();
        double n_c = update_score(time,10f);
        System.out.println(n_c);
        System.out.println("=====ss====");
        long d30 = 30*24*60*60;
        long d20 = 20*24*60*60;
        long d15 = 15*24*60*60;
        long d10 = 10*24*60*60;
        long d05 = 5*24*60*60;
        double score = f(d30);
        double score20 = f(d20);
        double score15 = f(d15);
        double score10 = f(d10);
        double score05 = f(d05);
        System.out.println(score);
        System.out.println(score20);
        System.out.println(score15);
        System.out.println(score10);
        System.out.println(score05);

        System.out.println("===========");
        double af = Math.log(0.001)/d30;
        System.out.println(af);
//        double af = 0.000000004056306160229;

        System.out.println("=========曲线值");

        double onday = f(1 * 24 * 60 * 60);
        System.out.println(1.001*onday);

        System.out.println("======falg");
        System.out.println(f(1*24*60*60,1.2));
        System.out.println(f(1*24*60*60,1));
        System.out.println(f(30*24*60*60,1));
        System.out.println("==========");
        String osv = "12.2";
        if (!NumberUtils.isParsable(osv)) {
            osv = "1";
        }
        System.out.println(osv);

        System.out.println("===========");
        String a = null;
        if(a==null){
            System.out.println("null");
        }



    }

    /**
     *
     * @param timestamp 上一次活跃时间
     * @param score 上一次分数
     * @return
     */
    public static double update_score(long timestamp,float score){
        long t = System.currentTimeMillis() - timestamp;
        double new_score = Ta+ (score - Ta) * Math.exp(-0.01 * t);
        return new_score;
    }

    public static double f(long x){
        double alp = -0.00000266502904282;
//        double alp = 4.056305306160229E-9;
        double pa = alp * x;
        return Math.exp(pa);
    }

    public static double f(long x,double oldScore){
        double alpha = -0.00000266502904282;
        double pa = alpha * x;
        return oldScore * Math.exp(pa);
    }

}
