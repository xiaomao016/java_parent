package com.zx.work.test.date;

import com.zx.work.utils.DateUtilFinal;

/**
 * @author zx
 * @description:
 * @date 2022/7/7
 */
public class Test0707 {
    public static void main(String[] args) {
        String d = DateUtilFinal.getDateFormatyyyyMMddHHmmss(1657162169000l);
        String queryBaseSQLSegment = "with temp_t0 as (\n" +
                "  select\n" +
                "  get_json_object(tb_cs.click_log,'$.country_code') as country_code,\n" +
                "  get_json_object(tb_cs.click_log,'$.exchange') as `exchange`,\n" +
                "  get_json_object(tb_cs.click_log,'$.os_version') as os_version,\n" +
                "  get_json_object(tb_cs.click_log,'$.brand') as `brand`,\n" +
                "  get_json_object(tb_cs.click_log,'$.language') as `language`,\n" +
                "  get_json_object(tb_cs.click_log,'$.timestamp') as `timestamp`,\n" +
                "  get_json_object(tb_cs.click_log,'$.app_id') as `app_id`,\n" +
                "  get_json_object(tb_cs.click_log,'$.deviceId') as `deviceId`\n" +
                "  from %s as tb_cs\n" +
                "  where code='%s' AND DAY='%s' AND TOLOWER(get_json_object(tb_cs.click_log,'$.platform'))='%s' " +
                "AND get_json_object(tb_cs.click_log,'$.timestamp')>%s AND get_json_object(tb_cs.click_log,'$.timestamp')<%s)";

        queryBaseSQLSegment = String.format(queryBaseSQLSegment,"dwd_click_data_all",1,2,3,4,5);
        System.out.println(queryBaseSQLSegment);
//        System.out.println(System.currentTimeMillis()/1000);


        System.out.println(d);
    }
}
