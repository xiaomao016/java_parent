package com.zx.work.test.file;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.val;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static com.zx.work.utils.FileUtils.createFileIfNotExist;

public class T1107 {
    public static void main(String[] args) {
        List<String> top50 = getTop50();

//        String json = "{\"com.staplegames.blocksClassicSGGP\":0.01,\"com.peoplefun.wordcross\":0.02,\"com.cashwalklabs.cashwalk\":0.0,\"com.easybrain.make.music\":0.13,\"com.tumblr\":0.02,\"com.interfocusllc.patpat\":0.0,\"com.opera.mini.native\":0.04,\"com.gogii.textplus\":0.14,\"com.grindrapp.android\":0.02,\"jp.gocro.smartnews.android\":0.03,\"com.tripledot.woodoku\":0.0,\"com.americasbestpics\":0.05,\"kik.android\":0.0,\"com.fugo.wow\":0.13,\"com.audiomack\":0.01,\"sixpack.sixpackabs.absworkout.abexercises.tv\":0.03,\"com.easybrain.nonogram\":0.2,\"com.opera.mini.native.beta\":0.01,\"com.zynga.words3\":0.0,\"com.mobilityware.spider\":0.02,\"com.taggedapp\":0.0,\"com.lightinthebox.android\":0.0,\"puzzle.blockpuzzle.cube.relax\":0.08,\"com.freemannew.videoforyoutube\":0.0,\"mobi.ifunny\":0.0,\"com.myyearbook.m\":0.01,\"com.domobile.applockwatcher\":0.0,\"word.connect.games.world.wordforest\":0.0,\"com.particlenews.newsbreak\":0.0,\"com.inspiredsquare.jupiter\":0.02,\"com.psafe.msuite\":0.0,\"wp.wattpad\":0.0,\"com.handmark.expressweather\":0.0,\"absworkout.femalefitness.workoutforwomen.loseweight.tv\":0.0,\"com.callapp.contacts\":0.02,\"com.snaptube.premium\":0.0,\"com.loop.match3d\":0.13,\"com.enflick.android.TextNow\":0.0,\"com.adfone.aditup\":0.0,\"tunein.player\":0.02,\"com.mobilityware.solitaire\":0.08,\"com.aws.android\":0.0,\"com.pixel.art.coloring.color.number\":0.02,\"com.smamolot.gusher\":0.03,\"in.playsimple.tripcross\":0.07,\"imoblife.androidsensorbox\":0.07,\"es.socialpoint.wordlife\":0.02,\"jigsaw.puzzle.game.banana\":0.01,\"com.tripledot.solitaire\":0.01,\"in.playsimple.wordtrip\":0.01}";
        String json = "{\"com.staplegames.blocksClassicSGGP\":0.01,\"com.peoplefun.wordcross\":0.02,\"com.cashwalklabs.cashwalk\":0.01,\"com.easybrain.make.music\":0.31,\"com.tumblr\":0.04,\"com.interfocusllc.patpat\":0.0,\"com.opera.mini.native\":0.13,\"com.gogii.textplus\":0.03,\"com.grindrapp.android\":0.05,\"jp.gocro.smartnews.android\":0.03,\"com.tripledot.woodoku\":0.0,\"com.americasbestpics\":0.01,\"kik.android\":0.0,\"com.fugo.wow\":0.34,\"com.audiomack\":0.02,\"sixpack.sixpackabs.absworkout.abexercises.tv\":0.07,\"com.easybrain.nonogram\":1.08,\"com.opera.mini.native.beta\":0.11,\"com.zynga.words3\":0.01,\"com.mobilityware.spider\":0.01,\"com.taggedapp\":0.0,\"com.lightinthebox.android\":0.0,\"puzzle.blockpuzzle.cube.relax\":0.21,\"com.freemannew.videoforyoutube\":0.0,\"mobi.ifunny\":0.0,\"com.myyearbook.m\":0.02,\"com.domobile.applockwatcher\":0.0,\"word.connect.games.world.wordforest\":0.0,\"com.particlenews.newsbreak\":0.0,\"com.inspiredsquare.jupiter\":0.01,\"com.psafe.msuite\":0.01,\"wp.wattpad\":0.01,\"absworkout.femalefitness.workoutforwomen.loseweight.tv\":0.0,\"com.handmark.expressweather\":0.0,\"com.callapp.contacts\":0.05,\"com.snaptube.premium\":0.0,\"com.loop.match3d\":0.33,\"com.enflick.android.TextNow\":0.0,\"com.adfone.aditup\":0.0,\"tunein.player\":0.03,\"com.mobilityware.solitaire\":0.14,\"com.aws.android\":0.0,\"com.pixel.art.coloring.color.number\":0.02,\"com.smamolot.gusher\":0.08,\"in.playsimple.tripcross\":0.2,\"imoblife.androidsensorbox\":0.26,\"es.socialpoint.wordlife\":0.05,\"jigsaw.puzzle.game.banana\":0.02,\"com.tripledot.solitaire\":0.03,\"in.playsimple.wordtrip\":0.02}";

        Map<String, Double> map = new HashMap<>();
        JSONObject overLap = JSON.parseObject(json);
        for (String app : top50) {
            map.put(app, overLap.getDoubleValue(app));
        }
        System.out.println(JSON.toJSONString(map));


        List<Map.Entry<String, Double>> list = new ArrayList(map.entrySet()); //将map的entryset放入list集合
        //对list进行排序，并通过Comparator传入自定义的排序规则
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                if (o2.getValue() > o1.getValue()) {
                    return 1;
                } else if (o2.getValue() < o1.getValue()) {
                    return -1;
                }
                return 0;
            }
        });
        //用迭代器对list中的键值对元素进行遍历
        Iterator<Map.Entry<String, Double>> iterA = list.iterator();
//        String filePath = "C:\\Users\\dell\\Desktop\\临时\\11.7\\1106.csv";
        String filePath = "C:\\Users\\dell\\Desktop\\临时\\11.7\\3333.csv";
        createFileIfNotExist(filePath);

        try {
            FileWriter fw = new FileWriter(filePath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fw, 1024 * 1024);
            while (iterA.hasNext()) {
                Map.Entry<String, Double> item = iterA.next();
                String key = item.getKey();
                Double value = item.getValue();
                bufferedWriter.append(key + "," + value);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void write(String path) {
        String filePath = "E:\\back\\test\\aa.txt";
        long s = System.currentTimeMillis();
        createFileIfNotExist(filePath);
        try {
            FileWriter fw = new FileWriter(filePath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fw, 1024 * 1024);
            for (int i = 0; i < 10000000; ++i) {
                String jsonStr = "this is a content:" + i;
                bufferedWriter.append(jsonStr);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("time cost:" + (System.currentTimeMillis() - s));
    }

    public static List<String> getTop50() {
        String top50Json = "[\"word.connect.games.world.wordforest\",\"com.handmark.expressweather\",\"absworkout.femalefitness.workoutforwomen.loseweight.tv\",\"com.easybrain.nonogram\",\"com.particlenews.newsbreak\",\"com.grindrapp.android\",\"com.pixel.art.coloring.color.number\",\"es.socialpoint.wordlife\",\"com.callapp.contacts\",\"com.aws.android\",\"com.tripledot.woodoku\",\"com.peoplefun.wordcross\",\"sixpack.sixpackabs.absworkout.abexercises.tv\",\"mobi.ifunny\",\"com.tripledot.solitaire\",\"wp.wattpad\",\"com.smamolot.gusher\",\"com.opera.mini.native.beta\",\"kik.android\",\"com.opera.mini.native\",\"com.zynga.words3\",\"com.mobilityware.spider\",\"com.snaptube.premium\",\"in.playsimple.wordtrip\",\"com.psafe.msuite\",\"puzzle.blockpuzzle.cube.relax\",\"com.enflick.android.TextNow\",\"com.taggedapp\",\"jigsaw.puzzle.game.banana\",\"com.freemannew.videoforyoutube\",\"in.playsimple.tripcross\",\"tunein.player\",\"jp.gocro.smartnews.android\",\"com.audiomack\",\"com.myyearbook.m\",\"com.tumblr\",\"com.interfocusllc.patpat\",\"com.mobilityware.solitaire\",\"com.adfone.aditup\",\"imoblife.androidsensorbox\",\"com.cashwalklabs.cashwalk\",\"com.lightinthebox.android\",\"com.easybrain.make.music\",\"com.americasbestpics\",\"com.staplegames.blocksClassicSGGP\",\"com.domobile.applockwatcher\",\"com.loop.match3d\",\"com.fugo.wow\",\"com.inspiredsquare.jupiter\",\"com.gogii.textplus\"]";
        JSONArray array = JSONObject.parseArray(top50Json);
        List<String> top50 = new ArrayList<>();
        Iterator<Object> iter = array.iterator();
        while (iter.hasNext()) {
            top50.add((String) iter.next());
        }
        return top50;
    }
}
