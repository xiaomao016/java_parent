package com.zx.work.test.http;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * @author zx
 * @description:
 * @date 2022/9/2
 */
@Slf4j
public class ProxyHttp {

    public JSONObject httpPost(String url, JSONObject jsonParam) {

        // post请求返回结果
        CloseableHttpClient httpClient = HttpClients.createDefault();
        JSONObject jsonResult = null;
        HttpPost httpPost = new HttpPost(url);
        HttpHost target = new HttpHost("qa.chetong.net/ailoss/getAiLossPageURL", 8080,
                "http");
        HttpHost proxy = new HttpHost("47.252.17.103", 7000, "http");
        // 设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(2000).setConnectTimeout(2000).setProxy(proxy).build();
        httpPost.setConfig(requestConfig);
        try {

            if (null != jsonParam) {

                // 解决中文乱码问题
                StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json");
                httpPost.setEntity(entity);
            }
            CloseableHttpResponse result = httpClient.execute(target,httpPost);
            // 请求发送成功，并得到响应
            if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

                String str = "";
                try {

                    // 读取服务器返回过来的json字符串数据
                    str = EntityUtils.toString(result.getEntity(), "utf-8");
                    // 把json字符串转换成json对象
                    jsonResult = JSONObject.parseObject(str);
                } catch (Exception e) {

                    log.error("post请求提交失败:" + url, e);
                }
            }
        } catch (IOException e) {

            log.error("post请求提交失败:" + url, e);
            System.out.println(e);
        } finally {

            httpPost.releaseConnection();
        }
        return jsonResult;
    }

    public String httpGet(String url) {

        // post请求返回结果
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String jsonResult = null;
        HttpGet httpPost = new HttpGet(url);
//        HttpHost target = new HttpHost("47.253.44.16", 8089, "http");
        HttpHost target = new HttpHost("www.baidu.com", 80, "http");
        HttpHost proxy = new HttpHost("47.252.17.103", 8081, "http");
        // 设置请求和传输超时时间
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(20000).setConnectTimeout(20000).setProxy(proxy).build();
        httpPost.setConfig(requestConfig);
        try {

            CloseableHttpResponse result = httpClient.execute(target,httpPost);
            // 请求发送成功，并得到响应
            if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

                String str = "";
                try {
                    // 读取服务器返回过来的json字符串数据
                    str = EntityUtils.toString(result.getEntity(), "utf-8");
                    jsonResult = str;
                    log.info("result:{}",str);
                } catch (Exception e) {
                    log.error("post请求提交失败:" + url, e);
                }
            }
        } catch (IOException e) {

            log.error("post请求提交失败:" + url, e);
            System.out.println(e);
        } finally {

            httpPost.releaseConnection();
        }
        return jsonResult;
    }

    //测试main方法
    public static void main(String[] args) {

        String url = "http://47.253.44.16:8089/etl/health";

        String json = "";
        ProxyHttp httpClient = new ProxyHttp();
        String jsonObject = httpClient.httpGet(url);
        System.out.println(jsonObject);

    }
}
