package com.zx.work.test.string;

import java.util.regex.Pattern;

/**
 * @author zx
 * @description:
 * @date 2022/7/12
 */
public class T712 {
    public static void main(String[] args) {

//        String str = "";
//        String str = "??";
//        String str = "12";
//        String str = "12.3a";
        String str = null;

        boolean int_flag = Pattern.compile("^-?[1-9]\\d*$").matcher(str).find();

        boolean double_flag = Pattern.compile("^-?([1-9]\\d*\\.\\d*|0\\.\\d*[1-9]\\d*|0?\\.0+|0)$").matcher(str).find();

        System.out.println("is int:"+int_flag +"..."+"is double:"+double_flag);

    }
}
