package com.zx.work.test.base;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.zx.design.decorate.simple.A;
import com.zx.work.test.y23.AdjustPb;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

/**
 * @author zx
 * @description:
 * @date 2022/8/4
 */
public class T0804 {
    public static void main(String[] args) {

        List<String> domains = Lists.newArrayList("abc","abb","ccc","ddd");

        List<String> blackDomains = Lists.newArrayList("aaa0","ccc","ddd","eee");


        List<AdjustPb> adjustPbs = new ArrayList<>();
        for(int i=0;i<3;++i){
            AdjustPb pb = new AdjustPb();
            pb.setId(i);
            pb.setPbDomain("aaa"+i);
            if(i==0 || i==1){
                pb.setStatus("active");
            }else{
                pb.setStatus("pendding");
            }
            adjustPbs.add(pb);

        }
        System.out.println("===="+JSON.toJSONString(adjustPbs));

        long count  = adjustPbs.stream().filter(a->a.getStatus().equalsIgnoreCase("active")).count();
        System.out.println(count);
        List<AdjustPb> result = adjustPbs.stream().filter(a -> {
            if (!blackDomains.contains(a.getPbDomain())) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        result = result.stream().filter(a->a.getStatus().equalsIgnoreCase("pendding")).collect(Collectors.toList());

        System.out.println(JSON.toJSONString(result));

        AdjustPb adjustPb = result.get(random(result.size()));
        System.out.println(JSON.toJSONString(adjustPb));

    }

    public static int random(int range) {
        Random ran = new Random();
        return ran.nextInt(range);
    }
}
