package com.zx.work.test.ip.util;


import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import inet.ipaddr.AddressStringException;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author zx
 * @description: ipv6地址封装类
 * 根据CIDR格式构建ipv6对象
 * IP地址16字节，用8个部分表示
 * 为了简化，不考虑十进制表示的兼容IPV4的写法，只考虑对前导0和全0简化的情况。
 * 示例1：
 * 2001:124A:2000:0000:0000:0000:0000:0000/48
 * 可以简写为：
 * 2001:124A:2000:0:0:0:0:0/48
 * 或：
 * 2001:124A:2000::/48
 */
public class IPV6 {


    public static void main(String[] args) throws Exception {
        String ipv6Str = "::/64";
        File database = new File("C:\\Users\\dell\\Desktop\\临时\\10.11\\GeoLite2-City_20221011\\GeoLite2-City.mmdb");

        // This reader object should be reused across lookups as creation of it is
        // expensive.
        DatabaseReader reader = new DatabaseReader.Builder(database).build();

// If you want to use caching at the cost of a small (~2MB) memory overhead:
// new DatabaseReader.Builder(file).withCache(new CHMCache()).build();

//        InetAddress ipAddress = InetAddress.getByName("2a01:4b00:e019:e800:1a89:24ff:fec9:bdd8");
        InetAddress ipAddress = InetAddress.getByName("2401:4900:1cc5:9fb3:c2e:5e92:a7:e1df");

        CityResponse response = reader.city(ipAddress);

        Country country = response.getCountry();
        System.out.println(country.getIsoCode());
    }
}

