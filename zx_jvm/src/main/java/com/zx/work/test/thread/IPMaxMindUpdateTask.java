package com.zx.work.test.thread;

import javax.annotation.Resource;

public class IPMaxMindUpdateTask implements Runnable {

    /**
     * 每月更新两次
     * @return
     */
    public String getCron() {
        return "0 0 0 1/15 * ?";
    }

    @Override
    public void run() {
        System.out.println("refresh db");
    }
}
