package com.zx.work.test.y22;

import com.zx.work.utils.DateUtil;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/12/14 0014 16:13
 * @description:
 */
public class Test1214 {
    public static void main(String[] args) {
        String re = DateUtil.currentDateStr("yyyyMMddHHmmss");
        System.out.println(re);
    }
}
