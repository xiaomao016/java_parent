package com.zx.work.test.y23;

import com.zx.work.utils.DateUtilFinal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

public class T616 {
    public static void main(String[] args) {
        long utcTimestamp = Instant.now().toEpochMilli();
        long systime = System.currentTimeMillis();
        long timestamp = Clock.systemUTC().millis();

        System.out.println(utcTimestamp);
        System.out.println(systime);
        System.out.println(timestamp);

        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(systime));

        System.out.println((1686905793992L - 1686905327349L) / 1000);

        // 获取当前机器的默认时区
        TimeZone timeZone = TimeZone.getDefault();

        // 获取当前时区的 ID
        String timeZoneId = timeZone.getID();
        System.out.println(timeZoneId);

        System.out.println("=====================");

        // 定义日期时间格式
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        // 要转换的字符串日期
        String dateString = "2023-06-15 10:30:00";

        try {
            // 将字符串日期解析为 Date 对象
            Date date = dateFormat.parse(dateString);


            // 获取时间戳（以毫秒为单位）
            long timestamp2 = date.getTime();

            // 打印时间戳
            System.out.println(timestamp2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
