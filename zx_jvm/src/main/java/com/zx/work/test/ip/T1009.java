package com.zx.work.test.ip;

import java.util.HashMap;

/**
 * @author zx
 * @description:
 * @date 2022/10/9
 *
 * 示例：
 * 那么 从 172.168.16.10/25 可以得到的信息如以下
 *
 * Ipv4 地址 = 172.168.16.10
 * 子网掩码 = 255.255.255.128
 * 网络地址 = 172.168.16.0
 * 广播地址 = 172.168.16.127
 * 地址范围 = 172.168.16.0 - 172.168.16.127
 *
 * 可使用地址 = 172.168.16.1 - 172.168.16.126
 *
 */
public class T1009 {
    /**
     *
     * 103.164.89.0/24
     *
     * 103.164.124.0/23
     *
     * 1.计算主机号位数
     * 2.计算地址数
     * 3.用网络号
     *
     */
    public static void main(String[] args) {
        String ipStr = "103.164.124.0/23";
        String[] ip_mask = ipStr.split("/");

        String subNetNumber = ip_mask[0];
        System.out.println(subNetNumber);
        String mask = ip_mask[1];
        System.out.println(mask);

        long ip = parseIp(subNetNumber);
        System.out.println(ip);
        System.out.println(Long.toBinaryString(ip));
        // 01100111  10100100  01111100 00000000

        //去掉子网络地址和广播地址
        //计算网络范围
//        long ip_range = (long) (Math.pow(2,32-Integer.parseInt(mask)) - 2);
        long ip_range = parseRange(Integer.parseInt(mask));
        System.out.println(ip_range);

        //计算ip范围
        long MIX = ip;
        long MAX = ip+ip_range;
        System.out.println(MIX+":"+MAX);






    }

    public static Long parseRange(Integer mask){
        //去掉子网络地址和广播地址
        //计算网络范围
        long ip_range_abs = (long) (Math.pow(2,32-mask) - 2);
        return ip_range_abs;
    }

    public static Long parseIp(String ipv4Address) {
        String[] ipStr = ipv4Address.trim()
                .split("\\.");
        if (ipStr.length != 4) {
            return 0L;
        }
        return Long.valueOf(ipStr[0]) * (1 << 24)
                + Long.valueOf(ipStr[1]) * (1 << 16)
                + Long.valueOf(ipStr[2]) * (1 << 8)
                + Long.valueOf(ipStr[3]);
    }

}
