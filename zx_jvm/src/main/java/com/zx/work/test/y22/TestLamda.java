package com.zx.work.test.y22;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/11/24 0024 13:45
 * @description:
 */
public class TestLamda {
    public static void main(String[] args) {
        TestLamda test = new TestLamda();
        String s = "110120";

        test.parseArray(s).stream().forEach(a-> System.out.println(a));
        int re = (int)Math.pow(2,10);
        System.out.println(re);
        int[]arr = new int[3];
        int a = arr.length;
    }




    public List<Long> parseArray(String s){
        return  Arrays.stream(s.split(","))
                .map(a->Long.valueOf(a))
                .collect(Collectors.toList());
    }
}
