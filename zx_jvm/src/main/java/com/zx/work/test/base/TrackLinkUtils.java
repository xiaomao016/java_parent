package com.zx.work.test.base;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.CharsetNames;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriUtils;

import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * eg:
 * https://app.adjust.io/9q26yqn?
 * campaign=RU_AP_APP_AND_Alfaleads_bdmat
 * &install_callback=http%3A%2F%2Fpb.bidamericangame.com%2Fpb%2Flsr%3Ftransaction_id%3D{transaction_id}
 * &event_callback_tcj9bq=http%3A%2F%2Fpb.bidamericangame.com%2Fevent%3Ftransaction_id%3D{transaction_id}%26event_name%3Dadd_to_cart
 * &action_id={ad_id}_{nonce}_{country}_{language}
 * &gps_adid={gaid}
 * &android_id={gaid}
 * &idfa={idfa}
 * &subpublisher_id={offer_id}_{affiliate_id}_{affiliate_sub_id}
 * &adgroup={offer_id}_{affiliate_sub_id}
 * &ip_address={ip}
 * &creative={offer_id}_{affiliate_id}_{affiliate_sub_id}
 */
@Slf4j
public class TrackLinkUtils {

    public static String replaceTrackLink(String trackingLink, String camp, String domain) {
        if(trackingLink.contains("   ")){
            throw new RuntimeException("特殊字符");
        }
        if(StringUtils.isEmpty(trackingLink)){
            return null;
        }
        String subUrl = trackingLink.substring(0, trackingLink.indexOf("?") + 1);
        if (StringUtils.isEmpty(subUrl)) {
            subUrl = trackingLink;
        }
        String params = trackingLink.substring(trackingLink.indexOf("?") + 1);
        Map<String, String> split;
        if (params.contains("&")) {
            split = Splitter.on("&").withKeyValueSeparator("=").split(params);
        } else {
            split = new HashMap<>();
        }
        Map<String, String> linkMap = new LinkedHashMap<>();
        Iterator<Map.Entry<String, String>> it = split.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            String parameter = entry.getKey();
            String value = entry.getValue();
            if ("campaign".equalsIgnoreCase(parameter) && StringUtils.isNotEmpty(camp)) {
                value = camp;
            } else if (parameter.startsWith("event_callback") || parameter.startsWith("install_callback") || parameter.startsWith("rejected_install_callback")) {
                String urlDecode = UriUtils.decode(entry.getValue(), CharsetNames.UTF_8);
                String rep = replaceDomainAndPort(domain, null, urlDecode);
                value = UriUtils.encode(rep, CharsetNames.UTF_8);
                value = value.replace("%7B", "{").replace("%7D", "}");
            }
            linkMap.put(entry.getKey(), value);
        }
        String parameters = Joiner.on("&").withKeyValueSeparator("=").join(linkMap);
        return subUrl + parameters;
    }

    public static void main(String[] args) {
        //String test = "https://app.adjust.com/6u82ew9?campaign=Yaqoot&adgroup=CO16_316_{affiliate_id}&creative={affiliate_sub_id}&idfa={idfa}&click_id={transaction_id}&gps_adid={gaid}&android_id={android_id}&tracker_limit=100000&event_callback_44mhit=http%3A%2F%2Fpb.tpgifo.top%2Fpostback%3Fclickid%3D{clickid}%26goal%3D44mhit\n";
        String test = "https://app.adjust.com/ro9kvb9?campaign=soy1128_ah_1194_{date}&adgroup=66_{date}&creative={affiliate_id}_{affiliate_sub_id}&idfa={idfa}&click_id={transaction_id}&gps_adid={gaid}&android_id={android_id}&ip_address={ip}&campaign_id={offer_id}&publisher_id=66_{date}&cost_type={event_name}&cost_amount={payout}&cost_currency=USD&install_callback=http%3A%2F%2Fpb.foldszzv.top%2Fpostback%3Ftransaction_id%3D{transaction_id}&tracker_limit=100000";
        String s = replaceTrackLink(test, "mediumsfoo", "mediumsfoo888");
        System.out.println(s);
    }

    /**
     * 替换url中的参数
     *
     * @param url
     * @param name
     * @param accessToken
     * @return
     */
    public static String replaceAccessTokenReg(String url, String name, String accessToken) {
        if (StringUtils.isNotBlank(url) && StringUtils.isNotBlank(accessToken)) {
            url = url.replaceAll("(" + name + "=[^&]*)", name + "=" + accessToken);
        }
        return url;
    }

    /**
     * 替换url的域名
     * eg:http://pb.bidamericangame.com/pb/lsr?transaction_id={transaction_id}
     * @param domain
     * @param port
     * @param url
     * @return
     */
    public static String replaceDomainAndPort(String domain, String port, String url) {
        String url_bak = "";
        if (url.indexOf("//") != -1) {
            String[] splitTemp = url.split("//");
            url_bak = splitTemp[0] + "//";
            if (port != null) {
                url_bak = url_bak + domain + ":" + port;
            } else {
                url_bak = url_bak + domain;
            }
            if (splitTemp.length >1 && splitTemp[1].indexOf("/") != -1) {
                String[] urlTemp2 = splitTemp[1].split("/");
                if (urlTemp2.length > 1) {
                    for (int i = 1; i < urlTemp2.length; i++) {
                        url_bak = url_bak + "/" + urlTemp2[i];
                    }
                }
            }
        }
        return url_bak;
    }

    public static Map<String,String> parseParams(String trackingLink){
        String subUrl = trackingLink.substring(0, trackingLink.indexOf("?") + 1);
        if (StringUtils.isEmpty(subUrl)) {
            subUrl = trackingLink;
        }
        System.out.println("subUrl:"+subUrl);
        Map<String, String> split = new HashMap<>();
        String params = trackingLink.substring(trackingLink.indexOf("?") + 1);
        if(StringUtils.isEmpty(params)){
            return split;
        }
        String[] kvStrArr = params.split("&");
        for(String kvs:kvStrArr){
            String[]kv = kvs.split("=");
            String key = kv[0];
            String value = kv[1];
            if(!split.containsKey(key)){
                split.put(key,value);
            }
        }
        return split;
    }

    public static String getParamByKey(String link,String key){
        if(StringUtils.isEmpty(link)){
            return null;
        }
        Map<String, String> params = parseParams(link);

        if(CollectionUtils.isEmpty(params)){
            return null;
        }

        if(StringUtils.isEmpty(key)){
            return null;
        }

        if(!params.containsKey(key)){
            return null;
        }

        return params.get(key);
    }


    public static String getAdjustPbDomain(String trackingLink){
        String adjustPbDomain = null;
        try {
            adjustPbDomain = null;
            String params = trackingLink.substring(trackingLink.indexOf("?") + 1);
            Map<String, String> paramMap = Splitter.on("&").withKeyValueSeparator("=").split(params);
            String callbackUrl = paramMap.get("install_callback");
            if(callbackUrl == null){
                for (String key : paramMap.keySet()) {
                    if(key.startsWith("event_callback")){
                        callbackUrl = paramMap.get(key);
                        break;
                    }
                }
            }
            if(callbackUrl != null){
                try {
                    String decodeCallbackUrl = URLDecoder.decode(callbackUrl, CharsetNames.UTF_8);
                    URL url = new URL(decodeCallbackUrl);
                    adjustPbDomain = url.getHost();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return adjustPbDomain;
    }
}
