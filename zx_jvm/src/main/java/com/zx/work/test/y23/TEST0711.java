package com.zx.work.test.y23;

import com.zx.work.utils.DateUtilFinal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TEST0711 {
    public static void main(String[] args) {
        Date now = getFixedMinuteDate(new Date());
//        Date to = now;
        Date startTime = addMinutes(now, ~10 + 1);


        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(now));
//        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(to));
        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(startTime));
    }

    public static double f(long x, double oldScore) {
        double alpha = -0.00000266502904282;
        double pa = alpha * x;
        return oldScore * Math.exp(pa);
    }

    public static Date getFixedMinuteDate(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String fixFormat = dayFormat.format(date) + ":00";
        try {
            return df.parse(fixFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date addMinutes(Date date, int minutes) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(12, minutes);
        return c.getTime();
    }

}
