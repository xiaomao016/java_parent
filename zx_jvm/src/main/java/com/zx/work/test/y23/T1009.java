package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

public class T1009 {
    public static void main(String[] args) {
        List<String> l = new ArrayList<>();
        l.add("zf");
        l.add("lz");
        l.add("dz");

        List<String> sc = new ArrayList<>();
        sc.addAll(l);

        sc.remove("zf");

        System.out.println(JSON.toJSONString(sc));
        System.out.println(JSON.toJSONString(l));

        System.out.println("=========");
        List<String> toDel = new ArrayList<>();
        for(String z:l){
            if(z.equalsIgnoreCase("dz")){
                toDel.add(z);
            }
        }

        l.removeAll(toDel);
        System.out.println(JSON.toJSONString(l));
    }
}
