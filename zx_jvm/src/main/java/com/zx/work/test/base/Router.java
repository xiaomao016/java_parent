package com.zx.work.test.base;

/**
 * @author zx
 * @description:
 * @date 2022/8/4
 */
public final class Router{
    public final String  ip;
    public final Integer port;
    public final String  iface;
    //构造函数
    public Router(String ip,
                  Integer port, String iface){
        this.ip = ip;
        this.port = port;
        this.iface = iface;
    }
    //重写equals方法
    public boolean equals(Object obj){
        if (obj instanceof Router) {
            Router r = (Router)obj;
            return iface.equals(r.iface) &&
                    ip.equals(r.ip) &&
                    port.equals(r.port);
        }
        return false;
    }
    public int hashCode() {
        //省略hashCode相关代码
        return 0;
    }
}
