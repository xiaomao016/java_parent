package com.zx.work.test.ctit;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class TestCTIT3 {
    public static void main(String[] args) {
        List<CTITProportionReportBean> reportList = new ArrayList<>();

        // 示例测试数据
        // 添加示例数据
        reportList.add(new CTITProportionReportBean(1, 1L, "Offer1", 1, "Affiliate1", "AffSub1", "Country1", 10L, 20L, "Prod1", parseDate("2023-10-24 10:00:01")));
        reportList.add(new CTITProportionReportBean(1, 1L, "Offer1", 1, "Affiliate1", "AffSub1", "Country1", 5L, 20L, "Prod1", parseDate("2023-10-24 10:00:01")));
        reportList.add(new CTITProportionReportBean(1, 2L, "Offer2", 1, "Affiliate1", "AffSub3", "Country2", 8L, 15L, "Prod1", parseDate("2023-10-24 12:00:00")));
        reportList.add(new CTITProportionReportBean(2, 3L, "Offer3", 1, "Affiliate2", "AffSub4", "Country1", 12L, 30L, "Prod2", parseDate("2023-10-25 09:00:00")));
        reportList.add(new CTITProportionReportBean(2, 3L, "Offer3", 1, "Affiliate2", "AffSub5", "Country1", 18L, 30L, "Prod2", parseDate("2023-10-25 09:00:00")));

        log.info("ori:{}", JSON.toJSONString(reportList));

        Map<String, List<CTITProportionReportBean>> groupedMap = reportList.stream()
                .collect(Collectors.groupingBy(bean ->
                        bean.getOfferId() + "-" +
                                bean.getAffiliateId() + "-" +
                                bean.getAffSub() + "-" +
                                bean.getCountry() + "-" +
                                formatDate(bean.getCreateTime())));

        groupedMap.values().forEach(group -> {
            long totalInstallCount = group.stream().mapToLong(CTITProportionReportBean::getInstallCount).sum();
            group.forEach(bean -> bean.setCtitProportion((double) bean.getInstallCount() / totalInstallCount));
        });
        log.info("group and cal ctit:{}", JSON.toJSONString(reportList));

        // 补全属性
        for (CTITProportionReportBean bean : reportList) {
            Date createTime = bean.getCreateTime();
            Long ctit = bean.getCtit();
            Long installCount = bean.getInstallCount();

            // 筛选相同日期的对象
            List<CTITProportionReportBean> sameDateBeans = filterByCreateTime(reportList, createTime);
            log.info("same date:{}",JSON.toJSONString(sameDateBeans));

            // 计算ctit为30的安装数占比
            Double ctitProportion_30 = calculateProportion(sameDateBeans, 30L, installCount);
            bean.setCtitProportion_30(ctitProportion_30);

            // 计算ctit为120的安装数占比
            Double ctitProportion_120 = calculateProportion(sameDateBeans, 120L, installCount);
            bean.setCtitProportion_120(ctitProportion_120);

            // 计算ctit为720的安装数占比
            Double ctitProportion_720 = calculateProportion(sameDateBeans,720L,installCount);
            bean.setCtitProportion_720(ctitProportion_720);
        }


        log.info("final re:{}",JSON.toJSONString(reportList));
    }


    private static Double calculateProportion(List<CTITProportionReportBean> reportList, Long ctitValue, Long installCount) {
        Long totalInstallCount = 0L;
        Long ctitInstallCount = 0L;

        // 遍历报告列表，计算总安装数和指定 ctit 的安装数
        for (CTITProportionReportBean bean : reportList) {
            totalInstallCount += bean.getInstallCount();

            if (bean.getCtit().equals(ctitValue)) {
                ctitInstallCount += bean.getInstallCount();
            }
        }

        if (totalInstallCount == 0) {
            return 0.0;
        }

        // 计算 ctit 比例
        return (double) ctitInstallCount / totalInstallCount;
    }

    private static List<CTITProportionReportBean> filterByCreateTime(List<CTITProportionReportBean> reportList, Date createTime) {
        List<CTITProportionReportBean> filteredList = new ArrayList<>();

        for (CTITProportionReportBean bean : reportList) {
            if (bean.getCreateTime().equals(createTime)) {
                filteredList.add(bean);
            }
        }

        return filteredList;
    }

    private static Date parseDate(String dateString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
