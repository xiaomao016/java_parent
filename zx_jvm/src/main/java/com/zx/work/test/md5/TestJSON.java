package com.zx.work.test.md5;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

public class TestJSON {
    public static void main(String[] args) {
        String str = "";
        JSONObject obj = JSONObject.parseObject(str);

        String os = "iOS";
        if(os.equalsIgnoreCase("IOS")){
            System.out.println("ios");
        }
    }

    /**
     * 格式化os-version
     *
     * @param osv
     * @return
     */
    private static String formatOsv(String osv) {
        if (StringUtils.isEmpty(osv)) {
            return "0";
        }
        String[] split = osv.split("\\.");
        if (split.length >= 3) {
            return split[0] + "." + split[1];
        } else {
            return osv;
        }
    }

}
