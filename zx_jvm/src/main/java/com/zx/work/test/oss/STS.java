package com.zx.work.test.oss;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.auth.sts.AssumeRoleRequest;
import com.aliyuncs.auth.sts.AssumeRoleResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;

/**
 * @author zx
 * @description:
 * @date 2022/10/20
 */
public class STS {
    public static void main(String[] args) {

        // oss-key-id: LTAI5tSVHkTXX6KKQbkfc39k
        //  oss-key-secret: IOiyNAZiZWDAPXoUloGx8B3aCCyiEp
        //  bucket-name-us: click-data-filter-mc
        //  endpoint-us: https://oss-us-east-1-internal.aliyuncs.com
        //构建一个阿里云客户端，用于发起请求。
        //设置调用者（RAM用户或RAM角色）的AccessKey ID和AccessKey Secret。
        String oss_key = OSSCommInfo.key;
        String secret = OSSCommInfo.secret;
        String regionId = "us-east-1";
        DefaultProfile profile = DefaultProfile.getProfile(regionId, oss_key, secret);
        IAcsClient client = new DefaultAcsClient(profile);

        //构造请求，设置参数。
        AssumeRoleRequest request = new AssumeRoleRequest();
        request.setRegionId(regionId);
        request.setRoleArn("acs:ram::1479860729258551:role/aliyunadamaccessingdatabaserole");
        request.setRoleSessionName("Bid_report");

        //发起请求，并得到响应。
        try {
            AssumeRoleResponse response = client.getAcsResponse(request);
            System.out.println(new Gson().toJson(response));
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
        }

    }
}
