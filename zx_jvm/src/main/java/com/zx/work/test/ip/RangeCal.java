package com.zx.work.test.ip;

/**
 * @author zx
 * @description:
 * @date 2022/10/10
 */
public class RangeCal {
    public static void main(String[] args) {
        //1746724839
        System.out.println(getIPRange("192.168.161.125/30"));

    }

    public static String  getIPRange(String ipv4){
        String[] ipv4_mask = ipv4.split("/");

        long ip_range_abs = parseRange(Integer.parseInt(ipv4_mask[1]));
        System.out.println(ip_range_abs);
        long ipFrom = parseIp(ipv4_mask[0]);
        long ipTo = ipFrom + ip_range_abs-1;
        return ipFrom+"~"+ipTo;
    }

    public static Long parseIp(String ipv4Address) {
        String[] ipStr = ipv4Address.trim()
                .split("\\.");
        if (ipStr.length != 4) {
            return 0L;
        }
        return Long.valueOf(ipStr[0]) * (1 << 24)
                + Long.valueOf(ipStr[1]) * (1 << 16)
                + Long.valueOf(ipStr[2]) * (1 << 8)
                + Long.valueOf(ipStr[3]);
    }
    /**
     * 根据子网掩码位数获取该子网的IP总数量
     * @param mask
     * @return
     */
    public static Long parseRange(Integer mask){
        //去掉子网络地址和广播地址
        //计算网络范围
        long ip_range_abs = (long) (Math.pow(2,32-mask)-2);
        return ip_range_abs;
    }

    public String getNetmask(Long midNumber,int netmaskNumeric) {
        StringBuffer sb = new StringBuffer(15);

        for (int shift = 24; shift > 0; shift -= 8) {
            // process 3 bytes, from high order byte down.
            sb.append(Integer.toString((netmaskNumeric >>> shift) & 0xff));
            sb.append('.');
        }
        sb.append(Integer.toString(netmaskNumeric & 0xff));

        return sb.toString();
    }
}
