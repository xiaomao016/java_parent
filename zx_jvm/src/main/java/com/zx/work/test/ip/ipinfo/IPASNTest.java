package com.zx.work.test.ip.ipinfo;

import com.maxmind.db.MaxMindDbConstructor;
import com.maxmind.db.MaxMindDbParameter;
import com.maxmind.db.Reader;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

public class IPASNTest {
    public static void main(String[] args) throws IOException {
        File database = new File("C:\\Users\\dell\\Desktop\\临时\\11.1-IP\\ipInfo\\asn.mmdb");
//        File database = new File("static/asn.mmdb");
        try (Reader reader = new Reader(database)) {
            InetAddress address = InetAddress.getByName("146.59.71.0");

            // get() returns just the data for the associated record
            LookupResult result = reader.get(address, LookupResult.class);
            System.out.println(result.asn); // Output → AS15169
            System.out.println(result.name); // Output → Google LLC
            System.out.println(result.domain); // Output → google.com

        }
    }

    public static class LookupResult {
        private final String asn;
        private final String name;
        private final String domain;

        @MaxMindDbConstructor
        public LookupResult(
                @MaxMindDbParameter(name = "asn") String asn,
                @MaxMindDbParameter(name = "name") String name,
                @MaxMindDbParameter(name = "domain") String domain
        ){
            this.asn = asn;
            this.name = name;
            this.domain = domain;
        }
    }
}


