package com.zx.work.test.staticField;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ClickSpeed {

    public ClickSpeed(String speedConfig,String countryCode,String prod){
        this.speedConfig = speedConfig;
        this.countryCode = countryCode;
        this.prod = prod;
    }

    private Long id;

    private Date createTime;

    private Date updateTime;

    private String countryCode;

    private String countryName;

    private String speedConfig;

    private String prod;

    private Boolean isAvg;
}
