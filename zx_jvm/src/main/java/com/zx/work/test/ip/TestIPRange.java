package com.zx.work.test.ip;

public class TestIPRange {
    public static void main(String[] args) {
        String symbolicIP = "255.0.0.254";
        String[] st = symbolicIP.split("\\.");
        int baseIPNumeric = 0;
        int i = 24;
        for (int n = 0; n < st.length; n++) {
            int value = Integer.parseInt(st[n]);
            //确保在+0~255内
            if (value != (value & 0xff)) {
                throw new NumberFormatException("Invalid IP address: " + symbolicIP);
            }
            baseIPNumeric += value << i;
            i -= 8;
        }
        System.out.println(baseIPNumeric);

        System.out.println("==========");
        System.out.println(parseIp("255.0.0.254"));
    }

    /**
     * 这里解析IP为long型，因为整形表示会有溢出情况。
     *
     * @param ipv4Address
     * @return
     */
    public static Integer parseIp(String ipv4Address) {
        String[] ipStr = ipv4Address.trim()
                .split("\\.");
        if (ipStr.length != 4) {
            return 0;
        }
        return Integer.valueOf(ipStr[0]) * (1 << 24)
                + Integer.valueOf(ipStr[1]) * (1 << 16)
                + Integer.valueOf(ipStr[2]) * (1 << 8)
                + Integer.valueOf(ipStr[3]);
    }
}
