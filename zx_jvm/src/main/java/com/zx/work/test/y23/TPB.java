package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TPB {
    public static void main(String[] args) {

        String json = "[{\"createTime\":1680148632000,\"id\":70,\"pbDomain\":\"pb.collaborationiszd.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"active\",\"updateTime\":1680148632000},{\"createTime\":1680158076000,\"id\":71,\"pbDomain\":\"pb.starttingeabf.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"active\",\"updateTime\":1680158076000},{\"createTime\":1679554709000,\"id\":74,\"pbDomain\":\"pb.bidfeyz6j.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"pendding\"},{\"createTime\":1679554734000,\"id\":75,\"pbDomain\":\"pb.allaccessdzj.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"pendding\"},{\"createTime\":1680162213000,\"id\":78,\"pbDomain\":\"pb.mdctegug.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"active\",\"updateTime\":1680162213000},{\"createTime\":1680166972000,\"id\":85,\"pbDomain\":\"pb.zmaogressd.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"active\",\"updateTime\":1680166972000},{\"createTime\":1679554767000,\"id\":86,\"pbDomain\":\"pb.pleanseasyl.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"pendding\"},{\"createTime\":1679554769000,\"id\":89,\"pbDomain\":\"pb.jumpstepren.com\",\"platformId\":31,\"remark\":\"batch save\",\"status\":\"pendding\"}]";

        JSONArray jsonArray = JSON.parseArray(json);

        List<AdjustPb> adjustPbs = jsonArray.toJavaList(AdjustPb.class);
        System.out.println(JSON.toJSONString(adjustPbs));
        List<String> blackDomains = Lists.newArrayList();
        List<AdjustPb> result = adjustPbs.stream().filter(a -> {
            if (!blackDomains.contains(a.getPbDomain())) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        long count = result.stream()
                .filter(pb -> pb.getStatus().equals("active"))
                .count();
        System.out.println(count);
        if(count<=15){//如果active数量小于15，用pendding替换
            System.out.println("xx");
            result = result.stream().filter(pb->pb.getStatus().equalsIgnoreCase("pendding")).collect(Collectors.toList());
        }


        System.out.println("待刷选pb:\n"+JSON.toJSONString(result));

    }

//    public AdjustPb getAnPenddingDomain(Integer platformId, List<String> blackDomains) {
//        List<AdjustPb> adjustPbs = adjustPbMapper.findAllPenddinAndActivePB(platformId);
//        List<AdjustPb> result = adjustPbs.stream().filter(a -> {
//            if (!blackDomains.contains(a.getPbDomain())) {
//                return true;
//            }
//            return false;
//        }).collect(Collectors.toList());
//
//        if (CollectionUtils.isEmpty(result)) {
//            log.error("no pb domain can use...");
//            return null;
//        }
//
//        long count = result.stream()
//                .filter(pb -> pb.getStatus().equals(AdjustPBConstant.STATUS_ACTIVE))
//                .count();
//        log.info("pb active count:{}",count);
//        if(count<=15){//如果active数量小于15，用pendding替换
//            result.stream().filter(pb->pb.getStatus().equalsIgnoreCase(AdjustPBConstant.STATUS_PENDDING)).collect(Collectors.toList());
//        }
//
//        if(result.size()<0){
//            log.error("exclusive active no pendding found..");
//            return null;
//        }
//        log.info("待刷选pb:{}",JSON.toJSONString(result));
//
//        AdjustPb adjustPb = result.get(AliDNSHelper.random(result.size()));
//        return adjustPb;
//    }
}
