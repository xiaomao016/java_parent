package com.zx.work.test.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class Semafere {
    private static final int MAX_DOWNLOAD_THREADS = 10;
    private static final Semaphore semaphore = new Semaphore(MAX_DOWNLOAD_THREADS);

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();

        // 模拟一些下载任务
        for (int i = 0; i < 20; i++) {
            final int fileId = i;
            executor.execute(() -> {
                try {
                    downloadFile(fileId);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        executor.shutdown();
    }

    private static void downloadFile(int fileId) throws InterruptedException {
        // 尝试获取信号量，如果当前下载线程数量大于10，则会阻塞等待
        semaphore.acquire();
        System.out.println("Downloading file " + fileId + ", Threads: " + (MAX_DOWNLOAD_THREADS - semaphore.availablePermits()));

        // 模拟文件下载过程
        Thread.sleep(2000);

        // 下载完成后释放信号量
        semaphore.release();
        System.out.println("Downloaded file " + fileId + ", Threads: " + (MAX_DOWNLOAD_THREADS - semaphore.availablePermits()));
    }
}
