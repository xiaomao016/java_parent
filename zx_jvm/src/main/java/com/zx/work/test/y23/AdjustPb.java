package com.zx.work.test.y23;

import lombok.Data;

import java.util.Date;

@Data
public class AdjustPb {
    private Integer id;
    private Integer platformId;
    private String pbDomain;
    private String status;
    private String remark;
    private Date createTime;
    private Date updateTime;

}