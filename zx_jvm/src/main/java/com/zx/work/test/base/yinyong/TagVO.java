package com.zx.work.test.base.yinyong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagVO {

    private String logicalOperator;

    private String tagName;

    private String arithmeticOperator;

    private String operator;   //临时过渡用的

    private String tagValue;





}
