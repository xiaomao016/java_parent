package com.zx.work.test.ctit;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestCTIT {
    public static void main(String[] args) {
        List<CTITProportionReportBean> reportList = new ArrayList<>();

        // 添加示例数据
        reportList.add(new CTITProportionReportBean(1, 1L, "Offer1", 1, "Affiliate1", "AffSub1", "Country1", 10L, 20L, "Prod1", parseDate("2023-10-24 10:00:01")));
        reportList.add(new CTITProportionReportBean(1, 1L, "Offer1", 1, "Affiliate1", "AffSub1", "Country1", 5L, 20L, "Prod1", parseDate("2023-10-24 10:00:01")));
        reportList.add(new CTITProportionReportBean(1, 2L, "Offer2", 1, "Affiliate1", "AffSub3", "Country2", 8L, 15L, "Prod1", parseDate("2023-10-24 12:00:00")));
        reportList.add(new CTITProportionReportBean(2, 3L, "Offer3", 1, "Affiliate2", "AffSub4", "Country1", 12L, 30L, "Prod2", parseDate("2023-10-25 09:00:00")));
        reportList.add(new CTITProportionReportBean(2, 3L, "Offer3", 1, "Affiliate2", "AffSub5", "Country1", 18L, 30L, "Prod2", parseDate("2023-10-25 09:00:00")));

        // 分组并计算installCount占比
        Map<String, List<CTITProportionReportBean>> groups = new HashMap<>();
        for (CTITProportionReportBean report : reportList) {
            String key = report.getOfferId() + "-" + report.getAffiliateId() + "-" + report.getAffSub() + "-" + formatDate(report.getCreateTime());
            groups.computeIfAbsent(key, k -> new ArrayList<>()).add(report);
        }

        List<CTITProportionReportBean> result = new ArrayList<>();
        for (List<CTITProportionReportBean> group : groups.values()) {
            long totalInstallCount = group.stream().mapToLong(CTITProportionReportBean::getInstallCount).sum();
            for (CTITProportionReportBean report : group) {
                double proportion = (double) report.getInstallCount() / totalInstallCount;
                report.setCtitProportion(roundToNDecimalPlaces(proportion,3));
                result.add(report);
            }
        }

        // 按ctitProportion属性降序排序
        result.sort(Comparator.comparingDouble(CTITProportionReportBean::getCtitProportion).reversed());

        // 打印结果
        for (CTITProportionReportBean report : result) {
            System.out.println(report);
        }
    }

    private static Date parseDate(String dateString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static double roundToNDecimalPlaces(double value, int decimalPlaces) {
        double scale = Math.pow(10, decimalPlaces);
        System.out.println(scale);
        return Math.round(value * scale) / scale;
    }
}
