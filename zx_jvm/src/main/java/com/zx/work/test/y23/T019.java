package com.zx.work.test.y23;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.List;

public class T019 {

    private static final int MAX_APPS = 10;
    private List<String> appList;

    public T019() {
        appList = new ArrayList<>();
    }

    public String updateString(String newApp) {
        // 如果新应用已经存在，移动它到最前面
        if (appList.contains(newApp)) {
            appList.remove(newApp);
        }
        // 追加或插入新应用
        appList.add(0, newApp);
        // 如果超过了最大应用数量，移除最后一个应用
        if (appList.size() > MAX_APPS) {
            appList.remove(MAX_APPS);
        }
        // 组成新的字符串
        StringBuilder sb = new StringBuilder();
        for (String app : appList) {
            sb.append(app).append(",");
        }
        // 去掉最后一个逗号
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public List<String> getAppList() {
        return appList;
    }

    public static void main(String[] args) {
        T019 appString = new T019();
        System.out.println("初始字符串：" + appString.updateString("App1"));
        System.out.println("当前应用列表：" + appString.getAppList());

        System.out.println("更新后的字符串：" + appString.updateString("App2"));
        System.out.println("当前应用列表：" + appString.getAppList());

        System.out.println("更新后的字符串：" + appString.updateString("App3"));
        System.out.println("当前应用列表：" + appString.getAppList());

        System.out.println("更新后的字符串：" + appString.updateString("App4"));
        System.out.println("当前应用列表：" + appString.getAppList());

        System.out.println("更新后的字符串：" + appString.updateString("App2"));
        System.out.println("当前应用列表：" + appString.getAppList());
    }

}
