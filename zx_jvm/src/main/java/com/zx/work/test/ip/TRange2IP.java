package com.zx.work.test.ip;

public class TRange2IP {
    public static void main(String[] args) {

        String ipRange = "486539265-503316478";
        String[]ips= ipRange.split("-");
        Integer ipFrom = Integer.parseInt(ips[0]);
        Integer ipTo = Integer.parseInt(ips[1]);

        System.out.println(convertNumericIpToSymbolic(ipFrom));
        System.out.println(convertNumericIpToSymbolic(ipTo));





    }

    /**
     * 根据ip地址值获取点分10进制的写法表达式
     *
     * @param ip
     * @return
     */
    public static String convertNumericIpToSymbolic(Integer ip) {
        StringBuffer sb = new StringBuffer(15);
        for (int shift = 24; shift > 0; shift -= 8) {
            // 处理3字节，从高往低
            sb.append((ip >>> shift) & 0xff);
            sb.append('.');
        }
        sb.append(ip & 0xff);
        return sb.toString();
    }

}
