package com.zx.work.test.y23;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class T0927 {

    public static void main(String[] args) {
        List<String> ossObjectSummaryList = new ArrayList<>(); // 原始List，数据量较大

        for(int i=0;i<9;++i){
            ossObjectSummaryList.add("string"+i);
        }

        int dataSize = ossObjectSummaryList.size();
        int groupSize = Math.max(Math.round(dataSize / 24), 1); // 计算每组的大小（每10个元素为一组）
        System.out.println(groupSize);

        // 将原始List分组，每10个元素一组
        Map<Integer, List<String>> groupedMap = new HashMap<>();
        for (int i = 0; i < ossObjectSummaryList.size(); i += groupSize) {
            int endIndex = Math.min(i + groupSize, ossObjectSummaryList.size());
            List<String> subList = ossObjectSummaryList.subList(i, endIndex);
            groupedMap.put(i, subList);
        }

        System.out.println(groupedMap.size());
        int num = 0;
        // 打印测试结果
        for (Map.Entry<Integer, List<String>> entry : groupedMap.entrySet()) {
            System.out.println("Key: " + entry.getKey());
//            System.out.println("Grouped List: " + entry.getValue());
//            if(entry.getValue().size()<10){
                System.out.println("Grouped List: " + entry.getValue());
//            }
            num+=entry.getValue().size();
        }
        System.out.println("===========Total num=======");
        System.out.println(num);
    }
}
