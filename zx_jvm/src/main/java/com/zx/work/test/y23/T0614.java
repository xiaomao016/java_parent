package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
@Slf4j
public class T0614 {

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {


        JSONObject obj = new JSONObject();
        obj.put("deviceId", "123");
        System.out.println("obj:" + JSONObject.toJSONString(obj));

        for (int i = 0; i < 3; ++i) {
            JSONObject o1 = JSON.parseObject(JSON.toJSONString(obj));
            o1.put("app", i);
            System.out.println("o1:" + JSONObject.toJSONString(o1));
            for (int j = 4; j < 6; ++j) {
                JSONObject o2 = JSON.parseObject(JSON.toJSONString(o1));
                o2.put("source", j);
                System.out.println("o2:" + JSONObject.toJSONString(o2));
            }
        }

        JSONObject o = new JSONObject();
        o.put("a", "abc");
        String flag = o.getString("flag");
        String flag2 = o.getString("a");

        System.out.println(flag);
        System.out.println(flag2);

        System.out.println("=================");
        String code = "IN";
        String rowKey = "1_IN_18653f8c-fcac-2ba4-ce4f-18653f8cfcac";

        String did = rowKey.substring(3 + code.length());
        System.out.println(did);

        System.out.println("======split=====");
        List<String> rowRangeList = new ArrayList();
        rowRangeList.add("0-g");
        for (String range : rowRangeList) {
            String[] split = range.split("-");
            log.info("start -end:{},{}", split[0], split[1]);


        }
    }

}
