package com.zx.work.test.base;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.apache.commons.compress.utils.CharsetNames;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.UriUtils;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * eg:
 *  * https://app.adjust.io/9q26yqn?
 *  * campaign=RU_AP_APP_AND_Alfaleads_bdmat
 *  * &install_callback=http%3A%2F%2Fpb.bidamericangame.com%2Fpb%2Flsr%3Ftransaction_id%3D{transaction_id}
 *  * &event_callback_tcj9bq=http%3A%2F%2Fpb.bidamericangame.com%2Fevent%3Ftransaction_id%3D{transaction_id}%26event_name%3Dadd_to_cart
 *  * &action_id={ad_id}_{nonce}_{country}_{language}
 *  * &gps_adid={gaid}
 *  * &android_id={gaid}
 *  * &idfa={idfa}
 *  * &subpublisher_id={offer_id}_{affiliate_id}_{affiliate_sub_id}
 *  * &adgroup={offer_id}_{affiliate_sub_id}
 *  * &ip_address={ip}
 *  * &creative={offer_id}_{affiliate_id}_{affiliate_sub_id}
 */
public class StringSplitGoogle {
    public static void main(String[] args) {
//        String params = "https://47.252.21.90?campaign=RU_AP_APP_IOS_Alfaleads_bd&creative={offer_id}_{affiliate_id}_{affiliate_sub_id}&install_callback=http%3A%2F%2pb.tegneclgloadbtonebb.com%2Fpb%2Flsr%3Ftransaction_id%3D{transaction_id}&event_callback_yxskqx=http%3A%2F%2Fpb.tegneclgloadbtonebb.com%2Fevent%3Ftransaction_id%3D{transaction_id}%26event_name%3DPayment%20Success&deep_link=lamoda%3A%2F%2FRU&action_id={ad_id}_{nonce}_{country}_{language}&subpublisher_id={offer_id}_{affiliate_id}_{affiliate_sub_id}&adgroup={offer_id}&ip_address={ip}&idfa={idfa}";
        String p="https://impressions.onelink.me/O9SJ?pid=bidmatrix_int&c={campaign_name}&af_siteid={offer_id}_{affiliate_id}_{affiliate_sub_id}&af_cost_currency=USD&af_sub_siteid={offer_id}__{affiliate_id}_{affiliate_sub_id}&af_ad={offer_name}_{date}&af_ad_id={offer_id}_{date}&af_ad_type={ad_type}&af_adset={offer_name}_{date}_{affiliate_sub_id}&af_adset_id={offer_id}_{date}&af_c_id={offer_id}_{date}&af_sub3=c_yz0lwasimk&agent=c_yz0lwasimk&sub_id={bm}&utm_source={bm}&af_viewthrough_lookback=24h&clickid={transaction_id}&advertising_id={gaid}&idfa={idfa}&af_ua={ua}&af_lang={lang}&af_ip={ip}&af_media_type=app&af_model={model}&af_os_version={os_version}&aff_sub1={b_size}&af_prt=bidmatrix&&af_xplatform_vt_lookback=72h&af_xplatform=true&af_channel={real_aff}";

//        re(params,null);

        try {
//            String res = TrackLinkUtils.replaceTrackLink(p,null,null);
            String res = TrackLinkUtils.replaceTrackLink(p,null,"zzx");
            System.out.println(res);
            System.out.println(p);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }


    public static void re(String trackingLink){
        String subUrl = trackingLink.substring(0, trackingLink.indexOf("?") + 1);
        if (StringUtils.isEmpty(subUrl)) {
            subUrl = trackingLink;
        }
        System.out.println("sobUrl:"+subUrl);
        String domain = "com.new.pb";
        Map<String, String> split;
        String params = trackingLink.substring(trackingLink.indexOf("?") + 1);
        if (params.contains("&")) {
            split = Splitter.on("&").withKeyValueSeparator("=").split(params);
        } else {
            split = new HashMap<>();
        }

        System.out.println("split:"+ JSON.toJSONString(split));

        Map<String, String> linkMap = new LinkedHashMap<>();
        Iterator<Map.Entry<String, String>> it = split.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            String parameter = entry.getKey();
            String value = entry.getValue();
           if (parameter.startsWith("event_callback") || parameter.startsWith("install_callback") || parameter.startsWith("rejected_install_callback")) {
                String urlDecode = UriUtils.decode(entry.getValue(), CharsetNames.UTF_8);
                System.out.println("urlDecode:"+urlDecode);
                String rep = replaceDomainAndPort(domain, null, urlDecode);
                System.out.println("rep:"+rep);
                value = UriUtils.encode(rep, CharsetNames.UTF_8);
                System.out.println("value:"+rep);
                value = value.replace("%7B", "{").replace("%7D", "}");
            }
            linkMap.put(entry.getKey(), value);
        }
        String parameters = Joiner.on("&").withKeyValueSeparator("=").join(linkMap);
        String finalResult =  subUrl + parameters;
        System.out.println(parameters);
        System.out.println(finalResult);
    }

    public Map<String,String> parseParams(String trackingLink){
        String subUrl = trackingLink.substring(0, trackingLink.indexOf("?") + 1);
        if (StringUtils.isEmpty(subUrl)) {
            subUrl = trackingLink;
        }
        System.out.println("sobUrl:"+subUrl);
        Map<String, String> split;
        String params = trackingLink.substring(trackingLink.indexOf("?") + 1);
        if (params.contains("&")) {
            split = Splitter.on("&").withKeyValueSeparator("=").split(params);
        } else {
            split = new HashMap<>();
        }

        System.out.println("split:"+ JSON.toJSONString(split));
        return split;
    }

    /**
     * 替换url的域名
     *
     * @param domain
     * @param port
     * @param url
     * @return
     */
    public static String replaceDomainAndPort(String domain, String port, String url) {
        String url_bak = "";
        if (url.indexOf("//") != -1) {
            String[] splitTemp = url.split("//");
            url_bak = splitTemp[0] + "//";
            if (port != null) {
                url_bak = url_bak + domain + ":" + port;
            } else {
                url_bak = url_bak + domain;
            }

            if (splitTemp.length >= 1 && splitTemp[1].indexOf("/") != -1) {
                String[] urlTemp2 = splitTemp[1].split("/");
                if (urlTemp2.length > 1) {
                    for (int i = 1; i < urlTemp2.length; i++) {
                        url_bak = url_bak + "/" + urlTemp2[i];
                    }
                }
            } else {
            }
        }
        return url_bak;
    }
}
