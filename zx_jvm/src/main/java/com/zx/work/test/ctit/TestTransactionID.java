package com.zx.work.test.ctit;

import com.zx.work.utils.DateUtilFinal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Slf4j
public class TestTransactionID {
    public static void main(String[] args) {
        String ua = "Mozilla/5.0 (Linux; Android 12) applewebkit/537.36 (khtml, like gecko) mobile safari/537.36; 22120RN86G Build/TP1A.220624.014; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/118.0.0.0 Mobile";

        String osv = parseOsv(ua);
        log.info("osv:{}",osv);


        Base64.Encoder urlEncoder = Base64.getUrlEncoder().withoutPadding();
        String brand = "brand";
        String osVersion = "12";
        String appPkg = "pak";
        String adx = "";
        String model = "";
        String os = "Android";
        String dataSourceId = "123";
        String pbDomain = "";
        String deviceId = "abc-sfas";
        String join = String.join(",", brand, osVersion, appPkg, adx, model, os, dataSourceId, pbDomain,deviceId);


        StringBuilder sb = new StringBuilder();


        sb.append(Long.toHexString(new Date().getTime() / 1000))
                .append(Long.toHexString(1200))
                .append("|")
                .append("10006").append("_").append("sub001").append("-->").append(join)
                .append("|")
                .append("1231")
                .append("|")
                .append("BR");

        sb.append("|")
                .append("");
        Byte b = null;
        String re = new String(urlEncoder.encode(compress(sb.toString().getBytes(), b)));
        log.info("transactionId:{}",re);
        String re2 = "R3gBJc69CsIwEADgF0rKXZqfu0UQCuKggkUdJW0u6lCrbUWEPryC07d-3lmDlsVlE2YERADw5z4_y-HWGfPQelHHbnzdLwqxANX2XfGWOF1lKE5_1djFYfrENKp6o1dMeFTLbbXfrSuFnn1gCoEsgbNMxEpJSyDWlBrL1mnrc9ZHRFDqKI1LiSk3YmZIAin5bOT3IxKOwuKTM5E8ko3zoZ6_cjo2pg";
        uncompressTrasactionId(re2);

    }

    private static String parseOsv(String ua) {
        String osVersion = null;
        if (!StringUtils.isEmpty(ua)) {
            String[] uas = ua.toLowerCase().split(";");
            if (uas[1].contains("android")) {
                osVersion = uas[1].replace("android ", "").replace(",","").trim();
            } else if (uas.length > 2 && uas[2].contains("android")) {
                osVersion = uas[2].replace("android ", "").trim();
                // 判断是否IOS14用户
            } else if (uas[1].contains("iphone") || uas[1].contains("ipad")) {
                String[] osArr = uas[1].trim().split(" ");
                if (osArr.length >= 4) {
                    String[] split = osArr[3].split("_");
                    if (split.length >= 2) {
                        osVersion = split[0] + "." + split[1];
                    } else {
                        osVersion = split[0];
                    }

                }
            }
        }
        return osVersion;
    }

    public static void uncompressTrasactionId(String transactionId){
        byte[] uncompress = uncompress(Base64.getUrlDecoder().decode(transactionId));
        String[] params = new String(uncompress).split("\\|");
        long clickTime = Long.parseLong(params[0].substring(0, 8), 16);
        System.out.println(DateUtilFinal.getDateFormatyyyyMMddHHmmss(clickTime*1000));
        int index = params[1].indexOf("_");
        String clickParam = params[1].substring(index + 1);

        String[] split = clickParam.split("-->");
        if (split.length == 2) {
            System.out.println(split[1]);
            String[]handleClickId = split[1].split(",",-1);;

            if(handleClickId.length>=7){
                String s = handleClickId[6];
                String s2 = handleClickId[5];
                System.out.println(s);
                System.out.println(s2);
            }
        }
    }

    public static byte[] compress(byte[] data, Byte b) {
        Deflater compressor = new Deflater(1);

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Throwable var4 = null;

            try {
                compressor.setInput(data);
                compressor.finish();
                byte[] buf = new byte[128];

                while(!compressor.finished()) {
                    if (b == null) {
                        b = (byte)(ThreadLocalRandom.current().nextInt(256) - 128);
                    }

                    buf[0] = b;
                    int count = compressor.deflate(buf, 1, buf.length - 1);
                    bos.write(buf, 0, count + 1);
                }

                byte[] var28 = bos.toByteArray();
                return var28;
            } catch (Throwable var24) {
                var4 = var24;
                throw var24;
            } finally {
                if (bos != null) {
                    if (var4 != null) {
                        try {
                            bos.close();
                        } catch (Throwable var23) {
                            var4.addSuppressed(var23);
                        }
                    } else {
                        bos.close();
                    }
                }

            }
        } catch (IOException var26) {
            var26.printStackTrace();
        } finally {
            compressor.end();
        }

        return null;
    }

    public static byte[] uncompress(byte[] decode) {
        int num = (decode.length - 1) / 128 + 1;
        byte[] data = new byte[decode.length - num];
        int remainder = decode.length % 128;
        remainder = remainder == 0 ? 128 : remainder;

        for(int i = 0; i < num; ++i) {
            int length = i < num - 1 ? 127 : remainder - 1;
            System.arraycopy(decode, i * 128 + 1, data, i * 127, length);
        }

        Inflater decompressor = new Inflater();

        byte[] var35;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Throwable var6 = null;

            try {
                decompressor.setInput(data);
                byte[] buf = new byte[128];
                int loop = 0;

                while(!decompressor.finished() && loop < 10) {
                    ++loop;
                    int count = decompressor.inflate(buf);
                    bos.write(buf, 0, count);
                }

                if (loop >= 10) {
                    var35 = null;
                    return var35;
                }

                var35 = bos.toByteArray();
            } catch (Throwable var29) {
                var6 = var29;
                throw var29;
            } finally {
                if (bos != null) {
                    if (var6 != null) {
                        try {
                            bos.close();
                        } catch (Throwable var28) {
                            var6.addSuppressed(var28);
                        }
                    } else {
                        bos.close();
                    }
                }

            }
        } catch (Exception var31) {
            var31.printStackTrace();
            return null;
        } finally {
            decompressor.end();
        }

        return var35;
    }
}
