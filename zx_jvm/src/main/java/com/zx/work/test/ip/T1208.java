package com.zx.work.test.ip;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class T1208 {
    public static void main(String[] args) throws UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        System.out.println(ip);


        LocalDateTime dateTime = LocalDateTime.now().minusDays(1);
//            LocalDateTime dateTime = LocalDateTime.now();
        String dateStr = localDateTime2String(dateTime,"yyyyMMdd");

        System.out.println(dateStr);
    }
    private static  String localDateTime2String(LocalDateTime dateTime,String format) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
            return dtf.format(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
