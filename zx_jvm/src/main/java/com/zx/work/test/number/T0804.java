package com.zx.work.test.number;

/**
 * @author zx
 * @description:
 * @date 2022/8/4
 */
public class T0804 {
    public static void main(String[] args) {
        int a = 3;
        int b = 2;
        int c = 1;

        System.out.println(~a+1);
        System.out.println(~b+1);
        System.out.println(~c+1);

    }
}
