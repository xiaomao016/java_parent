package com.zx.work.test.y22;

import org.apache.commons.lang3.StringUtils;

public class T1124 {
    public static void main(String[] args) {

        String ex = "1590581369354489857";

        boolean isNum = StringUtils.isNumeric(ex);
        System.out.println(isNum);

        System.out.println(ex.length());
        System.out.println("=========");
        String timeOs = "dispatch-3_android_RU";
        int sub_start =timeOs.indexOf("-");
        System.out.println(timeOs.substring(sub_start+1));
        System.out.println("=================11.29");

        System.out.println("01".substring(1));
        System.out.println(genScheduleConf("10:15"));
        System.out.println(genScheduleConf("10:05"));
        System.out.println(genScheduleConf("10:00"));
        System.out.println(genScheduleConf("10:0"));
        System.out.println(genScheduleConf("10:1"));
        System.out.println(genScheduleConf("09:12"));
        System.out.println(genScheduleConf("00:12"));
        System.out.println(genScheduleConf("8:12"));
    }

    public static String genScheduleConf(String dispatchParams) {
        try {
            String[] h_m = dispatchParams.split(":");
            int m_index;
            if(h_m[1].startsWith("0") && h_m[1].length()>1){
                m_index = Integer.parseInt(h_m[1].substring(1));
            }else{
                m_index = Integer.parseInt(h_m[1]);
            }

            int h_index;
            if(h_m[0].startsWith("0") && h_m[0].length()>1){
                h_index = Integer.parseInt(h_m[0].substring(1));
            }else {
                h_index = Integer.parseInt(h_m[0]);
            }
            String cron = String.format("0 %s %s * * ?", m_index, h_index);
            return cron;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
