package com.zx.work.test.number;

import java.util.Map;

public class T1031 {
    public static void main(String[] args) {
        //0-12, 13-25,26-38,39-53
        int size = 53;
        int index = 3;
        int batch = (int) (size * 0.25);
        System.out.println("bach:" + batch);

        int start = index * batch;
        int end = 0;
        if (index == 3) {
            end = size;
        } else {
            end = batch * (index + 1) - 1;
        }
        System.out.println("start:" + start + ",end:" + end);

        System.out.println("-===========");

        int MapIndex = 5390 / 100 + 1;
        System.out.println((int) 5390 / 100 + 1);

        for (int i = 0; i < MapIndex; ++i) {
            System.out.println("start:" + (i * 100));
            if (i == (MapIndex - 1)) {

                System.out.println("end:" + (5390 - 1));
            } else {
                System.out.println("end:" + (i * 100 + 100 - 1));

            }
        }

    }
}
