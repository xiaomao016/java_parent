package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zx.work.utils.EncryptUtils;

import java.sql.SQLOutput;
import java.util.List;

public class T0825 {
    public static void main(String[] args) {
        String json = "[\"string1\", \"string2\", \"string3\"]";

        Gson gson = new Gson();
        List<String> list = gson.fromJson(json, new TypeToken<List<String>>(){}.getType());


//        List<String> addressList = gson.fromJson(json, List.class);
//        System.out.println(list);

        List<String> addressList = fromJsonList(json,String.class);
        System.out.println(addressList);
    }

    public static <T> List<T> fromJsonList(String json, Class<T> classOfT) {
        Gson gson= new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        return gson.fromJson(
                json,
                new com.google.gson.reflect.TypeToken<List<T>>() {
                }.getType()
        );
    }
}
