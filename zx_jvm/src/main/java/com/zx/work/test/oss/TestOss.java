package com.zx.work.test.oss;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.model.OSSObjectSummary;

import java.util.List;

/**
 * @author zx
 * @description:
 * @date 2022/7/18
 */
public class TestOss {
    public static void main(String[] args) {
        OSSClientUtils ossClientUtils = new OSSClientUtils();
        ossClientUtils.init();

      String localPath = "C:\\Users\\dell\\Desktop\\临时\\10.20\\tt_upload.csv";

//      ossClientUtils.uploadFile("test/tt_upload.csv",localPath);
        List<String> strings = ossClientUtils.listObjectFromPrefix("temp/test/ttt02101027/");
        System.out.println(JSON.toJSONString(strings));

        System.out.println("=======读取文件=====");
//        ossClientUtils.readFile2();
        System.out.println("==============listfile======");
        List<OSSObjectSummary> ossObjectSummaries = ossClientUtils.listObjects();
        System.out.println(ossObjectSummaries.size());


    }



}
