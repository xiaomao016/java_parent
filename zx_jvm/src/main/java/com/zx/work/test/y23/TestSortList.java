package com.zx.work.test.y23;

import lombok.Data;

import java.text.DecimalFormat;
import java.util.*;

public class TestSortList {

    public static void main(String[] args) {
        testDocumentSorting();
    }

    public static void testDocumentSorting() {
        Map<Double, Long> map = new HashMap<>();
        map.put(3.0, 10L);
        map.put(1.5, 20L);
        map.put(2.7, 5L);

        List<Document> documentList = new ArrayList<>();
        for (Map.Entry<Double, Long> entry : map.entrySet()) {
            Document document = new Document();
            document.setKey(entry.getKey());
            document.setDoc_count(entry.getValue());
            documentList.add(document);
        }

        Collections.sort(documentList, Comparator.comparing(Document::getKey).reversed());

        // 验证排序后的结果是否正确
//        assertEquals(3.0, documentList.get(0).getKey(), 0.0);
//        assertEquals(2.7, documentList.get(1).getKey(), 0.0);
//        assertEquals(1.5, documentList.get(2).getKey(), 0.0);

        // 打印排序结果
        documentList.forEach(System.out::println);
        Float a = 0.3f;
        double aa = 1-a;
        System.out.println(decimalFormat(aa));
    }

    public static double decimalFormat(double score) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String result = decimalFormat.format(score);
        double fix_score = Double.parseDouble(result);
        return fix_score;
    }


    @Data
    static class Document {
        double key;
        long doc_count;
    }
}
