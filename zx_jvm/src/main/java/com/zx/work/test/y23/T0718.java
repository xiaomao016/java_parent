package com.zx.work.test.y23;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;

import java.util.*;


@Slf4j
public class T0718 {

    public static void main(String[] args) {
        String[] appArray = {"org.betwinner.client-Custom"};
        List<String> apps = new ArrayList<>(Arrays.asList(appArray));

        String jsonString = "{\n" +
                "    \"IAB18\":1,\n" +
                "    \"IAB1\":1,\n" +
                "    \"IAB14\":1,\n" +
                "    \"IAB7\":1\n" +
                "}";
        //将JSON字符串转换为Map
        Map<String,Double> catScore = new HashMap<>();
        Map<String, Double> map = JSON.parseObject(jsonString, new TypeReference<Map<String, Double>>(){});


        // 打印Map键值对
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        Double afafa = map.get("afafa");
        System.out.println(afafa);
        Float f = -1F;

        f(1F);


    }

    public static void f(Float cutOffPoint){
        if(cutOffPoint>0 || cutOffPoint==-1) {
            System.out.println(cutOffPoint);
        }
    }
}
