package com.zx.design.memo.demo;

import com.zx.design.iterator.demo02.ArrayList_;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 07:57
 * @description:
 */
public class Test {

    private static ArrayList<String> list = new ArrayList();
    private static boolean flag = true;
    public static void main(String[] args) {

        System.out.println(">");
        //1.接收输入数据
        Scanner sc = new Scanner(System.in);
        while(flag) {
            String input = sc.next();
            //3.进行处理
            handle(input);
        }
    }

    private static void handle(String opt) {
        if(opt==null){
            System.out.println("输入有误");
        }else if(opt.equals("list:")){
            listData();
        }else if(opt.equals("undo:")){
            undoOpt();
        }else if(opt.equals("quit:")){
            flag = false;
        }else{
            save(opt);
        }
    }

    private static void undoOpt() {
        list.remove(list.size()-1);
    }

    private static void save(String c) {
        list.add(c);
    }

    private static void listData() {
        StringBuffer buffer = new StringBuffer();
        for (int i= 0;i<list.size();++i){
            buffer.append(list.get(i));
        }
        System.out.println(buffer.toString());
    }

}
