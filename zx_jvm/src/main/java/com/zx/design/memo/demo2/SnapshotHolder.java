package com.zx.design.memo.demo2;

import java.util.Stack;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 09:01
 * @description:
 */
public class SnapshotHolder {
    private Stack<InputText> snapshots = new Stack<>();

    public InputText popSnapshot() {
        return snapshots.pop();
    }

    public void pushSnapshot(InputText inputText) {
        InputText deepClonedInputText = new InputText();
        deepClonedInputText.setText(inputText.getText());
        snapshots.push(deepClonedInputText);
    }
}
