package com.zx.design.memo.demo3;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 10:17
 * @description:
 */
public class InputText {

    private StringBuilder buffer= new StringBuilder();

    public String getText(){
        return buffer.toString();
    }

    public void append(String text){
        buffer.append(text);
    }

    public void restoreFromSnapShot(SnapShot snapShot){
        buffer.replace(0,this.buffer.length(),snapShot.getText());
    }

    public SnapShot createSnapShot(){
        SnapShot snapShot = new SnapShot();
        snapShot.setText(buffer.toString());
        return snapShot;
    }

}
