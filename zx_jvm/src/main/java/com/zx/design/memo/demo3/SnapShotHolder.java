package com.zx.design.memo.demo3;

import java.util.Stack;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 10:20
 * @description:
 */
public class SnapShotHolder {

    private Stack<SnapShot> snapShots = new Stack<>();

    public void push(SnapShot snapShot){
        this.snapShots.push(snapShot);
    }

    public SnapShot pop(){
        return snapShots.pop();
    }


}
