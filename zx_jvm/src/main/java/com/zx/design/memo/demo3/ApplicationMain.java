package com.zx.design.memo.demo3;

import java.util.Scanner;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 10:24
 * @description:
 */
public class ApplicationMain {
    public static void main(String[] args) {

        InputText inputText = new InputText();
        SnapShotHolder holder = new SnapShotHolder();

        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){

            String input = sc.next();

            if(input.equals(":list")){
                System.out.println(inputText.getText());
            }else if(input.equals(":undo")){
                SnapShot snapShot = holder.pop();
                inputText.restoreFromSnapShot(snapShot);
            }else{
                holder.push(inputText.createSnapShot());
                inputText.append(input);
            }

        }



    }
}
