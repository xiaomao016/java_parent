package com.zx.design.memo.demo2;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 09:00
 * @description:
 */
public class InputText {
    private StringBuilder text = new StringBuilder();

    public String getText() {
        return text.toString();
    }

    public void append(String input) {
        text.append(input);
    }

    public void setText(String text) {
        this.text.replace(0, this.text.length(), text);
        System.out.println("replace");
    }
}
