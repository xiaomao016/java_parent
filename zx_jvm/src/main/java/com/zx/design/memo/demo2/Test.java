package com.zx.design.memo.demo2;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/25 0025 08:55
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("hello");
        buffer.append("word");
        buffer.append("nihao");
        System.out.println(buffer.toString());
        buffer.replace(0,buffer.length(),"nihao");
        System.out.println(buffer.toString());



    }
}
