package com.zx.design.share;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/13 0013 08:36
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        //当我们通过自动装箱，也就是调用 valueOf() 来创建 Integer 对象的时候，
        // 如果要创建的 Integer 对象的值在 -128 到 127 之间，会从 IntegerCache 类中直接返回，否则才调用 new 方法创建。
        //实际上，JDK 也提供了方法来让我们可以自定义缓存的最大值，有下面两种方式。
        // 如果你通过分析应用的 JVM 内存占用情况，发现 -128 到 255 之间的数据占用的内存比较多，
        // 你就可以用如下方式，将缓存的最大值从 127 调整到 255。不过，这里注意一下，JDK 并没有提供设置最小值的方法。
        ////方法一：
        // -Djava.lang.Integer.IntegerCache.high=255
        //方法二：
        //-XX:AutoBoxCacheMax=255
        Integer i1 = 56;
        Integer i2 = 56;
        Integer i3 = 129;
        Integer i4 = 129;
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);




        String s1 = "xiaomao";
        String s2 = "xiaomao";
        String s3 = new String("xiaomao");

        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
    }
}
