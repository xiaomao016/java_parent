package com.zx.design.bridge;

import com.zx.design.bridge.sender.EmailMsgSender;

import java.util.ArrayList;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/8 0008 08:59
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        EmailMsgSender sender = EmailMsgSender.newInstance()
                .emailList(new ArrayList<>())
                .build();
        Notification notification = new Notification(sender);

        notification.notify("");
    }
}
