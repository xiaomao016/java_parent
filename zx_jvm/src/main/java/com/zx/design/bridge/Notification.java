package com.zx.design.bridge;

import com.zx.design.bridge.sender.MsgSender;

import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/8 0008 08:55
 * @description:
 */
public class Notification {

    private List<String> emailAddresses;
    private List<String> telephones;
    private List<String> wechatIds;

    private MsgSender sender;




    public Notification(MsgSender sender){
        this.sender = sender;
    }


    public void setEmailAddress(List<String> emailAddress) {
        this.emailAddresses = emailAddress;
    }

    public void setTelephones(List<String> telephones) {
        this.telephones = telephones;
    }

    public void setWechatIds(List<String> wechatIds) {
        this.wechatIds = wechatIds;
    }


    public void notify(String message) {
        sender.notify();
    }

}
