package com.zx.design.bridge.sender;


import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 11:30
 * @description:
 */
public class TelephoneMsgSender implements MsgSender {
    private List<String> list ;

    @Override
    public void send(String message) {
        for(String address:list){
            //给对应的地址发
        }
    }

    private TelephoneMsgSender(TelephoneMsgSender.Builder builder){
        this.list = builder.list;
    }

    public static TelephoneMsgSender.Builder newInstance(){
        return new TelephoneMsgSender.Builder();
    }

    public static class Builder{
        private List<String> list;


        public TelephoneMsgSender.Builder phoneList(List<String> list){
            this.list = list;
            return this;
        }

        public TelephoneMsgSender build(){
            if(list==null){
                throw new IllegalArgumentException("发送人列表不能为空");
            }
            return new TelephoneMsgSender(this);
        }

    }


}
