package com.zx.design.proxy.base;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 10:39
 * @description:
 */
public class MetricsCollector {
    public void recordRequest(RequestInfo info){
        //增强逻辑:比如保存每次请求的时间信息等
    }
}
