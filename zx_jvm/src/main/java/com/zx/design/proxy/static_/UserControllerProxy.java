package com.zx.design.proxy.static_;

import com.zx.design.proxy.base.MetricsCollector;
import com.zx.design.proxy.base.RequestInfo;
import com.zx.design.proxy.base.UserVo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 11:00
 * @description: 基于接口
 */
public class UserControllerProxy implements IUserController {
    private UserController userController;
    private MetricsCollector metricsCollector;


    public UserControllerProxy(UserController userController){
        this.userController = userController;
        this.metricsCollector = new MetricsCollector();
    }

    @Override
    public UserVo login(String telephone, String password) {
        long startTimestamp = System.currentTimeMillis();

        //1.原有逻辑委托原始类
        userController.login(telephone,password);
        //2.增强逻辑：比如日志，监控，统计等。
        long endTimeStamp = System.currentTimeMillis();
        long responseTime = endTimeStamp - startTimestamp;
        RequestInfo requestInfo = new RequestInfo("login", responseTime, startTimestamp);
        metricsCollector.recordRequest(requestInfo);
        return null;
    }

    @Override
    public UserVo register(String telephone, String password) {
        long startTimestamp = System.currentTimeMillis();

        //1.原有逻辑委托原始类
        userController.register(telephone,password);
        //2.增强逻辑：比如日志，监控，统计等。
        long endTimeStamp = System.currentTimeMillis();
        long responseTime = endTimeStamp - startTimestamp;
        RequestInfo requestInfo = new RequestInfo("login", responseTime, startTimestamp);
        metricsCollector.recordRequest(requestInfo);
        return null;
    }
}
