package com.zx.design.proxy.static_;

import com.zx.design.proxy.base.UserVo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 10:52
 * @description:
 */
public interface IUserController {
    UserVo login(String telephone, String password);
    UserVo register(String telephone, String password);
}
