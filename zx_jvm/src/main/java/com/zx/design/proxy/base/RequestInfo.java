package com.zx.design.proxy.base;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 11:11
 * @description: 请求信息封装类
 */

public class RequestInfo {
    private String apiName;
    private long responseTime;
    private long startTime;

    public RequestInfo(String apiName, long responseTime, long startTime) {
        this.apiName = apiName;
        this.responseTime = responseTime;
        this.startTime = startTime;
    }

    public RequestInfo() {
    }
}
