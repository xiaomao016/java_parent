package com.zx.design.proxy.static_;

import com.zx.design.proxy.base.MetricsCollector;
import com.zx.design.proxy.base.RequestInfo;
import com.zx.design.proxy.base.UserVo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 11:07
 * @description: 基于子类
 */
public class UserControllerProxy_ extends UserController {
    private MetricsCollector metricsCollector;

    public UserControllerProxy_() {
        this.metricsCollector = new MetricsCollector();
    }

    public UserVo login(String telephone, String password) {
        long startTimestamp = System.currentTimeMillis();

        //1.原有逻辑
        UserVo userVo = super.login(telephone, password);
        //2.增强逻辑
        long endTimeStamp = System.currentTimeMillis();
        long responseTime = endTimeStamp - startTimestamp;
        RequestInfo requestInfo = new RequestInfo("login", responseTime, startTimestamp);
        metricsCollector.recordRequest(requestInfo);
        return userVo;
    }

    public UserVo register(String telephone, String password) {
        long startTimestamp = System.currentTimeMillis();

        //1.原有逻辑
        UserVo userVo = super.register(telephone, password);
        //2.增强逻辑
        long endTimeStamp = System.currentTimeMillis();
        long responseTime = endTimeStamp - startTimestamp;
        RequestInfo requestInfo = new RequestInfo("login", responseTime, startTimestamp);
        metricsCollector.recordRequest(requestInfo);
        return userVo;
    }

}
