package com.zx.design.proxy;

import com.zx.design.proxy.dynamic.MetricsCollectorProxy;
import com.zx.design.proxy.static_.IUserController;
import com.zx.design.proxy.static_.UserController;
import com.zx.design.proxy.static_.UserControllerProxy;
import com.zx.design.proxy.static_.UserControllerProxy_;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 11:04
 * @description:
 */
public class TestCode {
    public static void main(String[] args) {
        //1.1.静态代理：基于接口
        IUserController userController = new UserControllerProxy(new UserController());
        userController.login("", "");
        //1.2.静态代理：基于子类
        UserController userController1 = new UserControllerProxy_();
        userController1.login("", "");

        //2.1.动态代理
        MetricsCollectorProxy proxy = new MetricsCollectorProxy();
        IUserController userController2 = (IUserController) proxy.createProxy(new UserController());
        userController2.login("","");
    }
}
