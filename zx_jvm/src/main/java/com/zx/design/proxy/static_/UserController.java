package com.zx.design.proxy.static_;

import com.zx.design.proxy.base.UserVo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/28 0028 10:58
 * @description:
 */
public class UserController implements IUserController {
    @Override
    public UserVo login(String telephone, String password) {
        //具体业务逻辑
        return null;
    }

    @Override
    public UserVo register(String telephone, String password) {
        //具体业务逻辑
        return null;
    }
}
