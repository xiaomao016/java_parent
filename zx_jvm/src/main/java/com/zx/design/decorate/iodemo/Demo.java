package com.zx.design.decorate.iodemo;

import java.io.*;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/8 0008 10:42
 * @description:
 */
public class Demo {

    public static void main(String[] args) throws Exception{
        //1.字节输入流
        //1.1.文件输入流
        FileInputStream fin = new FileInputStream("test.txt");
        //1.2.带缓冲功能的文件输入流
        BufferedInputStream bin = new BufferedInputStream(fin);

        //1.3.带缓冲功能和按数据类型读取的文件输入流
        DataInputStream din = new DataInputStream(bin);





        //2.字符输入流
        //2.1.输入字符流
        InputStreamReader reader = new InputStreamReader(new FileInputStream("test.txt"),"UTF-8");
        //2.2.带缓冲功能的字符输入流
        BufferedReader bufferedReader = new BufferedReader(reader);



    }

}
