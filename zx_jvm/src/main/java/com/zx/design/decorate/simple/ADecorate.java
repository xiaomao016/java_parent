package com.zx.design.decorate.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 10:50
 * @description:
 */
public class ADecorate implements IA {

    private IA a;

    public ADecorate(IA a){
        this.a = a;
    }

    @Override
    public void f() {
        //功能增强代码
        a.f();
        //功能增强代码
    }
}
