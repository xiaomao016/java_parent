package com.zx.design.decorate.upgrade;


/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/8 0008 10:14
 * @description:
 */
public class BufferedInputStream extends FilterInputStream {


    public BufferedInputStream(InputStream in) {
        super(in);
    }


    //增加缓冲功能，其他逻辑调用 传入的InputStream对象

}
