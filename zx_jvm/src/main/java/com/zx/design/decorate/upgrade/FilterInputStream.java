package com.zx.design.decorate.upgrade;


import com.zx.design.decorate.upgrade.InputStream;

import java.io.IOException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 10:25
 * @description:
 */
public class FilterInputStream  extends InputStream {
    protected volatile InputStream in;

    protected FilterInputStream(InputStream in) {
        this.in = in;
    }


    public int read() throws IOException {
        return in.read();
    }

    public int read(byte b[]) throws IOException {
        return in.read(b);
    }

    public int read(byte b[], int off, int len) throws IOException {
        return in.read(b,off,len);
    }


    public long skip(long n) throws IOException {
        return in.skip(n);
    }

    public boolean markSupported() {
        return in.markSupported();
    }
}
