package com.zx.design.decorate.upgrade;

import java.io.IOException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/8 0008 10:14
 * @description:
 */
public abstract class InputStream {


    public abstract int read() throws IOException;

    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }

    public int read(byte b[], int off, int len) throws IOException {
        //系统默认实现..
        return 0;
    }


    public long skip(long n) throws IOException {
        //系统默认实现..
        return 0;
    }

    public boolean markSupported() {
        //系统默认实现..
        return false;
    }
}
