package com.zx.design.decorate.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 10:49
 * @description:
 */
public interface IA {
    void f();
}
