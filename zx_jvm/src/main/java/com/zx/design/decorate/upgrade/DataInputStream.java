package com.zx.design.decorate.upgrade;

import java.io.IOException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/8 0008 10:14
 * @description:
 */
public class DataInputStream extends FilterInputStream{


    protected DataInputStream(InputStream in) {
        super(in);
    }


    //实现读取具体数据类型的功能：其他功能调用传入的InputStream 对象
}
