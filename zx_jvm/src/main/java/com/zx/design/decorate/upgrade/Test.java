package com.zx.design.decorate.upgrade;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 11:09
 * @description:
 * 1.这里InputStream 为何是抽象类，而不是接口？
 * 因为InputStream方法中有很多方法是有默认实现的。这样可以进行代码的复用。
 *
 * 2.为何要增加一个FilterInputStream？而且里边的方法实现都是一个调用的简单封装
 * 为了嵌套装饰,这样每一个调用，即使当前装饰类没有做增强，也要交给传入的对象去调用，
 * 因为传入的对象可能做了增强，而不应该直接调用根父类InputStream里的默认实现。这样把这层委托关系都
 * 抽象到FilterInputStream中统一处理。
 *
 *
 *
 *
 */
public class Test {
    public static void main(String[] args) {
        //文件输入流
        InputStream fin = new FileInputStream("/a.txt");

        //对文件输入流进行装饰,增加缓冲功能
        InputStream buffFin = new BufferedInputStream(fin);

        //再次进行装饰,增加可以读取基本数据类型的功能
        InputStream dataBuffFin = new DataInputStream(buffFin);




    }

}
