package com.zx.design.decorate.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 10:50
 * @description:
 */
public class A implements IA {
    @Override
    public void f() {
        //原始类函数实现
    }
}
