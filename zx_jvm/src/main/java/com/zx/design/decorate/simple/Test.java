package com.zx.design.decorate.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 11:07
 * @description:
 */
public class Test {
    public static void main(String[] args) {

        IA a = new A();
        //装饰类，相当于把原始类装饰增强了
        IA decorateA = new ADecorate(a);

        decorateA.f();
    }
}
