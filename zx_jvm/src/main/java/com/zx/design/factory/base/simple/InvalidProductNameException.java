package com.zx.design.factory.base.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:17
 * @description:
 */
public class InvalidProductNameException extends RuntimeException {
    public InvalidProductNameException(String message){
        super(message);
    }
}
