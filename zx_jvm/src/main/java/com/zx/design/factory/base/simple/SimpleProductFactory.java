package com.zx.design.factory.base.simple;

import com.zx.design.factory.base.beans.Clothes;
import com.zx.design.factory.base.beans.Hat;
import com.zx.design.factory.base.beans.Product;
import com.zx.design.factory.base.beans.Shoe;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:25
 * @description: 这里利用map提前缓存了产品类，可以加速获取对象。当然不是必须，也可以临时创建。
 * 这种方式有点单例模式和工厂模式结合的味道。
 *
 *
 */
public class SimpleProductFactory {

    private static final Map<String, Product> cacheProducts = new HashMap<>();

    static{
        cacheProducts.put(Hat.class.getName(),new Hat());
        cacheProducts.put(Shoe.class.getName(),new Shoe());
        cacheProducts.put(Clothes.class.getName(),new Clothes());

    }

    public static <T extends Product> T createProduct(Class<T> c) {
        return (T)cacheProducts.get(c.getName());
    }
}
