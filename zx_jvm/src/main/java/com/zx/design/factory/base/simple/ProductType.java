package com.zx.design.factory.base.simple;

import com.zx.design.factory.base.beans.Clothes;
import com.zx.design.factory.base.beans.Hat;
import com.zx.design.factory.base.beans.Shoe;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 14:46
 * @description:
 * 产品枚举类
 */
public enum ProductType {

    HAT(Hat.class.getName()),
    SHOE(Shoe.class.getName()),
    ClOTHES(Clothes.class.getName());

    private String name;
    private ProductType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
