package com.zx.design.factory.base.factory;

import com.zx.design.factory.base.beans.Hat;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:44
 * @description:
 */
public class HatFactory implements IProductFactory {
    @Override
    public Hat createProduct() {
        return new Hat();
    }
}
