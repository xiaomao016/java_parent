package com.zx.design.factory.base.factory;

import com.zx.design.factory.base.beans.Shoe;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:52
 * @description:
 */
public class ShoeFactory implements IProductFactory {
    @Override
    public Shoe createProduct() {
        return new Shoe();
    }
}
