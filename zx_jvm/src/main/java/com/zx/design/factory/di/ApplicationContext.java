package com.zx.design.factory.di;

public interface ApplicationContext {
    Object getBean(String beanId);
}
