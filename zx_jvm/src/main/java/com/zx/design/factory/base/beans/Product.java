package com.zx.design.factory.base.beans;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:10
 * @description:
 */
public abstract class Product {
    public void common(){
        System.out.println("工厂生产的商品");
    }

    public abstract void myname();
}
