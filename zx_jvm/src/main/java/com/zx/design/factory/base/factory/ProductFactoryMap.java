package com.zx.design.factory.base.factory;

import com.zx.design.factory.base.beans.Clothes;
import com.zx.design.factory.base.beans.Hat;
import com.zx.design.factory.base.beans.Product;
import com.zx.design.factory.base.beans.Shoe;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 14:10
 * @description:
 */
public class ProductFactoryMap {

    private static final Map<String, IProductFactory> cacheFactorys = new HashMap<>();

    static{
        cacheFactorys.put(HatFactory.class.getName(),new HatFactory());
        cacheFactorys.put(ShoeFactory.class.getName(),new ShoeFactory());
        cacheFactorys.put(ClothesFactory.class.getName(),new ClothesFactory());

    }

    public static IProductFactory getProductFactory(Class c){
        return cacheFactorys.get(c.getName());
    }

}
