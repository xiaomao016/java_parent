package com.zx.design.factory.base.abstract_;

import com.zx.design.factory.base.beans.Product;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 15:22
 * @description:
 */
public class DoorProductFactory implements IAbstractProductFactory {
    //生产更普通级别的left door
    @Override
    public Product createLeft() {
        return new LeftDoor();
    }

    //生产更普通别级别的right door
    @Override
    public Product createRight() {
        return new RightDoor();
    }
}
