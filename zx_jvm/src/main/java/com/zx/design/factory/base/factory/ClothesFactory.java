package com.zx.design.factory.base.factory;

import com.zx.design.factory.base.beans.Clothes;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:52
 * @description:
 */
public class ClothesFactory implements IProductFactory {
    @Override
    public Clothes createProduct() {
        return new Clothes();
    }
}
