package com.zx.design.factory.base.simple;

import com.zx.design.factory.base.beans.Product;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:25
 * @description: 这里每次尽量创建新的对象。
 *
 *
 */
public class SimpleProductFactoryLazy {

    public <T extends Product> T createProduct(Class<T> c) {
        Product product = null;
        try {
            product = (Product) Class.forName(c.getName()).newInstance();
        } catch (Exception e){
            throw new InvalidProductNameException("Product not found");
        }
        return (T)product;
    }
}
