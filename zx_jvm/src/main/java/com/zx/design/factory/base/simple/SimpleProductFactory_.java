package com.zx.design.factory.base.simple;

import com.zx.design.factory.base.beans.Clothes;
import com.zx.design.factory.base.beans.Hat;
import com.zx.design.factory.base.beans.Product;
import com.zx.design.factory.base.beans.Shoe;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:25
 * @description:
 * 用枚举区分后，可读性更好
 *
 *
 */
public class SimpleProductFactory_ {

    private static final Map<String, Product> cacheProducts = new HashMap<>();

    static{
        cacheProducts.put(Hat.class.getName(),new Hat());
        cacheProducts.put(Shoe.class.getName(),new Shoe());
        cacheProducts.put(Clothes.class.getName(),new Clothes());

    }

    public static <T extends Product> T createProduct(ProductType type) {
        return (T)cacheProducts.get(type.getName());
    }
}
