package com.zx.design.factory.base.factory;

import com.zx.design.factory.base.beans.Product;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:42
 * @description: 工厂抽象类，这样就对高层提供统一的访问接口。
 */
public interface IProductFactory {
    //抽象方法中已经不再需要传递相关参数了，因为每一个具体的工厂都已经非常明确自己的职责：创建自己负责的产品类对象。
    public Product  createProduct();
}
