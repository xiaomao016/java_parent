package com.zx.design.factory.base.abstract_;

import com.zx.design.factory.base.beans.Product;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 15:18
 * @description:
 */
public class RightDoor extends Product {
    @Override
    public void myname() {
        System.out.println("right door");
    }
}
