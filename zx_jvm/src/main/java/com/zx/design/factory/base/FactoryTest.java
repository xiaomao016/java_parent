package com.zx.design.factory.base;

import com.zx.design.factory.base.abstract_.DoorPlusProductFactory;
import com.zx.design.factory.base.abstract_.DoorProductFactory;
import com.zx.design.factory.base.abstract_.IAbstractProductFactory;
import com.zx.design.factory.base.beans.Clothes;
import com.zx.design.factory.base.beans.Hat;
import com.zx.design.factory.base.beans.Product;
import com.zx.design.factory.base.beans.Shoe;
import com.zx.design.factory.base.factory.HatFactory;
import com.zx.design.factory.base.factory.IProductFactory;
import com.zx.design.factory.base.factory.ProductFactoryMap;
import com.zx.design.factory.base.simple.InvalidProductNameException;
import com.zx.design.factory.base.simple.ProductType;
import com.zx.design.factory.base.simple.SimpleProductFactory;
import com.zx.design.factory.base.simple.SimpleProductFactory_;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:06
 * @description:
 */
public class FactoryTest {
    public static void main(String[] args) {
        //1.根据名称创建一个商品：不使用工厂模式
        FactoryTest t = new FactoryTest();
        t.createProduct("hat");

        //2.抽取到一个类中：即简单工厂
        Product hat = SimpleProductFactory.createProduct(Hat.class);
        hat.myname();
        //2.1.通过枚举类区分产品
        Product hat1 = SimpleProductFactory_.createProduct(ProductType.ClOTHES);
        hat1.myname();


        //3.1.工厂方法
        IProductFactory factory = new HatFactory();
        Hat h = (Hat) factory.createProduct();
        h.myname();

        //3.2.每次都new 不同的工厂类，再抽象一层，即创建一个工厂的工厂。
        IProductFactory factory1 = ProductFactoryMap.getProductFactory(HatFactory.class);
        Hat h1 = (Hat) factory1.createProduct();
        h1.myname();

        //4.抽象工厂
        //普通生产线
        IAbstractProductFactory doorProductFactory = new DoorProductFactory();
        //高级生产线
        IAbstractProductFactory doorPlusProductFactory = new DoorPlusProductFactory();

        //生产普通车门
        doorProductFactory.createLeft();
        doorProductFactory.createRight();

        //生产高级车门
        doorPlusProductFactory.createLeft();
        doorPlusProductFactory.createRight();




    }

    public Product createProduct(String name ){
        if(name.equals("hat")){
            return new Hat();
        }else if(name.equals("shoe")){
            return new Shoe();
        }else if(name.equals("clothes")){
            return new Clothes();
        }else{
            throw new InvalidProductNameException("product name not found");
        }
    }

}
