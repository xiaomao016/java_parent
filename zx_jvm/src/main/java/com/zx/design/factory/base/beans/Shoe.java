package com.zx.design.factory.base.beans;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 11:12
 * @description:
 */
public class Shoe extends Product {
    @Override
    public void myname() {
        System.out.println("鞋子");
    }
}
