package com.zx.design.factory.base.abstract_;

import com.zx.design.factory.base.beans.Product;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/25 0025 15:19
 * @description:
 */
public interface IAbstractProductFactory {
    public Product createLeft();
    public Product createRight();
}
