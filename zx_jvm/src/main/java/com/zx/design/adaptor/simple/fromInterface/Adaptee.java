package com.zx.design.adaptor.simple.fromInterface;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 13:21
 * @description:
 */
public class Adaptee {
    public void fa(){
        //..
    }

    public void fb(){
        //..
    }

    public void fc(){
        //..
    }

}
