package com.zx.design.adaptor.simple.fromInterface;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 11:33
 * @description:
 */
public interface ITarget {
    void f1();
    void f2();
    void f3();
}
