package com.zx.design.adaptor.simple.fromClass;

import com.zx.design.adaptor.simple.fromInterface.Adaptee;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 13:27
 * @description:
 */
public class Adaptor_ extends Adaptee {

    public void f1() {
        //适配器类如果只是进行方法名的修改，只需要委托给原始类
        super.fa();
    }

    public void f2() {
        //如果适配的新方法，需要修改，也可以进行重新实现
    }

    public void f3() {
        super.fc();
    }

}
