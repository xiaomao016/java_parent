package com.zx.design.adaptor.simple.fromInterface;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/11 0011 13:23
 * @description:
 */
public class Adaptor implements ITarget {


    private Adaptee adaptee;

    public Adaptor(Adaptee adaptee){
        this.adaptee = adaptee;
    }

    @Override
    public void f1() {
        //适配器类如果只是进行方法名的修改，只需要委托给原始类
        adaptee.fa();
    }

    @Override
    public void f2() {
        //如果适配的新方法，需要修改，也可以进行重新实现
    }

    @Override
    public void f3() {
        adaptee.fc();
    }
}
