package com.zx.design.interpreter.demo02;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 14:16
 * @description:
 */
public class BooleanExpression implements Expression {

    private boolean v;
    public BooleanExpression(boolean v){
        this.v = v;
    }

    @Override
    public boolean interpreter() {
        return v;
    }
}
