package com.zx.design.interpreter.calc;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 09:57
 * @description:
 */
public class Test {


    public static void main(String[] args) {

        Test t = new Test();

        String exp = "8 5 10 2 9 * - / +";
        long result = t.calc(exp);
        System.out.println(result);

    }


    public long calc(String exp){
        Deque<Long> deque = new LinkedList<>();
        String[]arr = exp.split(" ");
        for(int i=0;i<(arr.length+1)/2;++i){
            deque.offer(Long.parseLong(arr[i]));
        }

        for(int i=(arr.length+1)/2;i<arr.length;++i){
            boolean isValid = arr[i].equals("+")||arr[i].equals("-")||arr[i].equals("*")||arr[i].equals("/");

            if(!isValid){
                throw new IllegalArgumentException("操作符错误");
            }

            Expression expression = null;
            NumberExpression num1 = new NumberExpression(deque.poll());
            NumberExpression num2 = new NumberExpression(deque.poll());
            if(arr[i].equals("+")){
                expression = new AddExpression(num1,num2);
            }else if(arr[i].equals("-")){
                expression = new SubExpression(num1,num2);
            }else if(arr[i].equals("*")){
                expression = new MultiExpression(num1,num2);
            }else if(arr[i].equals("/")){
                expression = new DiviExpression(num1,num2);
            }

            deque.offerFirst(expression.interpreter());
        }


        if(deque.size()!=1){
            throw new RuntimeException("Expression is invalid" + exp);
        }

        return deque.poll();
    }

}
