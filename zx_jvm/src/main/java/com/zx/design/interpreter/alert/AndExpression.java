package com.zx.design.interpreter.alert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/27 0027 08:28
 * @description:
 */
public class AndExpression implements Expression {

    private List<Expression> expressionList = new ArrayList<>();

    public AndExpression(String strAndExpression) {
        String[] arr = strAndExpression.trim().split("&&");
        for (String exps : arr) {
            Expression expression;
            if (exps.contains(">")) {
                expression = new GreaterExpression(exps);
            } else if (exps.contains("<")) {
                expression = new LessenExpression(exps);
            } else if (exps.contains("==")) {
                expression = new EqualExpression(exps);
            } else {
                throw new IllegalArgumentException("表达式错误");
            }

            expressionList.add(expression);
        }
    }

    public AndExpression(List<Expression> list){
        this.expressionList.addAll(list);
    }

    @Override
    public boolean interpreter(Map<String, Long> stats) {
        for(Expression exps:expressionList){
            if(!exps.interpreter(stats)){
                return false;
            }
        }
        return true;
    }
}
