package com.zx.design.interpreter.alert;

import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/27 0027 09:09
 * @description:
 */
public class AlerRuleInterpreter {

    private Expression expression;

    public AlerRuleInterpreter(String strExpression){
        this.expression = new OrExpression(strExpression);
    }

    public boolean interprete(Map<String,Long> stats){
        return expression.interpreter(stats);
    }

}
