package com.zx.design.interpreter.calc;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 09:58
 * @description:
 */
public interface Expression {

    long interpreter();
}
