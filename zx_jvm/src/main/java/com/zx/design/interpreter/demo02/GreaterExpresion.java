package com.zx.design.interpreter.demo02;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 11:29
 * @description:
 */
public class GreaterExpresion implements Expression {

    private Long exp1;
    private Long exp2;
    public GreaterExpresion(Long exp1,Long exp2){
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    @Override
    public boolean interpreter() {
        return exp1>exp2;
    }
}
