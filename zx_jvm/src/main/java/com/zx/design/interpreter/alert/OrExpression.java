package com.zx.design.interpreter.alert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/27 0027 08:53
 * @description:
 */
public class OrExpression implements Expression {

    List<Expression> expressionList = new ArrayList<>();

    public OrExpression(String strOrExpression){
        String[]arr = strOrExpression.trim().split("\\|\\|");
        for(String exps:arr){
            expressionList.add(new AndExpression(exps));
        }
    }

    public OrExpression(List<Expression> list){
        expressionList.addAll(list);
    }

    @Override
    public boolean interpreter(Map<String, Long> stats) {
        for(Expression exp:expressionList){
            if(exp.interpreter(stats)){
                return true;
            }
        }
        return false;
    }
}
