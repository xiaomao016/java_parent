package com.zx.design.interpreter.demo02;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 11:18
 * @description:
 */
public class AlertRuleInterpreter {


    private Deque<Rule> rules = new LinkedList<>();
    private Deque<String> opers = new LinkedList<>();


    //// key1 > 100 && key2 < 1000 || key3 == 200
    public AlertRuleInterpreter(String expression){
        //1.分解
        String arr[] = expression.split(" ");
        //2.key-v 对和expression: Rule对象。用一个队列表示。合并操作符：用一个队列opers表示
        for(int i=0;i<arr.length;){
            String s = arr[i];
            if(s.equals("||") || s.equals("&&")){
                opers.offer(s);
                ++i;
            }else{
                ++i;
                String exp = arr[i];
                ++i;
                long thrsh = Long.parseLong(arr[i]);
                rules.offer(new Rule(s,thrsh,exp));
                ++i;
            }
        }
    }

    public boolean interpret(Map<String,Long> stats){
        //1.根据stats 生成 Expression队列
        Deque<Expression> exps = new LinkedList<>();
        while(!rules.isEmpty()){
            Rule rule = rules.poll();

            long threshold = rule.getThreshold();
            String e = rule.getExpression();
            String key = rule.getKey();

            Expression expression = null;
            if(!stats.containsKey(key)){
                expression = new BooleanExpression(false);
            }else{
                long value = stats.get(rule.getKey());
                if(e.equals(">")){
                    expression = new GreaterExpresion(value,threshold);
                }else if(e.equals("<")){
                    expression = new LessExpression(value,threshold);
                }else if(e.equals("==")){
                    expression = new EqualExpression(value,threshold);
                }
            }
            exps.offer(expression);

        }

        //2.根据Expression队列和opers队列进行合并计算。
        while(!opers.isEmpty()){
            String op = opers.poll();
            Expression left = exps.poll();
            Expression right = exps.poll();
            boolean r;
            if(op.equals("||")){
                r = left.interpreter() || right.interpreter();
            }else if(op.equals("&&")){
                r = left.interpreter() && right.interpreter();
            }else{
                throw new IllegalArgumentException("操作符错误");
            }
            exps.offerFirst(new BooleanExpression(r));
        }

        if(exps.size()!=1){
            throw new IllegalArgumentException("参数错误");
        }

        return exps.poll().interpreter();

    }

}
