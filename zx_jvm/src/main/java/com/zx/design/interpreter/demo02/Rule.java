package com.zx.design.interpreter.demo02;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 13:44
 * @description:
 */
public class Rule{
    private String key;
    private long threshold;
    private String expression;

    public Rule(String key,long threshold,String expression){
        this.key = key;
        this.threshold = threshold;
        this.expression = expression;
    }

    public String getExpression() {
        return expression;
    }

    public String getKey() {
        return key;
    }

    public long getThreshold() {
        return threshold;
    }


}
