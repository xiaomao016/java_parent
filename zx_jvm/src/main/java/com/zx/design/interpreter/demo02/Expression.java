package com.zx.design.interpreter.demo02;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 11:26
 * @description:
 */
public interface Expression {

    //||、&&、>、<、==
    boolean interpreter();

}
