package com.zx.design.interpreter.alert;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 14:49
 * @description:
 */
public class AppMain {
    public static void main(String[] args) {
        String strExpression = "key1 > 100 && key2 < 1000 || key3 == 200";
        // \\s 表示任意空格，比如空格，制表符，回车等。+表示匹配多个
        String[] elements = strExpression.trim().split("\\s+");

        for(String e:elements){
            System.out.println(e);
        }

        AlerRuleInterpreter interpreter = new AlerRuleInterpreter(strExpression);

        Map<String ,Long> stats = new HashMap<>();
        stats.put("key1",10L);
        stats.put("key2",120L);
        stats.put("key3",201L);

        boolean re = interpreter.interprete(stats);

        System.out.println(re);

    }


}
