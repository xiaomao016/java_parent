package com.zx.design.interpreter.alert;

import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/27 0027 08:17
 * @description:
 */
public class EqualExpression implements Expression{

    private String key;
    private long value;

    public EqualExpression(String strExpression) {
        String[] arr = strExpression.trim().split("\\s+");
        if(arr.length!=3 || !arr[1].equals("==")){
            throw new IllegalArgumentException("表达式错误");
        }

        this.key = arr[0].trim();
        this.value = Long.parseLong(arr[2].trim());

    }

    public EqualExpression(String key, long value){
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean interpreter(Map<String, Long> stats) {
        if(!stats.containsKey(key)){
            return false;
        }else{
            return stats.get(key)==value;
        }
    }




}
