package com.zx.design.interpreter.calc;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 10:04
 * @description:
 */
public class NumberExpression implements Expression {

    private long number ;

    public NumberExpression(long number){
        this.number = number;
    }


    @Override
    public long interpreter() {
        return this.number;
    }
}
