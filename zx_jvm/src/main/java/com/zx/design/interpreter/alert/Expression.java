package com.zx.design.interpreter.alert;

import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/27 0027 08:17
 * @description:
 */
public interface Expression {
    boolean interpreter(Map<String,Long> stats);
}
