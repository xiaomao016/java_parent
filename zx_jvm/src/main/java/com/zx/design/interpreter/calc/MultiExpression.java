package com.zx.design.interpreter.calc;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 10:05
 * @description:
 */
public class MultiExpression implements Expression {

    private Expression exp1;
    private Expression exp2;

    public MultiExpression(Expression exp1, Expression exp2){
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    @Override
    public long interpreter() {
        return exp1.interpreter() * exp2.interpreter();
    }
}
