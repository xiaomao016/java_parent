package com.zx.design.interpreter.demo;

import java.util.LinkedList;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 08:55
 * @description:
 *
 *
 * 5 4 + -
 * 1.按空格拆分
 * 2.将数字放入一个队列，将操作符放入一个队列
 * 3.每次取出两个数字，一个操作符，进行计算，之后将结果放入队列首部。
 * 4.如果只能取出一个数字，则已经计算完毕
 *
 */
public class Interpreter {


    public static void main(String[] args) {

        Interpreter interpreter = new Interpreter();
        String str = "8 5 5 5 - * -";
        int result = interpreter.interpretNumber(str);
        System.out.println(result);

    }


    public int interpretNumber(String serial){

        //1.将字符串分解后放入两个队列
        LinkedList<Integer> numbers = new LinkedList<>();
        LinkedList<String> operators = new LinkedList<>();

        String[]arr = serial.split(" ");

        for(int i=0;i<arr.length;++i){
            if(arr[i].matches("[0-9]+")){
                numbers.offer(Integer.parseInt(arr[i]));
            }else if(isOperator(arr[i])){
                operators.offer(arr[i]);
            }else{
                throw new IllegalArgumentException("字符串包含非法字符01");
            }
        }

        //2.开始计算

        while(!numbers.isEmpty()){
            int first = numbers.poll();
            //1.如果此时numbers为空了，说明已经计算完毕
            if(numbers.isEmpty()){
                return first;
            }else{
                //2.取出第二个操作数
                int secound = numbers.poll();
                //3.如果此时没有操作符，返回中间结果即可
                if(operators.isEmpty()){
                    return first;
                }else{
                    //4.取出操作符
                    String oper = operators.poll();
                    //5.进行计算
                    int result = calculate(first,secound,oper);
                    numbers.offerFirst(result);
                }
            }
        }

        return -1;



    }

    public boolean isOperator(String text){
        if(text.length()>1){
            return false;
        }else if(text.equals("+")
                ||text.equals("-")
                ||text.equals("*")
                ||text.equals("/")){
            return true;
        }else {
            return false;
        }
    }

    public int calculate(int first,int secound,String oper){

        if(oper==null){
            throw new IllegalArgumentException("操作符为空");
        }else if(oper.equals("+")){
            return first+secound;
        }else if(oper.equals("-")){
            return first - secound;
        }else if(oper.equals("*")){
            return  first * secound;
        }else if(oper.equals("/")){
            return first/secound;
        }else {
            throw new IllegalArgumentException("操作符错误");
        }
    }

}
