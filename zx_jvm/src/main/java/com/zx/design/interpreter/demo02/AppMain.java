package com.zx.design.interpreter.demo02;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 11:18
 * @description:
 */
public class AppMain {

    public static void main(String[] args) {

        String expression = "key1 > 100 && key2 < 1000 || key3 == 200";
        Map<String ,Long> stats = new HashMap<>();
        stats.put("key1",10L);
        stats.put("key2",120L);
        stats.put("key3",201L);

        AlertRuleInterpreter alertRuleInterpreter = new AlertRuleInterpreter(expression);
        boolean re = alertRuleInterpreter.interpret(stats);
        System.out.println(re);

    }

}
