package com.zx.design.interpreter.demo;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/26 0026 08:41
 * @description:
 *
 *
 *
 *
 *
 */
public class Demo {

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(5);
        queue.offer(4);
        queue.offer(3);

        queue.poll();
        ((LinkedList<Integer>) queue).offerFirst(6);

        while(!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }

}
