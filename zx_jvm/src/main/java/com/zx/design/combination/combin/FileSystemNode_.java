package com.zx.design.combination.combin;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 08:50
 * @description:
 */
public abstract class FileSystemNode_ {

    protected String path;

    public FileSystemNode_(String path){
        this.path = path;
    }

    public abstract long countFileNumbers();

    public abstract long countFileSize();


    public String getPath(){
        return path;
    }


}
