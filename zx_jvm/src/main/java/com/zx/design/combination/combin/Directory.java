package com.zx.design.combination.combin;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 09:47
 * @description:
 */
public class Directory extends FileSystemNode_ {

    private List<FileSystemNode_> subNodes = new ArrayList<>();

    public Directory(String path) {
        super(path);
    }

    @Override
    public long countFileNumbers() {
        long numofFiles = 0;
        for(FileSystemNode_ node:subNodes){
            numofFiles+=node.countFileNumbers();
        }
        return numofFiles;
    }

    @Override
    public long countFileSize() {
        long totalSize = 0;
        for(FileSystemNode_ node:subNodes){
            totalSize +=node.countFileSize();
        }

        return 0;
    }

    public void addSubNode(FileSystemNode_ subNode){
        subNodes.add(subNode);
    }

    public void removeSubNode(FileSystemNode_ subNode){
        int size = subNodes.size();
        int i=0;
        for(;i<size;++i){
            if(subNodes.get(i).getPath().equalsIgnoreCase(subNode.getPath())){
                break;
            }
        }

        if(i<size){
            subNodes.remove(subNode);
        }
    }
}
