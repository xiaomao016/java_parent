package com.zx.design.combination.origin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 08:42
 * @description:
 */
public class FileSystemNode {

    private List<FileSystemNode> nodes = new ArrayList<>();

    private boolean isFile = false;

    private String path;


    public FileSystemNode(String path,boolean isFile){
        this.isFile = isFile;
        this.path = path;
    }

    public void addNode(FileSystemNode node){
        nodes.add(node);
    }


    public boolean isFile(){
        return isFile;
    }


    public void removeSubNode(FileSystemNode fileOrDir){
        int size = nodes.size();
        int i=0;
        for(;i<size;++i){
            if(nodes.get(i).getPath().equalsIgnoreCase(fileOrDir.getPath())){
                break;
            }
        }
        if(i<size){
            nodes.remove(fileOrDir);
        }
    }


    public long countFileNumbers(){
        if(isFile){
            return 1;
        }
        int fileNumbers = 0;
        for(FileSystemNode fileOrDir:nodes){
            fileNumbers+= fileOrDir.countFileNumbers();
        }
        return fileNumbers;
    }

    public long countFileSize(){
        if(isFile){
            File file = new File(this.path);
            if(!file.exists()){
                return 0;
            }
            return file.length();
        }
        long totalSize = 0;
        for(FileSystemNode node:nodes){
            totalSize+=node.countFileSize();
        }
        return totalSize;
    }


    public String getPath() {
        return path;
    }
}
