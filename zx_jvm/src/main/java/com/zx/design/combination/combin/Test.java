package com.zx.design.combination.combin;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 09:49
 * @description:
 */
public class Test {


    public static void main(String[] args) {


        /**
         * /
         * /zx/
         * /zx/a.txt
         * /zx/ac.txt
         * /zx/ad.txt
         * /ximao/
         * /ximao/tt/b.txt
         *
         */

        Directory root = new Directory("/");

        Directory zx = new Directory("/zx/");
        root.addSubNode(zx);

        File a = new File("/zx/a.txt");
        File c = new File("/zx/ac.txt");
        File d = new File("/zx/ad.txt");
        zx.addSubNode(a);
        zx.addSubNode(c);
        zx.addSubNode(d);

        Directory ximao = new Directory("/ximao/");
        root.addSubNode(ximao);

        File b = new File("/ximao/tt/b.txt");
        Directory ximao_m = new Directory("/ximao/tt/");
        ximao.addSubNode(ximao_m);
        ximao_m.addSubNode(b);


        System.out.println(root.countFileNumbers());



    }

}
