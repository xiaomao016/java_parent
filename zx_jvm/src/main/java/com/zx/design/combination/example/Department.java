package com.zx.design.combination.example;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 10:19
 * @description:
 */
public class Department extends HumanResource {

    private List<HumanResource> subNodes = new ArrayList<>();

    public Department(long id) {
        super(id);
    }

    @Override
    public double calculateSalary() {

        double total = 0;
        for(HumanResource hr:subNodes){
            total+=hr.calculateSalary();
        }
        this.salary = total;
        return total;
    }


    public void addSubNode(HumanResource hr){
        subNodes.add(hr);
    }

}
