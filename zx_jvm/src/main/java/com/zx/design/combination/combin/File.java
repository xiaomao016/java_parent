package com.zx.design.combination.combin;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 09:46
 * @description:
 */
public class File extends FileSystemNode_ {

    public File(String path) {
        super(path);
    }

    @Override
    public long countFileNumbers() {
        return 1;
    }

    @Override
    public long countFileSize() {
        java.io.File file = new java.io.File(path);

        if(!file.exists())return 0;
        return file.length();
    }
}
