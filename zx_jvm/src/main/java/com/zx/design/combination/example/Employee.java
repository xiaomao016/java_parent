package com.zx.design.combination.example;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 10:18
 * @description:
 */
public class Employee extends HumanResource {
    private double salary;
    public Employee(long id,double salary) {
        super(id);
        this.salary = salary;
    }

    @Override
    public double calculateSalary() {
        return salary;
    }



}
