package com.zx.design.combination.example;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/12 0012 10:16
 * @description:
 */
public abstract class HumanResource {

    protected long id;
    protected double salary;

    public HumanResource(long id){
        this.id = id;
    }

    public long getId(){
        return id;
    }

    public abstract double calculateSalary();

}
