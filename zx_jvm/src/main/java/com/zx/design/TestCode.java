package com.zx.design;

import com.zx.design.factory.base.beans.Hat;
import com.zx.design.singleton.SingletonEnum;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/18 0018 09:08
 * @description:
 */
public class TestCode {
    public static void main(String[] args) throws Exception{

        SingletonEnum.INSTANCE.getId();
        String r = SingletonEnum.INSTANCE.name();
        System.out.println(r);

        String name = Hat.class.getName();
        System.out.println(name);//com.zx.design.factory.base.beans.Hat

        Class c = Hat.class.forName("com.zx.design.factory.base.beans.Hat");

        long rr = 1429689227154690048L;
        System.out.println(rr);
        String rt = "1429689227154690048";
        System.out.println(Long.valueOf(rt));
    }
}
