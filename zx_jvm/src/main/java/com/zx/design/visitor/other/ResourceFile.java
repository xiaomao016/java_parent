package com.zx.design.visitor.other;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 08:54
 * @description:
 */
public abstract class  ResourceFile {
    private String filePath;

    public ResourceFile(String filePath){
        this.filePath = filePath;
    }


    public abstract void visitedBy(Visitor visitor);

}
