package com.zx.design.visitor.other;


public interface Visitor {

    void visit(PDFFile file);
    void visit(PPTFile file);
}
