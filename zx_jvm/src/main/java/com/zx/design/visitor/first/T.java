package com.zx.design.visitor.first;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 09:01
 * @description:
 */
public class T {

    public static void main(String[] args) {

        //First
        List<ResourceFile> list = new ArrayList<>();
        list.add(new PDFFile("./a.pdf"));
        list.add(new WordFile("./a.word"));
        list.add(new PPTFile("./a.ppd"));


        Iterator<ResourceFile> iter = list.iterator();
        while(iter.hasNext()){
            iter.next().extract2Txt();
        }



    }
}
