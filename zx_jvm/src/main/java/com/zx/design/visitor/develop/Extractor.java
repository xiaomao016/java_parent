package com.zx.design.visitor.develop;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 09:48
 * @description:
 */
public class Extractor {

    public void extract(PDFFile file){
        //具体逻辑
        System.out.println("提取pdf");
    }

    public void extract(WordFile file){
        //具体逻辑
        System.out.println("提取world");
    }

    public void extract(PPTFile file){
        //具体逻辑
        System.out.println("提取ppt");
    }



}
