package com.zx.design.visitor.realease;

public interface Visitor {

    void visit(PDFFile file);
    void visit(WordFile file);
    void visit(PPTFile file);
}
