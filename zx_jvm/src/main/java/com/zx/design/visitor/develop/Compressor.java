package com.zx.design.visitor.develop;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 10:07
 * @description:
 */
public class Compressor {

    public void compress(PDFFile file){
        //具体逻辑
        System.out.println("压缩pdf");
    }

    public void compress(WordFile file){
        //具体逻辑
        System.out.println("压缩word");
    }

    public void compress(PPTFile file){
        //具体逻辑
        System.out.println("压缩ppt");
    }
}
