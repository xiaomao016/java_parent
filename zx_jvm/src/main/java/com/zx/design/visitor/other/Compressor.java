package com.zx.design.visitor.other;

import javax.tools.Tool;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 10:07
 * @description:
 */
public class Compressor implements Visitor {

    @Override
    public void visit(PDFFile file) {
        //具体逻辑
        System.out.println("压缩pdf");
    }

    @Override
    public void visit(PPTFile file) {
        //具体逻辑
        System.out.println("压缩ppt");
    }
}
