package com.zx.design.visitor.other;



import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 09:01
 * @description:
 */
public class T {

    public static void main(String[] args) {

        List<ResourceFile> list = new ArrayList<>();
        list.add(new PDFFile("./a.pdf"));
        list.add(new PPTFile("./a.ppd"));

        Extractor e = new Extractor();
        Compressor c = new Compressor();
        for(ResourceFile r:list){
            r.visitedBy(e);
            r.visitedBy(c);
        }

    }
}
