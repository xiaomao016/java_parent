package com.zx.design.visitor.other;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 08:58
 * @description:
 */
public class PDFFile extends ResourceFile {

    public PDFFile(String filePath) {
        super(filePath);
    }

    @Override
    public void visitedBy(Visitor visitor) {
        visitor.visit(this);
    }


}
