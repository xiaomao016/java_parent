package com.zx.design.visitor.other;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/22 0022 09:48
 * @description:
 */
public class Extractor implements Visitor {

    @Override
    public void visit(PDFFile file) {
        //具体逻辑
        System.out.println("提取pdf");
    }

    @Override
    public void visit(PPTFile file) {
        //具体逻辑
        System.out.println("提取ppt");
    }


}
