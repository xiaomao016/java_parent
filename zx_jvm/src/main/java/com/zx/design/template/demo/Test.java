package com.zx.design.template.demo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 13:49
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        BClass bClass = new BClass();
        bClass.templateMethod1(new ICallBack1() {
            @Override
            public void method1() {

            }

            @Override
            public void method2() {

            }
        });


    }
}
