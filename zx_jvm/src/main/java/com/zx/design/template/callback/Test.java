package com.zx.design.template.callback;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 13:30
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        BClass b = new BClass();
        b.process(() -> {
            System.out.println("Call back me");
        });
    }
}
