package com.zx.design.template.demo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 13:46
 * @description:
 */
public class BClass {

    public void templateMethod1(ICallBack1 callBack1){

        //公用流程
        callBack1.method1();
        callBack1.method2();

        //公用流程

    }

}
