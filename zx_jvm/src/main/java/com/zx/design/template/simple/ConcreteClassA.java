package com.zx.design.template.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 11:27
 * @description:
 */
public class ConcreteClassA extends AbstractClass {
    @Override
    public void method1() {
        //A自己扩展后的method1

    }

    @Override
    public void method2() {
        //A自己扩展的method2
    }
}
