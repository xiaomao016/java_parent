package com.zx.design.template.callback;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 11:44
 * @description:
 */
public class BClass {
    public void process(ICallback callback){
        //...
        callback.methodToCallBack();
        //...
    }


}
