package com.zx.design.template.demo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 13:45
 * @description:
 */
public interface ICallBack1 {

    void method1();
    void method2();
}
