package com.zx.design.template.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 10:49
 * @description:
 */
public abstract class AbstractClass {

    public void TemplateMethod(){

        //固定逻辑...
        method1();
        method2();
        //固定逻辑...

    }

    public abstract void method1();
    public abstract void method2();


}
