package com.zx.design.responsechain.simple2;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 10:52
 * @description:
 */
public class HandlerB extends IHandler {
    @Override
    public void doHandle() {
        //进行处理
    }
}
