package com.zx.design.responsechain.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 10:51
 * @description:
 */
public class HandlerA extends Handler {
    @Override
    public boolean doHandle() {
        //不做处理，传给下一个处理者
        return false;
    }

}
