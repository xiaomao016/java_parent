package com.zx.design.responsechain.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 10:46
 * @description:
 */
public abstract class Handler {
    protected Handler successor = null;

    public void setSuccessor(Handler handler){
        this.successor = handler;
    }

    public final void handle(){
        boolean handled = doHandle();
        if(successor!=null & !handled){
            successor.handle();
        }
    }


    public abstract boolean doHandle();
}
