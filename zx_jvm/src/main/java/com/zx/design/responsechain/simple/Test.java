package com.zx.design.responsechain.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 11:02
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        HandlerChain chain = new HandlerChain();
        chain.addHandler(new HandlerA());
        chain.addHandler(new HandlerB());
        chain.handle();
    }
}
