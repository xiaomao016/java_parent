package com.zx.design.responsechain.simple2;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 10:51
 * @description:
 */
public class HandlerA extends IHandler {
    @Override
    public void doHandle() {
        //每个处理者都要经手，可以选择处理或不处理

    }

}
