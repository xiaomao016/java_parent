package com.zx.design.responsechain.simple2;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 10:46
 * @description:
 */
public abstract class IHandler {
    protected IHandler successor = null;

    public void setSuccessor(IHandler handler){
        this.successor = handler;
    }

    public final void handle(){
        doHandle();
        if(successor!=null){
            successor.handle();
        }
    }


    public abstract void doHandle();
}
