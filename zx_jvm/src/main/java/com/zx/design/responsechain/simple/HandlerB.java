package com.zx.design.responsechain.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 10:52
 * @description:
 */
public class HandlerB extends Handler {
    @Override
    public boolean doHandle() {
        //进行处理
        return true;
    }
}
