package com.zx.design.status.checklist;

import com.zx.design.status.ifelse.State;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 08:54
 * @description:
 */
public class MarioStateMachine02 {

    private int score;
    private State currentState;

    private static final State[][] transitionTable = {
            {State.SUPER,State.CAPE,State.FIRE,State.SMALL},
            {State.SUPER,State.CAPE,State.FIRE,State.SMALL},
            {State.CAPE,State.CAPE,State.CAPE,State.SMALL},
            {State.FIRE,State.FIRE,State.FIRE,State.SMALL}
    };

    private static final int[][]actionTable = {
            {+100,+200,+300,+0},
            {+0,+200,+300,-100},
            {+0,+0,+0,-200},
            {+0,+0,+0,-300}
    };

    public MarioStateMachine02(){
        this.score = 0 ;
        this.currentState = State.SMALL;
    }

    public void obtainMushRoom(){
        executeEvent(Event.GOT_MUSHROOM);
    }

    public void obtainCape(){
        executeEvent(Event.GOT_CAPE);
    }

    public void obtainFire(){
        executeEvent(Event.GOT_FIRE);
    }

    public void meetMonster(){
        executeEvent(Event.MEETMONSTER);
    }

    private void executeEvent(Event event){
        int stateValue = currentState.getValue();
        int eventValue = event.getValue();
        this.currentState = transitionTable[stateValue][eventValue];
        this.score += actionTable[stateValue][eventValue];
    }

    public int getScore() {
        return score;
    }


    public State getCurrentState() {
        return currentState;
    }


}
