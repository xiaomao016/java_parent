package com.zx.design.status.ifelse;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 08:54
 * @description:
 */
public class MarioStateMachine01 {

    private int score;
    private State currentState;

    public MarioStateMachine01(){
        this.score = 0 ;
        this.currentState = State.SMALL;
    }


    public void obtainMushRoom(){
        if(currentState==State.SMALL){
            currentState = State.SUPER;
            score+=100;
        }
    }

    public void obtainCape(){
        if(currentState==State.SMALL || currentState==State.SUPER){
            currentState = State.CAPE;
            score+=200;
        }
    }

    public void obtainFire(){
        if(currentState==State.SMALL || currentState==State.SUPER){
            currentState = State.FIRE;
            score+=300;
        }
    }

    public void meetMonster(){
        if(currentState==State.CAPE){
            currentState = State.SMALL;
            score-=200;
            return;
        }

        if(currentState==State.FIRE){
            currentState = State.SMALL;
            score-=300;
            return;
        }

        if(currentState==State.SUPER){
            currentState = State.SMALL;
            score-=100;
            return;
        }

    }


    public int getScore() {
        return score;
    }


    public State getCurrentState() {
        return currentState;
    }


}
