package com.zx.design.status.ifelse;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 08:49
 * @description:
 */
public enum State {
    SMALL(0),
    SUPER(1),
    FIRE(2),
    CAPE(3);

    private int value;

    private State(int value){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }
}
