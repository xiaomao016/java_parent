package com.zx.design.status.checklist;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 10:39
 * @description:
 */
public enum Event {
    GOT_MUSHROOM(0),
    GOT_CAPE(1),
    GOT_FIRE(2),
    MEETMONSTER(3);

    private int value;
    private Event(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
