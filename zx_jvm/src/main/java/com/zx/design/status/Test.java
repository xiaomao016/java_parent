package com.zx.design.status;

import com.zx.design.status.ifelse.MarioStateMachine01;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 09:08
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        MarioStateMachine01 machine01 = new MarioStateMachine01();

        machine01.obtainCape();

        System.out.println(machine01.getCurrentState());
        System.out.println(+100);
    }

}
