package com.zx.design.observer.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 08:48
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        ConcreteSubject subject = new ConcreteSubject();
        subject.registObserver(new ConcreteObserverA());
        subject.registObserver(new ConcreteObserverB());

        boolean result = subject.business();
        if(result){
            subject.notifyObserver();
        }

    }
}
