package com.zx.design.observer.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 08:40
 * @description:
 */
public interface Subject {
    void registObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObserver();
}
