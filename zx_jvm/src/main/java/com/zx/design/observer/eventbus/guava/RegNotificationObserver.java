package com.zx.design.observer.eventbus.guava;

import com.google.common.eventbus.Subscribe;
import com.zx.design.observer.demo0.RegObserver;
import com.zx.design.observer.demo0.base.NotificationService;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:05
 * @description:
 */
public class RegNotificationObserver implements RegObserver {
    private NotificationService notificationService;

    @Override
    @Subscribe
    public void handleRegSuccess(long userId) {
        notificationService.sendInboxMessage(userId, "Welcome...");
    }
}
