package com.zx.design.observer.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 08:47
 * @description:
 */
public class ConcreteObserverB implements Observer {
    @Override
    public void handNotifyMsg() {
        //B逻辑
        System.out.println("增加积分");
    }
}
