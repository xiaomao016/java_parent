package com.zx.design.observer.test_eventbus;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 10:59
 * @description:
 */
public class Confirm_code {
    public static void main(String[] args) {
        Person<Boolean> re = new Person<Boolean>() {
            @Override
            protected Boolean initialValue() {
                return true;
            }
        };
    }
}
