package com.zx.design.observer.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 08:41
 * @description:
 */
public interface Observer {
    void handNotifyMsg();
}
