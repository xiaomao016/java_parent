package com.zx.design.observer.eventbus.zbus;

import java.util.concurrent.Executor;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/15 0015 15:20
 * @description:
 */
public class AsyncEventBus extends EventBus {
    public AsyncEventBus(Executor executor) {
        super(executor);
    }
}