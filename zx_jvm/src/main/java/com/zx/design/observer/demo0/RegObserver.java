package com.zx.design.observer.demo0;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:03
 * @description:
 */
public interface RegObserver {
    void handleRegSuccess(long userId);
}
