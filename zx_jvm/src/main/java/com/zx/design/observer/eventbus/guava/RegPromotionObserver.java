package com.zx.design.observer.eventbus.guava;

import com.google.common.eventbus.Subscribe;
import com.zx.design.observer.demo0.RegObserver;
import com.zx.design.observer.demo0.base.PromotionService;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 14:03
 * @description:
 */
public class RegPromotionObserver implements RegObserver {
    private PromotionService promotionService; // 依赖注入
    @Override
    @Subscribe
    public void handleRegSuccess(long userId) {
        promotionService.issueNewUserExperienceCash(userId);
    }
}
