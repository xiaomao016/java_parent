package com.zx.design.observer.simple;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/14 0014 08:47
 * @description:
 */
public class ConcreteObserverA implements Observer {
    @Override
    public void handNotifyMsg() {
        //A逻辑
        System.out.println("进行优惠券发放");
    }
}
