package com.zx.design.observer.test_eventbus;


import java.util.Queue;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 11:00
 * @description:
 */
public abstract class Dispatcher {
    Dispatcher() {

    }

    static Dispatcher perThreadDispatchQueue() {
        return new Dispatcher.PerThreadQueuedDispatcher();
    }


    private static final class PerThreadQueuedDispatcher extends Dispatcher{
        private final ThreadLocal<Boolean> dispatching;

        private PerThreadQueuedDispatcher() {
            this.dispatching = new ThreadLocal<Boolean>() {
                protected Boolean initialValue() {
                    return false;
                }
            };
        }
    }
}
