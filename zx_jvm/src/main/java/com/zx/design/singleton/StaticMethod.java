package com.zx.design.singleton;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/22 0022 09:35
 * @description:
 */
public class StaticMethod {

    public static void main(String[] args) {

        new Thread(() -> {
            for (int i = 1; i < 40; ++i){
                System.out.println(Thread.currentThread().getName()+":"+IdGeneratorStatic.getId());
            }
        }, "A").start();

        new Thread(()->{
            for(int i = 1 ;i< 40 ; ++i) {
                System.out.println(Thread.currentThread().getName()+":"+IdGeneratorStatic.getId());
            }
        },"B").start();


    }
}

//通过静态方法实现单例效果
class IdGeneratorStatic {
    private static AtomicLong id = new AtomicLong(0);

    public static long getId() {
        return id.incrementAndGet();
    }
}
