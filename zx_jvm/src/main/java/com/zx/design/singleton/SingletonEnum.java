package com.zx.design.singleton;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/18 0018 09:07
 * @description:
 */
public enum SingletonEnum {

    INSTANCE;
    private AtomicLong id = new AtomicLong(0);

    public long getId(){
        return id.incrementAndGet();
    }


}
