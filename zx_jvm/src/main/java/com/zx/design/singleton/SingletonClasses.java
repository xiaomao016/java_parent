package com.zx.design.singleton;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/18 0018 10:24
 * @description:
 */
public class SingletonClasses {
}


/**
 *
 * 恶汉模式：优点，提前初始化好实例，使用快速启动。缺点，实例占用资源过多，使用不频繁，对内存是一种浪费。
 * 但基于Fail-First原则，应该越早发现问题越好,所以恶汉模式优点也很明显。
 *
 */
class IdGenerator001{
    private AtomicLong id = new AtomicLong(0);
    private static IdGenerator001 instance = new IdGenerator001();

    private IdGenerator001(){
    }

    public static IdGenerator001 getInstance(){
        return instance;
    }

    public long getId(){
        return id.incrementAndGet();
    }
}


/**
 * 懒汉模式：优点是延迟加载，缺点是加了一把大锁，并发度是1
 *
 */
class IdGenerator002{

    private AtomicLong id = new AtomicLong(0);
    private static IdGenerator002 instance;

    private IdGenerator002(){
    }

    public  static synchronized IdGenerator002 getInstance(){
        if(instance == null){
            instance = new IdGenerator002();
        }
        return instance;
    }

}


/**
 *
 * 双重监测：将锁范围缩小，只要instance已经创建，则不存在并发性能问题。
 *
 */
class IdGenerator003 {
    private AtomicLong id = new AtomicLong(0);
    private  static IdGenerator003 instance;

    private IdGenerator003() {
    }

    public static IdGenerator003 getInstance() {
        if (instance == null) {
            synchronized (IdGenerator003.class) { // 此处为类级别的锁
                if (instance == null) {
                    instance = new IdGenerator003();
                }
            }
        }
        return instance;
    }

    public long getId() {
        return id.incrementAndGet();
    }
}



/**
 *
 * 静态内部类：支持懒加载
 *
 */

class IdGenerator004{
    private AtomicLong id = new AtomicLong(0);

    private IdGenerator004(){
    }

    private static class InstanceHolder{
        private static final IdGenerator004 instance = new IdGenerator004();
    }

    public static IdGenerator004 getInstance(){
        return InstanceHolder.instance;
    }

    public long getId(){
        return id.incrementAndGet();
    }
}

