package com.zx.design.singleton.Test;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/2/27 0027 22:36
 * @description:
 */
public class Singlett {

}

class A {
    private  static final A instance =  new A();

    private A(){}

    public static A getInstance(){
        return instance;
    }


}

class B{
    private static B instance ;

    private B(){}

    public static B getInstance(){
        if(instance==null){
            synchronized(B.class){
                if(instance==null){
                    instance = new B();
                }
            }
        }
        return instance;
    }
}

class C{

    private C(){}
    static class InstanceHolder{
        private  static final C instance = new C();
    }

    public static C getInstance(){
        return InstanceHolder.instance;
    }
}
