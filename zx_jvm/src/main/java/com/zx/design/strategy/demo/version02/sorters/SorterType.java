package com.zx.design.strategy.demo.version02.sorters;

public enum SorterType {

    QUICK_SORTER,
    EXTERNAL_SORTER,
    EXTERNAL_MULTITHREAD_SORTER,
    MAPREDUCE_SORTER

}
