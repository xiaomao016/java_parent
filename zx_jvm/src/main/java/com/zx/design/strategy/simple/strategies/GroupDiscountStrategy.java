package com.zx.design.strategy.simple.strategies;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:12
 * @description:
 */
public class GroupDiscountStrategy implements IDiscountStrategy{
    @Override
    public double discount(double money) {
        return money*0.6;
    }
}
