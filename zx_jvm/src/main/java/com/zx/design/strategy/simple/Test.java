package com.zx.design.strategy.simple;

import com.zx.design.strategy.simple.strategies.NormalDiscountStrategy;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:40
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        StrategyFactory_.createStrategy(NormalDiscountStrategy.class);
    }
}
