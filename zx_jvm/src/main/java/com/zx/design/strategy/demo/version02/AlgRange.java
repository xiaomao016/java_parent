package com.zx.design.strategy.demo.version02;

import com.zx.design.strategy.demo.version02.sorters.ISortFile;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 09:55
 * @description:
 */
public class AlgRange {
    private long start;
    private long end;
    private ISortFile sorter;

    public AlgRange(long start, long end, ISortFile sorter) {
        this.start = start;
        this.end = end;
        this.sorter = sorter;
    }

    public ISortFile getSorter() {
        return this.sorter;
    }

    public boolean isInRange(long size) {
        return size >= start && size < end;
    }
}
