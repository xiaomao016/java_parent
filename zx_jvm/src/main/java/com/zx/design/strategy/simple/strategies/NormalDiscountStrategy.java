package com.zx.design.strategy.simple.strategies;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:11
 * @description:
 */
public class NormalDiscountStrategy implements IDiscountStrategy {
    @Override
    public double discount(double money) {
        return money;
    }
}
