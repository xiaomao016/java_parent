package com.zx.design.strategy.demo.version02;

import com.zx.design.strategy.demo.version02.sorters.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 09:11
 * @description:
 */
public class FileSorterFactory {

    private static final Map<String, ISortFile> sorters = new HashMap<>();

    static{

        sorters.put(SorterType.QUICK_SORTER.name(),new QuickSort());
        sorters.put(SorterType.EXTERNAL_SORTER.name(),new ExternalSort());
        sorters.put(SorterType.EXTERNAL_MULTITHREAD_SORTER.name(),new ExternalMultiThreadSort());
        sorters.put(SorterType.MAPREDUCE_SORTER.name(),new MapReduceSort());

    }


    public static ISortFile createFileSorter(SorterType type){
        return sorters.get(type.name());
    }

}
