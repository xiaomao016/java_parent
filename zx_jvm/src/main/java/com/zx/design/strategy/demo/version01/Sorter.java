package com.zx.design.strategy.demo.version01;

import java.io.File;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 08:41
 * @description:
 */
public class Sorter {

    private static final int GB =  1000*1000*1000;
    public void SortFile(String filePath){
        File file = new File(filePath);
        long fileSize = file.length();
        if(fileSize<4*GB){
            //内存中排序
        }else if(fileSize<10*GB){
            //外部排序
        }else if(fileSize<100*GB){
            //外部多线程排序
        }else {
            //MR排序
        }
    }


    public void quickSort(File file){

    }

    public void externalSort(File file){

    }

    public void externalSortMultiThread(File file){

    }

    public void MapReduce(File file){

    }


}
