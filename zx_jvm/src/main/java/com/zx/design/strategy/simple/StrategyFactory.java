package com.zx.design.strategy.simple;

import com.zx.design.strategy.badcode.OrderType;
import com.zx.design.strategy.simple.strategies.GroupDiscountStrategy;
import com.zx.design.strategy.simple.strategies.IDiscountStrategy;
import com.zx.design.strategy.simple.strategies.NormalDiscountStrategy;
import com.zx.design.strategy.simple.strategies.PromotionDiscountStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:16
 * @description: 饿汉式
 */
public class StrategyFactory {

    private static final Map<String, IDiscountStrategy> strategyMap = new HashMap<>();

    static{
        strategyMap.put(OrderType.NORMAL.name(),new NormalDiscountStrategy());
        strategyMap.put(OrderType.GROUPON.name(),new GroupDiscountStrategy());
        strategyMap.put(OrderType.PROMOTION.name(),new PromotionDiscountStrategy());
    }


    public static IDiscountStrategy createStrategy(OrderType orderType) {
        return strategyMap.get(orderType.name());
    }

}
