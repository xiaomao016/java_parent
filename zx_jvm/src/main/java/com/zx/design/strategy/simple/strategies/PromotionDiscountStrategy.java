package com.zx.design.strategy.simple.strategies;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:15
 * @description:
 */
public class PromotionDiscountStrategy implements IDiscountStrategy {
    @Override
    public double discount(double money) {
        return 0.8*money;
    }
}
