package com.zx.design.strategy.demo.version02;

import com.zx.design.strategy.demo.version02.sorters.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 09:07
 * @description:
 */
public class Sorter_ {

    private static final long GB = 1000 * 1000 * 1000;
    private static final List<AlgRange> algs = new ArrayList<>();

    static{
        algs.add(new AlgRange(0,4*GB,new QuickSort()));
        algs.add(new AlgRange(4*GB,100*GB,new ExternalSort()));
        algs.add(new AlgRange(100*GB,1000*GB,new ExternalMultiThreadSort()));
        algs.add(new AlgRange(1000*GB,Long.MAX_VALUE,new MapReduceSort()));
    }

    public void sortFile(String path){
        File file = new File(path);

        long size = file.length();

        ISortFile sorter = null;
        for(AlgRange alg:algs){
            if(alg.isInRange(size)){
                sorter = alg.getSorter();
                break;
            }
        }
        sorter.sort(file);
    }

}
