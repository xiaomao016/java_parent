package com.zx.design.strategy.simple.strategies;

public interface IDiscountStrategy {
    double discount(double money);
}
