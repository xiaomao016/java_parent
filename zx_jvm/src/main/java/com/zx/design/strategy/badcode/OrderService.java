package com.zx.design.strategy.badcode;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:07
 * @description:
 */

public class OrderService {
    public double discount(Order order) {
        double discount = 0.0;
        OrderType type = order.getOrderType();
        if (type.equals(OrderType.NORMAL)) { // 普通订单
            //...省略折扣计算算法代码
        } else if (type.equals(OrderType.GROUPON)) { // 团购订单
            //...省略折扣计算算法代码
        } else if (type.equals(OrderType.PROMOTION)) { // 促销订单
            //...省略折扣计算算法代码
        }
        return discount;
    }
}
