package com.zx.design.strategy.demo.version02.sorters;

import java.io.File;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/18 0018 09:07
 * @description:
 */
public interface ISortFile {
    void sort(File file);
}
