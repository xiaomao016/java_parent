package com.zx.design.strategy.simple;

import com.zx.design.strategy.simple.strategies.IDiscountStrategy;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:16
 * @description: 懒汉式
 */
public class StrategyFactory_ {


    public static <T extends IDiscountStrategy> T createStrategy(Class<T> c) {
        try {
            return (T) Class.forName(c.getName()).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
