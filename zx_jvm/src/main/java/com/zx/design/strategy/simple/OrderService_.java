package com.zx.design.strategy.simple;

import com.zx.design.strategy.badcode.Order;
import com.zx.design.strategy.badcode.OrderType;
import com.zx.design.strategy.simple.strategies.IDiscountStrategy;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/16 0016 14:07
 * @description:
 */

public class OrderService_ {
    public double discount(Order order) {
        double discount = 0.0;
        OrderType type = order.getOrderType();
        IDiscountStrategy strategy = StrategyFactory.createStrategy(type);
        return strategy.discount(order.getMoney());
    }
}
