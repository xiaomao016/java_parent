package com.zx.design.builder;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/26 0026 09:50
 * @description:
 */
public class ConstructorArg {
    private boolean isRef;
    private Class type;
    private Object arg;



    private ConstructorArg(boolean isRef,Class type,Object arg){
        this.isRef = isRef;
        this.type = type;
        this.arg = arg;
    }

    public static ConstructorArg.Builder newInstance(){
        return new ConstructorArg.Builder();
    }


    public static class Builder{
        private boolean isRef = false;
        private Class type = null;
        private Object arg = null;

        public Builder(){
        }

        public ConstructorArg.Builder isRef(boolean isRef){
            this.isRef = isRef;
            return this;
        }

        public ConstructorArg.Builder type(Class type){
            this.type = type;
            return this;
        }

        public ConstructorArg.Builder arg(Object arg){
            this.arg = arg;
            return this;
        }

        public ConstructorArg build(){
            if(isRef){

                if(arg==null){
                    throw new IllegalArgumentException("arg can not be null");
                }

                if(!(arg instanceof String)){
                    throw new IllegalArgumentException("arg type is wrong");
                }

            }else{
                if(this.type==null || this.arg==null){
                    throw new IllegalArgumentException("type or arg can not be null");
                }
            }

            return new ConstructorArg(this.isRef,this.type,this.arg);
        }
    }

}
