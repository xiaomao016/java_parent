package com.zx.design.builder;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/26 0026 10:45
 * @description:
 */
public class TestBuild {
    public static void main(String[] args) {
        ConstructorArg constructorArg = ConstructorArg.newInstance()
                .isRef(true)
                .type(String.class)
                .arg("/abc")
                .build();


    }
}
