package com.zx.design.zzz.open;

import java.util.Calendar;

import static java.util.Calendar.MONDAY;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/28 0028 09:23
 * @description:
 */
public class CalendarSource {

    public static void main(String[] args) {
        //建造者模式
        //The following code produces a {@code Calendar} with date 2012-12-31
        Calendar cal = new Calendar.Builder()
                .setCalendarType("iso8601")
                .setWeekDate(2013, 1, MONDAY)
                .build();

        //工厂模式
        Calendar cal2 = Calendar.getInstance();


        test();
    }


    //获取某个月份的
    public int dayOfMonth(int year,int month){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,year);
        cal.set(Calendar.MONTH,month-1);//默认1月为0月

        int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return days;
    }

    public static void test(){
        Calendar cal = new Calendar.Builder()
                .setDate(2021,10,28)
                .build();
        String type = cal.getCalendarType();
        System.out.println(type);
    }
}
