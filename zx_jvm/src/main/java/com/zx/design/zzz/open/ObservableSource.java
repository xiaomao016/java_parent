package com.zx.design.zzz.open;

import java.util.Observable;
import java.util.Observer;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/29 0029 10:25
 * @description:
 *
 *
 * 在 notifyObservers() 函数中，我们先拷贝一份观察者列表，赋值给函数的局部变量，我们知道，
 * 局部变量是线程私有的，并不在线程间共享。这个拷贝出来的线程私有的观察者列表就相当于一个快照。
 * 我们遍历快照，逐一执行每个观察者的 update() 函数。而这个遍历执行的过程是在快照这个局部变量上操作的，
 * 不存在线程安全问题，不需要加锁。所以，我们只需要对拷贝创建快照的过程加锁，加锁的范围减少了很多，并发性能提高了。
 *
 */
public class ObservableSource {

    public static void main(String[] args) {
        ObervableInstance observable = new ObervableInstance();
        observable.addObserver(new ObserverOne());
        observable.addObserver(new ObserverTwo());

        //业务逻辑
        System.out.println("The plastic folders were a fraction of the price of leather ones");
        //通知观察者
        observable.setChanged();
        observable.notifyObservers();

    }



    public static class ObserverOne implements Observer{

        @Override
        public void update(Observable o, Object arg) {
            System.out.println("All things considered ,I think we have done a great job");
        }
    }

    public static class ObserverTwo implements Observer{

        @Override
        public void update(Observable o, Object arg) {
            System.out.println("All in all ,the response to the new campaign is very positive");
        }
    }


    public static class ObervableInstance extends Observable{
        @Override
        protected synchronized void setChanged() {
            super.setChanged();
        }
    }


}
