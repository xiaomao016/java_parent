package com.zx.design.zzz.open;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/28 0028 10:42
 * @description:
 *
 * Collections 类是一个集合容器的工具类，提供了很多静态方法，用来创建各种集合容器，
 * 比如通过 unmodifiableColletion() 静态方法，来创建 UnmodifiableCollection 类对象。
 * 而这些容器类中的 UnmodifiableCollection 类、CheckedCollection 和 SynchronizedCollection 类，
 * 就是针对 Collection 类的装饰器类。
 *
 */
public class CollectionsSource {

    public static void main(String[] args) {
        //1.1.Collections 应用装饰器模式
        Collections.synchronizedList(new ArrayList<>());

        //1.2.应用模板模式
        List<Student> students = new ArrayList<>();
        students.add(new Student("Alice", 19, 89.0f));
        students.add(new Student("Peter", 20, 78.0f));
        students.add(new Student("Leo", 18, 99.0f));
        Collections.sort(students, new AgeAscComparator());
        print(students);
        Collections.sort(students, new NameAscComparator());
        print(students);
        Collections.sort(students, new ScoreDescComparator());
        print(students);

    }


    public static void print(List<Student> students) {
        for (Student s : students) {
            System.out.println(s.getName() + " " + s.getAge() + " " + s.getScore());
        }
    }

    //按照年龄升序排序
    public static class AgeAscComparator implements Comparator<Student> {
        @Override
        public int compare(Student o1, Student o2) {
            return o1.getAge() - o2.getAge();
        }
    }

    public static class NameAscComparator implements Comparator<Student> {
        @Override
        public int compare(Student o1, Student o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    //按成绩降序
    public static class ScoreDescComparator implements Comparator<Student> {
        @Override
        public int compare(Student o1, Student o2) {
            if (Math.abs(o1.getScore() - o2.getScore()) < 0.001) {
                return 0;
            } else if (o1.getScore() < o2.getScore()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

}
