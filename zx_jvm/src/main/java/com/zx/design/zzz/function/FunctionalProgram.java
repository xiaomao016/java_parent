package com.zx.design.zzz.function;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/11/2 0002 08:57
 * @description:
 */
public class FunctionalProgram {

    public static void main(String[] args) {
        Optional<Integer> result = Stream.of("f", "ba", "hello") // of返回Stream<String>对象
                .map(s -> s.length()) // map返回Stream<Integer>对象
                .filter(l -> l <= 3) // filter返回Stream<Integer>对象
                .max((o1, o2) -> o1 - o2); // max终止操作：返回Optional<Integer>
        System.out.println(result.get()); // 输出2


        // 还原为函数接口的实现方式
        Optional<Integer> result2 = Stream.of("fo", "bar", "hello")
                .map(new Function<String, Integer>() {
                    @Override
                    public Integer apply(String s) {
                        return s.length();
                    }
                })
                .filter(new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer l) {
                        return l <= 3;
                    }
                })
                .max(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o1 - o2;
                    }
                });

    }
}
