package com.zx.design.priciple.ocp.ocpcode.sender;

import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 11:30
 * @description:
 */
public class WechatMsgSender implements MsgSender {

    private List<String> wechatList ;

    @Override
    public void send(String message) {
        for(String address:wechatList){
            //给对应的地址发送微信语音
        }
    }

    private WechatMsgSender(WechatMsgSender.Builder builder){
        this.wechatList = builder.wechatList;
    }

    public static WechatMsgSender.Builder newInstance(){
        return new Builder();
    }

    public static class Builder{
        private List<String> wechatList;


        public WechatMsgSender.Builder weChatList(List<String> list){
            this.wechatList = list;
            return this;
        }

        public WechatMsgSender build(){
            if(wechatList==null){
                throw new IllegalArgumentException("发送人列表不能为空");
            }
            return new WechatMsgSender(this);
        }

    }
}
