package com.zx.design.priciple.ocp.ocpcode.notification;

import com.zx.design.priciple.ocp.ocpcode.sender.MsgSender;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 11:36
 * @description:
 */
public class NormalNotification extends Notification_ {

    public NormalNotification(MsgSender sender) {
        super(sender);
    }

    @Override
    public void notify(String message) {
        msgSender.send(message);
    }
}
