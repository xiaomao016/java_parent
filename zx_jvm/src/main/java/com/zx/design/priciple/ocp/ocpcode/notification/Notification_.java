package com.zx.design.priciple.ocp.ocpcode.notification;

import com.zx.design.priciple.ocp.ocpcode.sender.MsgSender;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 11:32
 * @description:
 */
public abstract class Notification_ {

    protected MsgSender msgSender;

    public Notification_(MsgSender sender){
        this.msgSender = sender;
    }

    public abstract void notify(String message);
}
