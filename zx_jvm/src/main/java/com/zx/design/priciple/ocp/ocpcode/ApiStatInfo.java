package com.zx.design.priciple.ocp.ocpcode;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 10:20
 * @description:
 */
public class ApiStatInfo {

    private String api;
    private long requestCount;
    private long errorCount;
    private long timeOutCount;
    private long durationOfSeconds;


    public ApiStatInfo() {
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public long getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(long requestCount) {
        this.requestCount = requestCount;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }

    public long getTimeOutCount() {
        return timeOutCount;
    }

    public void setTimeOutCount(long timeOutCount) {
        this.timeOutCount = timeOutCount;
    }

    public long getDurationOfSeconds() {
        return durationOfSeconds;
    }

    public void setDurationOfSeconds(long durationOfSeconds) {
        this.durationOfSeconds = durationOfSeconds;
    }
}
