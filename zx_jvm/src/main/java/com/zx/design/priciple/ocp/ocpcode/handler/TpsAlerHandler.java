package com.zx.design.priciple.ocp.ocpcode.handler;

import com.zx.design.priciple.ocp.base.AlertRuleFactory;
import com.zx.design.priciple.ocp.ocpcode.ApiStatInfo;
import com.zx.design.priciple.ocp.ocpcode.notification.Notification_;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 10:29
 * @description:
 */
public class TpsAlerHandler extends AlertHandler {
    public TpsAlerHandler(Notification_ notification) {
        super(notification);
    }

    @Override
    public void check(ApiStatInfo apiStatInfo) {
        long tps = apiStatInfo.getRequestCount() / apiStatInfo.getDurationOfSeconds();
        if (tps > AlertRuleFactory.getMatchedRule(apiStatInfo.getApi()).getMaxTps()) {
            notification.notify("...");
        }
    }
}
