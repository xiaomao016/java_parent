package com.zx.design.priciple.ocp;

import com.zx.design.priciple.ocp.badcode.Alert;
import com.zx.design.priciple.ocp.badcode.Notification;
import com.zx.design.priciple.ocp.base.AlertRule;
import com.zx.design.priciple.ocp.ocpcode.Alert_;
import com.zx.design.priciple.ocp.ocpcode.ApiStatInfo;
import com.zx.design.priciple.ocp.ocpcode.ApplicatinoContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 09:18
 * @description:
 */
public class TestCode {

    public static void main(String[] args) {

        //1.普通方式使用alert
        Notification notification = new Notification();
        List<String> listEmail = new ArrayList<>();
        List<String> listPhone = new ArrayList<>();
        List<String> listWechat = new ArrayList<>();
        notification.setEmailAddress(listEmail);
        notification.setTelephones(listPhone);
        notification.setWechatIds(listWechat);

        AlertRule rule = AlertRule.newInstance()
                .maxTps(10L)
                .maxErrorCount(10L)
                .build();

        Alert alert = new Alert(rule, notification);

        alert.check("", 100, 10, 100);


        //2.采用桥接模式改造后，更加符合OCP原则
        ApiStatInfo info = new ApiStatInfo();
        info.setApi("");//可以用建造者模式进行优化
        ApplicatinoContext.getInstance().getAlert().check(info);


    }

}
