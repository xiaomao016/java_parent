package com.zx.design.priciple.ocp.base;

public enum NotificationEmergencyLevel {
    SEVERE, URGENCY, NORMAL, TRIVIAL
}
