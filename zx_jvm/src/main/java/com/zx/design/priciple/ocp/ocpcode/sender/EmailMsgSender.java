package com.zx.design.priciple.ocp.ocpcode.sender;

import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 11:30
 * @description:
 */
public class EmailMsgSender implements MsgSender {
    private List<String> list ;

    @Override
    public void send(String message) {
        for(String address:list){
            //给对应的地址发
        }
    }

    private EmailMsgSender(EmailMsgSender.Builder builder){
        this.list = builder.list;
    }

    public static EmailMsgSender.Builder newInstance(){
        return new EmailMsgSender.Builder();
    }

    public static class Builder{
        private List<String> list;


        public EmailMsgSender.Builder emailList(List<String> list){
            this.list = list;
            return this;
        }

        public EmailMsgSender build(){
            if(list==null){
                throw new IllegalArgumentException("发送人列表不能为空");
            }
            return new EmailMsgSender(this);
        }

    }
}
