package com.zx.design.priciple.inversion;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/9 0009 10:25
 * @description:
 */
public class JunitApplication {

    public static final List<TestCase> testCases = new ArrayList<>();


    public static void register(TestCase testCase){
        testCases.add(testCase);
    }


    public static void main(String[] args) {
        for(TestCase cs:testCases){
            cs.run();
        }
    }

}
