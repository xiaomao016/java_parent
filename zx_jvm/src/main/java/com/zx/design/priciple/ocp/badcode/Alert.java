package com.zx.design.priciple.ocp.badcode;

import com.zx.design.priciple.ocp.base.AlertRule;
import com.zx.design.priciple.ocp.base.NotificationEmergencyLevel;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/29 0029 14:53
 * @description:
 */

public class Alert {
    private AlertRule rule;
    private Notification notification;

    public Alert(AlertRule rule, Notification notification) {
        this.rule = rule;
        this.notification = notification;
    }

    public void check(String api, long requestCount, long errorCount, long durationOfSeconds) {
        long tps = requestCount / durationOfSeconds;
        if (tps > rule.getMaxTps()) {
            notification.notify(NotificationEmergencyLevel.URGENCY, "...");
        }
        if (errorCount > rule.getMaxErrorCount()) {
            notification.notify(NotificationEmergencyLevel.SEVERE, "...");
        }
    }
}
