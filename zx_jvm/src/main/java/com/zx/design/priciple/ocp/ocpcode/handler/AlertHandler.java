package com.zx.design.priciple.ocp.ocpcode.handler;

import com.zx.design.priciple.ocp.badcode.Notification;
import com.zx.design.priciple.ocp.ocpcode.ApiStatInfo;
import com.zx.design.priciple.ocp.ocpcode.notification.Notification_;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 10:27
 * @description:
 */
public abstract class AlertHandler {
    protected Notification_ notification;

    public AlertHandler(Notification_ notification) {
        this.notification = notification;
    }

    public abstract void check(ApiStatInfo apiStatInfo);
}
