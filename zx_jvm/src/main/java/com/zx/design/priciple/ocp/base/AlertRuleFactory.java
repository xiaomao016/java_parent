package com.zx.design.priciple.ocp.base;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/29 0029 14:59
 * @description:
 * 简单工厂模式进行告警规则创建
 */

public class AlertRuleFactory {

    private static Map<String ,AlertRule> alertRules = new HashMap<>();


    static {
        alertRules.put("/api/login",AlertRule
                .newInstance()
                .maxTps(10L)
                .maxErrorCount(10L).build()
        );
        alertRules.put("/api/register",
                AlertRule.newInstance()
                .maxTps(10L)
                .maxErrorCount(10L)
                .build()
                );
    }


    public static AlertRule getMatchedRule(String api){
        return alertRules.get(api);
    }




}
