package com.zx.design.priciple.inversion;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/9 0009 10:22
 * @description:
 */
public abstract class TestCase {
    public void run(){
        if(doTest()){
            System.out.println("Test success");
        }else{
            System.out.println("Test failed");
        }
    }

    public abstract boolean doTest();
}
