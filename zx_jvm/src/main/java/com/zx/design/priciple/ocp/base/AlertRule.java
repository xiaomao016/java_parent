package com.zx.design.priciple.ocp.base;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/29 0029 15:26
 * @description: 利用建造者模式构建AlertRule
 */
public class AlertRule {
    private long maxTps;
    private long maxErrorCount;

    private AlertRule(AlertRule.Builder builder) {
        this.maxTps = builder.maxTps;
        this.maxErrorCount = builder.maxErrorCount;
    }

    public static AlertRule.Builder newInstance() {
        return new AlertRule.Builder();
    }

    public static class Builder {

        private long maxTps = Long.MAX_VALUE;
        private long maxErrorCount = Long.MIN_VALUE;

        public Builder() {
        }

        public AlertRule.Builder maxTps(long maxTps) {
            this.maxTps = maxTps;
            return this;
        }

        public AlertRule.Builder maxErrorCount(long maxErrorCount) {
            this.maxErrorCount = maxErrorCount;
            return this;
        }


        public AlertRule build() {
            return new AlertRule(this);
        }

    }

    public long getMaxTps() {
        return maxTps;
    }

    public long getMaxErrorCount() {
        return maxErrorCount;
    }
}
