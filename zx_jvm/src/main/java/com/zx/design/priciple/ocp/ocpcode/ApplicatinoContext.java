package com.zx.design.priciple.ocp.ocpcode;

import com.zx.design.priciple.ocp.ocpcode.handler.ErrorCountAlertHandler;
import com.zx.design.priciple.ocp.ocpcode.handler.TpsAlerHandler;
import com.zx.design.priciple.ocp.ocpcode.notification.SevereNotification;
import com.zx.design.priciple.ocp.ocpcode.notification.UrgencyNotification;
import com.zx.design.priciple.ocp.ocpcode.sender.TelephoneMsgSender;
import com.zx.design.priciple.ocp.ocpcode.sender.WechatMsgSender;

import java.util.ArrayList;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 10:45
 * @description:
 * 饿汉式单例模式：在这里做初始化工作。包括对象的注入和初始化
 */
public class ApplicatinoContext {
    private Alert_ alert;
    private static final ApplicatinoContext instance = new ApplicatinoContext();
    private ApplicatinoContext(){
        initialBeans();
    }

    public static ApplicatinoContext getInstance(){
        return instance;
    }


    public void initialBeans(){
        alert = new Alert_();
        WechatMsgSender wechatMsgSender = WechatMsgSender
                .newInstance()
                .weChatList(new ArrayList<>())
                .build();
        alert.addHandler(new TpsAlerHandler(new SevereNotification(wechatMsgSender)));
        TelephoneMsgSender telephoneMsgSender = TelephoneMsgSender
                .newInstance()
                .phoneList(new ArrayList<>())
                .build();
        alert.addHandler(new ErrorCountAlertHandler(new UrgencyNotification(telephoneMsgSender)));
    }

    public Alert_ getAlert(){
        return alert;
    }


}
