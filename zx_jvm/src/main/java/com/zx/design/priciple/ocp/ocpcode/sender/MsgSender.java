package com.zx.design.priciple.ocp.ocpcode.sender;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/30 0030 11:29
 * @description:
 */
public interface MsgSender {
    void send(String message);
}
