package com.zx.design.priciple.ocp.ocpcode;

import com.zx.design.priciple.ocp.ocpcode.handler.AlertHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/29 0029 14:53
 * @description:
 */

public class Alert_ {

    private List<AlertHandler> handlers = new ArrayList<>();

    public void addHandler(AlertHandler handler){
        handlers.add(handler);
    }

    public void check(ApiStatInfo apiStatInfo) {

        for(AlertHandler handler:handlers){
            handler.check(apiStatInfo);
        }

    }
}
