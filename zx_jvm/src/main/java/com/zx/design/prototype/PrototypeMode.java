package com.zx.design.prototype;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/27 0027 10:49
 * @description:
 * 原型模式：1.当对象创建复杂，而多个对象差别不大。可通过现有对象复制方式得到一个对象。即为原型模式
 *          2.实现方式分为：深拷贝，浅拷贝
 *
 *          区别：浅拷贝只会拷贝基本数据类型，和对象的引用，不会拷贝对象本身。故引用类型新旧版本是共用。
 *               深拷贝则会拷贝完全独立的对象，两者完全互不干扰。
 */

public class PrototypeMode {
    public static void main(String[] args) {
//        BeanUtils
    }
}
