package com.zx.design.iterator.demo;

import java.util.ArrayList;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:04
 * @description:
 */
public class ArrayList_ implements List_<Integer> {

    private ArrayList<Integer> list = new ArrayList<>();

    @Override
    public void add(Integer data) {
        list.add(data);
    }
    @Override
    public Integer get(int index) {
        return list.get(index);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Iterator_<Integer> iterator() {
        return new ArrayListIterator_(this);
    }
}
