package com.zx.design.iterator.demo02;


import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:06
 * @description:
 */
public class ArrayListSnapShotIterator implements SnapShotIterator<Integer> {

    private int cursor;
    private ArrayList_ list;

    public ArrayListSnapShotIterator(ArrayList_ list){
        this.list = copy(list);
        cursor = 0;
    }

    @Override
    public void next() {
        cursor++;
    }

    @Override
    public Integer currentItem() {
        return list.get(cursor);
    }

    @Override
    public boolean hasNext() {
        return cursor!=list.size();
    }

    private ArrayList_ copy(ArrayList_ list){
        ArrayList_ result = new ArrayList_();
        for(int i = 0;i<list.size();++i){
            result.add(list.get(i));
        }
        return result;
    }
}
