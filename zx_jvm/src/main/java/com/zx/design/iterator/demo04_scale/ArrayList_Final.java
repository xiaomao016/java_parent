package com.zx.design.iterator.demo04_scale;


import java.util.Arrays;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:04
 * @description:
 */
public class ArrayList_Final<T> implements List_<T> {
    private static final int DEFAULT_CAPACITY = 10;

    private Object[] EMPTY_ELEMENTDATA = {};//默认数组为空，当添加一个元素时，扩容为DEFAULT_CAPACITY
    private long[] EMPTY_DELTIIMESTAMP = {};//默认数组为空，当添加一个元素时，扩容为DEFAULT_CAPACITY
    private long[] EMPTY_ADDTIMESTAMP = {};//默认数组为空，当添加一个元素时，扩容为DEFAULT_CAPACITY
    private Object[] elementData;
    private long[] delTimeStamps;
    private long[] addTimeStamps;

    private int totalSize;//全部元素总数


    public ArrayList_Final(){
        this.elementData = EMPTY_ELEMENTDATA;
        this.delTimeStamps = EMPTY_DELTIIMESTAMP;
        this.addTimeStamps = EMPTY_ADDTIMESTAMP;
        totalSize = 0;
    }

    @Override
    public void add(T data) {
        ensureCapacityInternal(totalSize + 1);

        elementData[totalSize] = data;
        addTimeStamps[totalSize] = System.nanoTime();
        delTimeStamps[totalSize] = Long.MAX_VALUE;
        totalSize++;

    }


    private void ensureCapacityInternal(int minCapacity) {
        //如果是第一次添加元素，此时扩容为DEFAULT_CAPACITY
        if (elementData == EMPTY_ELEMENTDATA) {
            minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
        }

        ensureExplicitCapacity(minCapacity);
    }

    private void ensureExplicitCapacity(int minCapacity) {
        // overflow-conscious code
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

    private void grow(int minCapacity) {
        // overflow-conscious code
        int oldCapacity = elementData.length;
        //超过默认容量时，每次扩容50%
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;

        //进行数据搬移
        elementData = Arrays.copyOf(elementData, newCapacity);
        delTimeStamps = Arrays.copyOf(delTimeStamps, newCapacity);
        addTimeStamps = Arrays.copyOf(addTimeStamps, newCapacity);
    }

    @Override
    public void remove(T data) {
        for(int i=0;i<totalSize;++i){
            if(elementData[i].equals(data) && delTimeStamps[i]==Long.MAX_VALUE){//防止重复删除
                delTimeStamps[i] = System.nanoTime();
            }
        }
    }

    @Override
    public SnapShotIterator<T> iterator() {
        return new ArrayListSnapShotIterator_Final<>(this);
    }

    public T get(int i){
        if(i>=totalSize){
            throw new IndexOutOfBoundsException();
        }
        return (T) elementData[i];
    }

    public int getTotalSize() {
        return totalSize;
    }

    public long getDelTimeStamps(int i) {
        if(i>=totalSize){
            throw new IndexOutOfBoundsException();
        }
        return delTimeStamps[i];
    }

    public long getAddTimeStamps(int i) {
        if(i>=totalSize){
            throw new IndexOutOfBoundsException();
        }
        return addTimeStamps[i];
    }
}
