package com.zx.design.iterator.demo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:03
 * @description:
 */
public interface List_<T> {
    void add(T data);

    T get(int index);

    int size();

    Iterator_<T> iterator();
}
