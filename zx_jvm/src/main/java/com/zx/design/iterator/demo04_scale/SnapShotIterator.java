package com.zx.design.iterator.demo04_scale;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:04
 * @description:
 */
public interface SnapShotIterator<T> {

    T next();
    boolean hasNext();

}
