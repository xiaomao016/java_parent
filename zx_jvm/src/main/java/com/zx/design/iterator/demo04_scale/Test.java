package com.zx.design.iterator.demo04_scale;


/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/21 0021 07:57
 * @description:
 */
public class Test {
    public static void main(String[] args) throws Exception{
        ArrayList_Final<Integer> list = new ArrayList_Final<>();

        list.add(3);
        list.add(8);
        list.add(2);

        SnapShotIterator<Integer> iter1 = list.iterator();
        list.remove(3);
        SnapShotIterator<Integer> iter2 = list.iterator();
        list.remove(8);
        SnapShotIterator<Integer> iter3 = list.iterator();


        while(iter1.hasNext()){
            System.out.println(iter1.next());
        }
        while(iter2.hasNext()){
            System.out.println(iter2.next());
        }
        while(iter3.hasNext()){
            System.out.println(iter3.next());
        }

    }
}
