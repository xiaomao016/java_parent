package com.zx.design.iterator.demo04_scale;


/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:03
 * @description:
 */
public interface List_<T> {
    void add(T data);

    void remove(T data);

    SnapShotIterator<T> iterator();
}
