package com.zx.design.iterator.demo02;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/21 0021 07:57
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        List_ list = new ArrayList_();
        list.add(3);
        list.add(8);
        list.add(2);

        SnapShotIterator iterator = list.iterator();
        list.remove(2);

        SnapShotIterator iterator2 = list.iterator();
        list.remove(3);

        SnapShotIterator iter3 = list.iterator();

        while(iterator.hasNext()){
            System.out.println(iterator.currentItem());
            iterator.next();
        }

        System.out.println("==============");

        while(iterator2.hasNext()){
            System.out.println(iterator2.currentItem());
            iterator2.next();
        }
        System.out.println("==============");

        while(iter3.hasNext()){
            System.out.println(iter3.currentItem());
            iter3.next();
        }

    }
}
