package com.zx.design.iterator.demo03_snapshot;




/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:04
 * @description:
 */
public class ArrayList_<T> implements List_<T> {
    private static final int DEFAULT_CAPACITY = 10;

    private Object[] elements;

    private int totalSize;//全部元素总数

    private long[] delTimeStamps;
    private long[] addTimeStamps;

    public ArrayList_(){
        this.elements = new Object[DEFAULT_CAPACITY];
        totalSize = 0;
        delTimeStamps = new long[DEFAULT_CAPACITY];
        addTimeStamps = new long[DEFAULT_CAPACITY];
    }

    @Override
    public void add(T data) {
        elements[totalSize] = data;
        addTimeStamps[totalSize] = System.nanoTime();
        delTimeStamps[totalSize] = Long.MAX_VALUE;
        totalSize++;

    }

    @Override
    public void remove(T data) {
        for(int i=0;i<totalSize;++i){
            if(elements[i].equals(data) && delTimeStamps[i]==Long.MAX_VALUE){//防止重复删除
                delTimeStamps[i] = System.nanoTime();
            }
        }
    }

    @Override
    public SnapShotIterator<T> iterator() {
        return new ArrayListSnapShotIterator<T>(this);
    }

    public T get(int i){
        if(i>=totalSize){
            throw new IndexOutOfBoundsException();
        }
        return (T) elements[i];
    }

    public int getTotalSize() {
        return totalSize;
    }

    public long getDelTimeStamps(int i) {
        if(i>=totalSize){
            throw new IndexOutOfBoundsException();
        }
        return delTimeStamps[i];
    }

    public long getAddTimeStamps(int i) {
        if(i>=totalSize){
            throw new IndexOutOfBoundsException();
        }
        return addTimeStamps[i];
    }
}
