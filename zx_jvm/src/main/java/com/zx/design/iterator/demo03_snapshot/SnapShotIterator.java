package com.zx.design.iterator.demo03_snapshot;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:04
 * @description:
 */
public interface SnapShotIterator<T> {

    T next();
    boolean hasNext();

}
