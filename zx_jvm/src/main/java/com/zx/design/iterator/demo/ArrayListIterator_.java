package com.zx.design.iterator.demo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:06
 * @description:
 */
public class ArrayListIterator_ implements Iterator_<Integer> {

    private int cursor;
    private ArrayList_ list;

    public ArrayListIterator_ (ArrayList_ list){
        this.list = list;
        cursor = 0;
    }

    @Override
    public void next() {
        if(cursor<list.size()-1) {
            cursor++;
        }else{
            throw new IndexOutOfBoundsException("已经没有元素了");
        }
    }

    @Override
    public Integer currentItem() {
        return list.get(cursor);
    }

    @Override
    public boolean hasNext() {
        return cursor<=(list.size()-1);
    }
}
