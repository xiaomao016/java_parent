package com.zx.design.iterator.demo04_scale;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/21 0021 14:28
 * @description:
 */
public class ArrayListSnapShotIterator_Final<T> implements SnapShotIterator<T> {

    private long snapshotTimeStamp;
    private int cursorInAll;//整个容器的下标，而非快照的下标
    private ArrayList_Final<T> arrayList;


    public ArrayListSnapShotIterator_Final(ArrayList_Final<T> arrayList){
        this.snapshotTimeStamp = System.nanoTime();
        this.cursorInAll = 0;
        this.arrayList = arrayList;
        moveToValidNext();
    }


    @Override
    public T next() {
        T currentItem = arrayList.get(cursorInAll);
        cursorInAll++;
        moveToValidNext();
        return currentItem;
    }

    @Override
    public boolean hasNext() {
        return cursorInAll != arrayList.getTotalSize();
    }

    private void moveToValidNext(){

        while(cursorInAll< arrayList.getTotalSize()){
            long addTimeStamp = arrayList.getAddTimeStamps(cursorInAll);
            long delTimeStamp = arrayList.getDelTimeStamps(cursorInAll);
            if(snapshotTimeStamp>addTimeStamp && snapshotTimeStamp<delTimeStamp){
                //此时是有效元素，
                break;
            }
            cursorInAll++;
        }

    }
}
