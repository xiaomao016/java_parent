package com.zx.design.iterator.demo;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/10/20 0020 14:04
 * @description:
 */
public interface Iterator_<T> {

    void next();
    T currentItem();
    boolean hasNext();

}
