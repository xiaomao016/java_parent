package com.zx.clean;

import java.util.Iterator;

public class IntegerArgumentMarshaler implements ArgumentMarshaler {
    private int intValue = 0;
    @Override
    public void set(Iterator<String> currentArgument)  {
        String parameter = null;
        parameter = currentArgument.next();
        intValue = Integer.parseInt(parameter);

    }

    public static int getValue(ArgumentMarshaler am){
        if(am!=null && am instanceof IntegerArgumentMarshaler)
            return ((IntegerArgumentMarshaler)am).intValue;
        else
            return 0;
    }
}
