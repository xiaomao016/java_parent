package com.zx.clean;

import java.util.Iterator;

public class StringArgumentMarshaler implements ArgumentMarshaler {
    private String stringValue = "";
    @Override
    public void set(Iterator<String> currentArgument) {
        stringValue = currentArgument.next();
    }

    public static String getValue(ArgumentMarshaler am){
        if(am!=null && am instanceof StringArgumentMarshaler)
            return ((StringArgumentMarshaler) am).stringValue;
         else
             return "";
    }
}
