package com.zx.netty.demo_chat.util;


import com.zx.netty.demo_chat.attribute.Attributes;
import io.netty.channel.Channel;
import io.netty.util.Attribute;

public class LoginUtil {
    public static void markAsLogin(Channel ch){
        ch.attr(Attributes.LOGIN).set(true);
    }

    public static boolean hasLogin(Channel ch){
        Attribute<Boolean> logInAttr = ch.attr(Attributes.LOGIN);

        return logInAttr.get() !=null;
    }
}
