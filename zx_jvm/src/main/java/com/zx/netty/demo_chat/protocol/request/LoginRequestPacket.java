package com.zx.netty.demo_chat.protocol.request;

import com.zx.netty.demo_chat.protocol.Packet;
import lombok.Data;

import static com.zx.netty.demo_chat.protocol.command.Command.LOGIN_REQUEST;

@Data
public class LoginRequestPacket extends Packet {
    private String userId;

    private String username;

    private String password;

    @Override
    public Byte getCommand() {
        return LOGIN_REQUEST;
    }
}
