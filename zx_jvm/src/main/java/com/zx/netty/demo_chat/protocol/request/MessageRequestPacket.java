package com.zx.netty.demo_chat.protocol.request;

import com.zx.netty.demo_chat.protocol.Packet;
import lombok.Data;

import static com.zx.netty.demo_chat.protocol.command.Command.MESSAGE_REQUEST;

@Data
public class MessageRequestPacket extends Packet {

    private String message;

    @Override
    public Byte getCommand() {
        return MESSAGE_REQUEST;
    }
}
