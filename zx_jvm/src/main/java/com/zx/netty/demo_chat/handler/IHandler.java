package com.zx.netty.demo_chat.handler;

import com.zx.netty.demo_chat.protocol.Packet;
import io.netty.channel.ChannelHandlerContext;

public interface IHandler {

    void handMsg(Packet packet, ChannelHandlerContext ctx);
}
