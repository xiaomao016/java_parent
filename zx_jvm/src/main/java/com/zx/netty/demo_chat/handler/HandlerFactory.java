package com.zx.netty.demo_chat.handler;

import java.util.HashMap;
import java.util.Map;

public class HandlerFactory {

    private static final Map<String, IHandler> handlerMap = new HashMap<>();

    static{
        handlerMap.put(PacketType.LOGIN.name(),new HandleLoginMsg());
        handlerMap.put(PacketType.CLIENT_MSG.name(),new HandleClientMsg());
        handlerMap.put(PacketType.LOGOUT.name(),new HandleLogOutMsg());
    }


    public static IHandler createStrategy(PacketType packetType) {
        return handlerMap.get(packetType.name());
    }

}
