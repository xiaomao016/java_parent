package com.zx.netty.demo_chat.protocol.response;

import com.zx.netty.demo_chat.protocol.Packet;
import lombok.Data;

import static com.zx.netty.demo_chat.protocol.command.Command.LOGIN_RESPONSE;

@Data
public class LoginResponsePacket extends Packet {
    private boolean success;

    private String reason;


    @Override
    public Byte getCommand() {
        return LOGIN_RESPONSE;
    }
}
