package com.zx.netty.demo_chat.protocol.response;

import com.zx.netty.demo_chat.protocol.Packet;
import lombok.Data;

import static com.zx.netty.demo_chat.protocol.command.Command.MESSAGE_RESPONSE;

@Data
public class MessageResponsePacket extends Packet {

    private String message;

    @Override
    public Byte getCommand() {
        return MESSAGE_RESPONSE;
    }
}
