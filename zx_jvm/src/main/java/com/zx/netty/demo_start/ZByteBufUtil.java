package com.zx.netty.demo_start;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.nio.charset.Charset;

public class ZByteBufUtil {

    public static  ByteBuf getByteBuf(ChannelHandlerContext ctx,String data) {
        byte[] bytes = data.getBytes(Charset.forName("utf-8"));

        ByteBuf buffer = ctx.alloc().buffer();

        buffer.writeBytes(bytes);

        return buffer;
    }
}
